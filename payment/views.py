#coding=utf8
import hashlib
import datetime

from django.http import HttpResponseRedirect, Http404, HttpResponse
from django.views.decorators.csrf import csrf_exempt
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.shortcuts import render
from django.conf import settings

from payment.models import Invoice, INVOICE_MODE_CHOICE
from common.views import auth_check
from profile.services.user import UserService

# тестовые
# RK_PASS1 = 'jBb0NO7v4rjacsRY5i2T'
# RK_PASS2 = 'w6GgrZ9CKN6I2BWn0JJJ'
# RK_DEBUG = True

#боевые
RK_PASS1 = 'J1cOj2u2VFoc6K1cKWNL'
RK_PASS2 = 'Kl8yax4LDx0bIGN0XP9P'
RK_DEBUG = False

RK_LOGIN = 'lastmanstanding'

YK_SCID = 66012
YK_SHOP_ID = 59718
YK_SHOP_PASS = 'fdRNHJFNds8367654321'


def get_rk_sign(*args):
    """
    Возвращает подпись робокассы
    """
    m = hashlib.md5()
    m.update(':'.join([str(d) for d in args]))
    return m.hexdigest().upper()


def get_yk_sign(*args):
    """
    Возвращает подпись Яндекс.Кассы
    """
    m = hashlib.md5()
    m.update(';'.join([str(d) for d in args]))
    return m.hexdigest().upper()


def get_yk_url(invoice):
    """
    Возвращает ссылку на Яндекс.Кассу
    """
    return "https://money.yandex.ru/eshop.xml" \
          "?shopId=" + str(YK_SHOP_ID) + \
          "&scid=" + str(YK_SCID) + \
          "&sum=" + str(invoice.sum) + \
          "&customerNumber=" + str(invoice.user.id) + \
          "&orderNumber=" + str(invoice.id) + \
          "&shopSuccessURL=http://ws.last-man.org/payment/list/" + \
          "&shopFailURL=http://ws.last-man.org/payment/form/"


def get_rk_url(invoice):
    """
    Возвращает ссылку робокассы
    """
    crc = get_rk_sign(RK_LOGIN, invoice.sum, invoice.id, RK_PASS1)
    return "http://auth.robokassa.ru/Merchant/Index.aspx?" \
        "MerchantLogin=" + RK_LOGIN + \
        "&OutSum=" + str(invoice.sum) + \
        "&InvoiceID=" + str(invoice.id) + \
        "&Description=" + invoice.get_description() + \
        "&SignatureValue=" + crc


@csrf_exempt
def result(request):
    inv_id = request.POST.get('inv_id')
    out_summ = request.POST.get('out_summ')
    crc = request.POST.get('crc').upper()

    try:
        invoice = Invoice.objects.get(pk=inv_id)
    except:
        return HttpResponse(u'Ошибка при оплате. (код #11)')

    try:
        my_crc = get_rk_sign(out_summ, inv_id, RK_PASS2)
        if my_crc != crc:
            raise Exception(u'код #12: Ошибка подписи')

        try:
            sm = int(out_summ.split('.')[0])
        except:
            raise Exception(u'код #12: Ошибка суммы')

        if invoice.sum != sm:
            raise Exception(u'код #14: Сумма не соответствует')

        if invoice.status != 0:
            raise Exception(u'код #15: Заказ уже оплачен')

        if not RK_DEBUG and request.POST.get('IsTest') == '1':
            raise Exception(u'код #16: Тестовый запрос')

    except Exception as e:
        invoice.set_status(3)
        invoice.comment = unicode(e)
        invoice.save()
        return HttpResponse(invoice.comment)

    # сделать установку периода оплаты
    invoice.set_status(1)
    invoice.save()

    if invoice.mode == 'subscribe':
        invoice.user.add_subscribe(invoice.months)
        UserService.open_trains(invoice.user)

    return HttpResponse(u'OK%d' % (invoice.id))


@csrf_exempt
@auth_check(ajax=False)
def fail(request):
    try:
        invoice = Invoice.objects.get(pk=request.POST['inv_id'])
    except:
        raise Http404(u'Ошибка при оплате. (код #31). Пожалуйста сообщите нам в чат.')

    return render(request, 'payment/fail.html', {
        'invoice': invoice,
    })


#@auth_check(ajax=False)
def form(request):
    return render(request, 'payment/form.html', {
        'sale': request.user.is_authenticated() and request.user.has_discount(),
    })


@auth_check(ajax=False)
def pay(request):
    mode = request.GET.get('mode')

    if mode not in INVOICE_MODE_CHOICE.keys():
        raise Http404(u'Неверный тип оплаты')

    invoice = Invoice()
    invoice.user = request.user
    invoice.mode = mode

    if invoice.mode == 'subscribe':
        try:
            months = int(request.GET.get('m'))
        except:
            raise Http404(u'Неверные параметры')

        try:
            invoice.set_month(months)
        except Exception as e:
            raise Http404(unicode(e))
    elif mode == 'advice':
        invoice.sum = 500

    qs = Invoice.objects \
        .filter(user_id=invoice.user_id,
                months=invoice.months,
                sum=invoice.sum,
                mode=invoice.mode,
                status=0)

    if qs.count() > 0:
        invoice = qs[0]
    else:
        invoice.save()

    if request.GET.get('ps', '') == 'rk':
        invoice.payment_system = 'rk'
        url = get_rk_url(invoice)
    else:
        invoice.payment_system = 'yk'
        url = get_yk_url(invoice)

    invoice.save()

    return HttpResponseRedirect(url)


@csrf_exempt
@auth_check(ajax=False)
def payments(request):
    qs = Invoice.objects \
        .filter(user=request.user) \
        .order_by('-created_at')

    paginator = Paginator(qs, 10)
    page = request.GET.get('page')
    try:
        objs = paginator.page(page)
    except PageNotAnInteger:
        objs = paginator.page(1)
    except EmptyPage:
        objs = paginator.page(paginator.num_pages)

    msg = None
    out_summ = None
    try:
        inv_id = None
        if request.GET.get('action'):
            inv_id = request.GET['orderNumber']
            out_summ = request.GET['orderSumAmount']
        else:
            inv_id = request.POST['inv_id']
            out_summ = request.POST['out_summ']

        invoice = Invoice.objects.get(pk=inv_id)
    except:
        invoice = None

    if invoice:
        try:
            if invoice.user_id != request.user.id:
                raise Exception(u'Ошибка доступа.')

            sm = 0
            try:
                sm = int(out_summ.split('.')[0])
            except:
                raise Exception(u'Ошибка при оплате. (код #23). Пожалуйста сообщите нам в чат.')

            if invoice.sum != sm:
                raise Exception(u'Ошибка при оплате. (код #24). Внесено недостаточно средств. Пожалуйста сообщите нам в чат.')

            if invoice.status != 1:
                raise Exception(u'Заказ не оплачен')
        except Exception as e:
            msg = unicode(e)

    return render(request, 'payment/list.html', {
        'invoice': invoice,
        'msg': msg,
        'invoices': objs,
        'sale': request.user.has_discount(),
    })


@csrf_exempt
def yk_check(request):
    action = request.POST.get('action')
    invoiceId = request.POST.get('invoiceId')
    customerNumber = request.POST.get('customerNumber')
    orderNumber = request.POST.get('orderNumber')
    orderSumAmount = request.POST.get('orderSumAmount')
    orderSumCurrencyPaycash = request.POST.get('orderSumCurrencyPaycash')
    orderSumBankPaycash = request.POST.get('orderSumBankPaycash')

    try:
        invoice = Invoice.objects.get(pk=orderNumber)
    except:
        invoice = None

    md5 = get_yk_sign(action, orderSumAmount, orderSumCurrencyPaycash,
                      orderSumBankPaycash, YK_SHOP_ID, invoiceId, customerNumber, YK_SHOP_PASS)

    code = 0
    try:
        if md5 != request.POST.get('md5'):
            code = 1
            raise Exception(u'Неверная подпись')

        if not invoice:
            code = 100
            raise Exception(u'Заказ не найден')

        if invoice.status != 0:
            code = 100
            raise Exception(u'Заказ уже оплачем')

        if action == 'checkOrder':
            pass

        elif action == 'paymentAviso':
            invoice.set_status(1)
            invoice.save()
            if invoice.mode == 'subscribe':
                invoice.user.add_subscribe(invoice.months)
                UserService.open_trains(invoice.user)

    except Exception as e:
        response = u'<%sResponse performedDatetime="%s" code="%s" message="%s"/>' % \
                   (action, datetime.datetime.now().isoformat(), code, unicode(e))
    else:
        response = u'<%sResponse performedDatetime="%s" code="%s" invoiceId="%s" shopId="%s"/>' % \
                   (action, datetime.datetime.now().isoformat(), code, invoiceId, YK_SHOP_ID)

    return HttpResponse(response, content_type='application/xml')


@auth_check(ajax=False)
def advice(request):
    qs = Invoice.objects.filter(mode='advice', status=1)

    count = qs.count()
    lost = max(10-count, 1)

    # if count > 5:
    #     raise Http404(u'Страница недоступна')

    return render(request, 'payment/advice.html', {
        'lost': lost,
    })
