#coding=utf8
from datetime import timedelta

from django.db import models
from django.utils import timezone
from django.contrib.auth.models import User


INVOICE_STATUS_CHOICE = {
    0: u'ожидание',
    1: u'оплачено',
    2: u'отмена',
    3: u'ошибка',
}

PAYMENT_SYSTEM_CHOICE = {
    'rk': u'Робокасса',
    'yk': u'Яндекс.Касса'
}
INVOICE_MODE_CHOICE = {
    'subscribe': u'Подписка',
    'advice': u'Консультация',
    'donate': u'Донат',
}


class Invoice(models.Model):
    user = models.ForeignKey(User)
    created_at = models.DateTimeField(auto_now_add=True)
    modified_at = models.DateTimeField(default=None, null=True)

    sum = models.PositiveIntegerField()
    months = models.PositiveSmallIntegerField(null=True, blank=True, default=True)
    mode = models.CharField(max_length=10, choices=INVOICE_MODE_CHOICE.items(), default='subscribe')

    status = models.PositiveSmallIntegerField(choices=INVOICE_STATUS_CHOICE.items(), default=0)
    comment = models.CharField(max_length=200, default=None, null=True, blank=True)
    payment_system = models.CharField(max_length=4, choices=PAYMENT_SYSTEM_CHOICE.items(),
                                      default=None, blank=True, null=True)

    def get_status(self):
        return INVOICE_STATUS_CHOICE[self.status]

    def set_month(self, months):
        self.months = months
        if months == 1:
            self.sum = 150 if self.user.has_discount() else 390
        elif months == 3:
            self.sum = 400 if self.user.has_discount() else 990
        elif months == 6:
            self.sum = 750 if self.user.has_discount() else 1990
        else:
            raise Exception(u'Ошибка установки месяца')

    def get_description(self):
        if self.mode == 'subscribe':
            return u'Подписка на сервис на %d мес' % self.months
        elif self.mode == 'advice':
            return u'Консультация'
        elif self.mode == 'donate':
            return u'Донат'

    def set_status(self, status):
        self.status = status
        self.modified_at = timezone.now()
