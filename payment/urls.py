from django.conf.urls import patterns, url

from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    url(r'^advice/$', 'payment.views.advice', name='payment_advice'),
    url(r'^form/$', 'payment.views.form', name='payment_form'),
    url(r'^list/$', 'payment.views.payments', name='payment_list'),
    url(r'^pay/$', 'payment.views.pay', name='payment_pay'),
    url(r'^result/$', 'payment.views.result', name='payment_result'),
    url(r'^success/$', 'payment.views.payments', name='payment_success'),
    url(r'^fail/$', 'payment.views.fail', name='payment_fail'),
    url(r'^yk/check/$', 'payment.views.yk_check', name='payment_yk_check'),
    url(r'^yk/aviso/$', 'payment.views.yk_check', name='payment_yk_aviso'),
)