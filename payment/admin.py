from django.contrib import admin

from payment.models import *


class InvoiceAdmin(admin.ModelAdmin):
    list_display = ['id', 'payment_system', 'get_description', 'sum', 'user', 'status', 'created_at', 'modified_at']

    list_filter = ['status', 'mode']
admin.site.register(Invoice, InvoiceAdmin)