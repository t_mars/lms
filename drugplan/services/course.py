#coding=utf8
from datetime import timedelta

from django.db import connection
from django.db.models import Q

from main.utils.dates import get_timestamp
from common.service import Service
from drugplan.models import Dose, Course, Drug
from drugplan.services.drug import DrugService
from profile.services.user import UserService


class CourseService(Service):

    @classmethod
    def create_course(cls, user, name, start_date, is_public):
        """
        Создает курс
        """
        if start_date.weekday() != 0:
            raise Exception(u'Начать СПМ можно только в понедельник')

        course = Course()
        course.author = user
        course.patient = user
        course.start_date = start_date
        course.name = name
        course.is_public = is_public
        course.save()

        return course

    @classmethod
    def get_course_access(cls, user, course):
        """
        Возвращает данные о курсе для пользователя
        """
        return {
            'show': course.access_check(user, 'show'),
            'delete': course.access_check(user, 'delete'),
            'edit': course.access_check(user, 'edit'),
            'mark': course.access_check(user, 'mark')
        }

    @classmethod
    def get_course_data(cls, course):
        """
        Возвращает основные данные курсе
        """
        return {
            'id': course.id,
            'name': course.name,
            'is_public': course.is_public,
            'patient': UserService.get_user_short_info(course.patient_id),
            'author': UserService.get_user_short_info(course.author_id)
        }

    @classmethod
    def get_drug_list(cls, user, course):
        """
        Возвращает список медикаментов для пользователя
        """
        qs = Drug.objects.filter(Q(user__isnull=True) | Q(user=user))

        return [cls.get_drug_data(course, d) for d in qs]

    @classmethod
    def get_drug_data(cls, course, drug):
        return {
            'id': drug.id,
            'name': drug.name,
            'short_name': drug.get_short_name(),
            'measure': course.get_measure(drug)
        }

    @classmethod
    def edit_course(cls, user, course, name, author, patient, is_public):
        """
        Изменяет данные о курсе
        """
        if not course.access_check(user, 'edit'):
            raise Exception(u'У вас нет прав для редактирования этого СПМ.')

        course.name = name or course.name
        course.author = author
        course.patient = patient
        course.is_public = is_public
        course.save()

    @classmethod
    def get_drugs_by_days(cls, course, dates):
        """
        Получить по дням медикаменты
        """
        qs = Dose.objects \
            .filter(course=course, date__in=dates) \
            .select_related('drug')

        drug_by_date = cls.get_drugs_by_dates(course, qs)
        return {get_timestamp(d): drug_by_date.get(d, {}) for d in dates}

    @classmethod
    def delete_doses(cls, user, course, drug, dates):
        """
        Удаляет медикамент из курса по списку дат
        """
        if not course.access_check(user, 'edit'):
            raise Exception(u'У вас нет прав для редактирования этого СПМ')

        Dose.objects \
            .filter(course=course, drug=drug, date__in=dates) \
            .delete()

    @classmethod
    def get_weeks(cls, course, start, end):
        """
        Данные курсе по неделям
        """
        def get_weeknum(d):
            return (d - course.start_date).days / 7

        qs = Dose.objects \
            .filter(course=course, date__range=[start, end]) \
            .select_related('drug')

        # упаковывает приемы по препаратам
        drug_by_date = cls.get_drugs_by_dates(course, qs)

        weeks = []
        d = start
        days = []
        while d <= end:
            day = {
                'date': get_timestamp(d),
                'drugs': drug_by_date.get(d, {}),
            }
            days.append(day)
            if d.weekday() == 6:
                weeks.append({
                    'num': get_weeknum(d) + 1,
                    'days': days
                })
                days = []
            d += timedelta(days=1)

        return weeks

    @classmethod
    def get_doses_by_drugs(cls, course, doses):
        ds = {}
        for dose in doses:
            if dose.drug.id not in ds:
                ds[dose.drug.id] = DrugService.get_drug_info(dose.drug)
                ds[dose.drug.id]['measure'] = course.get_measure(dose.drug)
                ds[dose.drug.id]['doses'] = []

            ds[dose.drug.id]['doses'].append({
                'id': dose.id,
                'comment': dose.comment,
                'amount': dose.amount,
                'is_marked': dose.is_marked,
                'marked_at': get_timestamp(dose.marked_at) if dose.marked_at else None,
            })
        return ds.values()

    @classmethod
    def get_drugs_by_dates(cls, course, qs):
        # упаковывает препараты по дням
        dose_by_days = {}
        for q in qs:
            if q.date not in dose_by_days:
                dose_by_days[q.date] = []
            dose_by_days[q.date].append(q)

        # упаковывает приемы по препаратам
        drug_by_date = {}
        for date, qs in dose_by_days.items():
            drug_by_date[date] = sorted(cls.get_doses_by_drugs(course, qs), key=lambda d: d['id'])
        return drug_by_date

    @classmethod
    def add_doses(cls, user, course, drug, dates, doses):
        """
        Добавляет приемы лекарств
        """
        if not course.access_check(user, 'edit'):
            raise Exception(u'У вас нет прав для редактирования этого СПМ')

        ds = []
        for date in dates:
            for d in doses:
                dose = Dose()
                dose.drug = drug
                dose.course = course
                dose.amount = d['amount']
                dose.comment = d['comment']
                dose.date = date
                ds.append(dose)

        Dose.objects.bulk_create(ds)


    @classmethod
    def add_regimen(cls, course, drug, scheme, date_param, amount_comments):
        """
        Добавляет схему приема
        """
        dates = []
        if date_param['type'] == 'range':
            # указан период
            start_date = date_param['start_date']
            end_date = date_param['end_date']
            if scheme.startswith('every'):
                _, days = scheme.split(':')
                days = int(days)
                d = start_date
                while d <= end_date:
                    dates.append(d)
                    d += timedelta(days=days)

            elif scheme.startswith('weekdays'):
                _, weekdays = scheme.split(':')
                weekdays = [int(wd) for wd in weekdays.split(',')]
                d = start_date
                while d <= end_date:
                    if d.weekday() in weekdays:
                        dates.append(d)
                    d += timedelta(days=1)
        else:
            for d in date_param['dates']:
                dates.append(d)

        cursor = connection.cursor()
        query = """SELECT COUNT(distinct id),COUNT(distinct drug_id), date
            FROM drugplan_dose
            WHERE date in (%s)
                AND course_id=%d
            GROUP BY date""" % (
            ','.join([d.strftime("'%Y-%m-%d'") for d in dates]),
            course.id
        )
        cursor.execute(query)
        for row in cursor.fetchall():
            if row[0] >= 50 or row[1] >= 10:
                raise Exception(u'%s: нельзя добавить больше 50 приемов и 10 медикаментов в один день' % d.strftime('%d.%m.%Y'))

        # сохраняем лекарство если не сохранено
        if not drug.pk:
            drug.save()

        # вставляем в таблицу и возвращаем ответ
        doses = []
        for d in dates:
            for amount, comment in amount_comments:
                dose = Dose()
                dose.drug = drug
                dose.course = course
                dose.amount = amount
                dose.comment = comment
                dose.date = d
                doses.append(dose)

        Dose.objects.bulk_create(doses)