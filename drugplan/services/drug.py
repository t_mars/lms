#coding=utf8
from django.db.models import Q

from common.service import Service
from drugplan.models import Drug, MEASURE_CHOICE


class DrugService(Service):

    @classmethod
    def create_drug(cls, user, name, short_name, measure):
        """
        Создает новый медикамент
        """
        if not name:
            raise Exception(u'Укажите название медикамента')

        if measure not in MEASURE_CHOICE.keys():
            raise Exception(u'Неверно указано мера измерения')

        drug = Drug()
        drug.name = name
        drug.short_name = short_name
        drug.user = user
        drug.measure = measure
        drug.save()

        return drug

    @classmethod
    def get_drug_info(cls, drug):
        """
        Информация о медикаменте
        """
        return {
            'id': drug.id,
            'name': drug.name,
            'short_name': drug.get_short_name(),
            'measure': drug.measure
        }

    @classmethod
    def get_measures(cls):
        """"
        Возвращает меры измерения
        """
        return MEASURE_CHOICE.keys()
