#coding=utf8
from datetime import timedelta
import json

from django.db import models
from django.db.models import Max, Min
from django.contrib.auth.models import User
from django.utils import timezone

from main.models import prev_weekday, next_weekday

MEASURE_CHOICE = {
    u'мкг': u'мкг',
    u'мг': u'мг',
    u'мл': u'мл',
    u'МЕ': u'МЕ',
    u'ед': u'ед',
    u'г': u'г',
    u'таб': u'таб',
}


# Группа лекарств
class DrugGroup(models.Model):
    name = models.CharField(verbose_name=u'Название', max_length=100)

    def __str__(self):
        return self.name.encode('utf8')


class Drug(models.Model):
    group = models.ForeignKey(DrugGroup, verbose_name='Группа', null=True, default=True)

    short_name = models.CharField(verbose_name=u'Короткое название препарата', max_length=100)
    name = models.CharField(verbose_name=u'Название препарата', max_length=100)
    user = models.ForeignKey(User, null=True, blank=True, default=None, verbose_name=u'Создатель')
    measure = models.CharField(verbose_name=u'Мера измерени', choices=MEASURE_CHOICE.items(), max_length=3, default=None)
    pack_size = models.PositiveSmallIntegerField(verbose_name=u'Количество в упаковке по умолчанию', default=0)

    def __str__(self):
        return self.name.encode('utf8')

    def get_short_name(self):
        return self.short_name or self.name


# План приема лекартв
class Course(models.Model):
    name = models.CharField(verbose_name=u'Название', max_length=100)
    start_date = models.DateField(verbose_name=u'Дата начала')
    created_at = models.DateTimeField(verbose_name=u'Время создания', auto_now_add=True)

    author = models.ForeignKey(User, verbose_name=u'Создатель', default=None)
    is_deleted = models.BooleanField(verbose_name=u'Удален', default=False)

    view_count = models.PositiveIntegerField(verbose_name=u'Количество просмотров', default=0)
    last_view = models.DateTimeField(null=True, blank=True, default=None)

    patient = models.ForeignKey(User, verbose_name=u'Создатель', default=None, null=True, blank=True, related_name='patient')
    is_public = models.BooleanField(verbose_name=u'Открытый доступ', default=False)
    measures = models.TextField(verbose_name=u'Меры измерения для лекарств', null=True, blank=True, default=None)

    def save(self, *args, **kwargs):
        if hasattr(self, '_measures'):
            self.measures = json.dumps(self._measures)
        super(Course, self).save(*args, **kwargs)

    def parse_measures(self):
        if hasattr(self, '_measures'):
            return
        try:
            self._measures = json.loads(self.measures)
        except:
            self._measures = {}

    def set_measure(self, drug, measure):
        self.parse_measures()
        self._measures[str(drug.id)] = measure

    def get_measure(self, drug):
        self.parse_measures()
        return self._measures.get(str(drug.id), drug.measure)

    def access_check(self, user, mode):
        if self.is_deleted:
            return False

        if mode == 'show':
            if user.is_superuser:
                return True
            if self.is_public:
                return True
            if self.author_id == user.id:
                return True
            if self.patient_id == user.id:
                return True 
        
        if mode == 'delete':
            if self.author_id == user.id:
                return True
        if mode == 'edit':
            if self.author_id == user.id:
                return True
        
        if mode == 'mark':
            if self.patient_id == user.id:
                return True
        
        return False

    def get_limit_dates(self):
        qs = Dose.objects.filter(course=self) 
        r = qs.aggregate(dmin=Min('date'), dmax=Max('date'))
        if r['dmin']:
            self._first_date = prev_weekday(r['dmin'], 0)
        else:
            self._first_date = prev_weekday(self.start_date, 0)

        if r['dmax']:
            self._last_date = next_weekday(r['dmax'], 6)
        else:
            self._last_date = next_weekday(self.start_date, 6)

    @property
    def first_date(self):
        if not hasattr(self, '_first_date'):
            self.get_limit_dates()
        return self._first_date
    
    @property
    def last_date(self):
        if not hasattr(self, '_last_date'):
            self.get_limit_dates()
        return self._last_date

    def view_it(self):
        self.view_count += 1
        self.last_view = timezone.now()
        self.save()

    @property
    def end_date(self):
        if not hasattr(self, '_end_date'):
            self._end_date = next_weekday(self.start_date + timedelta(days=365), 6)
        return self._end_date


class Dose(models.Model):
    drug = models.ForeignKey(Drug, verbose_name=u'Препарат')
    comment = models.CharField(verbose_name=u'Комментарий', max_length=100)
    course = models.ForeignKey(Course, verbose_name=u'Пользователь', default=None)
    amount = models.PositiveSmallIntegerField(verbose_name=u'Дозировка')
    
    date = models.DateField(verbose_name=u'Дата приема')
    time = models.DateTimeField(verbose_name=u'Время приема', null=True, blank=True, default=None)
    is_marked = models.NullBooleanField(verbose_name=u'Употреблено', default=None)
    marked_at = models.DateTimeField(verbose_name=u'Употреблено', null=True, blank=True, default=None)