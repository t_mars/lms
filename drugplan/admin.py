from django.contrib import admin

from drugplan.models import *


class DrugGroupAdmin(admin.ModelAdmin):
    pass
admin.site.register(DrugGroup, DrugGroupAdmin)


class DrugAdmin(admin.ModelAdmin):
    list_display = ['name', 'user', 'group']
admin.site.register(Drug, DrugAdmin)


class DoseAdmin(admin.ModelAdmin):
    pass
admin.site.register(Dose, DoseAdmin)


class CourseAdmin(admin.ModelAdmin):
    list_display = ['name', 'author', 'patient', 'is_deleted']
admin.site.register(Course, CourseAdmin)