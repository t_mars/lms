from django.conf.urls import patterns, include, url

from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    url(r'^scheme/list/$', 'drugplan.views.scheme.list', name='spm_scheme_list'),
    url(r'^scheme/(?P<num>\d+)/$', 'drugplan.views.scheme.show', name='spm_scheme_show'),
    url(r'^scheme/(?P<num>\d+)/create/$', 'drugplan.views.scheme.create', name='spm_scheme_create'),
    
    url(r'^drug/list/$', 'drugplan.views.drug.list', name='drug_list'),
    url(r'^drug/(?P<id>\d+)/$', 'drugplan.views.drug.show', name='drug_show'),
    
    url(r'^spm/create/$', 'drugplan.views.base.create', name='spm_create'),
    url(r'^spm/list/$', 'drugplan.views.base.list', name='spm_list'),
    url(r'^spm/add_form/$', 'drugplan.views.edit.add_form', name='spm_drug_add_form'),
    url(r'^spm/(?P<id>\d+)/$', 'drugplan.views.edit.show', name='spm_show'),
    url(r'^spm/(?P<id>\d+)/edit$', 'drugplan.views.edit.edit', name='spm_edit'),
    url(r'^spm(?P<id>\d+)/mark/$', 'drugplan.views.edit.delete', name='spm_delete'),
    url(r'^spm/(?P<id>\d+)/ajax/$', 'drugplan.views.edit.ajax', name='spm_ajax'),
    url(r'^spm/(?P<id>\d+)/add/$', 'drugplan.views.edit.add', name='spm_drug_add'),
    url(r'^spm/(?P<id>\d+)/delete/$', 'drugplan.views.edit.drug_delete', name='spm_drug_delete'),
    url(r'^spm/(?P<id>\d+)/period/$', 'drugplan.views.edit.period', name='spm_period'),
    url(r'^spm/(?P<id>\d+)/update/$', 'drugplan.views.edit.update', name='spm_update'),
    url(r'^spm/(?P<id>\d+)/mark/$', 'drugplan.views.edit.mark', name='spm_mark'),
    url(r'^spm/dose_mark/$', 'drugplan.views.edit.dose_mark', name='spm_dose_mark'),
)