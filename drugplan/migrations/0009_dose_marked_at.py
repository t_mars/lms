# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('drugplan', '0008_dose_is_marked'),
    ]

    operations = [
        migrations.AddField(
            model_name='dose',
            name='marked_at',
            field=models.DateTimeField(default=None, null=True, verbose_name='\u0423\u043f\u043e\u0442\u0440\u0435\u0431\u043b\u0435\u043d\u043e', blank=True),
        ),
    ]
