# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('drugplan', '0006_auto_20160103_0821'),
    ]

    operations = [
        migrations.AddField(
            model_name='dose',
            name='comment',
            field=models.CharField(default='', max_length=100, verbose_name='\u041a\u043e\u043c\u043c\u0435\u043d\u0442\u0430\u0440\u0438\u0439'),
            preserve_default=False,
        ),
    ]
