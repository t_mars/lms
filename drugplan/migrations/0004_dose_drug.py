# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('drugplan', '0003_dose_user'),
    ]

    operations = [
        migrations.AddField(
            model_name='dose',
            name='drug',
            field=models.ForeignKey(default=None, verbose_name='\u041f\u0440\u0435\u043f\u0430\u0440\u0430\u0442', to='drugplan.Drug'),
            preserve_default=False,
        ),
    ]
