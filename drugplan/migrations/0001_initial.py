# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Dose',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('date', models.DateField(verbose_name='\u0414\u0430\u0442\u0430 \u043f\u0440\u0438\u0435\u043c\u0430')),
                ('time', models.DateTimeField(verbose_name='\u0412\u0440\u0435\u043c\u044f \u043f\u0440\u0438\u0435\u043c\u0430')),
                ('amount', models.PositiveSmallIntegerField(verbose_name='\u0414\u043e\u0437\u0438\u0440\u043e\u0432\u043a\u0430')),
            ],
        ),
        migrations.CreateModel(
            name='Drug',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=100, verbose_name='\u041d\u0430\u0437\u0432\u0430\u043d\u0438\u0435 \u043f\u0440\u0435\u043f\u0430\u0440\u0430\u0442\u0430')),
                ('user', models.ForeignKey(default=None, blank=True, to=settings.AUTH_USER_MODEL, null=True, verbose_name='\u0421\u043e\u0437\u0434\u0430\u0442\u0435\u043b\u044c')),
            ],
        ),
        migrations.CreateModel(
            name='Regimen',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('amount', models.PositiveSmallIntegerField(verbose_name='\u0414\u043e\u0437\u0438\u0440\u043e\u0432\u043a\u0430')),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name='\u0412\u0440\u0435\u043c\u044f \u0441\u043e\u0437\u0434\u0430\u043d\u0438\u044f')),
                ('start_date', models.DateField(verbose_name='\u041d\u0430\u0447\u0430\u043b\u043e \u043f\u0440\u0438\u0435\u043c\u0430')),
                ('end_date', models.DateField(verbose_name='\u041a\u043e\u043d\u0435\u0446 \u043f\u0440\u0438\u0435\u043c\u0430')),
                ('scheme', models.CharField(max_length=20, verbose_name='\u0421\u0445\u0435\u043c\u0430 \u043f\u0440\u0438\u0435\u043c\u0430')),
                ('drug', models.ForeignKey(verbose_name='\u041f\u0440\u0435\u043f\u0430\u0440\u0430\u0442', to='drugplan.Drug')),
                ('user', models.ForeignKey(verbose_name='\u041f\u043e\u043b\u044c\u0437\u043e\u0432\u0430\u0442\u0435\u043b\u044c', to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.AddField(
            model_name='dose',
            name='regimen',
            field=models.ForeignKey(verbose_name='\u0420\u0435\u0436\u0438\u043c \u043f\u0440\u0438\u0435\u043c\u0430', to='drugplan.Regimen'),
        ),
    ]
