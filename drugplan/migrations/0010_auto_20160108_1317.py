# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('drugplan', '0009_dose_marked_at'),
    ]

    operations = [
        migrations.AddField(
            model_name='course',
            name='is_public',
            field=models.BooleanField(default=False, verbose_name='\u041e\u0442\u043a\u0440\u044b\u0442\u044b\u0439 \u0434\u043e\u0441\u0442\u0443\u043f'),
        ),
        migrations.AddField(
            model_name='course',
            name='patient',
            field=models.ForeignKey(related_name='patient', default=None, blank=True, to=settings.AUTH_USER_MODEL, null=True, verbose_name='\u0421\u043e\u0437\u0434\u0430\u0442\u0435\u043b\u044c'),
        ),
        migrations.AddField(
            model_name='course',
            name='start_date',
            field=models.DateField(default=None, verbose_name='\u0414\u0430\u0442\u0430 \u043d\u0430\u0447\u0430\u043b\u0430'),
            preserve_default=False,
        ),
    ]
