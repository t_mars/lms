# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('drugplan', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='dose',
            name='time',
            field=models.DateTimeField(default=None, null=True, verbose_name='\u0412\u0440\u0435\u043c\u044f \u043f\u0440\u0438\u0435\u043c\u0430', blank=True),
        ),
    ]
