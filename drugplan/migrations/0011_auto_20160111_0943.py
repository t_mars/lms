# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('drugplan', '0010_auto_20160108_1317'),
    ]

    operations = [
        migrations.AddField(
            model_name='drug',
            name='measure',
            field=models.CharField(default=None, max_length=3, verbose_name='\u041c\u0435\u0440\u0430 \u0438\u0437\u043c\u0435\u0440\u0435\u043d\u0438', choices=[(b'\xd0\xb3', b'\xd0\xb3'), (b'\xd0\xbc\xd0\xbb', b'\xd0\xbc\xd0\xbb'), (b'\xd0\xbc\xd0\xb3', b'\xd0\xbc\xd0\xb3'), (b'\xd0\xb5\xd0\xb4', b'\xd0\xb5\xd0\xb4'), (b'\xd0\x9c\xd0\x95', b'\xd0\x9c\xd0\x95'), (b'\xd0\xbc\xd0\xba\xd0\xb3', b'\xd0\xbc\xd0\xba\xd0\xb3')]),
        ),
        migrations.AddField(
            model_name='drug',
            name='pack_size',
            field=models.PositiveSmallIntegerField(default=0, verbose_name='\u041a\u043e\u043b\u0438\u0447\u0435\u0441\u0442\u0432\u043e \u0432 \u0443\u043f\u0430\u043a\u043e\u0432\u043a\u0435 \u043f\u043e \u0443\u043c\u043e\u043b\u0447\u0430\u043d\u0438\u044e'),
        ),
    ]
