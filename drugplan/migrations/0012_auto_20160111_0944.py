# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('drugplan', '0011_auto_20160111_0943'),
    ]

    operations = [
        migrations.AlterField(
            model_name='drug',
            name='measure',
            field=models.CharField(default=None, max_length=3, verbose_name='\u041c\u0435\u0440\u0430 \u0438\u0437\u043c\u0435\u0440\u0435\u043d\u0438', choices=[('\u041c\u0415', '\u041c\u0415'), ('\u043c\u0433', '\u043c\u0433'), ('\u0435\u0434', '\u0435\u0434'), ('\u043c\u043b', '\u043c\u043b'), ('\u0433', '\u0433'), ('\u043c\u043a\u0433', '\u043c\u043a\u0433')]),
        ),
    ]
