# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('drugplan', '0007_dose_comment'),
    ]

    operations = [
        migrations.AddField(
            model_name='dose',
            name='is_marked',
            field=models.NullBooleanField(default=None, verbose_name='\u0423\u043f\u043e\u0442\u0440\u0435\u0431\u043b\u0435\u043d\u043e'),
        ),
    ]
