# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('drugplan', '0004_dose_drug'),
    ]

    operations = [
        migrations.AddField(
            model_name='drug',
            name='short_name',
            field=models.CharField(default='', max_length=100, verbose_name='\u041a\u043e\u0440\u043e\u0442\u043a\u043e\u0435 \u043d\u0430\u0437\u0432\u0430\u043d\u0438\u0435 \u043f\u0440\u0435\u043f\u0430\u0440\u0430\u0442\u0430'),
            preserve_default=False,
        ),
    ]
