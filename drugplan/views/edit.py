# coding=utf8
import json
import datetime

from django.views.decorators.csrf import csrf_protect
from django.core.context_processors import csrf
from django.http import HttpResponse, Http404
from django.template import loader, Context
from django.shortcuts import render, redirect
from django import forms
from django.db.models import Q

from main.models import get_timestamp
from common.views import auth_check
from drugplan.models import *
from drugplan.services.course import CourseService


class MyModelChoiceField(forms.ModelChoiceField):
    def label_from_instance(self, obj):
        return u'%s, %s' % (obj.name, obj.measure)

class RegimenForm(forms.Form):
    new_drug_name = forms.CharField(label=u'Название', max_length=100, required=False)
    new_drug_short_name = forms.CharField(label=u'Сокращенное название', max_length=100, required=False)
    new_drug_measure = forms.ChoiceField(label=u'Мера', choices=MEASURE_CHOICE.items(), required=False)
    new_drug_pack_size = forms.IntegerField(label=u'Количество в упаковке', required=False, min_value=1, max_value=100000)

    days = forms.CharField()
    scheme = forms.ChoiceField(label=u'Режим приема', 
        choices=[('every:1',u'Каждый день'), ('every:2',u'Через день'), ('every',u'Раз в N дней'), ('weekdays',u'По дням недели')])
    every_n = forms.IntegerField(label=u'Раз в N дней', required=False) 
    weekdays = forms.MultipleChoiceField(label=u'Дни недели', choices=[(0,u'ПН'),(1,u'ВТ'),(2,u'СР'),(3,u'ЧТ'),(4,u'ПТ'),(5,u'СБ'),(6,u'ВС')], 
        widget=forms.CheckboxSelectMultiple(), required=False)
    
    def __init__(self, user, *args, **kwargs):
        self.user = user
        super(RegimenForm, self).__init__(*args, **kwargs)
        self.fields['amount'] = forms.IntegerField(label=u'Дозировка', min_value=1, max_value=100000)
        self.fields['comment'] = forms.CharField(label=u'Комментарий', required=False, max_length=100)
    
        for i in range(2,6):
            self.fields['amount_%d' % i] = forms.IntegerField(label=u'Дозировка', required=False, min_value=1, max_value=100000)
            self.fields['comment_%d' % i] = forms.CharField(label=u'Комментарий', required=False, max_length=100)

        qs = Drug.objects.filter(Q(user__isnull=True) | Q(user=self.user))
        self.fields['drug'] = MyModelChoiceField(label=u'Препарат', queryset=qs, required=False)

    
    def clean(self):
        cleaned_data = super(RegimenForm, self).clean()
        self.has_new_drug = False
        if not cleaned_data.get('drug'):
            self.has_new_drug = True
            f = True
            if not cleaned_data.get('new_drug_name'):
                self.add_error('new_drug_name', 'Обязательное поле')
                f = False
                
            if not cleaned_data.get('new_drug_short_name'):
                self.add_error('new_drug_short_name', 'Обязательное поле')
                f = False
                
            if not cleaned_data.get('new_drug_measure'):
                self.add_error('new_drug_measure', 'Обязательное поле')
                f = False
                
            if not cleaned_data.get('new_drug_pack_size'):
                self.add_error('new_drug_pack_size', 'Обязательное поле')
                f = False

            drug = Drug()
            drug.name = cleaned_data.get('new_drug_name')
            drug.short_name = cleaned_data.get('new_drug_short_name')
            drug.measure = cleaned_data.get('new_drug_measure')
            drug.pack_size = cleaned_data.get('new_drug_pack_size')
            drug.user = self.user
            cleaned_data['new_drug'] = drug
        
        try:
            date_param = parse_date_param(cleaned_data.get('days'))
        except:
            self.add_error(None, u'Неверный период')
            return

        if date_param['type'] == 'range':
            if date_param['start_date'] > date_param['end_date']:
                self.add_error(None, u'Неверный конец периода')
        
            if date_param['start_date'] < self.course.start_date:
                self.add_error(None, u'Неверная дата')
        
            if date_param['end_date'] > self.course.end_date:
                self.add_error(None, u'Неверная дата')
        else:
            for date in date_param['dates']:
                if date > self.course.end_date or date < self.course.start_date:
                    self.add_error(None, u'Неверная дата')
                    break

        cleaned_data['date_param'] = date_param

        scheme = cleaned_data.get('scheme')
        if scheme == 'every':
            if not cleaned_data.get('every_n'):
                self.add_error('every_n', u'Обязательное поле.')
            else:
                cleaned_data['scheme_val'] = 'every:%d' % cleaned_data.get('every_n')
        elif scheme == 'weekdays':
            if not cleaned_data.get('weekdays'):
                self.add_error('weekdays', u'Обязательное поле.')
            else:
                cleaned_data['scheme_val'] = 'weekdays:%s' % (','.join([str(v) for v in cleaned_data.get('weekdays')]))
        else:
            cleaned_data['scheme_val'] = scheme

        return cleaned_data


def get_drug_data(start_date, end_date, course):
    def get_weeknum(d):
        return (d - course.start_date).days / 7 + 1

    today = timezone.now().date()
    qs = Dose.objects \
        .filter(course=course, date__range=[start_date, end_date]) \
        .select_related('drug')

    # упаковывает препараты по дням
    days = {}
    for q in qs:
        if q.date not in days:
            days[q.date] = []
        days[q.date].append(q)
    drug_by_date = {}
    for date, items in days.items():
        ds = {}
        for dose in items:
            if dose.drug.id not in ds:
                ds[dose.drug.id] = {
                    'drug': CourseService.get_drug_data(course, dose.drug),
                    'doses': [],
                    'amount': 0,
                    'is_marked': True
                }
            ds[dose.drug.id]['doses'].append(dose)
            ds[dose.drug.id]['amount'] += dose.amount
            if not dose.is_marked:
                ds[dose.drug.id]['is_marked'] = False
        drug_by_date[date] = ds
    
    cur_date = start_date
    data = []
    week = {
        'days': [],
        'total': {},
    }
    while cur_date <= end_date:
        drugs = drug_by_date.get(cur_date, {})
        for drug_id,drug in drugs.items():
            if drug_id not in week['total']:
                week['total'][drug_id] = {
                    'amount': 0,
                    'count': 0,
                    'drug': drug['drug'],
                }
            for dose in drug['doses']:
                week['total'][drug_id]['amount'] += dose.amount
                week['total'][drug_id]['count'] += 1

        week['days'].append({
            'drugs': drugs,
            'date': cur_date,
            'today': cur_date == today,
        })
        if 'date' not in week:
            week['date'] = cur_date
            week['weeknum'] = get_weeknum(cur_date)
        if cur_date.weekday() == 6:
            data.append(week)
            week = {
                'days': [],
                'total': {},
            }
        cur_date += timedelta(1)
    if start_date == end_date and len(week['days']):
        data.append(week)
    return data

def parse_date_param(date_str):
    days = json.loads(date_str)
    
    res = {'type': days['type']}
    
    if days['type'] == 'range':
        res['start_date'] = datetime.date.fromtimestamp(float(days['start']))
        res['end_date'] = datetime.date.fromtimestamp(float(days['end']))
        res['date_filter'] = {'date__range': [res['start_date'],res['end_date']]}
        res['day_count'] = (res['end_date'] - res['start_date']).days + 1
        res['min_date'] = prev_weekday(res['start_date'], 0)
        res['max_date'] = next_weekday(res['end_date'], 6)
    
    else:
        res['dates'] = [datetime.date.fromtimestamp(float(d)) for d in days['days']]
        res['date_filter'] = {'date__in': res['dates']}
        res['day_count'] = len(res['dates'])
        res['min_date'] = prev_weekday(min(res['dates']), 0)
        res['max_date'] = next_weekday(max(res['dates']), 6)
    res['min_date_ts'] = get_timestamp(res['min_date'])
    res['max_date_ts'] = get_timestamp(res['max_date'])
    res['week_count'] = (res['day_count'] - 1) / 7 + 1
    return res

def get_mark_form(d, course, user):
    qs = Dose.objects \
        .filter(course=course,date=d)

    data = {}
    for q in qs:
        if q.drug_id not in data:
            data[q.drug_id] = {
                'drug': CourseService.get_drug_data(course, q.drug),
                'doses': []
            }
        data[q.drug_id]['doses'].append(q)
    
    if not data:
        return None
    t = loader.get_template('drugplan/mark_form.html')
    c = Context({
        'course': course,
        'can_mark': course.access_check(user, 'mark'),
        'data': data.values(),
        'date': d,
    })
    return t.render(c)

@auth_check(ajax=False)
def edit(request, id):
    try:
        course = Course.objects.get(pk=id)
    except:
        raise Http404(u'Схема применения медикаментов не найдена.')

    if not course.access_check(request.user, 'edit'):
        raise Http404(u'У вас недостаточно прав.')

    class Form(forms.Form):
        name = forms.CharField(label=u'Название СМП', max_length=100)
        is_public = forms.BooleanField(label=u'Публичный доступ', required=False)
        sportsman = forms.CharField(label=u'Спортсмен', max_length=100)

        def clean(self):
            cleaned_data = super(Form, self).clean()
            username = cleaned_data.get('sportsman')
            if username:
                try:
                    cleaned_data['sportsman'] = User.objects.get(username=username)
                except:
                    self.add_error('sportsman', u'Пользователь не найден.')
            return cleaned_data

    if request.method == 'POST':
        form = Form(request.POST)
        if form.is_valid():
            name = request.POST.get('name') or plan.name
            course.name = form.cleaned_data['name']
            course.patient = form.cleaned_data['sportsman']
            course.is_public = form.cleaned_data['is_public']
            course.save()
            return redirect('spm_show', id=course.id)
        else:
            return render(request, 'drugplan/edit.html', {
                'course': course,    
                'form': form,    
            })
    else:
        form = Form(initial={
            'name': course.name,
            'is_public': course.is_public,
            'sportsman': course.patient.username,
        })

    return render(request, 'drugplan/edit.html', {
        'course': course,    
        'form': form,    
    })

@auth_check(ajax=False)
def ajax(request, id):
    try:
        course = Course.objects.get(pk=id)
    except:
        return HttpResponse(json.dumps({
            'status': 'error',
            'error': u'Схема применения медикаментов не найдена.',    
        }))

    direct = request.GET.get('direct', 'down')
    try:
        ts = float(request.GET.get('ts'))
        d = datetime.date.fromtimestamp(ts)
    except:
        d = None
    
    if direct == 'down':
        start_date = d
        end_date = start_date + timedelta(4*7)
        timestamp = get_timestamp(end_date)
    elif direct == 'up':
        end_date = d - timedelta(1)
        start_date = end_date - timedelta(4*7-1)
        timestamp = get_timestamp(start_date)
    
    if end_date < course.start_date or start_date > course.end_date:
        content = None
        timestamp = None    
    else:
        data = get_drug_data(start_date, end_date, course)
        t = loader.get_template('drugplan/calendar.html')
        c = Context({
            'data': data,
            'can_mark': course.access_check(request.user, 'mark'),
        })
        content = t.render(c)

    return HttpResponse(json.dumps({
        'ts': timestamp,
        'content': content,
    }))

@auth_check(ajax=False)
def update(request, id):
    try:
        course = Course.objects.get(pk=id)
    except:
        return HttpResponse(json.dumps({
            'status': 'error',
            'error': u'Неверный период', 
        }))

    try:
        start_date = datetime.date.fromtimestamp(float(request.GET.get('start')))
        end_date = datetime.date.fromtimestamp(float(request.GET.get('end')))
    except:
        return HttpResponse(json.dumps({
            'status': 'error',
            'error': u'Неверные даты.',    
        }))
    
    data = get_drug_data(start_date, end_date, course)
    t = loader.get_template('drugplan/calendar.html')
    c = Context({
        'data': data,
        'can_mark': course.access_check(request.user, 'mark'),
    })
    content = t.render(c)

    return HttpResponse(json.dumps({
        'status': 'success',
        'content': content.strip(),
    }))

@auth_check(ajax=False)
def show(request, id):
    try:
        course = Course.objects.get(pk=id)
    except:
        raise Http404(u'Схема применения медикаментов не найдена.')
    
    if not course.access_check(request.user, 'show'):
        raise Http404(u'У вас нет прав для просмотра схемы.')

    course.view_it()

    d = prev_weekday(timezone.now().date(), 0)
    start_date = max(d, course.start_date)
    end_date = start_date + timedelta(7*6)

    return render(request, 'drugplan/main.html', {
        'data': get_drug_data(start_date, end_date, course),
        'form': RegimenForm(request.user),
        'start_date': start_date,
        'end_date': end_date,
        'navbar_fixed': True,
        'course': course,
        'can_mark': course.access_check(request.user, 'mark'),
        'can_edit': course.access_check(request.user, 'edit'),
    })

@auth_check(ajax=False)
def add_form(request):
    form = RegimenForm(request.user)
    t = loader.get_template('drugplan/regimen_form.html')
    c = {'form': form}
    c.update(csrf(request))
    c = Context(c)        
    
    return HttpResponse(json.dumps({
        'status': 'success',
        'form': t.render(c),
    }))

@auth_check(ajax=False)
def add(request, id):
    try:
        course = Course.objects.get(pk=id)
    except:
        return HttpResponse(json.dumps({
            'status': 'error',
            'error': u'Схема применения медикаментов не найдена.',    
        }))

    form = None
    if request.method == 'POST':
        form = RegimenForm(request.user, request.POST)
        form.course = course
    
        if form.is_valid():
            scheme = form.cleaned_data['scheme_val']
            date_param = form.cleaned_data['date_param']

            amount_comments = [(form.cleaned_data['amount'], form.cleaned_data['comment'])]
            for i in range(2, 6):
                a = form.cleaned_data['amount_%d' % i]
                c = form.cleaned_data['comment_%d' % i]
                if a:
                    amount_comments.append((a, c))

            drug = form.cleaned_data.get('drug') or form.cleaned_data.get('new_drug')

            CourseService.add_regimen(course, drug, scheme, date_param, amount_comments)

            return HttpResponse(json.dumps({
                'status': 'success',
                'min_date': date_param['min_date_ts'],
                'max_date': date_param['max_date_ts'],
            }))

    if not form:
        form = RegimenForm(request.user)
    
    t = loader.get_template('drugplan/regimen_form.html')
    c = {'form': form}
    c.update(csrf(request))
    c = Context(c)        
    
    return HttpResponse(json.dumps({
        'status': 'error',
        'form': t.render(c)
    }))

@auth_check(ajax=False)
def period(request, id):
    try:
        course = Course.objects.get(pk=id)
    except:
        return HttpResponse(json.dumps({
            'status': 'error',
            'error': u'Схема применения медикаментов не найдена.',    
        }))

    try:
        date_param = parse_date_param(request.GET.get('days'))
    except:
        return HttpResponse(json.dumps({
            'status': 'error',
            'error': u'Неверный период', 
        }))

    qs = Dose.objects \
        .filter(course=course,**date_param['date_filter']) \
        .select_related('drug')
    doses = {}
    for d in qs:
        if d.drug_id not in doses:
            doses[d.drug_id] = {
                'amount': 0,
                'count': 0,
                'drug': {
                    'id': d.drug.id,
                    'name': d.drug.name,
                    'measure': course.get_measure(d.drug),
                    'pack_size': d.drug.pack_size,
                }
            }

        doses[d.drug_id]['amount'] += d.amount
        doses[d.drug_id]['count'] += 1
    
    for drug_id in doses:
        doses[drug_id]['avg_day'] = doses[drug_id]['amount'] / float(date_param['day_count'])
        doses[drug_id]['avg_week'] = doses[drug_id]['amount'] / float(date_param['week_count'])

    return HttpResponse(json.dumps({
        'status': 'success',
        'data': doses.values(),
        'day_count': date_param['day_count'],
        'week_count': date_param['week_count'],
    }))

@auth_check(ajax=False)
def drug_delete(request, id):
    try:
        course = Course.objects.get(pk=id)
    except:
        return HttpResponse(json.dumps({
            'status': 'error',
            'error': u'Схема применения медикаментов не найдена.',    
        }))

    try:
        date_param = parse_date_param(request.POST.get('days'))
    except:
        return HttpResponse(json.dumps({
            'status': 'error',
            'error': u'Неверный период', 
        }))
    
    try:
        ids = request.POST.getlist('for_delete')
    except:
        return HttpResponse(json.dumps({
            'status': 'error',
            'error': u'Неверные параметры', 
        }))

    if len(ids):
        qs = Dose.objects \
            .filter(course=course,drug_id__in=ids,**date_param['date_filter']) 
        qs.delete()

        return HttpResponse(json.dumps({
            'status': 'success',
            'min_date': date_param['min_date_ts'],
            'max_date': date_param['max_date_ts'],
        }))

@auth_check(ajax=False)
def mark(request, id):
    try:
        course = Course.objects.get(pk=id)
    except:
        return HttpResponse(json.dumps({
            'status': 'error',
            'error': u'Схема применения медикаментов не найдена.',    
        }))

    try:
        d = datetime.date.fromtimestamp(float(request.GET.get('ts')))
    except:
        return HttpResponse(json.dumps({
            'status': 'error',
            'error': u'Неверная дата.',    
        }))

    content = get_mark_form(d, course, request.user) or '<i>нет лекарств для приема</i>'

    return HttpResponse(json.dumps({
        'status': 'success',
        'content': content,
    }))

@auth_check(ajax=False)
def dose_mark(request):
    try:
        dose = Dose.objects.get(pk=request.GET.get('id'))
    except:
        return HttpResponse(json.dumps({
            'status': 'error',
            'error': u'Прием не найден.',    
        }))

    if dose.course.author_id != request.user.id:
        return HttpResponse(json.dumps({
            'status': 'error',
            'error': u'Ошибка доступа.',    
        }))

    if dose.is_marked == True:
        dose.is_marked = False
    elif dose.is_marked == None:
        dose.is_marked = True
    elif dose.is_marked == False:
        dose.is_marked = None
    dose.marked_at = timezone.now()
    dose.save()

    return HttpResponse(json.dumps({
        'status': 'success',
        'result': dose.is_marked,
    }))

@csrf_protect
@auth_check(ajax=False)
def delete(request, id):
    try:
        course = Course.objects.get(pk=id)
    except:
        raise Http404(u'Схема применения медикаментов не найдена.')

    if course.is_deleted:
        raise Http404(u'План удален.')

    if not course.access_check(request.user, 'delete'):
        raise Http404(u'У вас нет прав для удаления схемы применения медикаментов id=%d.' % course.id)

    course.is_deleted = True
    course.save()

    return redirect('spm_list')