#coding=utf8
from django.http import Http404
from django.db.models import Q
from django.shortcuts import render

from drugplan.models import *


def list(request):
    qs = Drug.objects \
        .select_related('group') \
        .order_by('group', 'name')

    if request.user.is_authenticated():
        qs = qs.filter(Q(user__isnull=True) | Q(user=request.user))
    else:
        qs = qs.filter(user__isnull=True)


    return render(request, 'drugplan/drug/list.html', {
        'groups': DrugGroup.objects.all(),
        'group_id': request.GET.get('group_id', '-1'),
        'name': request.GET.get('name', ''),
        'list': qs,
        'menu': 'drug_list',
    })


def show(request, id):
    try:
        e = Drug.objects.get(pk=id)
    except:
        raise Http404(u'Медикамент в базе не найден.')

    return render(request, 'drugplan/drug/show.html', {
        'obj': e,
    })