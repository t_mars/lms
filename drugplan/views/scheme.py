#coding=utf8
import json

from django.core.context_processors import csrf
from django.http import HttpResponse, Http404
from django.core.urlresolvers import reverse
from django.shortcuts import render
from django import forms
from django.template import loader, Context

from drugplan.models import *
from main.models import prev_weekday
from common.views import auth_check

from courses import courses

class CourseObject():
    def __init__(self, course):
        self.course = course
        for d in self.course['drugs']:
            if d['type'] == 'select':
                for o in d['options']:
                    o['doses'] = dict(o['doses'])
            elif d['type'] == 'checkbox':
                d['doses'] = dict(d['doses'])
            elif d['type'] == 'drug':
                d['doses'] = dict(d['doses'])

    def get_form(self, *args, **kwargs):
        class Form(forms.Form):
            name = forms.CharField(label=u'Название схемы приема медикаментов', max_length=100, initial=self.course['name']) 
            start_date = forms.DateField(label=u'Дата начала', 
                widget=forms.TextInput(attrs={
                    'data-provide':'datepicker',
                }),
                input_formats=['%d.%m.%Y'],
                initial=next_weekday(timezone.now().date(), 0).strftime('%d.%m.%Y'))

            def __init__(self, obj, *args, **kwargs):
                super(Form, self).__init__(*args, **kwargs)
                self.obj = obj
                for ind,d in enumerate(self.obj.course['drugs']):
                    if d['type'] == 'select':
                        choices = []
                        for o in d['options']:
                            choices.append( (o['drug_id'], o['name']) )
                        self.fields['f_%d' % ind] = forms.ChoiceField(label=d['name'], choices=choices)
                    elif d['type'] == 'checkbox':
                        self.fields['f_%d' % d['drug_id']] = forms.BooleanField(label=d['name'], required=False)
                
            def add_drug(self, d):
                self.drugs.append(d)

            def clean(self, *args, **kwags):
                cleaned_data = super(Form, self).clean(*args, **kwargs)

                self.drugs = []
                for ind,d in enumerate(self.obj.course['drugs']):
                    if d['type'] == 'select':
                        drug_id = int(cleaned_data['f_%d' % ind])
                        for o in d['options']:
                            if o['drug_id'] == drug_id:
                                self.add_drug(o)
                                break
                    elif d['type'] == 'checkbox':
                        if cleaned_data.get('f_%d' % d['drug_id']):
                            self.add_drug(d)
                    elif d['type'] == 'drug':
                        self.add_drug(d)

                cleaned_data['drugs'] = self.drugs

                return cleaned_data

        return Form(self, *args, **kwargs)

    @property
    def drugs(self):
        if not hasattr(self, '_drugs'):
            ids = []
            for d in self.course['drugs']:
                if d['type'] == 'select':
                    for o in d['options']:
                        ids.append(o['drug_id'])
                elif d['type'] == 'checkbox':
                    ids.append(d['drug_id'])
                elif d['type'] == 'drug':
                    ids.append(d['drug_id'])
            qs = Drug.objects.filter(pk__in=ids)
            self._drugs = {}
            for q in qs:
                self._drugs[q.id] = q
        
        return self._drugs

    def _table_add_drug(self, d):
        drug = self.drugs[d['drug_id']]
        self._table += u'<td class="drug_%d">%s, %s</td>' % (drug.id, drug.short_name, drug.measure)

    def _table_add_doses(self, d, day_ind):
        if day_ind in d['doses']:
            if d['drug_id'] not in self._week_count:
                self._week_count[d['drug_id']] = 0
            self._week_count[d['drug_id']] += sum([p[0] for p in d['doses'][day_ind]])

            self._table += '<td class="drug_%d">%s</td>' % (d['drug_id'], '+'.join(['<span title="%s">%d</span>' % (p[1], p[0]) for p in d['doses'][day_ind]]))
        else:
            self._table += '<td>-</td>'

    def _table_add_week(self, d):
        self._table += '<td class="drug_%s">%d</td>' % (d['drug_id'], self._week_count.get(d['drug_id'], 0))
        
    def get_table(self):
        self._table = '<table class="table table-striped">'
        day_count = 0
        
        # head
        self._table += u'<thead><tr><td>День</td>' 
        for ind,d in enumerate(self.course['drugs']):
            if d['type'] == 'select':
                for o in d['options']:
                    self._table_add_drug(o)
                    day_count = max(o['doses'].keys() + [day_count])
            elif d['type'] == 'checkbox':
                self._table_add_drug(d)
                day_count = max(d['doses'].keys() + [day_count])
            elif d['type'] == 'drug':
                self._table_add_drug(d)
                day_count = max(d['doses'].keys() + [day_count])
        self._table += '</tr></thead>'

        # body
        self._table += '<tbody>'
        self._week_count = {}
        week_ind = 1
        for day_ind in range(1,day_count+1):
            if day_ind % 7 == 1 or day_ind == 1:
                self._table += u'<tr class="info day"><td>Неделя %d</td>' % (week_ind)
                for ind,d in enumerate(self.course['drugs']):
                    if d['type'] == 'select':
                        for o in d['options']:
                            self._table += '<td class="drug_%d"></td>' % (o['drug_id'])
                    elif d['type'] == 'checkbox':
                        self._table += '<td class="drug_%d"></td>' % (d['drug_id'])
                    elif d['type'] == 'drug':
                        self._table += '<td class="drug_%d"></td>' % (d['drug_id'])
                        
                self._table += '</tr>'

            self._table += '<tr class="day"><td class="date" data-day-ind="%d"></td>' % day_ind
            for ind,d in enumerate(self.course['drugs']):
                if d['type'] == 'select':
                    for o in d['options']:
                        self._table_add_doses(o, day_ind)
                elif d['type'] == 'checkbox':
                    self._table_add_doses(d, day_ind)
                elif d['type'] == 'drug':
                    self._table_add_doses(d, day_ind)
            self._table += '</tr>'
            
            
            if (day_ind % 7 == 0 and day_ind != 0) or day_ind == day_count:
                self._table += u'<tr class="active"><td>Итог %d</td>' % (week_ind)
                week_ind += 1
                for ind,d in enumerate(self.course['drugs']):
                    if d['type'] == 'select':
                        for o in d['options']:
                            self._table_add_week(o)
                    elif d['type'] == 'checkbox':
                        self._table_add_week(d)
                    elif d['type'] == 'drug':
                        self._table_add_week(d)
                self._table += '</tr>'
                self._week_count = {}
        self._table += '</tbody>'
        self._table += '</table>'
        
        return self._table


def list(request):
    return render(request, 'drugplan/scheme/list.html', {
        'list': [CourseObject(c) for c in courses],
        'menu': 'spm_scheme_list',
    })


def show(request, num):
    try:
        course = CourseObject(courses[int(num)-1])
    except:
        raise Http404(u'СПМ не найдено.')

    return render(request, 'drugplan/scheme/show.html', {
        'form': course.get_form(),
        'obj': course.course,
        'table': course.get_table(),
    })


@auth_check(ajax=True)
def create(request, num):
    try:
        course = CourseObject(courses[int(num)-1])
    except:
        raise Http404(u'СПМ не найдено.')

    form = course.get_form(request.POST)
    if form.is_valid():
        course = Course()
        course.author = request.user
        course.patient = request.user
        course.name = form.cleaned_data['name']
        course.start_date = prev_weekday(form.cleaned_data['start_date'], 0)
        course.save()

        # сохраняем приемы
        doses = []
        for drug in form.cleaned_data['drugs']:
            for day_ind,ds in drug['doses'].items():
                d = course.start_date + timedelta(day_ind-1)
                for amount,comment in ds: 
                    dose = Dose()
                    dose.course = course
                    dose.drug_id = drug['drug_id']
                    dose.date = d
                    dose.amount = amount
                    dose.comment = comment
                    doses.append(dose)
        Dose.objects.bulk_create(doses)

        return HttpResponse(json.dumps({
            'status': 'success',
            'redirect': reverse('spm_show', args=[course.id]),
        }))
    else:
        t = loader.get_template('drugplan/scheme/form.html')
        c = {'form': form}
        c.update(csrf(request))
        c = Context(c)
        return HttpResponse(json.dumps({
            'status': 'error',
            'form': t.render(c),
        }))
