# coding=utf8

from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.core.urlresolvers import reverse
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django import forms

from drugplan.models import *

from common.views import auth_check


@auth_check(ajax=False)
def list(request):
    """
    Список курсов пользователя
    """
    created_qs = Course.objects \
        .filter(author=request.user, is_deleted=False)
    mine_qs = Course.objects \
        .filter(patient=request.user, is_deleted=False)
    public_qs = Course.objects \
        .filter(is_public=True, is_deleted=False)
    
    if 'created' in request.GET:
        qs = created_qs
        mode = 'created'
    elif 'public' in request.GET:
        qs = public_qs
        mode = 'public'
    else:
        qs = mine_qs
        mode = 'mine'
    
    paginator = Paginator(qs.select_related('patient', 'author'), 10)
    page = request.GET.get('page')
    try:
        objs = paginator.page(page)
    except PageNotAnInteger:
        objs = paginator.page(1)
    except EmptyPage:
        objs = paginator.page(paginator.num_pages)

    for o in objs:
        o.can_delete = o.access_check(request.user, 'delete')
    
    return render(request, 'drugplan/list.html', {
        'list': objs,
        'mode': mode,
        'created_count': created_qs.count(),
        'mine_count': mine_qs.count(),
        'public_count': public_qs.count(),
        'menu': 'spm_list',
    })


@auth_check(ajax=False)
def create(request):
    """
    Создание курса вручную
    """
    class CourseForm(forms.Form):
        name = forms.CharField(label=u'Название', max_length=100)
        start_date = forms.DateField(label=u'Дата начала', 
            widget=forms.TextInput(attrs={
                'data-provide':'datepicker',
                'data-date-days-of-week-disabled':'2,3,4,5,6,0',
            }),
            input_formats=['%d.%m.%Y'],
            initial=next_weekday(timezone.now().date(), 0).strftime('%d.%m.%Y'))

    if request.method == 'POST':
        form = CourseForm(request.POST)
        if form.is_valid():
            d = form.cleaned_data['start_date']
            if d != prev_weekday(d, 0):
                form.add_error('start_date', u'Начать курс можно только в понедельник')
            else:
                course = Course()
                course.author = request.user
                course.patient = request.user
                course.start_date = d
                course.name = form.cleaned_data['name']
                course.save()

                return HttpResponseRedirect(reverse('spm_show', args=[course.id]))
    else:
        form = CourseForm()

    return render(request, 'drugplan/create.html', {
        'form': form,    
    })
