#coding=utf8
import os

from django.core.management.base import BaseCommand

from upload_video import get_authenticated_service, initialize_upload

from videohub.services.video import VideoService
from videohub.models import Video

class Command(BaseCommand):
    def add_arguments(self, parser):
        parser.add_argument('video_id', type=int)

    def handle(self, *args, **options):
        service = VideoService()

        video_id = options['video_id']

        try:
            video = Video.objects.get(pk=video_id)
        except:
            print 'not found'
            return

        if video.is_uploaded is not None:
            print 'is uploaded'
            return

        filename = '/var/www/video/%s' % video.hash
        base_dir = os.path.dirname(__file__)
        storage_filename = os.path.join(base_dir, 'youtube-oauth2.json')
        client_secrets_filename = os.path.join(base_dir, 'client_secret.json')

        if not os.path.exists(filename):
            print 'error file'

        title = video.name
        if video.title:
            title += (u' (%s)' % video.title)

        args = {
            'file': filename,
            'title': title,
            'description': video.description,
            'category': "22",
            'keywords': "",
            'privacyStatus': "unlisted", # ("public", "private", "unlisted")
        }

        class Struct2:
            def __init__(self, **entries):
                self.__dict__.update(entries)
        args = Struct2(**args)

        def progress(status):
            p = int(status.progress() * 100)
            service.set_video_upload_progress(video_id, {'status': 'progress', 'percent': p})
            print '%d%%' % p

        youtube = get_authenticated_service(storage_filename, client_secrets_filename, args)
        try:
            youtube_id = initialize_upload(youtube, args, progress)
        except Exception, e:
            service.set_video_upload_progress(video_id, {'status': 'error'})
            print unicode(e)
            return

        video.youtube_id = youtube_id
        video.is_uploaded = True
        video.save()

        service.set_video_upload_progress(video_id, {'status': 'success'})

        print 'video_id =', video_id
