#coding=utf8
import tempfile

from django.shortcuts import render
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.http.response import Http404, HttpResponseRedirect
from django.core.urlresolvers import reverse
from django.views.decorators.csrf import csrf_protect

from common.views import auth_check
from videohub.services.video import VideoService
from videohub.models import Video
from main.models import Exercise


@auth_check(ajax=False)
def video_list(request):
    """
    Личный список видео пользователя
    """
    qs = Video.objects.get_active().filter(user=request.user)
    paginator = Paginator(qs, 50)
    page = request.GET.get('page')
    try:
        objs = paginator.page(page)
    except PageNotAnInteger:
        objs = paginator.page(1)
    except EmptyPage:
        objs = paginator.page(paginator.num_pages)

    return render(request, 'videohub/list.html', {
        'objs': objs,
        'menu': 'video_list',
    })


def show(request, id):
    try:
        video = Video.objects.get_active().get(pk=id)
    except:
        raise Http404()

    if not video.is_public and \
            (not request.user.is_authenticated() or request.user.id != video.user_id):
        raise Http404()

    return render(request, 'videohub/show.html', {
        'video': video,
    })


@csrf_protect
@auth_check(ajax=False)
def delete(request, id):
    try:
        video = Video.objects.get_active().get(pk=id)
    except:
        raise Http404(u'Видео не найдено')

    if request.user.id != video.user_id:
        raise Http404(u'Нет доступа.')

    VideoService.delete_video(video)

    return HttpResponseRedirect(request.META.get('HTTP_REFERER', reverse('video_list')))
