#coding=utf8
from django.core.cache import caches

from common.service import Service
from main.models import TrainingScheme, Exercise
from videohub.models import Video


class VideoService(Service):
    @classmethod
    def get_video_upload_progress(cls, video_id):
        """
        Возвращает статус загрузки видео в ютуб
        """
        cache = caches['temp']
        k = cls.generate_cache_key([cls.TMP_VIDEO_UPLOAD_PROGRESS, video_id])
        return cache.get(k)

    @classmethod
    def set_video_upload_progress(cls, video_id, progress):
        """
        Устанавливает статус загрузки видео в ютуб
        """
        cache = caches['temp']
        k = cls.generate_cache_key([cls.TMP_VIDEO_UPLOAD_PROGRESS, video_id])
        return cache.set(k, progress, timeout=60)

    @classmethod
    def reset_video_upload_progress(cls, video_id):
        """
        Сбрасывает статус загрузки видео в ютуб
        """
        cache = caches['temp']
        k = cls.generate_cache_key([cls.TMP_VIDEO_UPLOAD_PROGRESS, video_id])
        return cache.delete(k)

    @classmethod
    def add_video(cls, title, description, is_public, user, youtube_id=None, hash=None, executed_at=None,
                     scheme_id=None, exercise_id=None, weight=None, reps=None, app=None):
        video = Video()

        # источник
        if hash:
            video.hash = hash
            # Если файл загружался ранее, просто копируем его youtube_id
            vs = Video.objects.filter(hash=hash, is_uploaded=True)
            if vs.count() > 0:
                video.youtube_id = vs[0].youtube_id
                video.is_uploaded = True
            else:
                video.is_uploaded = None

        elif youtube_id:
            video.youtube_id = youtube_id
            video.is_uploaded = False

        # устаналиваем схему
        if scheme_id:
            try:
                scheme = TrainingScheme.objects \
                    .select_related('execution__training__plan') \
                    .get(pk=scheme_id)
            except:
                raise Exception(u'Не найдено указанное повторение')

            if app is None or app < 1 or app > scheme.apps:
                raise Exception(u'Номер подхода может быть от 1 до %d' % scheme.apps)

            if not scheme.execution.training.plan.access_check(user, 'add_video'):
                raise Exception(u'У вас недостаточно прав для добавления видео к этому повторению.')
            #TODO: проверять скрытая ли схема, иначе будет доступ к данным раскладки

            video.exercise_id = scheme.execution.exercise_id
            video.reps = scheme.reps
            video.weight = scheme.weight
            video.scheme_id = scheme.id
            video.executed_at = scheme.execution.training.date
            video.app = app
            video.user_id = scheme.execution.training.plan.sportsman_id

        else:
            try:
                Exercise.objects.get(pk=exercise_id)
            except:
                raise Exception(u'Указанное упражнение не найдено')

            if reps is None:
                raise Exception(u'Не указано количество повторений')
            elif reps < 1 or reps > 100:
                raise Exception(u'Количество повторений может быть от 1 до 100')

            if weight is None:
                raise Exception(u'Не указан вес')
            elif weight < 1 or weight > 600:
                raise Exception(u'Вес не может быть от 1 до 600 килограмм')

            if executed_at is None:
                raise Exception(u'Не указана дата выполнения')

            if not app is None and (app < 1 or app > 20):
                raise Exception(u'Номер подхода может быть от 1 до 20')

            video.exercise_id = exercise_id
            video.reps = reps
            video.weight = weight
            video.executed_at = executed_at
            video.app = app

        video.title = title
        video.description = description
        video.is_public = is_public
        video.user_id = user.pk
        video.save()

        if video.scheme:
            cls.clear_cache([
                [cls.CK_TRAIN_VISIBLE, video.scheme.execution.training_id],
                [cls.CK_TRAIN_HIDDEN, video.scheme.execution.training_id],
            ])

        return video

    @classmethod
    def delete_video(cls, video):
        video.is_deleted = True
        video.save()

        # удаляем кеш тренировки
        if video.scheme:
            cls.clear_cache([
                [cls.CK_TRAIN_VISIBLE, video.scheme.execution.training_id],
                [cls.CK_TRAIN_HIDDEN, video.scheme.execution.training_id],
            ])
