from django.contrib import admin

from videohub.models import *


class VideoAdmin(admin.ModelAdmin):
    list_display = ['name', 'user', 'youtube_id', 'is_uploaded', 'is_deleted', 'hash']
admin.site.register(Video, VideoAdmin)

