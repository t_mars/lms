#coding=utf8


from django.db import models
from django.contrib.auth.models import User
from django.utils import timezone

from main.models import Exercise, TrainingScheme


class VideoManager(models.Manager):
    def get_active(self):
        return super(VideoManager, self).get_queryset().filter(is_deleted=False, is_uploaded__isnull=False)


class Video(models.Model):
    objects = VideoManager()

    youtube_id = models.TextField(max_length=12)
    hash = models.TextField(max_length=32, null=True, blank=True, default=None)

    #  None - ожидает загрузки, True - видео загружено в канал LMS, False - видео вставленно по ссылке
    is_uploaded = models.NullBooleanField(default=False)
    is_public = models.BooleanField(default=True)
    is_deleted = models.BooleanField(default=False)

    title = models.CharField(max_length=200)
    description = models.TextField(null=True, blank=True, default=None)
    created_at = models.DateTimeField(auto_now_add=True)

    user = models.ForeignKey(User, verbose_name=u'Автор')
    executed_at = models.DateField(verbose_name=u'Дата выполнения')

    exercise = models.ForeignKey(Exercise, verbose_name=u'Упражнение', null=True, blank=None, default=None)
    weight = models.FloatField(verbose_name=u'Вес', null=True, blank=None, default=None)
    reps = models.SmallIntegerField(verbose_name=u'Повторения', null=True, blank=None, default=None)
    app = models.SmallIntegerField(verbose_name=u'Номер подхода', null=True, blank=None, default=None)

    scheme = models.ForeignKey(TrainingScheme, verbose_name=u'Выполнение', null=True, blank=True, default=None,
                               on_delete=models.SET_NULL)

    view_count = models.PositiveIntegerField(verbose_name=u'Количество просмотров', default=0)

    def save(self, *args, **kwargs):
        if not self.executed_at:
            self.executed_at = timezone.now().date()
        super(Video, self).save(*args, **kwargs)

    @property
    def name(self):
        res = u'%s %g на %s' % (self.exercise.name, self.weight, self.reps)
        if self.app:
            res += u' (%d подход)' % self.app
        return res
