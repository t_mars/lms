from django.conf.urls import patterns, url
from django.contrib import admin

admin.autodiscover()

urlpatterns = patterns('',
    url(r'^(?P<id>\d+)/$', 'videohub.views.show', name='video_show'),
    url(r'^(?P<id>\d+)/delete/$', 'videohub.views.delete', name='video_delete'),
    url(r'^list/$', 'videohub.views.video_list', name='video_list'),
)
