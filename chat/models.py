#coding=utf8

from django.db import models
from django.contrib.auth.models import User


class Message(models.Model):
    user = models.ForeignKey(User)
    content = models.CharField(max_length=1000)
    created_at = models.DateTimeField(auto_now_add=True)
    is_hidden = models.BooleanField(default=False)


class ChatStat(models.Model):
    count = models.PositiveIntegerField()
    guests_count = models.PositiveIntegerField(default=0)
    created_at = models.DateTimeField(auto_now_add=True)