# coding=utf8

import os
import json
import re
import time
import pprint
import datetime

from django.core.management.base import BaseCommand
import django
from django.conf import settings
from django.utils import timezone

import tornado.ioloop
import tornado.web
import tornado.websocket
import tornado.auth
import tornado.gen

from chat.models import Message, ChatStat, User
from main.models import get_timestamp
from profile.models import CustomUser

from HTMLParser import HTMLParser

current_time = lambda: int(round(time.time() * 1000))


class MLStripper(HTMLParser):
    def __init__(self):
        self.reset()
        self.fed = []

    def handle_data(self, d):
        self.fed.append(d)

    def get_data(self):
        return ''.join(self.fed)


def strip_tags(html):
    s = MLStripper()
    s.feed(html)
    return s.get_data()


class My404Handler(tornado.web.RequestHandler):
    def prepare(self):
        self.set_status(404)
        self.render("404.html")


class Application(tornado.web.Application):
    def __init__(self):
        handlers = [
            (r'/ws', WebSocketHandler),
        ]
        settings = dict(
            cookie_secret="your_cookie_secret",
            template_path=os.path.join('/var/www/lms_test/private/chat', 'templates'),
            static_path=os.path.join('/var/www/lms_test/private/chat', 'static'),
            login_url='/',
            xsrf_cookies=True,
            debug=True,
            default_handler_class=My404Handler,
        )
        tornado.web.Application.__init__(self, handlers, **settings)


class BaseHandler(tornado.web.RequestHandler):
    def get_current_user(self):
        user = self.get_secure_cookie("username")
        if user:
            user = user.decode("utf-8")
        return user

    def check_origin(self, origin):
        return True


class WebSocketHandler(BaseHandler, tornado.websocket.WebSocketHandler):
    connections = set()
    users = {}
    guests_count = 0
    auth_times = {}

    url_re = re.compile(ur'http[s]?://(?:[a-zA-Z]|[0-9]|[$-_@.&+]|[!*\(\),]|(?:%[0-9a-fA-F][0-9a-fA-F]))+')

    def pack_user(self, user):
        u = user if type(user) is CustomUser else CustomUser.GET(username=user.username)
        return {
            'name': u.fullname, 
            'username': u.username, 
            'id': u.id, 
            'is_superuser': u.is_superuser,
            'is_banned': u.is_banned,
            'avatar_50': u.get_avatar_50(),
            'is_mobile': WebSocketHandler.users.get(u.id, {}).get('is_mobile', False),
        }

    def pack_message(self, m):
        content = m.content

        recipients = []
        if '@' in content:
            usernames = [u[1:].lower() for u in re.findall(ur'@[а-яa-z0-9_\.]+', content, flags=re.I|re.U)]
            users = CustomUser.objects.filter(username__in=usernames).distinct()
            for u in users:
                username = u.username.lower()
                if username in usernames:
                    content = content.replace(
                        '@'+username, 
                        '<u><a href="//ws.last-man.org/user/%s/" target="_blank">%s</a></u>' % (u.username, u.fullname)
                    )
                    recipients.append(u.id)
        
        # links
        urls = self.url_re.findall(content)
        for url in urls:
            content = content.replace(
                url,
                '<u><a href="%s" target="_blank">%s</a></u>' % (url, url)
            )

        return {
            'id': m.id,
            'user': self.pack_user(m.user),
            'content': content,
            'recipients': recipients,
            'date': get_timestamp(m.created_at),
        }

    def auth(self, session_key, is_mobile=False):
        # проверка повторных запросов
        t = current_time()
        if self.request.remote_ip in WebSocketHandler.auth_times \
                and (t - WebSocketHandler.auth_times[self.request.remote_ip]) < 3000:
            print self.request.remote_ip, 'blocked'
            return False
        WebSocketHandler.auth_times[self.request.remote_ip] = t

        engine = django.utils.importlib.import_module(settings.SESSION_ENGINE)

        class Dummy(object):
            pass

        django_request = Dummy()
        django_request.session = engine.SessionStore(session_key)
        user = django.contrib.auth.get_user(django_request)

        if user.is_authenticated():
            self.user = CustomUser.GET(username=user.username)
            if self.user.id not in WebSocketHandler.users:
                WebSocketHandler.users[self.user.id] = {'count': 0}

            WebSocketHandler.users[self.user.id]['count'] += 1
            WebSocketHandler.users[self.user.id]['is_mobile'] = is_mobile


        else:
            self.user = user
            WebSocketHandler.guests_count += 1

        return True

    def open(self):
        # определяем пользователя
        session_key = self.get_cookie(settings.SESSION_COOKIE_NAME)

        if not self.auth(session_key):
            return
        WebSocketHandler.connections.add(self)

        # рассылаем последние сообщения
        mqs = Message.objects.filter(is_hidden=False).order_by('-id')[:30:-1]
        uqs = User.objects.filter(pk__in=WebSocketHandler.users.keys()).distinct().order_by('id')

        # рассылаем количество онлайн пользователей
        self.send_to_all({
            'type': 'online',
            'count': len(WebSocketHandler.users),
            'guests_count': WebSocketHandler.guests_count,
            'user': self.pack_user(self.user) if self.user.is_authenticated() else None,
        })
        self.write_message({
            'type': 'load',
            'user': self.pack_user(self.user) if self.user.is_authenticated() else None,
            'msgs': [self.pack_message(m) for m in mqs],
            'users': [self.pack_user(u) for u in uqs],
        })

    def on_close(self):
        WebSocketHandler.connections.remove(self)

        if self.user.is_authenticated():
            WebSocketHandler.users[self.user.id]['count'] -= 1
            if WebSocketHandler.users[self.user.id]['count'] == 0:
                del WebSocketHandler.users[self.user.id]
                deleted_id = self.user.id
        else:
            WebSocketHandler.guests_count -= 1
            deleted_id = None

            # рассылаем количество онлайн пользователей
        self.send_to_all({
            'type': 'offline',
            'count': len(WebSocketHandler.users),
            'guests_count': WebSocketHandler.guests_count,
            'user_id': deleted_id,
        })

    def on_message(self, data):
        try:
            data = json.loads(data)
            if data['type'] == 'message':
                self.send_messages(data['content'])

            elif data['type'] == 'auth':
                print 'auth', data
                self.auth(data['sessionid'], is_mobile=True)

            elif data['type'] == 'get_messages':
                d = timezone.now() - datetime.timedelta(days=3) 
                qs = Message.objects \
                    .filter(id__lt=data['id'], created_at__gte=d, is_hidden=False) \
                    .order_by('-id')[:50]
                ms = list(qs)
                self.write_message({
                    'type': 'messages',
                    'msgs': [self.pack_message(m) for m in ms]    
                })
        except:
            self.send_messages(data)

    def send_messages(self, content):
        content = strip_tags(content).strip()
        if not content:
            return

        if not self.user.is_authenticated():
            self.write_message({
                'type': 'alert',
                'content': u'Вы не авторизированы. Чтобы писать сообщения пожалуйста авторизируйтесь.'
            })
            return

        if self.user.is_banned:
            print 'alert'
            self.write_message({
                'type': 'alert',
                'content': u'Вы забанены и не можете оставлять сообщения в чате.'
            })
            return

        # перезагрузка и сброс
        if self.user.is_superuser and content in ['reload', 'reset']:
            self.send_to_all({'type': content})
            return

        # удаление сообщений
        if self.user.is_superuser and content.startswith('del'):
            qs = Message.objects.filter(pk__in=content.split(' ')[1:])
            ids = [m.id for m in qs]
            qs.update(is_hidden=True)
            self.send_to_all({
                'type': 'del',
                'ids': ids
            })
            return

        # бан пользователей
        if self.user.is_superuser and content.startswith('ban'):
            uids = [u[1:].lower() for u in re.findall(ur'@[а-яa-z0-9_\.]+', content, flags=re.I | re.U)]
            qs = CustomUser.objects \
                .filter(username__in=uids) \
                .distinct()
            for u in qs:
                u.is_banned = True
                u.save()
                self.send_to_user(u.id, {'type': 'reload'})

            self.send_to_all({
                'type': 'ban',
                'users': [self.pack_user(u) for u in qs]
            })
            return

        m = Message()
        m.user = self.user
        m.content = content
        m.save()

        self.send_to_all({
            'type': 'msg',
            'msg': self.pack_message(m)
        })

    def send_to_all(self, message):
        for conn in WebSocketHandler.connections:
            conn.write_message(message)

    def send_to_user(self, id, message):
        for conn in WebSocketHandler.connections:
            if conn.user.id != id:
                continue
            conn.write_message(message)


def test():
    print 'save_stat', len(WebSocketHandler.users)
    s = ChatStat()
    s.count = len(WebSocketHandler.users)
    s.guests_count = WebSocketHandler.guests_count
    s.save()


class Command(BaseCommand):

    def handle(self, *args, **options):
        port = int(os.environ.get("PORT", settings.CHAT_SERVER_PORT))
        print 'port', settings.CHAT_SERVER_PORT

        app = Application()
        app.listen(port)
        
        main_loop = tornado.ioloop.IOLoop.instance()
        
        sched = tornado.ioloop.PeriodicCallback(test,5*60*1000, io_loop = main_loop)
        'sched start'
        sched.start()
        
        'main_loop start'
        main_loop.start()