from django.contrib import admin

from chat.models import *

class MessageAdmin(admin.ModelAdmin):
    list_display = ['id', 'content', 'is_hidden', 'user', 'created_at']
admin.site.register(Message, MessageAdmin)

