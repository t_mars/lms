import sys
import os

sys.path.append('/var/www/lms_test/private')
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "main.settings")

from django.conf import settings
import django.contrib.auth
import django.utils.importlib
import django

django.setup()

import tornado.ioloop
import tornado.web
import tornado.websocket
import tornado.auth
import tornado.gen

from models import *

# from tornado.options import define, options, parse_command_line
# define('port', default=8000, help='run on the given port', type=int)


class Application(tornado.web.Application):
    def __init__(self):
        handlers = [
            (r'/', MainHandler),
            (r'/chat', ChatHandler),
            (r'/ws', WebSocketHandler),
            (r'/login', LoginHandler),
            (r'/logout', LogoutHandler),
        ]
        settings = dict(
            cookie_secret="your_cookie_secret",
            template_path=os.path.join(os.path.dirname(__file__), 'templates'),
            static_path=os.path.join(os.path.dirname(__file__), 'static'),
            login_url='/',
            xsrf_cookies=True,
            debug=True,
        )
        tornado.web.Application.__init__(self, handlers, **settings)


class BaseHandler(tornado.web.RequestHandler):
    def get_current_user(self):
        user = self.get_secure_cookie("username")
        if user:
            user = user.decode("utf-8")
        return user


class MainHandler(BaseHandler):
    def get(self):
        if self.current_user:
            self.redirect('/chat')
        self.render('index.html')


class ChatHandler(BaseHandler):
    @tornado.web.authenticated
    def get(self):
        self.render('chat.html', user=self.current_user)


class WebSocketHandler(BaseHandler, tornado.websocket.WebSocketHandler):
    connections = set()
    
    def open(self):
        conn = WebSocketHandler.connections.add(self)
        for message in self.messages:
            self.write_message(message)

    def on_close(self):
        WebSocketHandler.connections.remove(self)

    def on_message(self, msg):
        self.send_messages(msg)

    def send_messages(self, msg):
        for conn in self.connections:
            msg = 
            message = {'name': self.current_user, 'msg': msg}
            conn.write_message(message)

class LoginHandler(tornado.web.RequestHandler):
    @tornado.gen.coroutine
    def get(self):
        engine = django.utils.importlib.import_module(settings.SESSION_ENGINE)
        session_key = self.get_cookie(settings.SESSION_COOKIE_NAME)

        class Dummy(object):
            pass

        django_request = Dummy()
        django_request.session = engine.SessionStore(session_key)
        user = django.contrib.auth.get_user(django_request)
        self.set_secure_cookie('username', user.username)
        self.user = user
        self.redirect("/chat")
        return

class LogoutHandler(tornado.web.RequestHandler):
    def get(self):
        self.clear_all_cookies()
        self.redirect("/")

def main():
    # parse_command_line()
    port = int(os.environ.get("PORT", 5000))
    app = Application()
    app.listen(port)
    tornado.ioloop.IOLoop.instance().start()


if __name__ == '__main__':
    main()
