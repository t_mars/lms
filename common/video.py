#coding=utf8
from urlparse import urlparse, parse_qs
import urllib
import json


def get_video_info(url):
    video_id = get_youtube_video_id(url)
    if not video_id:
        return None

    return get_youtube_video_info(video_id)


def get_youtube_video_id(value):
    """
    Examples:
    - http://youtu.be/SA2iWivDJiE
    - http://www.youtube.com/watch?v=_oPAwA_Udwc&feature=feedu
    - http://www.youtube.com/embed/SA2iWivDJiE
    - http://www.youtube.com/v/SA2iWivDJiE?version=3&amp;hl=en_US
    """
    query = urlparse(value)
    if query.hostname == 'youtu.be':
        return query.path[1:]
    if query.hostname in ('www.youtube.com', 'youtube.com'):
        if query.path == '/watch':
            p = parse_qs(query.query)
            return p['v'][0]
        if query.path[:7] == '/embed/':
            return query.path.split('/')[2]
        if query.path[:3] == '/v/':
            return query.path.split('/')[2]

    return None


def get_youtube_video_info(video_id):
    url = 'https://noembed.com/embed?url=https://www.youtube.com/watch?v=' + video_id
    response = urllib.urlopen(url)
    data = json.loads(response.read())

    try:
        return {
            'title': data['title'],
            'preview': data['thumbnail_url'],
            'iframe': 'https://www.youtube.com/embed/' + video_id
        }
    except:
        return None
