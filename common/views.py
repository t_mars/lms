#coding=utf8
import json

from django.shortcuts import redirect
from django.http import HttpResponse
from django.contrib.auth import logout


def json_response(**kwargs):
    """
    Возвращает json
    """
    return HttpResponse(json.dumps(kwargs), content_type='application/json')


def get_client_ip(request):
    """
    Возвращает IP по запросу
    """
    x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
    if x_forwarded_for:
        ip = x_forwarded_for.split(',')[0]
    else:
        ip = request.META.get('REMOTE_ADDR', '')
    return ip


def auth_check(ajax):
    """
    Декоратор, который проверяет, чтобы запрос был от авторизированного пользователя.
    Если пользователь неавторизирован, переадресует на страницу авторизации
    """
    def decorator(function):
        def wrapper(request, *args, **kwargs):
            if request.user.is_authenticated() and not request.user.is_active:
                logout(request)
            if not request.user.is_authenticated():
                if ajax:
                    return json_response(
                        status='error',
                        type='auth_error',
                        error=u'Ошибка доступа.',
                        message=u'Ошибка доступа.'
                    )

                else:
                    request.session['redirect'] = request.path
                    return redirect('auth')

            return function(request, *args, **kwargs)

        wrapper.__doc__ = function.__doc__
        wrapper.__name__ = function.__name__
        return wrapper
    return decorator


def superuser_check(function):
    """
    Декоратор, который проверяет, чтобы запрос был от авторизированного СУПЕРпользователя.
    Если пользователь неавторизирован, переадресует на страницу авторизации
    Если не суперпользователь, переадресует на главную
    """
    def wrapper(request, *args, **kwargs):
        if not request.user.is_authenticated():
            return redirect('auth')
        if not request.user.is_superuser:
            return redirect('home')
        return function(request, *args, **kwargs)

    wrapper.__doc__ = function.__doc__
    wrapper.__name__ = function.__name__
    return wrapper


def json_error(message, **kwargs):
    """
    Возвращает json ошибку
    """
    return json_response(
        status='error',
        message=message,
        **kwargs
    )


def api_data(function):
    """
    Декоратор который парсит данных из тела заголовка json
    """
    def wrap(request, *args, **kwargs):
        try:
            data = json.loads(request.body)
        except:
            return json_error(u'Ошибка запроса.')
        return function(request, data, *args, **kwargs)

    wrap.__doc__ = function.__doc__
    wrap.__name__ = function.__name__
    return wrap