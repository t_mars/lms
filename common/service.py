#coding=utf8

from django.core.cache import cache
from django.conf import settings


class Service(object):
    CK_TRAIN_VISIBLE = 'train:v4:visible'
    CK_TRAIN_HIDDEN = 'train:v4:hidden'
    CK_WEEK_STAT = 'week_stat:v1'
    CK_PLAN_STAT = 'plan_stat:v2'
    CK_CYCLE = 'cycle:v5'
    CK_CYCLE_NEW = 'cycle_new:v3'
    CK_ALL_STAT = 'all_stat:v1'

    TMP_VIDEO_UPLOAD_PROGRESS = 'video_upload_progress:v1'

    @classmethod
    def generate_cache_key(cls, args):
        """
        Генерирует ключ по которому идет кеширование
        """
        return '_'.join([str(arg) for arg in args])

    @classmethod
    def cache_get(cls, keys, refresh_func, timeout):
        """
        Проверяет наличие данных,
            если данных нет обновляет и возвращает
        """

        cache_key = cls.generate_cache_key(keys)

        is_cached = True
        if not cache.get(cache_key) or settings.DEBUG:
            is_cached = False
            cache.set(cache_key, refresh_func(), timeout)

        data = cache.get(cache_key)
        data['__is_cached'] = is_cached

        return data

    @classmethod
    def cache_set_data(cls, keys, data, timeout):
        """
        Устанавливает данные в кеш по ключю
        """
        cache.set(cls.generate_cache_key(keys), data, timeout)

    @classmethod
    def cache_get_data(cls, keys):
        """
        Возвращает данные из кеша
        """
        return cache.get(cls.generate_cache_key(keys))

    @classmethod
    def clear_cache(cls, list_keys):
        """
        Сбрасывает из кеша по списку ключей
        """
        cache.delete_many([cls.generate_cache_key(keys) for keys in list_keys])
