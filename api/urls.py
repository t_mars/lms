from django.conf.urls import patterns, include, url

from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    url(r'^auth/$', 'api.views.auth.main', name='api_auth'),
    url(r'^auth/login/(?P<backend>[^/]+)/$', 'api.views.auth.auth', name='socialauth_begin'),
    url(r'^auth/complete/(?P<backend>[^/]+)/$', 'api.views.auth.complete', name='socialauth_complete'),
    url(r'^auth/disconnect/$', 'api.views.auth.disconnect', name='api_auth_disconnect'),
    url(r'^check-auth/$', 'api.views.auth.check', name='api_check_auth'),
    url(r'^auth/done/$', 'api.views.auth.done', name='api_auth_done'),

    url(r'^ws/trains/$', 'api.views.ws.trains', name='api_ws_trains'),
    url(r'^ws/meds/$', 'api.views.ws.meds', name='api_ws_meds'),
    url(r'^ws/resume/$', 'api.views.ws.resume', name='api_ws_resume'),
    
    url(r'^dose/mark/$', 'api.views.spm.dose_mark', name='api_dose_mark'),
    url(r'^spm/(?P<id>[\d]+)/(?P<ts>[\d]+)/$', 'api.views.spm.show', name='api_spm_show'),
    url(r'^spm/(?P<id>[\d]+)/up/(?P<ts>[\d]+)/$', 'api.views.spm.show_up', name='api_spm_show_up'),
    url(r'^spm/(?P<id>[\d]+)/down/(?P<ts>[\d]+)/$', 'api.views.spm.show_down', name='api_spm_show_down'),
    url(r'^spm/list/$', 'api.views.spm.list', name='api_spm_list'),
    
    url(r'^cycle/list/$', 'api.views.cycle.list', name='api_cycle_list'),
    url(r'^cycle/(?P<num>[\w\d]+)/$', 'api.views.cycle.show', name='api_cycle_show'),
    url(r'^cycle/(?P<num>[\w\d]+)/create-plan/$', 'api.views.cycle.create_plan', name='api_cycle_create_plan'),
    
    url(r'^train/(?P<id>[\d]+)/$', 'api.views.train.show', name='api_train_show'),
    url(r'^train/(?P<id>[\d]+)/record/$', 'api.views.train.record', name='api_train_record'),

    url(r'^plan/(?P<id>[\d]+)/(?P<ts>[\d]+)/$', 'api.views.plan.show', name='api_plan_show'),
    url(r'^plan/(?P<id>[\d]+)/up/(?P<ts>[\d]+)/$', 'api.views.plan.show_up', name='api_plan_show_up'),
    url(r'^plan/(?P<id>[\d]+)/down/(?P<ts>[\d]+)/$', 'api.views.plan.show_down', name='api_plan_show_down'),
    url(r'^plan/list/$', 'api.views.plan.list', name='api_plan_list'),

    url(r'^exercise/list/$', 'api.views.exercise.list', name='api_exercise_list'),
    url(r'^exercise/(?P<id>[\d]+)/$', 'api.views.exercise.show', name='api_exercise_show'),
    
    url(r'^util/converter/$', 'api.views.util.converter', name='api_util_converter'),
    
    url(r'^profile/add-weight/$', 'api.views.user.add_weight', name='api_profile_add_weight'),
    url(r'^profile/weight-stat/$', 'api.views.user.weight_stat', name='api_profile_weight_stat'),
    url(r'^user/(?P<id>[\d]+)/$', 'api.views.user.show', name='api_user_show'),
    url(r'^user/edit/$', 'api.views.user.edit', name='api_user_edit'),

    url(r'^video/list/$', 'api.views.video.video_list', name='api_video_list'),
    url(r'^video/info/$', 'api.views.video.info', name='api_video_info'),
    url(r'^video/save/$', 'api.views.video.save', name='api_video_save'),
    url(r'^video/info/$', 'api.views.video.info', name='api_video_info'),
    url(r'^video/show/$', 'api.views.video.show', name='api_video_show'),
    url(r'^video/delete/$', 'api.views.video.delete', name='api_video_delete'),
    url(r'^video/get_progress/$', 'api.views.video.get_progress', name='api_video_get_progress'),
    url(r'^video/exercise_list/$', 'api.views.video.exercise_list', name='api_video_exercise_list'),

    url(r'^payment/list/$', 'api.views.payment_views.payment_list', name='api_payment_list'),
    url(r'^payment/pay/$', 'api.views.payment_views.pay', name='api_payment_pay'),
    url(r'^payment/done/$', 'api.views.payment_views.done', name='api_payment_done'),

    url(r'^like/toggle/$', 'api.views.like.toggle', name='api_like_toggle'),
    url(r'^like/list/$', 'api.views.like.list', name='api_like_list'),

    # v2
    url(r'^v2/', include('api.v2_urls'))
)