#coding=utf8
import json

from django.views.decorators.csrf import csrf_exempt
from django.core.files.images import get_image_dimensions
import awesome_avatar

from main.models import get_timestamp
from common.views import auth_check, json_response, json_error, api_data
from profile.models import *
from profile.services.user import UserService
from profile.forms import UserEditForm


@auth_check(ajax=True)
def weight_stat(request):
    qs = WeightRecord.objects \
        .filter(user=request.user) \
        .order_by('-date')
    
    stat = []
    for q in qs[:30]:
        stat.append({
            'date': get_timestamp(q.date),
            'weight': q.value,    
        })

    return json_response(
        stat=stat[::-1]
    )


@csrf_exempt
@auth_check(ajax=True)
@api_data
def add_weight(request, data):
    try:
        try:
            weight = float(data.get('weight'))
        except:
            raise Exception(u'Не указан вес')

        try:
            d = datetime.datetime.strptime(data['date'], '%d.%m.%y').date()
        except:
            raise Exception(u'Не указана дата.')
    
        if d < datetime.date(2016, 1, 1):
            raise Exception(u'Дата не может быть раньше 1 января 2016 года.')
        if d > timezone.now().date() + timedelta(days=1):
            raise Exception(u'Дата не может быть больше сегодняшнего числа.')

        try:
            r = WeightRecord.objects.get(user=request.user, date=d)
        except:
            r = WeightRecord()
            r.user = request.user
            r.date = d
        r.value = weight
        r.save()

        return json_response(
            status='success'
        )
    except Exception as e:
        return json_response(
            status='error',
            message=unicode(e)
        )


@auth_check(ajax=True)
def show(request, id):
    try:
        user = User.objects.get(id=id)
    except:
        return json_error(u'Пользователь не найден')

    return json_response(
        status='success',
        user=UserService.get_user_info(id)
    )


@csrf_exempt
@auth_check(ajax=True)
def edit(request):
    if request.method != 'POST':
        return json_error(u'Ошибка запроса')

    data = None
    try:
        data = json.loads(request.body)
    except:
        pass

    if data:
        request.POST._mutable = True
        for k, v in data.iteritems():
            request.POST[k] = v

    form = UserEditForm(request.POST, request.FILES, initial={'avatar': request.user.avatar})
    form.user = request.user
    if not form.is_valid():
        return json_error(u'Ошибка данных', fields=form.errors)

    data = form.clean()
    if data['avatar'] and isinstance(data['avatar'], dict):
        w, h = get_image_dimensions(data['avatar']['file'])
        if abs(w-h) < 2:
            w = h = min(w, h)
        if w != h:
            return json_error(u'Изображение неверного разрешения %d на %d' % (w, h))

        data['avatar']['box'] = [0, 0, w, h]
        avatar_field = awesome_avatar.fields.AvatarField()
        avatar_field.name = 'avatar'
        avatar_field.save_form_data(request.user, data['avatar'])

    request.user.username = data['username'].lower()
    request.user.first_name = data['first_name']
    request.user.last_name = data['last_name']
    request.user.gender = data['gender']
    request.user.allpowerlifting = data['allpowerlifting']
    request.user.save()

    return json_response(
        status='success',
        user=UserService.get_user_info(request.user.id)
    )
