#coding=utf8
import datetime
from django.db.models import Q

from drugplan.models import *
from drugplan.services.course import CourseService
from profile.services.user import UserService
from main.models import *
from common.views import auth_check, json_response, json_error


def course_access(func):
    def decorator(request, id, *args, **kwargs):
        try:
            course = Course.objects.get(pk=id)
        except:
            return json_error(u'СПМ не найден. (#1)')

        if not course.access_check(request.user, 'show'):
            return json_error(u'У вас нет прав для просмотра СПМ. (#2)')

        return func(request, course, *args, **kwargs)
    return decorator


def get_drugs(course, start, end):
    def get_weeknum(d):
        return (d - course.start_date).days / 7 + 1

    qs = Dose.objects \
        .filter(course=course,date__range=[start,end]) \
        .select_related('drug')

    # упаковывает препараты по дням
    dose_by_days = {}
    for q in qs:
        if q.date not in dose_by_days:
            dose_by_days[q.date] = []
        dose_by_days[q.date].append(q)
    
    # упаковывает приемы по препаратам
    drug_by_date = {}
    for date, doses in dose_by_days.items():
        ds = {}
        for dose in doses:
            if dose.drug.id not in ds:
                ds[dose.drug.id] = CourseService.get_drug_data(course, dose.drug)
                ds[dose.drug.id]['doses'] = []

            ds[dose.drug.id]['doses'].append({
                'comment': dose.comment,
                'amount':  dose.amount,
                'is_marked': dose.is_marked,
                'marked_at': get_timestamp(dose.marked_at) if dose.marked_at else None,    
            })
        drug_by_date[date] = ds.values()
    
    d = start
    days = []
    while d <= end:
        d += timedelta(days=1)
        day = {
            'date': get_timestamp(d),
            'drugs': drug_by_date.get(d, {}),
        }
        if d.weekday() == 0:
            day['weeknum'] = get_weeknum(d)
        days.append(day)
    return days


@auth_check(ajax=True)
def dose_mark(request):
    from drugplan.models import Dose

    try:
        dose = Dose.objects.get(pk=request.GET.get('id'))
    except:
        return json_error(u'Прием не найден.')

    if dose.course.patient_id != request.user.id:
        return json_error(u'Ошибка доступа.')

    if dose.is_marked:
        dose.is_marked = False
    elif dose.is_marked is None:
        dose.is_marked = True
    else:
        dose.is_marked = None
    dose.marked_at = timezone.now()
    dose.save()

    return json_response(
        status='success',
        is_marked=dose.is_marked
    )


@auth_check(ajax=True)
def list(request):
    qs = Course.objects \
        .filter(Q(author=request.user) or Q(patient=request.user), is_deleted=False)
    
    objs = []
    for q in qs:
        obj = {
            'name': q.name,
            'id': q.id,
            'can_delete': q.access_check(request.user, 'delete'),
            'patient': UserService.get_user_short_info(q.patient_id),
            'author': UserService.get_user_short_info(q.author_id),
        }
        objs.append(obj)

    return json_response(
        list=objs
    )


@auth_check(ajax=True)
@course_access
def show(request, course, ts):
    d = datetime.date.fromtimestamp(float(ts))
    d = max(d, course.start_date)
    d = min(d, course.end_date)
    start = prev_weekday(d, 0) - timedelta(days=1)
    end = start + timedelta(days=13)

    return json_response(
        days=get_drugs(course, start, end),
        start=get_timestamp(start),
        end=get_timestamp(end),
        course={
            'id': course.id,
            'name': course.name,
            'patient': UserService.get_user_short_info(course.patient_id),
            'author': UserService.get_user_short_info(course.author_id)
        },
    )


@auth_check(ajax=True)
@course_access
def show_down(request, course, ts):
    d = datetime.date.fromtimestamp(float(ts))

    if d >= course.end_date:
        return json_response(
            days=[],
            end=None
        )

    start = d + timedelta(days=1)
    end = min(start + timedelta(days=13), course.end_date-timedelta(days=1))

    return json_response(
        days=get_drugs(course, start, end),
        end=get_timestamp(end)
    )


@auth_check(ajax=True)
@course_access
def show_up(request, course, ts):
    d = datetime.date.fromtimestamp(float(ts))

    if d <= course.start_date:
        return json_response(
            days=[],
            start=None
        )

    end = d - timedelta(days=1)
    start = max(end - timedelta(days=13), course.start_date-timedelta(days=1))

    return json_response(
        days=get_drugs(course, start, end),
        start=get_timestamp(start)
    )
