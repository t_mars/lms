#coding=utf8
import datetime

from django.utils import timezone

from common.views import auth_check, json_response
from main.services.train import TrainService
from main.services.plan import PlanService
from drugplan.services.course import CourseService

@auth_check(ajax=True)
def resume(request):
    from main.models import Training
    from drugplan.models import Dose
    
    try:
        d = datetime.datetime.strptime(request.GET.get('date'), '%d.%m.%y').date()
    except:
        d = timezone.now().date()
    
    plans = request.user.get_plans()
    trains = Training.objects.filter(plan__in=plans, date=d).count()
    
    courses = request.user.get_courses()
    meds = Dose.objects.filter(course__in=courses,date=d).count()

    return json_response(
        train_count=trains,
        med_count=meds
    )


@auth_check(ajax=True)
def trains(request):
    from main.models import Training
    
    try:
        d = datetime.datetime.strptime(request.GET.get('date'), '%d.%m.%y').date()
    except:
        d = timezone.now().date()
    
    trains = []
    for p in request.user.get_plans():
        qs = Training.objects.filter(plan=p, date=d)
        for t in qs:
            data = TrainService.get_train_data(t, request.user)
            data['plan'] = PlanService.get_plan_data(p)
            trains.append(data)

    return json_response(
        list=trains
    )


@auth_check(ajax=True)
def meds(request):
    from drugplan.models import Dose

    try:
        d = datetime.datetime.strptime(request.GET.get('date'), '%d.%m.%y').date()
    except:
        d = timezone.now().date()

    meds = []
    for course in request.user.get_courses():
        qs = Dose.objects \
            .filter(course=course,date=d)

        drugs = {}
        for q in qs:
            if q.drug_id not in drugs:
                drugs[q.drug_id] = CourseService.get_drug_data(course, q.drug)
                drugs[q.drug_id]['doses'] = []

            drugs[q.drug_id]['doses'].append({
                'id': q.id,
                'comment': q.comment,
                'amount': q.amount,
                'is_marked': q.is_marked,
            })
        if drugs:
            meds.append({
                'course': {
                    'id': course.id,
                    'name': course.name,
                },
                'drugs': drugs.values()
            })

    return json_response(
        list=meds
    )
