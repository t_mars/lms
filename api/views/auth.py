#coding=utf8
from social_auth.views import auth as sa_auth, complete as sa_complete

from django.shortcuts import render
from django.views.decorators.clickjacking import xframe_options_exempt
from django.contrib.auth import logout
from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse

from common.views import json_response, json_error
from profile.services.user import UserService


@xframe_options_exempt
def main(request):
    return render(request, 'api/auth.html', {
        'message': request.GET.get('message', '')
    })


@xframe_options_exempt
def auth(request, backend):
    if request.user.is_authenticated():
        logout(request)
    request.GET._mutable = True
    request.GET['next'] = reverse('api_auth_done')
    return sa_auth(request, backend)


@xframe_options_exempt
def complete(request, backend):
    return sa_complete(request, backend)


@xframe_options_exempt
def disconnect(request):
    if request.user.is_authenticated():
        logout(request)
    return json_response(status='success')


@xframe_options_exempt
def done(request):
    if request.user.is_authenticated():
        return render(request, 'api/auth_done.html')
    return HttpResponseRedirect(reverse('api_auth') + u'?message=Авторизация не удалась, пожалуйста попробуйте снова.')


def check(request):
    if not request.user.is_authenticated():
        return json_error(u'Вы уже авторизированы')

    return json_response(
        status='success',
        user=UserService.get_user_info(request.user.id),
        csrftoken=request.COOKIES.get('csrftoken'),
        sessionid=request.COOKIES.get('sessionid'),
    )

