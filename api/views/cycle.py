#coding=utf8
import datetime

from django.views.decorators.csrf import csrf_exempt
from django.middleware.csrf import _get_new_csrf_key
from django.conf import settings

from common.views import auth_check, api_data, json_response
from main.models import Cycle, Plan
from main.services.cycle import CycleService
from profile.services.user import UserService


def list(request):
    from main.models import Cycle
    
    cycles = []
    
    qs = Cycle.objects.all().order_by('order')
    if not settings.DEBUG:
        qs = qs.filter(is_active=True)

    ratios = {
        'dec': 'Сниженное',
        'norm': 'Нормальное',
        'inc': 'Повышенное',
    }

    for c in qs:
        cycles.append({
            'num': c.num,
            'name': c.name,
            'sport': c.sport,
            'gender': c.gender,
            'level': c.level,
            'mode': c.mode,
            'abs_weight': c.abs_weight,
            'extra': c.extra,
            'hw_ratio': ratios.get(c.hw_ratio),
        })

    return json_response(
        list=cycles
    )


def show(request, num):

    try:
        cycle = Cycle.objects.get(num=num)
    except:
        return json_response(
            status='error',
            message=u'Цикл не найден',
        )

    data = CycleService.get_cycle_data(cycle)

    return json_response(
        status='success',
        data=data,
        csrf=_get_new_csrf_key(),
    )


@csrf_exempt
@auth_check(ajax=True)
@api_data
def create_plan(request, data, num):
    from main.models import Cycle

    try:
        cycle = Cycle.objects.get(num=num)
    except:
        return json_response(
            status='error',
            message='СРЦ не найдено.'
        )

    # принимаем параметры
    try:
        try:
            week_days = [int(d) for d in data.get('week_days')]
        except:
            raise Exception(u'Неверно выбраны дни недели.')

        if len(week_days) < len(cycle.get_week_days()):
            raise Exception(u'Неверно выбраны дни недели.')

        start_weights = {}
        name = data.get('plan_name')
        if not name:
            raise Exception(u'Введите название плана.')

        for e in cycle.get_exercises():
            eid = str(e.id)
            if eid not in data['weights']:
                raise Exception(u'Не установлен вес для упражнения "%s"' % e.name)
            try:
                w = float(data['weights'][eid])
            except:
                raise Exception(u'Не корректный вес для упражнения "%s"' % e.name)
            
            if w <= 0 or w > 500:
                raise Exception(u'Указан неверный вес для упражнения "%s" (допустимо больше 0 до 500)' % e.name)
            
            start_weights[eid] = w
        try:
            correct_percent = float(data['correct_percent'])
        except:
            raise Exception(u'Не установлено значение для процента корректировки')
        
        if correct_percent < -5 or correct_percent > 5:
            raise Exception(u'Неверное значение для процента корректировки (допустимо от -5 до 5)')
        
        try:
            start_date = datetime.datetime.strptime(data['start_date'], '%d.%m.%y').date()
        except:
            raise Exception(u'Неверная установлена дата начала.')
        if start_date.weekday() != 0:
            raise Exception(u'Начать можно только в понедельник.')

        # проверка активных планов на указанный период
        plans = CycleService.check_plans(request.user, start_date)

        if not request.user.is_superuser and len(plans) > 2:
            raise Exception(u"На выбранный период у вас активно %d планов по СРЦ<br>"
                            u"Вы не можете иметь более 2 активных планов по СРЦ одновременно. "
                            u"Удалите один из планов или поменяйте дату начала." % len(plans))

    except Exception as e:
        return json_response(
            status='error',
            message=unicode(e)
        )

    plan = CycleService.create_plan(cycle, name, start_date, week_days, start_weights, request.user, correct_percent)
    UserService.open_trains(request.user)

    return json_response(
        status='success',
        id=plan.id
    )
