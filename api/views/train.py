#coding=utf8
import datetime

from django.views.decorators.csrf import csrf_exempt

from common.views import auth_check, json_error, json_response, api_data
from main.models import Training
from main.services.train import TrainService


@auth_check(ajax=True)
def show(request, id):
    """
    Возвращает данные о тренировке, если есть доступ.
    """
    try:
        train = Training.objects.get(pk=id)
    except:
        return json_error(u'Тренировка не найдена')

    if not train.plan.access_check(request.user, 'show'):
        return json_error(u'Ошибка доступа')

    return json_response(
        status='success',
        train=TrainService.get_train_data(train, request.user)
    )


@csrf_exempt
@auth_check(ajax=True)
@api_data
def record(request, data, id):
    """
    Сохранение данные о состоянии тренировки.
    """
    try:
        train = Training.objects.get(pk=id)
    except:
        return json_error(u'Тренировка не найдена')

    if not train.plan.access_check(request.user, 'train_edit'):
        return json_error(u'Ошибка доступа')

    try:
        start = datetime.datetime.utcfromtimestamp(int(data['start'])/1000)
    except:
        return json_error(u'Неверное время начала тренировки.')

    try:
        end = datetime.datetime.utcfromtimestamp(int(data['end'])/1000)
    except:
        return json_error(u'Неверное время конца тренировки.')

    if start > end:
        return json_error(u'Неверное время конца тренировки.')

    if 'time_breaks' in data:
        time_breaks = data['time_breaks']
    else:
        return json_error(u'Неверные данные.')

    resume = data.get('resume', '')

    try:
        TrainService.record_state(train, request.user, resume, start, end, time_breaks)
    except Exception as e:
        return json_error(unicode(e))

    return json_response(
        status='success',
        train=TrainService.get_train_data(train, request.user)
    )
