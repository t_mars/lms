#coding=utf8

import json

from django.contrib.contenttypes.models import ContentType
from django.core.exceptions import ObjectDoesNotExist
from django.http.response import HttpResponse
from django.template import loader, Context
from django.utils import timezone

from profile.models import Like
from forum.models import Message
from profile.services.notification import NotificationService


def jsonp_response(func):
    """
    A decorator thats takes a view response and turns it
    into json. If a callback is added through GET or POST
    the response is JSONP.
    """
    def decorator(request, *args, **kwargs):
        objects = func(request, *args, **kwargs)
        if isinstance(objects, HttpResponse):
            return objects
        try:
            data = json.dumps(objects)
            # a jsonp response!
            data = '%s(%s);' % (request.REQUEST['callback'], data)
            return HttpResponse(data, "text/javascript")
        except:
            data = json.dumps(str(objects))
        return HttpResponse(data, "application/json")
    return decorator


@jsonp_response
def toggle(request):
    if not request.user.is_authenticated():
        return {
            'status': 'error',
            'message': u'Чтобы отметь запись лайком, нужно авторизироваться'
        }

    try:
        ps = request.GET['id'].split('_')
        content_type_id = int(ps[0])
        object_id = int(ps[1])
    except:
        return {
            'status': 'error',
            'message': u'Ошибка параметров'
        }

    content_type = ContentType.objects.get(id=content_type_id)

    object = None
    try:
        if content_type_id == 42: # forum message
            object = Message.objects.get(pk=object_id)
        else:
            return {
                'status': 'error',
                'message': u'Неверный тип объекта'
            }

    except ObjectDoesNotExist:
        return {
            'status': 'error',
            'message': u'Объект не найден'
        }

    like, created = Like.objects.get_or_create(
        target_content_type_id=content_type_id,
        target_object_id=object_id,
        user_id=request.user.id
    )

    if not created:
        like.is_active = not like.is_active
        like.created_at = timezone.now()
        like.save()

    # удаление уведомлений
    if content_type_id == 42:
        if like.is_active:
            NotificationService.notify('forum_message_like', like, object.user)
        else:
            NotificationService.delete_notifies('forum_message_like', like, object.user)

    likes = Like.objects.filter(
        target_content_type_id=content_type.id,
        target_object_id=object_id,
        is_active=True
    )

    users = []
    for l in likes[:3]:
        users.append({
            'username': l.user.username,
            'fullname': l.user.fullname,
            'avatar': l.user.get_avatar(),
            'id': l.user.id,
        })

    t = loader.get_template('profile/like_widget.html')
    c = Context({
        'is_active': like.is_active,
        'count': likes.count(),
        'users': users,
        'id': '%d_%d' % (content_type_id, object_id)
    })

    return {
        'status': 'success',
        'template': t.render(c)
    }


@jsonp_response
def list(request):
    try:
        ps = request.GET['id'].split('_')
        content_type_id = int(ps[0])
        object_id = int(ps[1])
    except:
        return {
            'status': 'error',
            'message': u'Неверный тип объекта'
        }

    content_type = ContentType.objects.get(id=content_type_id)

    likes = Like.objects.filter(
        target_content_type_id=content_type.id,
        target_object_id=object_id,
        is_active=True
    ).select_related('user')

    t = loader.get_template('profile/like_list.html')
    c = Context({'likes': likes})

    return {
        'status': 'success',
        'template': t.render(c)
    }
