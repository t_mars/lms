#coding=utf8
import datetime

from django.db.models import Q

from common.views import auth_check, json_response, json_error
from profile.services.user import UserService
from main.models import *
from main.services.plan import PlanService


def plan_access(func):
    def decorator(request, id, *args, **kwargs):
        try:
            plan = Plan.objects.get(pk=id)
        except:
            return json_error(u'План не найден. (#1)')

        if not plan.access_check(request.user, 'show'):
            return json_error(u'У вас нет прав для просмотра плана. (#2)')

        return func(request, plan, *args, **kwargs)
    return decorator


@auth_check(ajax=True)
@plan_access
def show(request, plan, ts):
    # plan.view_it()
    
    sportsman = None
    if plan.sportsman:
        sportsman = {
            'username': plan.sportsman.username,
            'id': plan.sportsman.id,
        }
    
    d = prev_weekday(datetime.date.fromtimestamp(float(ts)), 0)
    
    if d < plan.start_date:
        start = plan.start_date
    elif d <= plan.last_date:
        start = d
    elif (d - plan.last_date).days < 7:
        start = d - timedelta(days=7)
    else:
        start = plan.first_date
    end = start + timedelta(days=13)
    
    return json_response(
        plan={
            'id': plan.id,
            'name': plan.name,
            'sportsman': sportsman,
            'author': {
                'username': plan.author.username,
                'id': plan.author.id,
            }
        },
        start_str=start.strftime("%d.%m.%Y"),
        end_str=end.strftime("%d.%m.%Y"),
        days=PlanService.get_plan_days(plan, start, end, request.user),
        start=get_timestamp(start),
        end=get_timestamp(end)
    )


@auth_check(ajax=True)
@plan_access
def show_down(request, plan, ts):
    d = datetime.date.fromtimestamp(float(ts))

    if d >= plan.end_date:
        return json_response(
            days=[],
            end=None
        )

    start = d + timedelta(days=1)
    end = min(start + timedelta(days=13), plan.end_date-timedelta(days=1))

    return json_response(
        start_str=start.strftime("%d.%m.%Y"),
        end_str=end.strftime("%d.%m.%Y"),
        days=PlanService.get_plan_days(plan, start, end, request.user),
        end=get_timestamp(end)
    )


@auth_check(ajax=True)
@plan_access
def show_up(request, plan, ts):
    d = datetime.date.fromtimestamp(float(ts))

    if d <= plan.start_date:
        return json_response(
            days=[],
            start=None
        )

    end = d - timedelta(days=1)
    start = max(end - timedelta(days=13), plan.start_date)

    return json_response(
        start_str=start.strftime("%d.%m.%Y"),
        end_str=end.strftime("%d.%m.%Y"),
        days=PlanService.get_plan_days(plan, start, end, request.user),
        start=get_timestamp(start)
    )


@auth_check(ajax=True)
def list(request):
    qs = Plan.objects \
        .filter(Q(author=request.user) or Q(sportsman=request.user), is_deleted=False) \
        .order_by('-created_at') \
        .select_related('sportsman', 'author')

    objs = []
    for p in qs:
        plan = {
            'name': p.name,
            'id': p.id,
            'can_delete': p.access_check(request.user, 'delete'),
            'sportsman': UserService.get_user_short_info(p.sportsman_id),
            'author': UserService.get_user_short_info(p.author_id)
        }
        objs.append(plan)
    
    return json_response(
        list=objs,
    )
