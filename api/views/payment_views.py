#coding=utf8

from django.http import HttpResponseRedirect

from payment.models import Invoice, INVOICE_MODE_CHOICE
from payment.views import get_rk_url, get_yk_url
from common.views import json_error, json_response, auth_check


@auth_check(ajax=True)
def payment_list(request):
    qs = Invoice.objects \
        .filter(user=request.user) \
        .order_by('-created_at')

    invoices = []
    for i in qs:
        invoices.append({
            'status': i.get_status(),
            'sum': i.sum,
            'description': i.get_description(),
            'date': i.created_at.strftime('%d.%m.%y %H:%M')
        })

    taxes = []
    for m in [1, 3, 6]:
        p = Invoice()
        p.user = request.user
        p.set_month(m)
        taxes.append({
            'months': m,
            'sum': p.sum,
        })

    return json_response(
        status='success',
        invoices=invoices,
        taxes=taxes
    )


@auth_check(ajax=True)
def pay(request):
    invoice = Invoice()
    invoice.user = request.user
    invoice.mode = 'subscribe'

    try:
        months = int(request.GET.get('m'))
    except:
        months = 3

    try:
        invoice.set_month(months)
    except Exception as e:
        return json_error(unicode(e))

    qs = Invoice.objects \
        .filter(user_id=invoice.user_id,
                months=invoice.months,
                sum=invoice.sum,
                mode=invoice.mode,
                status=0)

    if qs.count() > 0:
        invoice = qs[0]
    else:
        invoice.save()

    invoice.payment_system = 'rk'
    invoice.save()

    url = get_rk_url(invoice)

    return HttpResponseRedirect(url)


@auth_check(ajax=True)
def done(request):
    pass