#coding=utf8

import subprocess
import sys
import hashlib
import datetime

from django.views.decorators.csrf import csrf_exempt, csrf_protect
from django.conf import settings
from ratelimit.utils import is_ratelimited

from common.views import auth_check, json_error, json_response
from common.video import get_video_info, get_youtube_video_id
from main.models import Exercise
from videohub.services.video import VideoService
from videohub.models import Video


@auth_check(ajax=True)
def video_list(request):
    """
        Личный список видео пользователя
        """
    qs = Video.objects \
             .get_active() \
             .filter(user=request.user) \
             .order_by('-created_at')[:100]
    data = []
    for video in qs:
        data.append({
            'id': video.id,
            'view_count': video.view_count,
            'executed_at': video.executed_at.strftime("%d.%m.%Y"),
            'created_at': video.created_at.strftime("%d.%m.%Y"),
            'youtube_id': video.youtube_id,
            'title': video.title,
            'name': video.name,
            'description': video.description,
        })
    return json_response(status='success', videos=data)


@auth_check(ajax=True)
def info(request):
    url = request.GET.get('url')

    if not url:
        return json_error(u'Cсылка является неправильной')
    data = get_video_info(url)

    if data is None:
        return json_error(u'Видеосервис не поддерживается, либо ссылка является неправильной')

    return json_response(**data)


def show(request):
    """
    Возвращает инфу о видео
    """
    try:
        video = Video.objects.get(pk=request.GET.get('id'))
    except:
        return json_error(u'Видео не найдено')

    if not video.is_public:
        if (not request.user.is_authenticated()) or request.user.id != video.user_id:
            return json_error(u'Нет доступа.')
    if video.is_deleted:
        return json_error(u'Нет доступа.')

    if not is_ratelimited(request, group="video_count_%d" % video.id, key='user_or_ip', rate='1/5m', increment=True):
        video.view_count += 1
        video.save()

    data = {
        'id': video.id,
        'view_count': video.view_count,
        'executed_at': video.executed_at.strftime("%d.%m.%Y"),
        'created_at': video.created_at.strftime("%d.%m.%Y"),
        'youtube_id': video.youtube_id,
        'title': video.title,
        'name': video.name,
        'is_public': video.is_public,
        'description': video.description,
        'exercise': {
            'name': video.exercise.name,
            'id': video.exercise.id,
        },
        'user': {
            'pk': video.user.pk,
            'username': video.user.username,
            'fullname': video.user.fullname,
        },
        'reps': video.reps,
        'weight': video.weight,
    }
    return json_response(status='success', video=data)


@csrf_protect
@auth_check(ajax=True)
def delete(request):
    """
    Удаляет видео
    """
    try:
        video = Video.objects.get(pk=request.GET.get('id'))
    except:
        return json_error(u'Видео не найдено')

    if request.user.id != video.user_id:
        return json_error(u'Нет доступа.')

    VideoService.delete_video(video)

    return json_response(status='success')


@auth_check(ajax=True)
def get_progress(request):
    try:
        video_id = request.GET['video_id']
    except:
        return json_error(u'Укажите video_id')

    try:
        video = Video.objects.get(pk=video_id)
    except:
        return json_error(u'Видео не найдено')

    service = VideoService()
    progress = service.get_video_upload_progress(video_id)

    if not progress:
        return json_response(status='wait')
    if video.is_uploaded is not None:
        return json_response(status='success', video={'id': video.id, 'name': video.name, 'youtube_id': video.youtube_id, 'app': video.app})
    if progress['status'] == 'error':
        return json_response(status='error', message=u'Неизвестная ошибка при обработке видео')
    if progress['status'] == 'progress':
        return json_response(status='process', percent=progress['percent'])


@csrf_exempt
@auth_check(ajax=True)
def save(request):
    service = VideoService()

    typ = request.POST.get('type', '')
    title = request.POST.get('title', '')
    description = request.POST.get('description', '')
    is_public = request.POST.get('is_public', '1') == '1'
    kwargs = {}

    # указание схемы
    if 'scheme_id' in request.POST:
        try:
            kwargs['scheme_id'] = int(request.POST.get('scheme_id'))
        except:
            return json_error(u'Неверно указана схема')
        try:
            kwargs['app'] = int(request.POST.get('app'))
        except:
            return json_error(u'Неверно указан номер подхода')
    else:
        try:
            kwargs['exercise_id'] = int(request.POST.get('exercise_id'))
        except:
            return json_error(u'Не указано упражнение')
        try:
            kwargs['reps'] = int(request.POST.get('reps'))
        except:
            return json_error(u'Не указано количество повторений')
        try:
            kwargs['weight'] = float(request.POST.get('weight'))
        except:
            return json_error(u'Не указан вес')
        try:
            kwargs['executed_at'] = datetime.datetime.strptime(request.POST.get('executed_at'), "%d.%m.%Y").date()
        except:
            return json_error(u'Не указана дата выполнения')
        try:
            kwargs['app'] = int(request.POST.get('app'))
        except:
            pass

    # источник
    if typ == 'youtube':
        try:
            kwargs['youtube_id'] = get_youtube_video_id(request.POST['url'])
        except:
            return json_error(u'Видеосервис не поддерживается, либо ссылка является неправильной')

    elif typ == 'file':
        try:
            file = request.FILES['file']
        except:
            return json_error(u'Файл не указан.')

        if not file.content_type.startswith('video/'):
            return json_error(u'Неверный формат файла.')

        if (file._size >> 20) > 250:
            return json_error(u'Размер файла не может быть больше 250 мб')

        if is_ratelimited(request, group="upload_video", key='user', rate='25/h', increment=True):
            return json_error(u'Нельзя загрузить более 25 видео в час.')

        # считаем хеш
        md5 = hashlib.md5()
        for chunk in file.chunks():
            md5.update(chunk)
        kwargs['hash'] = md5.hexdigest()

        # записываем в файл
        with open('/var/www/video/%s' % kwargs['hash'], 'wb+') as dest:
            for chunk in file.chunks():
                dest.write(chunk)
    else:
        return json_error(u'Неверный запрос.')

    if is_ratelimited(request, group="add_video", key='user', rate='50/h', increment=True):
        return json_error(u'Нельзя добавить более 50 видео в час.')

    # добавление видео
    try:
        video = service.add_video(title, description, is_public, request.user, **kwargs)
    except Exception as e:
        return json_error(unicode(e))

    # запускаем загрузку на сервер youtube если необходимо
    if video.is_uploaded is None:
        subprocess.Popen([sys.executable, settings.BASE_DIR + '/manage.py', 'upload_youtube', str(video.id)],
                         stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        return json_response(status='progress', video_id=video.id)

    return json_response(status='success', video_id=video.id, video={'id': video.id, 'name': video.name, 'youtube_id': video.youtube_id, 'app': video.app})


@auth_check(ajax=True)
def exercise_list(request):
    exercises = Exercise.objects.filter(is_basic=True).order_by('name')

    return json_response(exercises=[{'name': e.name, 'id': e.id} for e in exercises])