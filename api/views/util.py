#coding=utf8

from common.views import auth_check, json_response
from main.views.util import analyzes


@auth_check(ajax=True)
def converter(request):
    return json_response(list=analyzes)
