#coding=utf8
from main.models import Exercise, ExerciseGroup

from common.views import auth_check, json_response


def list(request):
    qs = Exercise.objects.filter(is_checked=True, is_basic=True)

    objs = []
    for q in qs:
        objs.append({
            'id': q.id,
            'name': q.name,
            'group_id': q.group_id,
        })

    groups = {}
    for q in ExerciseGroup.objects.all():
        groups[q.id] = {
            'id': q.id,
            'name': q.name,
        }

    return json_response(
        list=objs,
        groups=groups
    )


def show(request, id):
    try:
        o = Exercise.objects.select_related('group').get(pk=id)
    except:
        return json_response(
            status='error',
            error='Не найдено упражнение'
        )

    return json_response(
        name=o.name,
        group={
            'name': o.group.name,
            'id': o.group.id,
        },
        description=o.description,
        content=o.content,
    )
