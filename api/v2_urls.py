from django.conf.urls import patterns, url

urlpatterns = patterns('',
    url(r'^auth/check/$', 'api.v2.auth.check', name='api_v2_auth_check'),

    url(r'^ws/message/$', 'api.v2.ws.message', name='api_v2_ws_message'),
    url(r'^ws/resume/$', 'api.v2.ws.resume', name='api_v2_ws_resume'),
    url(r'^ws/$', 'api.v2.ws.index', name='api_v2_ws'),

    url(r'^train/(?P<id>[\d]+)/$', 'api.v2.train.show', name='api_v2_train_show'),

    url(r'^user/(?P<id>[\d]+)/$', 'api.v2.user.show', name='api_v2_user_show'),
    url(r'^user/(?P<id>[\d]+)/plans/$', 'api.v2.user.plans', name='api_v2_user_plans'),
    url(r'^user/(?P<id>[\d]+)/courses/$', 'api.v2.user.courses', name='api_v2_user_courses'),
    url(r'^user/(?P<id>[\d]+)/videos/$', 'api.v2.user.videos', name='api_v2_user_videos'),
    url(r'^user/(?P<id>[\d]+)/students/$', 'api.v2.user.students', name='api_v2_user_students'),
    url(r'^user/(?P<id>[\d]+)/weight-stat/$', 'api.v2.user.weight_stat', name='api_v2_user_weight_stat'),

    url(r'^user/search/$', 'api.v2.user.search', name='api_v2_user_search'),
    url(r'^profile/$', 'api.v2.profile_views.index', name='api_v2_profile'),
    url(r'^profile/edit/$', 'api.v2.profile_views.edit', name='api_v2_profile_edit'),
    url(r'^profile/counts/$', 'api.v2.profile_views.counts', name='api_v2_profile_counts'),
    url(r'^profile/students/$', 'api.v2.profile_views.students', name='api_v2_profile_students'),
    url(r'^profile/favorites/$', 'api.v2.profile_views.favorites', name='api_v2_profile_favorites'),
    url(r'^profile/notifications/$', 'api.v2.profile_views.notifications', name='api_v2_profile_notifications'),

    url(r'^payment/list/$', 'api.v2.payment_views.payment_list', name='api_v2_payment_list'),
    url(r'^payment/donate/$', 'api.v2.payment_views.donate', name='api_v2_payment_donate'),
    url(r'^payment/subscribe/$', 'api.v2.payment_views.subscribe', name='api_v2_payment_subscribe'),
    url(r'^payment/pay/(?P<id>[\d]+)/$', 'api.v2.payment_views.pay', name='api_v2_payment_pay'),
    url(r'^payment/is-done/(?P<id>[\d]+)/$', 'api.v2.payment_views.is_done', name='api_v2_payment_is_done'),

    url(r'^dose/mark/$', 'api.v2.course.dose_mark', name='api_v2_dose_mark'),
    url(r'^course/create/$', 'api.v2.course.create', name='api_v2_course_create'),
    url(r'^course/list/$', 'api.v2.course.list', name='api_v2_course_list'),
    url(r'^course/(?P<id>[\d]+)/$', 'api.v2.course.show', name='api_v2_course_show'),
    url(r'^course/(?P<id>[\d]+)/delete/$', 'api.v2.course.delete', name='api_v2_course_delete'),
    url(r'^course/(?P<id>[\d]+)/edit/$', 'api.v2.course.edit', name='api_v2_course_edit'),
    url(r'^course/(?P<id>[\d]+)/up/(?P<ts>[\d]+)/$', 'api.v2.course.show_up', name='api_v2_course_show_up'),
    url(r'^course/(?P<id>[\d]+)/down/(?P<ts>[\d]+)/$', 'api.v2.course.show_down', name='api_v2_course_show_down'),
    url(r'^course/(?P<id>[\d]+)/doses/delete/$', 'api.v2.course.doses_delete', name='api_v2_course_doses_delete'),
    url(r'^course/(?P<id>[\d]+)/doses/add/$', 'api.v2.course.doses_add', name='api_v2_course_doses_add'),
)