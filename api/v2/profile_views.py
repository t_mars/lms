# coding=utf8
import datetime

from django.views.decorators.csrf import csrf_exempt
from django.core.files.images import get_image_dimensions
from django.template import Context, loader
from django.utils import timezone
import awesome_avatar

from main.utils.dates import get_timestamp
from profile.models import User, WeightRecord
from profile.services.user import UserService
from profile.forms import UserEditForm

from . import json_response, auth_check, api_data


@json_response
@auth_check
def index(request):
    return {
        'status': 'success',
        'user': UserService.get_user_info(request.user.id),
    }


@csrf_exempt
@api_data
@json_response
@auth_check
def edit(request, request_data):
    if request.method != 'POST':
        return {
            'status': 'error',
            'message': u'Ошибка запроса'
        }

    if request_data:
        request.POST._mutable = True
        for k, v in request_data.iteritems():
            request.POST[k] = v

    form = UserEditForm(request.POST, request.FILES, initial={'avatar': request.user.avatar})
    form.user = request.user
    if not form.is_valid():
        return {
            'status': 'error',
            'message': u'Ошибка данных',
            'fields': form.errors
        }

    data = form.clean()
    if data['avatar'] and isinstance(data['avatar'], dict):
        w, h = get_image_dimensions(data['avatar']['file'])
        if abs(w-h) < 2:
            w = h = min(w, h)
        if w != h:
            return {
                'status': 'error',
                'message': u'Изображение неверного разрешения %d на %d' % (w, h)
            }

        data['avatar']['box'] = [0, 0, w, h]
        avatar_field = awesome_avatar.fields.AvatarField()
        avatar_field.name = 'avatar'
        avatar_field.save_form_data(request.user, data['avatar'])

    try:
        trainer = User.objects.get(id=request_data['trainer_id'])
    except:
        trainer = None

    request.user.trainer = trainer
    request.user.trainer_confirmed = True

    request.user.username = data['username'].lower()
    request.user.first_name = data['first_name']
    request.user.last_name = data['last_name']
    request.user.gender = data['gender']
    request.user.allpowerlifting = data['allpowerlifting']
    request.user.save()

    return {
        'status': 'success',
        'user': UserService.get_user_info(request.user.id)
    }


@json_response
@auth_check
def counts(request):
    return {
        'status': 'success',
        'counts': {
            'favorites': len(request.user.get_favorites()),
            'students': request.user.students.count(),
            'notifications': request.user.notifications.unread().count()
        }
    }


@json_response
@auth_check
def students(request):
    return {
        'status': 'success',
        'list': [UserService.get_user_short_info(u.id) for u in request.user.students.all()]
    }


@json_response
@auth_check
def favorites(request):
    data = []
    for obj in request.user.get_favorite_list():
        data.append({
            'type': obj['type'],
            'id': obj['id'],
            'name': obj['obj'].name,
        })

    return {
        'status': 'success',
        'list': data
    }


@json_response
@auth_check
def notifications(request):
    qs = request.user.notifications.all().select_related('target_content_type', 'recipient')

    data = []
    template = loader.get_template('api/notifications.html')

    for n in qs[:30]:
        if not n.target:
            continue
        context = Context({'n': n})
        data.append({
            'content': template.render(context)
        })

    qs.mark_all_as_read()

    return {
        'status': 'success',
        'list': data
    }