#coding=utf8
from main.models import Training
from main.services.train import TrainService
from main.services.plan import PlanService

from . import json_response, auth_check


@json_response
@auth_check
def show(request, id):
    """
    Возвращает данные о тренировке, если есть доступ.
    """
    try:
        train = Training.objects.get(pk=id)
    except:
        return {
            'status': 'error',
            'message': u'Тренировка не найдена'
        }

    if not train.plan.access_check(request.user, 'show'):
        return {
            'status': 'error',
            'message': u'Ошибка доступа'
        }

    data = TrainService.get_train_data(train, request.user)
    data['plan'] = PlanService.get_plan_data(train.plan)

    return {
        'status': 'success',
        'train': data
    }