#coding=utf8
import simplejson

from django.http import HttpResponse


class JSONException(Exception):
    def __init__(self, message, type=None):
        super(JSONException, self).__init__(message)
        self.type = type


def auth_check(function):
    """
    Декоратор, который проверяет, чтобы запрос был от авторизированного пользователя.
    """
    def decorator(request, *args, **kwargs):
        if not request.user.is_authenticated():
            return {
                'status': 'error',
                'type': 'auth_error',
                'message': u'Ошибка доступа.'
            }
        return function(request, *args, **kwargs)
    return decorator


def json_response(function):
    def decorator(request, *args, **kwargs):
        try:
            objects = function(request, *args, **kwargs)
            if isinstance(objects, HttpResponse):
                return objects
        except JSONException as e:
            objects = {
                'status': 'error',
                'message': unicode(e),
            }
            if e.type:
                objects['type'] = e.type
        return HttpResponse(simplejson.dumps(objects), content_type='application/json')
    return decorator


def api_data(function):
    """
    Декоратор который парсит данных из тела заголовка json
    """
    def wrap(request, *args, **kwargs):
        try:
            data = simplejson.loads(request.body)
        except:
            return {
                'status': 'error',
                'message': u'Ошибка запроса.'
            }
        return function(request, data, *args, **kwargs)
    return wrap