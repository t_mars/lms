#coding=utf8
from datetime import timedelta
import datetime

from django.utils import timezone
from django.db.models import Q
from django.views.decorators.csrf import csrf_protect

from main.utils.dates import get_timestamp, parse_timestamp, prev_weekday, next_weekday
from profile.models import CustomUser
from drugplan.models import Course, Drug, Dose, MEASURE_CHOICE
from drugplan.services.course import CourseService
from drugplan.services.drug import DrugService
from . import json_response, auth_check, api_data, JSONException


def course_get(function):
    def wrapper(request, id, *args, **kwargs):
        try:
            course = Course.objects.get(id=id)
        except:
            raise JSONException(u'СПМ не найден')

        if not course.access_check(request.user, 'show'):
            raise JSONException(u'У вас нет прав для просмотра этого СПМ')
        return function(request, course, *args, **kwargs)
    return wrapper


@json_response
@course_get
@auth_check
def show(request, course):
    d = prev_weekday(timezone.now().date(), 0)

    if d < course.start_date:
        start = course.start_date
    elif d <= course.last_date:
        start = d
    elif (d - course.last_date).days < 7:
        start = d - timedelta(days=7)
    else:
        start = course.first_date
    end = start + timedelta(days=27)
    # start = course.start_date
    # end = course.end_date

    return {
        'status': 'success',
        'drugs': CourseService.get_drug_list(request.user, course),
        'measures': DrugService.get_measures(),
        'weeks': CourseService.get_weeks(course, start, end),
        'start': None if start == course.start_date else get_timestamp(start),
        'end': None if end == course.end_date else get_timestamp(end),
        'access': CourseService.get_course_access(request.user, course),
        'course': CourseService.get_course_data(course),
    }


@json_response
@course_get
@api_data
@auth_check
@csrf_protect
def edit(request, data, course):
    if 'patient' not in data:
        raise JSONException(u'Не указан спортсмен')

    if 'author' not in data:
        raise JSONException(u'Не указан редактор')

    if 'name' not in data:
        raise JSONException(u'Не указано название')

    if 'is_public' not in data:
        raise JSONException(u'Не указан флаг публичности')

    try:
        patient = CustomUser.objects.get(username=data['patient'])
    except:
        raise JSONException(u'Пользователь %s не найден.' % (data['patient']))

    try:
        author = CustomUser.objects.get(username=data['author'])
    except:
        raise JSONException(u'Пользователь %s не найден.' % (data['author']))

    name = data.get('name')
    is_public = data.get('is_public')

    try:
        CourseService.edit_course(request.user, course, name, author, patient, is_public)
    except Exception as e:
        raise JSONException(unicode(e))

    return {
        'status': 'success',
        'course': CourseService.get_course_data(course),
        'access': CourseService.get_course_access(request.user, course)
    }


@json_response
@course_get
@auth_check
@csrf_protect
def delete(request, course):
    if not course.access_check(request.user, 'delete'):
        raise JSONException(u'У вас нет прав для удаления этого СПМ.')

    course.is_deleted = True
    course.save()

    return {
        'status': 'success',
    }


@json_response
@api_data
@auth_check
@csrf_protect
def create(request, data):
    if not data.get('name'):
        raise JSONException(u'Не указано название')
    name = data['name']

    try:
        start_date = datetime.datetime.strptime(data['start_date'], '%d.%m.%y').date()
    except:
        raise JSONException(u'Неверно указана дата начала')

    if 'is_public' not in data:
        raise JSONException(u'Не указан флаг публичности')
    is_public = data['is_public']

    try:
        course = CourseService.create_course(request.user, name, start_date, is_public)
    except Exception as e:
        raise JSONException(unicode(e))

    return {
        'status': 'success',
        'course': CourseService.get_course_data(course)
    }


@json_response
@auth_check
def list(request):
    qs = Course.objects \
        .filter(Q(author=request.user) or Q(patient=request.user), is_deleted=False) \
        .order_by('-created_at')

    return {
        'status': 'success',
        'list': [CourseService.get_course_data(course) for course in qs]
    }


@json_response
@course_get
@auth_check
def show_up(request, course, ts):
    try:
        d = parse_timestamp(ts)
    except:
        raise JSONException(u'Ошибка параметров.')

    d = next_weekday(d-timedelta(1), 6)

    if d <= course.start_date:
        raise JSONException(u'Загружено полностью', type='finish')

    end = d
    start = max(end - timedelta(days=13), course.start_date)

    return {
        'status': 'success',
        'weeks': CourseService.get_weeks(course, start, end),
        'start': None if start == course.start_date else get_timestamp(start)
    }


@json_response
@course_get
@auth_check
def show_down(request, course, ts):
    try:
        d = parse_timestamp(ts)
    except:
        raise JSONException(u'Ошибка параметров.')

    d = prev_weekday(d + timedelta(1), 0)

    if d >= course.end_date:
        raise JSONException(u'Загружено полностью', type='finish')

    start = d
    end = min(start + timedelta(days=27), course.end_date)

    return {
        'status': 'success',
        'weeks': CourseService.get_weeks(course, start, end),
        'end': None if end == course.end_date else get_timestamp(end)
    }


@json_response
@course_get
@api_data
@auth_check
@csrf_protect
def doses_delete(request, data, course):
    try:
        drug = Drug.objects.get(pk=data['drug_id'])
    except:
        raise JSONException(u'Выбранные медикамент не найден.')

    dates = []
    try:
        for d in data['dates']:
            dates.append(parse_timestamp(d))
    except:
        raise JSONException(u'Неверные даты.')

    try:
        CourseService.delete_doses(request.user, course, drug, dates)
    except Exception as e:
        raise JSONException(unicode(e))

    return {
        'status': 'success',
        'days': CourseService.get_drugs_by_days(course, dates)
    }


@json_response
@course_get
@api_data
@auth_check
@csrf_protect
def doses_add(request, data, course):
    dates = []
    try:
        for d in data['dates']:
            dates.append(parse_timestamp(d))
    except:
        raise JSONException(u'Неверные даты.')

    if len(dates) == 0:
        raise JSONException(u'Не указаны даты приема')

    doses = []
    if not data.get('doses'):
        raise JSONException(u'Не указаны дозировки')
    else:
        for d in data['doses']:
            try:
                doses.append({
                    'amount': int(d['amount']),
                    'comment': d['comment'] or ''
                })
            except:
                continue

    if len(doses) == 0:
        raise JSONException(u'Не указаны дневные приемы')

    drug = None
    measure = None
    if data.get('drug_id'):
        try:
            drug = Drug.objects.get(pk=data['drug_id'])
        except:
            raise JSONException(u'Не найден выбранный медикамент')
        if data.get('measure') not in MEASURE_CHOICE:
            raise JSONException(u'Не указана мера измерения')
        measure = data['measure']

    if data.get('new_drug'):
        new_data = data['new_drug']
        try:
            drug = DrugService.create_drug(request.user, new_data['name'], new_data['short_name'], new_data['measure'])
            measure = new_data['measure']
        except Exception as e:
            raise JSONException(unicode(e))

    if not drug:
        raise JSONException(u'Не указан медикамент')

    course.set_measure(drug, measure)
    course.save()

    try:
        CourseService.add_doses(request.user, course, drug, dates, doses)
    except Exception as e:
        raise JSONException(unicode(e))

    drug_data = DrugService.get_drug_info(drug)
    drug_data['measure'] = course.get_measure(drug)

    return {
        'status': 'success',
        'days': CourseService.get_drugs_by_days(course, dates),
        'drug': drug_data
    }


@json_response
@auth_check
def dose_mark(request):
    try:
        dose = Dose.objects.get(pk=request.GET.get('id'))
    except:
        raise JSONException(u'Прием не найден.')

    if not dose.course.access_check(request.user, 'mark'):
        raise JSONException(u'Ошибка доступа.')

    if dose.is_marked:
        dose.is_marked = False
    elif dose.is_marked is None:
        dose.is_marked = True
    else:
        dose.is_marked = None
    dose.marked_at = timezone.now()
    dose.save()

    return {
        'status': 'success',
        'is_marked': dose.is_marked,
        'marked_at': get_timestamp(dose.marked_at)
    }
