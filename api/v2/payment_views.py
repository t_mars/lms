# coding=utf8
from django.http import HttpResponseRedirect, Http404

from common.views import auth_check as auth_check_html
from main.utils.dates import get_timestamp
from payment.models import Invoice
from payment.views import get_yk_url, get_rk_url

from . import json_response, auth_check


@json_response
@auth_check
def payment_list(request):
    qs = Invoice.objects \
        .filter(user=request.user) \
        .order_by('-created_at', '-modified_at')

    invoices = []
    for i in qs:
        invoices.append({
            'status': i.get_status(),
            'sum': i.sum,
            'description': i.get_description(),
            'created_at': get_timestamp(i.created_at) if i.created_at else None,
            'modified_at': get_timestamp(i.modified_at) if i.modified_at else None,
        })

    taxes = []
    for m in [1, 3, 6]:
        p = Invoice()
        p.user = request.user
        p.set_month(m)
        taxes.append({
            'months': m,
            'sum': p.sum,
        })

    return {
        'status': 'success',
        'invoices': invoices,
        'taxes': taxes
    }


@json_response
@auth_check
def donate(request):
    try:
        s = int(request.GET['sum'])
    except:
        return {
            'status': 'error',
            'message': u'Неверная сумма пожертвования'
        }

    if s < 50:
        return {
            'status': 'error',
            'message': u'Cумма пожертвования может быть от 50 рублей'
        }

    invoice = Invoice()
    invoice.user = request.user
    invoice.mode = 'donate'
    invoice.comment = request.GET.get('comment', None)
    invoice.sum = s

    qs = Invoice.objects.filter(user_id=invoice.user_id, sum=invoice.sum, mode=invoice.mode, status=0)

    if qs.count() > 0:
        invoice = qs[0]
    else:
        invoice.save()

    invoice.payment_system = 'rk'
    invoice.save()

    return {
        'status': 'success',
        'id': invoice.id
    }


@json_response
@auth_check
def subscribe(request):
    try:
        months = int(request.GET['m'])
    except:
        return {
            'status': 'error',
            'message': u'Неверные параметры'
        }

    invoice = Invoice()
    invoice.user = request.user
    invoice.mode = 'subscribe'

    try:
        invoice.set_month(months)
    except Exception as e:
        return {
            'status': 'error',
            'message': unicode(e)
        }

    qs = Invoice.objects.filter(user_id=invoice.user_id,
                                months=invoice.months, sum=invoice.sum, mode=invoice.mode, status=0)

    if qs.count() > 0:
        invoice = qs[0]
    else:
        invoice.save()

    invoice.payment_system = 'rk'
    invoice.save()

    return {
        'status': 'success',
        'id': invoice.id
    }


def pay(request, id):
    try:
        invoice = Invoice.objects.get(pk=id)
    except:
        raise Http404(u'Счет с таким номером не найден')

    # if request.user.id != invoice.user_id:
    #     raise Http404(u'Вы не являетесь владельцем этого счета #%s' % id)

    if invoice.status != 0:
        raise Http404(u'Данный счет #%s уже оплачен' % id)

    if invoice.payment_system == 'yk':
        return HttpResponseRedirect(get_yk_url(invoice))
    return HttpResponseRedirect(get_rk_url(invoice))


@json_response
@auth_check
def is_done(request, id):
    try:
        invoice = Invoice.objects.get(pk=id)
    except:
        return {
            'status': 'error',
            'message': u'Платеж не найден'
        }

    if request.user.id != invoice.user_id:
        return {
            'status': 'error',
            'message': u'Ошибка доступа'
        }

    return {
        'status': 'success',
        'result': invoice.status == 1
    }