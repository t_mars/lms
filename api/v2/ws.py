#coding=utf8
import datetime

from django.utils import timezone

from main.services.train import TrainService
from main.services.plan import PlanService
from main.models import Training
from drugplan.models import Dose
from drugplan.services.course import CourseService
from . import json_response, auth_check


@json_response
@auth_check
def index(request):

    try:
        d = datetime.datetime.strptime(request.GET.get('date'), '%d.%m.%y').date()
    except:
        d = timezone.now().date()
    
    trains = []
    for p in request.user.get_plans():
        qs = Training.objects.filter(plan=p, date=d)
        for t in qs:
            data = TrainService.get_train_data(t, request.user)
            data['plan'] = PlanService.get_plan_data(p)
            trains.append(data)

    meds = []
    for course in request.user.get_courses():
        qs = Dose.objects \
            .filter(course=course, date=d)

        drugs = CourseService.get_doses_by_drugs(course, qs)
        if len(drugs):
            meds.append({
                'course': {
                    'id': course.id,
                    'name': course.name,
                },
                'drugs': drugs
            })

    return {
        'status': 'success',
        'meds': meds,
        'trains': trains,
    }


@json_response
@auth_check
def resume(request):
    try:
        d = datetime.datetime.strptime(request.GET.get('date'), '%d.%m.%y').date()
    except:
        d = timezone.now().date()

    plans = request.user.get_plans()
    qs = Training.objects.filter(plan__in=plans, date=d)
    trains_count = qs.count()
    if trains_count:
        trains_done = sum([1 if t.is_done else 0 for t in qs])
    else:
        trains_done = 0

    courses = request.user.get_courses()
    qs = Dose.objects.filter(course__in=courses, date=d)
    meds_count = qs.count()
    if meds_count:
        meds_done = qs.filter(is_marked__isnull=False).count()
    else:
        meds_done = 0

    return {
        'status': 'success',
        'trains_count': trains_count,
        'trains_done': trains_done,
        'meds_count': meds_count,
        'meds_done': meds_done
    }


@json_response
@auth_check
def message(request):
    if request.user.id == 2851 or request.user.id == 2:
        return {
            'status': 'success',
            'content': 'test'
        }

    return {
        'status': 'error'
    }
