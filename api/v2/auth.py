#coding=utf8
from django.utils import timezone

from profile.services.user import UserService

from . import json_response, auth_check


@json_response
@auth_check
def check(request):
    f = False

    if request.GET.get('platform'):
        f = True
        request.user_session.platform = request.GET.get('platform')

    if request.GET.get('version'):
        f = True
        request.user_session.version = request.GET.get('version')

    if not request.user_session.useragent:
        f = True
        request.user_session.useragent = request.META.get('HTTP_USER_AGENT', '')

    if f:
        request.user_session.modified_at = timezone.now()
        request.user_session.save()

    return {
        'status': 'success',
        'user': UserService.get_user_info(request.user.id),
        'csrftoken': request.COOKIES.get('csrftoken'),
        'sessionid': request.COOKIES.get('sessionid')
    }
