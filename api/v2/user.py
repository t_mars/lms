#coding=utf8
from django.db.models import Q
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger

from main.utils.dates import get_timestamp
from profile.models import CustomUser, User, WeightRecord
from profile.services.user import UserService

from . import json_response, auth_check


def user_get(function):
    def wrapper(request, id, *args, **kwargs):
        try:
            user = User.objects.get(id=id)
        except:
            return {
                'status': 'error',
                'message': u'Пользователь не найден'
            }
        return function(request, user, *args, **kwargs)
    return wrapper


@json_response
@auth_check
def search(request):
    qs = CustomUser.objects.all()

    q = request.GET.get('q', '')
    if q:
        qs = qs \
            .filter(Q(first_name__icontains=q) | Q(last_name__icontains=q) | Q(username__icontains=q)) \
            .order_by('first_name', 'last_name', 'username', '-last_request')
    else:
        qs = qs.order_by('-last_request')

    paginator = Paginator(qs, request.GET.get('limit', 20))
    page = request.GET.get('page')
    try:
        objs = paginator.page(page)
    except PageNotAnInteger:
        objs = paginator.page(1)
    except EmptyPage:
        objs = paginator.page(paginator.num_pages)

    return {
        'list': [UserService.get_user_short_info(u.id) for u in objs]
    }


@json_response
@user_get
@auth_check
def show(request, user):
    user_data = {
        'id': user.id,
        'first_name': user.first_name,
        'last_name': user.last_name,
        'fullname': user.fullname,
        'username': user.username,
        'avatar_50': user.get_avatar_50(),
        'avatar_100': user.get_avatar_100(),
        'gender': user.gender,
        'is_subscribed': user.is_subscribed(),
        'allpowerlifting': user.allpowerlifting,
        'subscription_date': user.subscription_date.strftime('%d.%m.%y') if user.is_subscribed() else None,
        'trainer': UserService.get_user_short_info(user.trainer_id) if user.trainer else None,
        'vk': None,
        'last_request': get_timestamp(user.last_request) if user.last_request else None,
        'date_joined': get_timestamp(user.date_joined)
    }

    sauth = user.social_auth.get()
    if sauth.provider == 'vk-oauth':
        user_data['vk'] = 'https://vk.com/id%s' % sauth.uid

    badges = {
        'plan': user.get_plans_for_user(request.user).count(),
        'course': user.get_courses_for_user(request.user).count(),
        'video': user.get_videos_for_user(request.user).count(),
        'weight': 0,
        'student': user.students.count(),
    }

    qs = WeightRecord.objects.filter(user=user).order_by('-date')
    if qs.count() > 0:
        badges['weight'] = qs[0].value


    return {
        'status': 'success',
        'user': user_data,
        'badges': badges,
    }


@json_response
@user_get
@auth_check
def plans(request, user):
    data = []
    for p in user.get_plans_for_user(request.user).order_by('-created_at'):
        data.append({
            'name': p.name,
            'id': p.id,
            'can_delete': p.access_check(request.user, 'delete'),
            'sportsman': UserService.get_user_short_info(p.sportsman_id),
            'author': UserService.get_user_short_info(p.author_id)
        })

    return {
        'status': 'success',
        'list': data,
    }


@json_response
@user_get
@auth_check
def courses(request, user):
    data = []
    for q in user.get_courses_for_user(request.user).order_by('-created_at'):
        data.append({
            'name': q.name,
            'id': q.id,
            'can_delete': q.access_check(request.user, 'delete'),
            'patient': UserService.get_user_short_info(q.patient_id),
            'author': UserService.get_user_short_info(q.author_id),
        })

    return {
        'status': 'success',
        'list': data,
    }


@json_response
@user_get
@auth_check
def videos(request, user):
    data = []
    for video in user.get_videos_for_user(request.user).order_by('-created_at')[:100]:
        data.append({
            'id': video.id,
            'view_count': video.view_count,
            'executed_at': video.executed_at.strftime("%d.%m.%Y"),
            'created_at': video.created_at.strftime("%d.%m.%Y"),
            'youtube_id': video.youtube_id,
            'title': video.title,
            'name': video.name,
            'description': video.description,
        })

    return {
        'status': 'success',
        'list': data,
    }


@json_response
@user_get
@auth_check
def students(request, user):
    return {
        'status': 'success',
        'list': [UserService.get_user_short_info(u.id) for u in user.students.all()]
    }


@json_response
@user_get
@auth_check
def weight_stat(request, user):
    if not request.user.is_superuser and request.user.id != user.trainer_id:
        return {
            'status': 'error',
            'message': u'У вас нет доступа для просмотра статистика веса.'
        }

    qs = WeightRecord.objects \
        .filter(user=user) \
        .order_by('-date')

    stat = []
    for q in qs[:30]:
        stat.append({
            'date': get_timestamp(q.date),
            'weight': q.value,
        })

    return {
        'status': 'success',
        'list': stat
    }