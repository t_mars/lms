#coding=utf8
from django.contrib import admin
from django.db.models import Count

from main.models import *


class ExerciseGroupAdmin(admin.ModelAdmin):
    pass
admin.site.register(ExerciseGroup, ExerciseGroupAdmin)


class ExerciseProposedAdmin(admin.ModelAdmin):
    list_display = ['name', 'user', 'created_at']
admin.site.register(ExerciseProposed, ExerciseProposedAdmin)


class ExerciseAdmin(admin.ModelAdmin):
    list_display = ['name', 'is_basic', 'is_dual', 'is_checked', '_group', '_parent', '_author']
    list_filter = ['is_checked', 'is_basic']
    search_fields = ['name']
    fields = ['name', 'group', 'order', 'is_dual', 'content', 'is_checked', 'author', 'is_basic']
    readonly_fields = ['is_basic']

    def get_queryset(self, request):
        return Exercise.objects \
            .order_by('-id')

    def _group(self, obj):
        if obj.group:
            return obj.group.name

    def _author(self, obj):
        if obj.author:
            return obj.author.username

    def _parent(self, obj):
        if obj.parent:
            return '%s * %1.2f' % (obj.parent.name, obj.parent_rate)

admin.site.register(Exercise, ExerciseAdmin)


class CycleAdmin(admin.ModelAdmin):
    list_display = ['name', 'sport', 'gender', 'level', 'mode', '_count']

    def get_queryset(self, obj):
        return Cycle.objects.all() \
            .annotate(count=Count('cycleexecution'))

    def _count(self, obj):
        return obj.count
admin.site.register(Cycle, CycleAdmin)


class CycleExecutionAdmin(admin.ModelAdmin):
    pass
admin.site.register(CycleExecution, CycleExecutionAdmin)


class CycleSchemeAdmin(admin.ModelAdmin):
    pass
admin.site.register(CycleScheme, CycleSchemeAdmin)


class PlanAdmin(admin.ModelAdmin):
    list_display = ['name', 'author', 'start_date', 'is_deleted']
admin.site.register(Plan, PlanAdmin)


class PlanPMAdmin(admin.ModelAdmin):
    list_display = ['plan', 'exercise', 'date', 'weight']
admin.site.register(PlanPM, PlanPMAdmin)


class TrainingAdmin(admin.ModelAdmin):
    pass
admin.site.register(Training, TrainingAdmin)


class TrainingExecutionAdmin(admin.ModelAdmin):
    pass
admin.site.register(TrainingExecution, TrainingExecutionAdmin)


class TrainingSchemeAdmin(admin.ModelAdmin):
    pass
admin.site.register(TrainingScheme, TrainingSchemeAdmin)


class RequestLogAdmin(admin.ModelAdmin):
    list_display = ['user', 'host', 'path', 'method', 'ip', 'created_at']
admin.site.register(RequestLog, RequestLogAdmin)


class FederationAdmin(admin.ModelAdmin):
    list_display = ['short_name']
admin.site.register(Federation, FederationAdmin)


class SportNormAdmin(admin.ModelAdmin):
    list_display = ['federation', 'sport', 'gender', 'equip', 'doping_control']
admin.site.register(SportNorm, SportNormAdmin)
