import datetime
from datetime import timedelta
import time


def next_weekday(d, weekday):
    if d.weekday() == weekday:
        return d
    days_ahead = weekday - d.weekday()
    if days_ahead <= 0: # Target day already happened this week
        days_ahead += 7
    return d + timedelta(days_ahead)


def prev_weekday(d, weekday):
    if d.weekday() == weekday:
        return d
    return next_weekday(d, weekday) - timedelta(days=7)    


def get_timestamp(d):
    return int(time.mktime(d.timetuple()))


def parse_timestamp(ts):
    return datetime.date.fromtimestamp(float(ts))


def get_week_number(start, d):
    return (d - start).days / 7 + 1