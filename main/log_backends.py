# coding=utf8
from django.http.response import Http404, HttpResponseServerError
from django.utils.encoding import force_bytes
from django.utils import timezone
from django.shortcuts import render

from main.models import RequestLog
from common.views import get_client_ip


class RequestLogMiddleware(object):

    def process_request(self, request):
        try:
            l = RequestLog()
            l.user = request.user if request.user.is_authenticated() else None
            l.path = request.path_info
            l.useragent = request.META.get('HTTP_USER_AGENT', '')
            l.host = request.META.get('HTTP_HOST', '')
            l.method = request.method
            l.ip = get_client_ip(request)
            l.save()
        except:
            pass

    def process_exception(self, request, exception):
        if isinstance(exception, Http404):
            return render(request, '404.html', {
                'exception': force_bytes(exception, errors='replace'),
            })

        if isinstance(exception, HttpResponseServerError):
            return render(request, '500.html', {
                'exception': force_bytes(exception, errors='replace'),
            })