# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0022_plan_factors'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='exercise',
            name='with_factor',
        ),
        migrations.AddField(
            model_name='exercise',
            name='factor',
            field=models.PositiveSmallIntegerField(default=None, null=True, verbose_name='\u0421 \u043f\u043b\u0438\u0442\u043a\u0430\u043c\u0438', blank=True),
        ),
    ]
