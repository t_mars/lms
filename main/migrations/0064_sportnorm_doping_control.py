# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0063_auto_20160118_1307'),
    ]

    operations = [
        migrations.AddField(
            model_name='sportnorm',
            name='doping_control',
            field=models.CharField(default='', max_length=6, verbose_name='\u0414\u043e\u043f\u0438\u043d\u0433 \u043a\u043e\u043d\u0442\u0440\u043e\u043b\u044c', choices=[(b'male', '\u043c\u0443\u0436'), (b'female', '\u0436\u0435\u043d')]),
            preserve_default=False,
        ),
    ]
