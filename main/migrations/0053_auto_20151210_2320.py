# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0052_cycle_is_active'),
    ]

    operations = [
        migrations.AlterField(
            model_name='cycle',
            name='is_active',
            field=models.BooleanField(default=False, verbose_name=b'\xd0\x90\xd0\xba\xd1\x82\xd0\xb8\xd0\xb2\xd0\xbd\xd0\xbe\xd1\x81\xd1\x82\xd1\x8c'),
        ),
    ]
