# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0050_exerciseproposed'),
    ]

    operations = [
        migrations.AlterField(
            model_name='cycle',
            name='num',
            field=models.CharField(unique=True, max_length=10, verbose_name='\u041d\u043e\u043c\u0435\u0440'),
        ),
    ]
