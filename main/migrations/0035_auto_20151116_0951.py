# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import tinymce.models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0034_auto_20151115_1134'),
    ]

    operations = [
        migrations.AddField(
            model_name='cycle',
            name='content',
            field=tinymce.models.HTMLField(default=None, null=True, verbose_name='\u0421\u043e\u0434\u0435\u0440\u0436\u0430\u043d\u0438\u0435', blank=True),
        ),
        migrations.AddField(
            model_name='exercise',
            name='content',
            field=tinymce.models.HTMLField(default=None, null=True, verbose_name='\u0421\u043e\u0434\u0435\u0440\u0436\u0430\u043d\u0438\u0435', blank=True),
        ),
    ]
