# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0020_auto_20151108_1044'),
    ]

    operations = [
        migrations.AddField(
            model_name='exercise',
            name='other_names',
            field=models.TextField(default=True, null=True, verbose_name='\u0414\u0440\u0443\u0433\u0438\u0435 \u043d\u0430\u0437\u0432\u0430\u043d\u0438\u044f', blank=True),
        ),
    ]
