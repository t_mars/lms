# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0032_plan_is_deleted'),
    ]

    operations = [
        migrations.AddField(
            model_name='cycle',
            name='num',
            field=models.PositiveSmallIntegerField(default=None, verbose_name='\u041d\u043e\u043c\u0435\u0440'),
        ),
    ]
