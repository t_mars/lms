# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0008_planpm_plan'),
    ]

    operations = [
        migrations.RenameField('cycleexecution', 'week_num', 'micro_num'),
    ]
