# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0005_auto_20151101_0744'),
    ]

    operations = [
        migrations.AlterField(
            model_name='exercise',
            name='parent_rate',
            field=models.DecimalField(decimal_places=2, default=None, max_digits=3, blank=True, null=True, verbose_name='\u041a\u043e\u044d\u0444\u0444\u0438\u0446\u0435\u043d\u0442'),
        ),
    ]
