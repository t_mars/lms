# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0053_auto_20151210_2320'),
    ]

    operations = [
        migrations.AddField(
            model_name='training',
            name='is_fresh',
            field=models.BooleanField(default=True),
        ),
    ]
