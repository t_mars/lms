# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0010_auto_20151101_1253'),
    ]

    operations = [
        migrations.AddField(
            model_name='trainingexecution',
            name='level',
            field=models.CharField(default=None, choices=[(b'light', '\u041b\u0435\u0433\u043a\u0430\u044f'), (b'hard', '\u0422\u044f\u0436\u0435\u043b\u0430\u044f'), (b'middle', '\u0421\u0440\u0435\u0434\u043d\u044f\u044f')], max_length=6, blank=True, null=True, verbose_name='\u0423\u0440\u043e\u0432\u0435\u043d\u044c \u043d\u0430\u0433\u0440\u0443\u0437\u043a\u0438'),
        ),
        migrations.AlterField(
            model_name='cycleexecution',
            name='order',
            field=models.PositiveSmallIntegerField(default=0, verbose_name='\u041f\u043e\u0434\u0440\u044f\u0434\u043e\u043a \u0443\u043f\u0440\u0430\u0436\u043d\u0435\u043d\u0438\u044f'),
        ),
    ]
