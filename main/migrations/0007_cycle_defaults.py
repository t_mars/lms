# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0006_auto_20151101_0750'),
    ]

    operations = [
        migrations.AddField(
            model_name='cycle',
            name='defaults',
            field=models.TextField(default=None, null=True, verbose_name='\u0412\u0435\u0441\u0430 \u0443\u043f\u0440\u0430\u0436\u043d\u0435\u043d\u0438\u044f \u043f\u043e \u0443\u043c\u043e\u043b\u0447\u0430\u043d\u0438\u044e', blank=True),
        ),
    ]
