# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Cycle',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=100, verbose_name='\u041d\u0430\u0437\u0432\u0430\u043d\u0438\u0435')),
                ('description', models.TextField(verbose_name='\u041e\u043f\u0438\u0441\u0430\u043d\u0438\u0435')),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name='\u0412\u0440\u0435\u043c\u044f \u0441\u043e\u0437\u0434\u0430\u043d\u0438\u044f')),
            ],
        ),
        migrations.CreateModel(
            name='CycleExecution',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('week_num', models.PositiveSmallIntegerField(verbose_name='\u041d\u043e\u043c\u0435\u0440 \u043d\u0435\u0434\u0435\u043b\u0438')),
                ('day_num', models.PositiveSmallIntegerField(verbose_name='\u041d\u043e\u043c\u0435\u0440 \u0434\u043d\u044f')),
                ('order', models.PositiveSmallIntegerField(verbose_name='\u041f\u043e\u0434\u0440\u044f\u0434\u043e\u043a \u0443\u043f\u0440\u0430\u0436\u043d\u0435\u043d\u0438\u044f')),
                ('level', models.CharField(max_length=6, verbose_name='\u0423\u0440\u043e\u0432\u0435\u043d\u044c \u043d\u0430\u0433\u0440\u0443\u0437\u043a\u0438', choices=[(b'light', '\u041b\u0435\u0433\u043a\u0430\u044f'), (b'hard', '\u0422\u044f\u0436\u0435\u043b\u0430\u044f'), (b'middle', '\u0421\u0440\u0435\u0434\u043d\u044f\u044f')])),
                ('cycle', models.ForeignKey(verbose_name='\u0421\u0420\u0426', to='main.Cycle')),
            ],
        ),
        migrations.CreateModel(
            name='CycleScheme',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('percent', models.DecimalField(verbose_name='\u041f\u0440\u043e\u0446\u0435\u043d\u0442 \u043e\u0442 \u043c\u0430\u043a\u0441\u0438\u043c\u0443\u043c\u0430', max_digits=4, decimal_places=2)),
                ('reps', models.PositiveSmallIntegerField(verbose_name='\u041f\u043e\u0432\u0442\u043e\u0440\u0435\u043d\u0438\u044f')),
                ('apps', models.PositiveSmallIntegerField(verbose_name='\u041f\u043e\u0434\u0445\u043e\u0434\u044b')),
                ('execution', models.ForeignKey(verbose_name='\u0412\u044b\u043f\u043e\u043b\u043d\u0435\u043d\u0438\u0435 \u0443\u043f\u0440\u0430\u0436\u043d\u0435\u043d\u0438\u044f', to='main.CycleExecution')),
            ],
        ),
        migrations.CreateModel(
            name='Exercise',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=100, verbose_name='\u041d\u0430\u0437\u0432\u0430\u043d\u0438\u0435')),
                ('description', models.TextField(default=True, null=True, verbose_name='\u041e\u043f\u0438\u0441\u0430\u043d\u0438\u0435', blank=True)),
                ('rate', models.DecimalField(verbose_name='\u041a\u043e\u044d\u0444\u0444\u0438\u0446\u0435\u043d\u0442 \u0441\u043b\u043e\u0436\u043d\u043e\u0441\u0442\u0438', max_digits=4, decimal_places=2)),
            ],
        ),
        migrations.CreateModel(
            name='Plan',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=100, verbose_name='\u041d\u0430\u0437\u0432\u0430\u043d\u0438\u0435')),
                ('description', models.TextField(default=True, null=True, verbose_name='\u041e\u043f\u0438\u0441\u0430\u043d\u0438\u0435', blank=True)),
                ('start_date', models.DateField(verbose_name='\u0414\u0430\u0442\u0430 \u043d\u0430\u0447\u0430\u043b\u0430')),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name='\u0412\u0440\u0435\u043c\u044f \u0441\u043e\u0437\u0434\u0430\u043d\u0438\u044f')),
            ],
        ),
        migrations.CreateModel(
            name='PlanPM',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('weight', models.DecimalField(verbose_name='\u041f\u0440\u0435\u0434\u0435\u043b\u044c\u043d\u044b\u0439 \u043c\u0430\u043a\u0441\u0438\u043c\u0443\u043c', max_digits=5, decimal_places=2)),
                ('date', models.DateField(verbose_name='\u0414\u0430\u0442\u0430')),
                ('exercise', models.ForeignKey(verbose_name='\u0423\u043f\u0440\u0430\u0436\u043d\u0435\u043d\u0438\u0435', to='main.Exercise')),
            ],
        ),
        migrations.CreateModel(
            name='Training',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('comment', models.TextField(default=None, null=True, verbose_name='\u041a\u043e\u043c\u043c\u0435\u043d\u0442\u0430\u0440\u0438\u0439', blank=True)),
                ('date', models.DateField(verbose_name='\u0414\u0430\u0442\u0430')),
                ('plan', models.ForeignKey(verbose_name='\u041f\u043b\u0430\u043d', to='main.Plan')),
            ],
        ),
        migrations.CreateModel(
            name='TrainingExecution',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('order', models.PositiveSmallIntegerField(verbose_name='\u041f\u043e\u0434\u0440\u044f\u0434\u043e\u043a \u0443\u043f\u0440\u0430\u0436\u043d\u0435\u043d\u0438\u044f')),
                ('comment', models.TextField(default=None, null=True, verbose_name='\u041a\u043e\u043c\u043c\u0435\u043d\u0442\u0430\u0440\u0438\u0439', blank=True)),
                ('exercise', models.ForeignKey(verbose_name='\u0423\u043f\u0440\u0430\u0436\u043d\u0435\u043d\u0438\u0435', to='main.Exercise')),
                ('pm', models.ForeignKey(verbose_name='\u041f\u0440\u0435\u0434\u0435\u043b\u044c\u043d\u044b\u0439 \u043c\u0430\u043a\u0441\u0438\u043c\u0443\u043c', to='main.PlanPM')),
                ('training', models.ForeignKey(verbose_name='\u0422\u0440\u0435\u043d\u0438\u0440\u043e\u0432\u043a\u0430', to='main.Training')),
            ],
        ),
        migrations.CreateModel(
            name='TrainingScheme',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('percent', models.DecimalField(verbose_name='\u041f\u0440\u043e\u0446\u0435\u043d\u0442 \u043e\u0442 \u043c\u0430\u043a\u0441\u0438\u043c\u0443\u043c\u0430', max_digits=4, decimal_places=2)),
                ('reps', models.PositiveSmallIntegerField(verbose_name='\u041f\u043e\u0432\u0442\u043e\u0440\u0435\u043d\u0438\u044f')),
                ('apps', models.PositiveSmallIntegerField(verbose_name='\u041f\u043e\u0434\u0445\u043e\u0434\u044b')),
                ('execution', models.ForeignKey(verbose_name='\u0412\u044b\u043f\u043e\u043b\u043d\u0435\u043d\u0438\u0435 \u0443\u043f\u0440\u0430\u0436\u043d\u0435\u043d\u0438\u044f', to='main.TrainingExecution')),
            ],
        ),
        migrations.AddField(
            model_name='cycleexecution',
            name='exercise',
            field=models.ForeignKey(verbose_name='\u0423\u043f\u0440\u0430\u0436\u043d\u0435\u043d\u0438\u0435', to='main.Exercise'),
        ),
    ]
