# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0013_auto_20151101_1951'),
    ]

    operations = [
        migrations.AlterIndexTogether(
            name='planpm',
            index_together=set([('plan', 'date', 'exercise')]),
        ),
    ]
