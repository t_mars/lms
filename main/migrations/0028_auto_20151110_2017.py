# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0027_auto_20151110_1934'),
    ]

    operations = [
        migrations.CreateModel(
            name='PlanWeekStat',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('date', models.DateField(verbose_name='\u0414\u0430\u0442\u0430 \u043d\u0430\u0447\u0430\u043b\u0430 \u043d\u0435\u0434\u0435\u043b\u0438')),
                ('is_active', models.BooleanField(default=True)),
                ('tonag', models.FloatField(default=None, null=True, verbose_name='\u0422\u043e\u043d\u043d\u0430\u0436', blank=True)),
                ('kps', models.PositiveIntegerField(default=None, null=True, verbose_name='\u041a\u041f\u0428', blank=True)),
                ('percent', models.FloatField(default=None, null=True, verbose_name='\u0418\u041e', blank=True)),
                ('avg_weight', models.FloatField(default=None, null=True, verbose_name='\u0421\u0440\u0435\u0434\u043d\u0438\u0439 \u0432\u0435\u0441', blank=True)),
                ('plan', models.ForeignKey(verbose_name='\u041f\u043b\u0430\u043d', to='main.Plan')),
            ],
        ),
        migrations.AlterUniqueTogether(
            name='planweekstat',
            unique_together=set([('plan', 'date')]),
        ),
    ]
