# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0060_sportnorm_federation'),
    ]

    operations = [
        migrations.AlterField(
            model_name='sportnorm',
            name='division',
            field=models.CharField(default=None, max_length=20, null=True, verbose_name='\u0414\u0438\u0432\u0438\u0437\u0438\u043e\u043d', blank=True),
        ),
    ]
