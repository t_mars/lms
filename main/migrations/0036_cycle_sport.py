# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0035_auto_20151116_0951'),
    ]

    operations = [
        migrations.AddField(
            model_name='cycle',
            name='sport',
            field=models.CharField(default=b'', max_length=100, verbose_name='\u0412\u0438\u0434 \u0441\u043f\u043e\u0440\u0442\u0430'),
        ),
    ]
