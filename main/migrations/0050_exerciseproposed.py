# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('main', '0049_auto_20151203_1650'),
    ]

    operations = [
        migrations.CreateModel(
            name='ExerciseProposed',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=100, verbose_name='\u041d\u0430\u0437\u0432\u0430\u043d\u0438\u0435')),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('user', models.ForeignKey(verbose_name='\u0414\u043e\u0431\u0430\u0432\u0438\u043b', to=settings.AUTH_USER_MODEL)),
            ],
        ),
    ]
