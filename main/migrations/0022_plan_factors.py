# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0021_exercise_other_names'),
    ]

    operations = [
        migrations.AddField(
            model_name='plan',
            name='factors',
            field=models.TextField(default=True, null=True, verbose_name='\u041c\u043d\u043e\u0436\u0438\u0442\u0435\u043b\u0438 \u0434\u043b\u044f \u0443\u043f\u0440\u0430\u0436\u043d\u0435\u043d\u0438\u0439', blank=True),
        ),
    ]
