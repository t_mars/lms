# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0017_auto_20151105_2121'),
    ]

    operations = [
        migrations.AlterField(
            model_name='cyclescheme',
            name='percent',
            field=models.FloatField(verbose_name='\u041f\u0440\u043e\u0446\u0435\u043d\u0442 \u043e\u0442 \u043c\u0430\u043a\u0441\u0438\u043c\u0443\u043c\u0430'),
        ),
        migrations.AlterField(
            model_name='exercise',
            name='parent_rate',
            field=models.FloatField(default=None, null=True, verbose_name='\u041a\u043e\u044d\u0444\u0444\u0438\u0446\u0435\u043d\u0442', blank=True),
        ),
        migrations.AlterField(
            model_name='exercise',
            name='rate',
            field=models.FloatField(verbose_name='\u041a\u043e\u044d\u0444\u0444\u0438\u0446\u0435\u043d\u0442 \u0441\u043b\u043e\u0436\u043d\u043e\u0441\u0442\u0438'),
        ),
        migrations.AlterField(
            model_name='planpm',
            name='weight',
            field=models.FloatField(verbose_name='\u041f\u0440\u0435\u0434\u0435\u043b\u044c\u043d\u044b\u0439 \u043c\u0430\u043a\u0441\u0438\u043c\u0443\u043c'),
        ),
        migrations.AlterField(
            model_name='trainingscheme',
            name='percent',
            field=models.FloatField(verbose_name='\u041f\u0440\u043e\u0446\u0435\u043d\u0442 \u043e\u0442 \u043c\u0430\u043a\u0441\u0438\u043c\u0443\u043c\u0430'),
        ),
    ]
