# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0054_training_is_fresh'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='training',
            name='is_fresh',
        ),
    ]
