# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0062_sportnorm_levels'),
    ]

    operations = [
        migrations.AlterField(
            model_name='sportnorm',
            name='equip',
            field=models.CharField(max_length=10, verbose_name='\u042d\u043a\u0438\u043f\u0430', choices=[(b'1', '\u0412 \u043e\u0434\u043d\u043e\u0441\u043b\u043e\u0439\u043d\u043e\u0439 \u044d\u043a\u0438\u043f\u0438\u0440\u043e\u0432\u043a\u0435'), (b'raw', '\u0411\u0435\u0437 \u044d\u043a\u0438\u043f\u0438\u0440\u043e\u0432\u043a\u0438'), (b'2', '\u0412 \u043c\u043d\u043e\u0433\u043e\u0441\u043b\u043e\u0439\u043d\u043e\u0439 \u044d\u043a\u0438\u043f\u0438\u0440\u043e\u0432\u043a\u0435')]),
        ),
    ]
