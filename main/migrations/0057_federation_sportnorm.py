# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0056_auto_20151224_1358'),
    ]

    operations = [
        migrations.CreateModel(
            name='Federation',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('short_name', models.CharField(max_length=20, verbose_name='\u041d\u0430\u0437\u0432\u0430\u043d\u0438\u0435')),
                ('name', models.CharField(max_length=50, verbose_name='\u041d\u0430\u0437\u0432\u0430\u043d\u0438\u0435')),
                ('weight_categories', models.CharField(max_length=100, verbose_name='\u0412\u0435\u0441\u043e\u0432\u044b\u0435 \u043a\u0430\u0442\u0435\u0433\u043e\u0440\u0438\u0438')),
            ],
        ),
        migrations.CreateModel(
            name='SportNorm',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('sport', models.CharField(max_length=20, verbose_name='\u0412\u0438\u0434 \u0441\u043f\u043e\u0440\u0442\u0430', choices=[(b'p', '\u041f\u0430\u0443\u044d\u0440\u043b\u0438\u0444\u0442\u0438\u043d\u0433'), (b'dl', '\u0421\u0442\u0430\u043d\u043e\u0432\u0430\u044f \u0442\u044f\u0433\u0430'), (b'bp', '\u0416\u0438\u043c \u043b\u0435\u0436\u0430')])),
                ('equip', models.CharField(max_length=10, verbose_name='\u042d\u043a\u0438\u043f\u0430', choices=[(b'1', '\u041e\u0434\u043d\u043e\u0441\u043b\u043e\u0439\u043d\u0430\u044f'), (b'raw', '\u0411\u0435\u0437 \u044d\u043a\u0438\u043f\u0438\u0440\u043e\u0432\u043a\u0438'), (b'2', '\u041c\u043d\u043e\u0433\u043e\u0441\u043b\u043e\u0439\u043d\u043e\u043d\u0430\u044f')])),
                ('gender', models.CharField(max_length=6, verbose_name='\u041f\u043e\u043b', choices=[(b'male', '\u043c\u0443\u0436'), (b'female', '\u0436\u0435\u043d')])),
                ('division', models.CharField(max_length=20, verbose_name='\u0414\u0438\u0432\u0438\u0437\u0438\u043e\u043d')),
                ('table', models.TextField(default=True, null=True, verbose_name='\u0422\u0430\u0431\u043b\u0438\u0446\u0430', blank=True)),
            ],
        ),
    ]
