# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0046_requestlog'),
    ]

    operations = [
        migrations.AddField(
            model_name='requestlog',
            name='host',
            field=models.CharField(default='', max_length=50, verbose_name='\u0425\u043e\u0441\u0442'),
            preserve_default=False,
        ),
    ]
