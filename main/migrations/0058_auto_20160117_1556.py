# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0057_federation_sportnorm'),
    ]

    operations = [
        migrations.RenameField(
            model_name='federation',
            old_name='weight_categories',
            new_name='female_weight_categories',
        ),
        migrations.AddField(
            model_name='federation',
            name='male_weight_categories',
            field=models.CharField(default='', max_length=100, verbose_name='\u0412\u0435\u0441\u043e\u0432\u044b\u0435 \u043a\u0430\u0442\u0435\u0433\u043e\u0440\u0438\u0438'),
            preserve_default=False,
        ),
    ]
