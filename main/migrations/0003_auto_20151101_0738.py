# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0002_auto_20151101_0734'),
    ]

    operations = [
        migrations.AlterField(
            model_name='cycle',
            name='created_at',
            field=models.DateTimeField(auto_now_add=True),
        ),
        migrations.AlterField(
            model_name='cycle',
            name='name',
            field=models.CharField(max_length=100),
        ),
        migrations.AlterField(
            model_name='cycleexecution',
            name='cycle',
            field=models.ForeignKey(to='main.Cycle'),
        ),
        migrations.AlterField(
            model_name='cycleexecution',
            name='exercise',
            field=models.ForeignKey(to='main.Exercise'),
        ),
        migrations.AlterField(
            model_name='cycleexecution',
            name='level',
            field=models.CharField(max_length=6, choices=[(b'light', '\u041b\u0435\u0433\u043a\u0430\u044f'), (b'hard', '\u0422\u044f\u0436\u0435\u043b\u0430\u044f'), (b'middle', '\u0421\u0440\u0435\u0434\u043d\u044f\u044f')]),
        ),
        migrations.AlterField(
            model_name='cyclescheme',
            name='execution',
            field=models.ForeignKey(to='main.CycleExecution'),
        ),
        migrations.AlterField(
            model_name='cyclescheme',
            name='percent',
            field=models.DecimalField(max_digits=4, decimal_places=2),
        ),
        migrations.AlterField(
            model_name='exercise',
            name='description',
            field=models.TextField(default=True, null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='exercise',
            name='name',
            field=models.CharField(max_length=100),
        ),
        migrations.AlterField(
            model_name='exercise',
            name='parent',
            field=models.ForeignKey(default=None, blank=True, to='main.Exercise', null=True),
        ),
        migrations.AlterField(
            model_name='exercise',
            name='parent_rate',
            field=models.DecimalField(default=None, null=True, max_digits=2, decimal_places=0, blank=True),
        ),
        migrations.AlterField(
            model_name='exercise',
            name='rate',
            field=models.DecimalField(max_digits=4, decimal_places=2),
        ),
        migrations.AlterField(
            model_name='plan',
            name='created_at',
            field=models.DateTimeField(auto_now_add=True),
        ),
        migrations.AlterField(
            model_name='plan',
            name='description',
            field=models.TextField(default=True, null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='plan',
            name='name',
            field=models.CharField(max_length=100),
        ),
        migrations.AlterField(
            model_name='planpm',
            name='exercise',
            field=models.ForeignKey(to='main.Exercise'),
        ),
        migrations.AlterField(
            model_name='planpm',
            name='weight',
            field=models.DecimalField(max_digits=5, decimal_places=2),
        ),
        migrations.AlterField(
            model_name='training',
            name='comment',
            field=models.TextField(default=None, null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='training',
            name='plan',
            field=models.ForeignKey(to='main.Plan'),
        ),
        migrations.AlterField(
            model_name='trainingexecution',
            name='comment',
            field=models.TextField(default=None, null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='trainingexecution',
            name='exercise',
            field=models.ForeignKey(to='main.Exercise'),
        ),
        migrations.AlterField(
            model_name='trainingexecution',
            name='pm',
            field=models.ForeignKey(to='main.PlanPM'),
        ),
        migrations.AlterField(
            model_name='trainingexecution',
            name='training',
            field=models.ForeignKey(to='main.Training'),
        ),
        migrations.AlterField(
            model_name='trainingscheme',
            name='execution',
            field=models.ForeignKey(to='main.TrainingExecution'),
        ),
        migrations.AlterField(
            model_name='trainingscheme',
            name='percent',
            field=models.DecimalField(max_digits=4, decimal_places=2),
        ),
    ]
