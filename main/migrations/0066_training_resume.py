# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0065_auto_20160118_1327'),
    ]

    operations = [
        migrations.AddField(
            model_name='training',
            name='resume',
            field=models.TextField(default=None, null=True, verbose_name='\u0418\u0442\u043e\u0433', blank=True),
        ),
    ]
