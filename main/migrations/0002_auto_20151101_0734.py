# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='exercise',
            name='parent',
            field=models.ForeignKey(default=None, blank=True, to='main.Exercise', null=True, verbose_name='\u0423\u043f\u0440\u0430\u0436\u043d\u0435\u043d\u0438\u0435'),
        ),
        migrations.AddField(
            model_name='exercise',
            name='parent_rate',
            field=models.DecimalField(decimal_places=0, default=None, max_digits=2, blank=True, null=True, verbose_name='\u041a\u043e\u044d\u0444\u0444\u0438\u0446\u0435\u043d\u0442'),
        ),
    ]
