# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0064_sportnorm_doping_control'),
    ]

    operations = [
        migrations.AlterField(
            model_name='sportnorm',
            name='doping_control',
            field=models.CharField(max_length=6, verbose_name='\u0414\u043e\u043f\u0438\u043d\u0433 \u043a\u043e\u043d\u0442\u0440\u043e\u043b\u044c', choices=[(b'yes', '\u0421 \u0434\u043e\u043f\u0438\u043d\u0433 \u043a\u043e\u043d\u0442\u0440\u043e\u043b\u0435\u043c'), (b'no', '\u0411\u0435\u0437 \u0434\u043e\u043f\u0438\u043d\u0433 \u043a\u043e\u043d\u0442\u0440\u043e\u043b\u044f')]),
        ),
    ]
