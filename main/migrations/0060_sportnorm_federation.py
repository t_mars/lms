# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0059_auto_20160117_1601'),
    ]

    operations = [
        migrations.AddField(
            model_name='sportnorm',
            name='federation',
            field=models.ForeignKey(default=None, verbose_name='\u0424\u0435\u0434\u0435\u0440\u0430\u0446\u0438\u044f', to='main.Federation'),
        ),
    ]
