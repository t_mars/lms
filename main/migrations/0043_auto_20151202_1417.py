# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0042_auto_20151202_1412'),
    ]

    operations = [
        migrations.AlterField(
            model_name='exercise',
            name='group',
            field=models.ForeignKey(default=True, blank=True, to='main.ExerciseGroup', null=True, verbose_name=b'\xd0\x93\xd1\x80\xd1\x83\xd0\xbf\xd0\xbf\xd0\xb0'),
        ),
    ]
