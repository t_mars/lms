# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0047_requestlog_host'),
    ]

    operations = [
        migrations.AlterField(
            model_name='requestlog',
            name='method',
            field=models.CharField(max_length=10, verbose_name='method'),
        ),
    ]
