# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0019_auto_20151107_1339'),
    ]

    operations = [
        migrations.AddField(
            model_name='exercise',
            name='is_dual',
            field=models.BooleanField(default=False, verbose_name='\u0414\u0432\u043e\u0439\u043d\u043e\u0435 \u0443\u043f\u0440\u0430\u0436\u043d\u0435\u043d\u0438\u0435'),
        ),
        migrations.AddField(
            model_name='exercise',
            name='with_factor',
            field=models.BooleanField(default=False, verbose_name='\u0421 \u043f\u043b\u0438\u0442\u043a\u0430\u043c\u0438'),
        ),
    ]
