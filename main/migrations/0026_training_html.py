# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0025_auto_20151108_1907'),
    ]

    operations = [
        migrations.AddField(
            model_name='training',
            name='html',
            field=models.TextField(default=None, null=True, verbose_name='HTML', blank=True),
        ),
    ]
