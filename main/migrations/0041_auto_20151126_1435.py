# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0040_auto_20151126_1427'),
    ]

    operations = [
        migrations.AlterField(
            model_name='plan',
            name='sportsman',
            field=models.ForeignKey(related_name='sportsman', default=None, blank=True, to=settings.AUTH_USER_MODEL, null=True, verbose_name='\u0421\u043e\u0437\u0434\u0430\u0442\u0435\u043b\u044c'),
        ),
    ]
