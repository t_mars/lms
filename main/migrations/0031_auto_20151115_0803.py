# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0030_cycle_table'),
    ]

    operations = [
        migrations.AddField(
            model_name='plan',
            name='source',
            field=models.ForeignKey(on_delete=django.db.models.deletion.SET_NULL, default=None, blank=True, to='main.Cycle', null=True, verbose_name='\u0418\u0441\u0442\u043e\u0447\u043d\u0438\u043a'),
        ),
        migrations.AddField(
            model_name='trainingexecution',
            name='source',
            field=models.ForeignKey(on_delete=django.db.models.deletion.SET_NULL, default=None, blank=True, to='main.CycleExecution', null=True, verbose_name='\u0418\u0441\u0442\u043e\u0447\u043d\u0438\u043a'),
        ),
        migrations.AddField(
            model_name='trainingscheme',
            name='source',
            field=models.ForeignKey(on_delete=django.db.models.deletion.SET_NULL, default=None, blank=True, to='main.CycleScheme', null=True, verbose_name='\u0418\u0441\u0442\u043e\u0447\u043d\u0438\u043a'),
        ),
    ]
