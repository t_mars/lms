# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0036_cycle_sport'),
    ]

    operations = [
        migrations.AddField(
            model_name='cycle',
            name='extra',
            field=models.CharField(default=b'', max_length=1000, verbose_name='\u0414\u043e\u043f\u043e\u043b\u043d\u0438\u0442\u0435\u043b\u044c\u043d\u043e'),
        ),
        migrations.AddField(
            model_name='cycle',
            name='level',
            field=models.CharField(default=b'', max_length=1000, verbose_name='\u0423\u0440\u043e\u0432\u0435\u043d\u044c'),
        ),
    ]
