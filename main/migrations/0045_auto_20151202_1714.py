# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0044_auto_20151202_1424'),
    ]

    operations = [
        migrations.AddField(
            model_name='cycle',
            name='abs_weight',
            field=models.CharField(default=b'', max_length=100, verbose_name='\u0410\u0431\u0441. \u0432\u0435\u0441', blank=True),
        ),
        migrations.AddField(
            model_name='cycle',
            name='gender',
            field=models.CharField(default='male', max_length=6, verbose_name='\u041f\u043e\u043b', choices=[(b'male', '\u043c\u0443\u0436'), (b'female', '\u0436\u0435\u043d')]),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='cycle',
            name='hw_ratio',
            field=models.CharField(default='norm', max_length=6, verbose_name='\u0412\u0435\u0441/\u0440\u043e\u0441\u0442 \u0441\u043e\u043e\u0442\u043d.', choices=[(b'dec', '\u0421\u043d\u0438\u0436\u0435\u043d\u043d\u043e\u0435'), (b'norm', '\u041d\u043e\u0440\u043c\u0430\u043b\u044c\u043d\u043e\u0435'), (b'inc', '\u041f\u043e\u0432\u044b\u0448\u0435\u043d\u043d\u043e\u0435')]),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='cycle',
            name='mode',
            field=models.CharField(default=b'', max_length=100, verbose_name='\u0422\u0438\u043f', blank=True),
        ),
    ]
