# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0009_auto_20151101_1224'),
    ]

    operations = [
        migrations.AlterField(
            model_name='cycleexecution',
            name='micro_num',
            field=models.PositiveSmallIntegerField(verbose_name='\u041d\u043e\u043c\u0435\u0440 \u0446\u0438\u043a\u043b\u0430'),
        ),
        migrations.AlterField(
            model_name='trainingexecution',
            name='order',
            field=models.PositiveSmallIntegerField(default=0, verbose_name='\u041f\u043e\u0434\u0440\u044f\u0434\u043e\u043a \u0443\u043f\u0440\u0430\u0436\u043d\u0435\u043d\u0438\u044f'),
        ),
    ]
