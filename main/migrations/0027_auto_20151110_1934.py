# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0026_training_html'),
    ]

    operations = [
        migrations.AddField(
            model_name='training',
            name='avg_weight_cache',
            field=models.FloatField(default=None, null=True, verbose_name='\u0421\u0440\u0435\u0434\u043d\u0438\u0439 \u0432\u0435\u0441', blank=True),
        ),
        migrations.AddField(
            model_name='training',
            name='kps_cache',
            field=models.PositiveIntegerField(default=None, null=True, verbose_name='\u041a\u041f\u0428', blank=True),
        ),
        migrations.AddField(
            model_name='training',
            name='percent_cache',
            field=models.FloatField(default=None, null=True, verbose_name='\u0418\u041e', blank=True),
        ),
        migrations.AddField(
            model_name='training',
            name='tonag_cache',
            field=models.FloatField(default=None, null=True, verbose_name='\u0422\u043e\u043d\u043d\u0430\u0436', blank=True),
        ),
    ]
