# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0004_auto_20151101_0739'),
    ]

    operations = [
        migrations.AlterField(
            model_name='cycle',
            name='created_at',
            field=models.DateTimeField(auto_now_add=True, verbose_name='\u0412\u0440\u0435\u043c\u044f \u0441\u043e\u0437\u0434\u0430\u043d\u0438\u044f'),
        ),
        migrations.AlterField(
            model_name='cycle',
            name='description',
            field=models.TextField(verbose_name='\u041e\u043f\u0438\u0441\u0430\u043d\u0438\u0435'),
        ),
        migrations.AlterField(
            model_name='cycle',
            name='name',
            field=models.CharField(max_length=100, verbose_name='\u041d\u0430\u0437\u0432\u0430\u043d\u0438\u0435'),
        ),
        migrations.AlterField(
            model_name='cycleexecution',
            name='cycle',
            field=models.ForeignKey(verbose_name='\u0421\u0420\u0426', to='main.Cycle'),
        ),
        migrations.AlterField(
            model_name='cycleexecution',
            name='day_num',
            field=models.PositiveSmallIntegerField(verbose_name='\u041d\u043e\u043c\u0435\u0440 \u0434\u043d\u044f'),
        ),
        migrations.AlterField(
            model_name='cycleexecution',
            name='exercise',
            field=models.ForeignKey(verbose_name='\u0423\u043f\u0440\u0430\u0436\u043d\u0435\u043d\u0438\u0435', to='main.Exercise'),
        ),
        migrations.AlterField(
            model_name='cycleexecution',
            name='level',
            field=models.CharField(max_length=6, verbose_name='\u0423\u0440\u043e\u0432\u0435\u043d\u044c \u043d\u0430\u0433\u0440\u0443\u0437\u043a\u0438', choices=[(b'light', '\u041b\u0435\u0433\u043a\u0430\u044f'), (b'hard', '\u0422\u044f\u0436\u0435\u043b\u0430\u044f'), (b'middle', '\u0421\u0440\u0435\u0434\u043d\u044f\u044f')]),
        ),
        migrations.AlterField(
            model_name='cycleexecution',
            name='order',
            field=models.PositiveSmallIntegerField(verbose_name='\u041f\u043e\u0434\u0440\u044f\u0434\u043e\u043a \u0443\u043f\u0440\u0430\u0436\u043d\u0435\u043d\u0438\u044f'),
        ),
        migrations.AlterField(
            model_name='cycleexecution',
            name='week_num',
            field=models.PositiveSmallIntegerField(verbose_name='\u041d\u043e\u043c\u0435\u0440 \u043d\u0435\u0434\u0435\u043b\u0438'),
        ),
        migrations.AlterField(
            model_name='cyclescheme',
            name='apps',
            field=models.PositiveSmallIntegerField(verbose_name='\u041f\u043e\u0434\u0445\u043e\u0434\u044b'),
        ),
        migrations.AlterField(
            model_name='cyclescheme',
            name='execution',
            field=models.ForeignKey(verbose_name='\u0412\u044b\u043f\u043e\u043b\u043d\u0435\u043d\u0438\u0435 \u0443\u043f\u0440\u0430\u0436\u043d\u0435\u043d\u0438\u044f', to='main.CycleExecution'),
        ),
        migrations.AlterField(
            model_name='cyclescheme',
            name='percent',
            field=models.DecimalField(verbose_name='\u041f\u0440\u043e\u0446\u0435\u043d\u0442 \u043e\u0442 \u043c\u0430\u043a\u0441\u0438\u043c\u0443\u043c\u0430', max_digits=4, decimal_places=2),
        ),
        migrations.AlterField(
            model_name='cyclescheme',
            name='reps',
            field=models.PositiveSmallIntegerField(verbose_name='\u041f\u043e\u0432\u0442\u043e\u0440\u0435\u043d\u0438\u044f'),
        ),
        migrations.AlterField(
            model_name='exercise',
            name='description',
            field=models.TextField(default=True, null=True, verbose_name='\u041e\u043f\u0438\u0441\u0430\u043d\u0438\u0435', blank=True),
        ),
        migrations.AlterField(
            model_name='exercise',
            name='name',
            field=models.CharField(max_length=100, verbose_name='\u041d\u0430\u0437\u0432\u0430\u043d\u0438\u0435'),
        ),
        migrations.AlterField(
            model_name='exercise',
            name='parent',
            field=models.ForeignKey(default=None, blank=True, to='main.Exercise', null=True, verbose_name='\u0423\u043f\u0440\u0430\u0436\u043d\u0435\u043d\u0438\u0435'),
        ),
        migrations.AlterField(
            model_name='exercise',
            name='parent_rate',
            field=models.DecimalField(decimal_places=0, default=None, max_digits=2, blank=True, null=True, verbose_name='\u041a\u043e\u044d\u0444\u0444\u0438\u0446\u0435\u043d\u0442'),
        ),
        migrations.AlterField(
            model_name='exercise',
            name='rate',
            field=models.DecimalField(verbose_name='\u041a\u043e\u044d\u0444\u0444\u0438\u0446\u0435\u043d\u0442 \u0441\u043b\u043e\u0436\u043d\u043e\u0441\u0442\u0438', max_digits=4, decimal_places=2),
        ),
        migrations.AlterField(
            model_name='plan',
            name='created_at',
            field=models.DateTimeField(auto_now_add=True, verbose_name='\u0412\u0440\u0435\u043c\u044f \u0441\u043e\u0437\u0434\u0430\u043d\u0438\u044f'),
        ),
        migrations.AlterField(
            model_name='plan',
            name='description',
            field=models.TextField(default=True, null=True, verbose_name='\u041e\u043f\u0438\u0441\u0430\u043d\u0438\u0435', blank=True),
        ),
        migrations.AlterField(
            model_name='plan',
            name='name',
            field=models.CharField(max_length=100, verbose_name='\u041d\u0430\u0437\u0432\u0430\u043d\u0438\u0435'),
        ),
        migrations.AlterField(
            model_name='plan',
            name='start_date',
            field=models.DateField(verbose_name='\u0414\u0430\u0442\u0430 \u043d\u0430\u0447\u0430\u043b\u0430'),
        ),
        migrations.AlterField(
            model_name='planpm',
            name='date',
            field=models.DateField(verbose_name='\u0414\u0430\u0442\u0430'),
        ),
        migrations.AlterField(
            model_name='planpm',
            name='exercise',
            field=models.ForeignKey(verbose_name='\u0423\u043f\u0440\u0430\u0436\u043d\u0435\u043d\u0438\u0435', to='main.Exercise'),
        ),
        migrations.AlterField(
            model_name='planpm',
            name='weight',
            field=models.DecimalField(verbose_name='\u041f\u0440\u0435\u0434\u0435\u043b\u044c\u043d\u044b\u0439 \u043c\u0430\u043a\u0441\u0438\u043c\u0443\u043c', max_digits=5, decimal_places=2),
        ),
        migrations.AlterField(
            model_name='training',
            name='comment',
            field=models.TextField(default=None, null=True, verbose_name='\u041a\u043e\u043c\u043c\u0435\u043d\u0442\u0430\u0440\u0438\u0439', blank=True),
        ),
        migrations.AlterField(
            model_name='training',
            name='date',
            field=models.DateField(verbose_name='\u0414\u0430\u0442\u0430'),
        ),
        migrations.AlterField(
            model_name='training',
            name='plan',
            field=models.ForeignKey(verbose_name='\u041f\u043b\u0430\u043d', to='main.Plan'),
        ),
        migrations.AlterField(
            model_name='trainingexecution',
            name='comment',
            field=models.TextField(default=None, null=True, verbose_name='\u041a\u043e\u043c\u043c\u0435\u043d\u0442\u0430\u0440\u0438\u0439', blank=True),
        ),
        migrations.AlterField(
            model_name='trainingexecution',
            name='exercise',
            field=models.ForeignKey(verbose_name='\u0423\u043f\u0440\u0430\u0436\u043d\u0435\u043d\u0438\u0435', to='main.Exercise'),
        ),
        migrations.AlterField(
            model_name='trainingexecution',
            name='order',
            field=models.PositiveSmallIntegerField(verbose_name='\u041f\u043e\u0434\u0440\u044f\u0434\u043e\u043a \u0443\u043f\u0440\u0430\u0436\u043d\u0435\u043d\u0438\u044f'),
        ),
        migrations.AlterField(
            model_name='trainingexecution',
            name='pm',
            field=models.ForeignKey(verbose_name='\u041f\u0440\u0435\u0434\u0435\u043b\u044c\u043d\u044b\u0439 \u043c\u0430\u043a\u0441\u0438\u043c\u0443\u043c', to='main.PlanPM'),
        ),
        migrations.AlterField(
            model_name='trainingexecution',
            name='training',
            field=models.ForeignKey(verbose_name='\u0422\u0440\u0435\u043d\u0438\u0440\u043e\u0432\u043a\u0430', to='main.Training'),
        ),
        migrations.AlterField(
            model_name='trainingscheme',
            name='apps',
            field=models.PositiveSmallIntegerField(verbose_name='\u041f\u043e\u0434\u0445\u043e\u0434\u044b'),
        ),
        migrations.AlterField(
            model_name='trainingscheme',
            name='execution',
            field=models.ForeignKey(verbose_name='\u0412\u044b\u043f\u043e\u043b\u043d\u0435\u043d\u0438\u0435 \u0443\u043f\u0440\u0430\u0436\u043d\u0435\u043d\u0438\u044f', to='main.TrainingExecution'),
        ),
        migrations.AlterField(
            model_name='trainingscheme',
            name='percent',
            field=models.DecimalField(verbose_name='\u041f\u0440\u043e\u0446\u0435\u043d\u0442 \u043e\u0442 \u043c\u0430\u043a\u0441\u0438\u043c\u0443\u043c\u0430', max_digits=4, decimal_places=2),
        ),
        migrations.AlterField(
            model_name='trainingscheme',
            name='reps',
            field=models.PositiveSmallIntegerField(verbose_name='\u041f\u043e\u0432\u0442\u043e\u0440\u0435\u043d\u0438\u044f'),
        ),
    ]
