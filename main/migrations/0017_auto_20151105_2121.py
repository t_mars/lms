# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0016_training_name'),
    ]

    operations = [
        migrations.AddField(
            model_name='trainingexecution',
            name='other_apps',
            field=models.CharField(default=None, max_length=20, null=True, verbose_name='\u041f\u043e\u0434\u0445\u043e\u0434\u044b', blank=True),
        ),
        migrations.AddField(
            model_name='trainingexecution',
            name='other_name',
            field=models.CharField(default=None, max_length=100, null=True, verbose_name='\u0414\u0440\u0443\u0433\u043e\u0435 \u0443\u043f\u0440\u0430\u0436\u043d\u0435\u043d\u0438\u0435', blank=True),
        ),
        migrations.AddField(
            model_name='trainingexecution',
            name='other_reps',
            field=models.CharField(default=None, max_length=20, null=True, verbose_name='\u041f\u043e\u0432\u0442\u043e\u0440\u0435\u043d\u0438\u044f', blank=True),
        ),
        migrations.AddField(
            model_name='trainingexecution',
            name='other_weight',
            field=models.CharField(default=None, max_length=20, null=True, verbose_name='\u0412\u0435\u0441', blank=True),
        ),
        migrations.AlterField(
            model_name='cycleexecution',
            name='level',
            field=models.CharField(max_length=6, verbose_name='\u0423\u0440\u043e\u0432\u0435\u043d\u044c \u043d\u0430\u0433\u0440\u0443\u0437\u043a\u0438', choices=[(b'light', '\u041b\u0435\u0433\u043a\u0430\u044f'), (b'hard', '\u0422\u044f\u0436\u0435\u043b\u0430\u044f'), (b'other', '\u041f\u0440\u043e\u0447\u0435\u0435'), (b'middle', '\u0421\u0440\u0435\u0434\u043d\u044f\u044f')]),
        ),
        migrations.AlterField(
            model_name='trainingexecution',
            name='exercise',
            field=models.ForeignKey(verbose_name='\u0423\u043f\u0440\u0430\u0436\u043d\u0435\u043d\u0438\u0435', blank=True, to='main.Exercise', null=True),
        ),
        migrations.AlterField(
            model_name='trainingexecution',
            name='level',
            field=models.CharField(default=None, choices=[(b'light', '\u041b\u0435\u0433\u043a\u0430\u044f'), (b'hard', '\u0422\u044f\u0436\u0435\u043b\u0430\u044f'), (b'other', '\u041f\u0440\u043e\u0447\u0435\u0435'), (b'middle', '\u0421\u0440\u0435\u0434\u043d\u044f\u044f')], max_length=6, blank=True, null=True, verbose_name='\u0423\u0440\u043e\u0432\u0435\u043d\u044c \u043d\u0430\u0433\u0440\u0443\u0437\u043a\u0438'),
        ),
        migrations.AlterField(
            model_name='trainingexecution',
            name='pm',
            field=models.ForeignKey(verbose_name='\u041f\u0440\u0435\u0434\u0435\u043b\u044c\u043d\u044b\u0439 \u043c\u0430\u043a\u0441\u0438\u043c\u0443\u043c', blank=True, to='main.PlanPM', null=True),
        ),
    ]
