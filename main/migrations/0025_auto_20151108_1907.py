# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0024_auto_20151108_1849'),
    ]

    operations = [
        migrations.AlterField(
            model_name='trainingexecution',
            name='other_color',
            field=models.CharField(default=b'default', max_length=10, verbose_name='\u0421\u0442\u0430\u0442\u0443\u0441', choices=[(b'default', b'default'), (b'info', b'info'), (b'warning', b'warning'), (b'success', b'success'), (b'danger', b'danger')]),
        ),
        migrations.AlterField(
            model_name='trainingscheme',
            name='color',
            field=models.CharField(default=b'default', max_length=10, verbose_name='\u0421\u0442\u0430\u0442\u0443\u0441', choices=[(b'default', b'default'), (b'info', b'info'), (b'warning', b'warning'), (b'success', b'success'), (b'danger', b'danger')]),
        ),
    ]
