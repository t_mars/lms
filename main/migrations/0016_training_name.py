# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0015_auto_20151105_1346'),
    ]

    operations = [
        migrations.AddField(
            model_name='training',
            name='name',
            field=models.CharField(default=None, max_length=100, null=True, verbose_name='\u041d\u0430\u0437\u0432\u0430\u043d\u0438\u0435', blank=True),
        ),
    ]
