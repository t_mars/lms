# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0055_remove_training_is_fresh'),
    ]

    operations = [
        migrations.AlterField(
            model_name='exercise',
            name='factor',
            field=models.FloatField(default=None, null=True, verbose_name='\u0421 \u043f\u043b\u0438\u0442\u043a\u0430\u043c\u0438', blank=True),
        ),
    ]
