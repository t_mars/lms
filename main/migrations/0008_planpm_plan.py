# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0007_cycle_defaults'),
    ]

    operations = [
        migrations.AddField(
            model_name='planpm',
            name='plan',
            field=models.ForeignKey(default=None, verbose_name='\u041f\u043b\u0430\u043d', to='main.Plan'),
        ),
    ]
