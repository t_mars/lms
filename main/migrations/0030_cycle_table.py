# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0029_plan_author'),
    ]

    operations = [
        migrations.AddField(
            model_name='cycle',
            name='table',
            field=models.TextField(default=None, null=True, verbose_name='\u0418\u0430\u0431\u043b', blank=True),
        ),
    ]
