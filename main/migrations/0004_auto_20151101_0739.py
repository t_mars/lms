# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0003_auto_20151101_0738'),
    ]

    operations = [
        migrations.AlterField(
            model_name='cycle',
            name='description',
            field=models.TextField(),
        ),
        migrations.AlterField(
            model_name='cycleexecution',
            name='day_num',
            field=models.PositiveSmallIntegerField(),
        ),
        migrations.AlterField(
            model_name='cycleexecution',
            name='order',
            field=models.PositiveSmallIntegerField(),
        ),
        migrations.AlterField(
            model_name='cycleexecution',
            name='week_num',
            field=models.PositiveSmallIntegerField(),
        ),
        migrations.AlterField(
            model_name='cyclescheme',
            name='apps',
            field=models.PositiveSmallIntegerField(),
        ),
        migrations.AlterField(
            model_name='cyclescheme',
            name='reps',
            field=models.PositiveSmallIntegerField(),
        ),
        migrations.AlterField(
            model_name='plan',
            name='start_date',
            field=models.DateField(),
        ),
        migrations.AlterField(
            model_name='planpm',
            name='date',
            field=models.DateField(),
        ),
        migrations.AlterField(
            model_name='training',
            name='date',
            field=models.DateField(),
        ),
        migrations.AlterField(
            model_name='trainingexecution',
            name='order',
            field=models.PositiveSmallIntegerField(),
        ),
        migrations.AlterField(
            model_name='trainingscheme',
            name='apps',
            field=models.PositiveSmallIntegerField(),
        ),
        migrations.AlterField(
            model_name='trainingscheme',
            name='reps',
            field=models.PositiveSmallIntegerField(),
        ),
    ]
