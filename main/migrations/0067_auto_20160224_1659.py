# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0066_training_resume'),
    ]

    operations = [
        migrations.AlterField(
            model_name='sportnorm',
            name='sport',
            field=models.CharField(max_length=20, verbose_name='\u0412\u0438\u0434 \u0441\u043f\u043e\u0440\u0442\u0430', choices=[(b'p', '\u041f\u0430\u0443\u044d\u0440\u043b\u0438\u0444\u0442\u0438\u043d\u0433'), (b'dl', '\u0421\u0442\u0430\u043d\u043e\u0432\u0430\u044f \u0442\u044f\u0433\u0430'), (b'bp', '\u0416\u0438\u043c \u043b\u0435\u0436\u0430'), (b'sq', '\u041f\u0440\u0438\u0441\u0435\u0434\u0430\u043d\u0438\u044f')]),
        ),
    ]
