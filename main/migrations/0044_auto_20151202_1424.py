# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0043_auto_20151202_1417'),
    ]

    operations = [
        migrations.AddField(
            model_name='exercise',
            name='is_checked',
            field=models.BooleanField(default=False, verbose_name='\u041f\u0440\u043e\u0432\u0435\u0440\u0435\u043d\u043e'),
        ),
        migrations.AlterField(
            model_name='exercise',
            name='group',
            field=models.ForeignKey(default=True, verbose_name=b'\xd0\x93\xd1\x80\xd1\x83\xd0\xbf\xd0\xbf\xd0\xb0', to='main.ExerciseGroup', null=True),
        ),
    ]
