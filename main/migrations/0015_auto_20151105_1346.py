# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0014_auto_20151104_2227'),
    ]

    operations = [
        migrations.AlterField(
            model_name='trainingscheme',
            name='percent',
            field=models.DecimalField(verbose_name='\u041f\u0440\u043e\u0446\u0435\u043d\u0442 \u043e\u0442 \u043c\u0430\u043a\u0441\u0438\u043c\u0443\u043c\u0430', max_digits=3, decimal_places=0),
        ),
    ]
