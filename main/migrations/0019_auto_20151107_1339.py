# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0018_auto_20151106_2040'),
    ]

    operations = [
        migrations.AlterUniqueTogether(
            name='planpm',
            unique_together=set([('plan', 'exercise', 'date')]),
        ),
        migrations.AlterIndexTogether(
            name='planpm',
            index_together=set([]),
        ),
    ]
