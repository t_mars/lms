# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0058_auto_20160117_1556'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='federation',
            name='female_weight_categories',
        ),
        migrations.RemoveField(
            model_name='federation',
            name='male_weight_categories',
        ),
        migrations.AlterField(
            model_name='federation',
            name='short_name',
            field=models.CharField(max_length=20, verbose_name='\u041a\u043e\u0440\u043e\u0442\u043a\u043e\u0435 \u043d\u0430\u0437\u0432\u0430\u043d\u0438\u0435'),
        ),
    ]
