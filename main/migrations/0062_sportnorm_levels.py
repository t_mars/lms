# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0061_auto_20160117_1605'),
    ]

    operations = [
        migrations.AddField(
            model_name='sportnorm',
            name='levels',
            field=models.TextField(default='', max_length=100, verbose_name='\u0417\u0432\u0430\u043d\u0438\u044f'),
            preserve_default=False,
        ),
    ]
