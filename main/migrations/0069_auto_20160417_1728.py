# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0068_exercise_author'),
    ]

    operations = [
        migrations.AlterField(
            model_name='exercise',
            name='rate',
            field=models.FloatField(default=1, verbose_name='\u041a\u043e\u044d\u0444\u0444\u0438\u0446\u0435\u043d\u0442 \u0441\u043b\u043e\u0436\u043d\u043e\u0441\u0442\u0438'),
        ),
    ]
