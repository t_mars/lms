from django.conf import settings
from django.conf.urls import patterns, include, url
from django.contrib import admin

admin.autodiscover()

urlpatterns = patterns('',
    url(r'', include('social_auth.urls')),
    url(r'', include('drugplan.urls')),
    url(r'', include('profile.urls')),
    url(r'^payment/', include('payment.urls')),
    url(r'^tinymce/', include('tinymce.urls')),
    url(r'^video/', include('videohub.urls')),

    url(r'^$', 'main.views.base.home', name='home'),

    url(r'^stream/$', 'main.views.base.stream', name='stream'),

    url(r'^admin/stat/$', 'main.views.admin.stat', name='admin_stat'),
    url(r'^admin/stat2/$', 'main.views.admin.stat2', name='admin_stat2'),
    url(r'^admin/stat/reg/$', 'main.views.admin.stat_users', {'mode': 'reg'}, name='admin_stat_reg'),
    url(r'^admin/stat/login/$', 'main.views.admin.stat_users', {'mode': 'login'}, name='admin_stat_login'),
    url(r'^admin/stat/online/$', 'main.views.admin.stat_users', {'mode': 'online'}, name='admin_stat_online'),
    url(r'^admin/stat/cycle/$', 'main.views.admin.stat_cycle', name='admin_stat_cycle'),
    url(r'^admin/stat/plan/$', 'main.views.admin.stat_plan', name='admin_stat_plan'),
    url(r'^admin/user/(?P<id>\d+)/$', 'main.views.admin.user', name='admin_user'),
    url(r'^admin/user/(?P<id>\d+)/plans/$', 'main.views.admin.user_plans', name='admin_user_plans'),
    url(r'^admin/user/(?P<id>\d+)/requests/$', 'main.views.admin.user_requests', name='admin_user_requests'),
    
    url(r'^util/warmup/$', 'main.views.util.warmup', name='util_warmup'),
    url(r'^util/maximum/$', 'main.views.util.maximum', name='util_maximum'),
    url(r'^util/norms/$', 'main.views.util.norms', name='util_norms'),
    url(r'^util/converter/$', 'main.views.util.converter', name='util_converter'),
    
    # url(r'^$', 'main.views.cycle.list', name='home'),
    url(r'^about/$', 'main.views.base.about', name='about'),
    url(r'^auth/$', 'main.views.base.auth', name='auth'),
    url(r'^auth/success/$', 'main.views.base.auth_success', name='auth_success'),
    url(r'^logout/$', 'main.views.base.logout_view', name='logout'),

    url(r'^exercise/list/$', 'main.views.exercise.list', name='exercise_list'),
    url(r'^exercise/my/list/$', 'main.views.exercise.private_list', name='exercise_private_list'),
    url(r'^exercise/(?P<id>\d+)/$', 'main.views.exercise.show', name='exercise_show'),

    url(r'^cycle/list/$', 'main.views.cycle.list', name='cycle_list'),
    url(r'^cycle/(?P<num>[\w\d]+)/$', 'main.views.cycle.show', name='cycle'),
    url(r'^cycle/(?P<num>[\w\d]+)/create_plan/$', 'main.views.cycle.create_plan', name='cycle_create_plan'),

    url(r'^plan/list/$', 'main.views.plan.list', name='plan_list'),
    url(r'^plan/create/$', 'main.views.plan.create', name='plan_create'),
    url(r'^plan/(?P<id>\d+)/print/$', 'main.views.plan.plan_print', name='plan_print'),
    

    url(r'^lms-admin/', include(admin.site.urls)),

    url(r'^plan/(?P<id>\d+)/delete/$', 'main.views.plan.delete', name='plan_delete'),
    url(r'^plan/(?P<id>\d+)/$', 'main.views.plan_show.show', name='plan'),
    url(r'^plan/(?P<id>\d+)/edit/$', 'main.views.plan_show.edit'),
    url(r'^plan/(?P<id>\d+)/load/(?P<ts>\d+)/$', 'main.views.plan_show.load'),
    url(r'^plan/(?P<id>\d+)/load/down/(?P<ts>\d+)/$', 'main.views.plan_show.load_down'),
    url(r'^plan/(?P<id>\d+)/load/up/(?P<ts>\d+)/$', 'main.views.plan_show.load_up'),
    url(r'^plan/(?P<id>\d+)/train/save/$', 'main.views.plan_show.train_save'),
    url(r'^plan/(?P<id>\d+)/plots/$', 'main.views.plan_show.plots'),
    url(r'^plan/(?P<id>\d+)/pm/list/(?P<ts>\d+)/$', 'main.views.plan_show.pm_list'),
    url(r'^plan/(?P<id>\d+)/pm/save/(?P<ts>\d+)/$', 'main.views.plan_show.pm_save'),
    url(r'^plan/(?P<id>\d+)/pm/get/(?P<d>\d{2}\.\d{2}\.\d{4})/$', 'main.views.plan_show.pm_get'),
    url(r'^plan/(?P<id>\d+)/exercise/create/$', 'main.views.plan_show.exercise_create'),
    url(r'^plan/(?P<id>\d+)/update/(?P<week_num1>\d+)/(?P<week_num2>\d+)/$', 'main.views.plan_show.update'),
    url(r'^plan/(?P<id>\d+)/train/(?P<train_id>\d+)/delete/$', 'main.views.plan_show.train_delete'),
    url(r'^plan/(?P<id>\d+)/exercises/get/$', 'main.views.plan_show.get_exercises'),
    url(r'^plan/(?P<id>\d+)/exercises/save/$', 'main.views.plan_show.save_exercises'),
    url(r'^plan/(?P<id>\d+)/break/add/(?P<week_num>\d+)/$', 'main.views.plan_show.break_add'),
    url(r'^plan/(?P<id>\d+)/break/del/(?P<week_num>\d+)/$', 'main.views.plan_show.break_del'),

    url(r'^news/(?P<mode>[\w\d\._-]+)/$', 'main.views.base.news', name='news'),
    url(r'^app/android/$', 'main.views.base.android'),
    url(r'^app/ios/$', 'main.views.base.ios')
)

if settings.DEBUG:
    import debug_toolbar
    urlpatterns += patterns('',
        url(r'^__debug__/', include(debug_toolbar.urls)),
    )