#coding=utf8
import datetime
import re

from django.shortcuts import render
from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from django.http import Http404
from django.db.models import Q
from ratelimit.utils import is_ratelimited

from main.models import *
from main.services.plan import PlanService
from main.services.train import TrainService
from common.views import auth_check, json_error, json_response


def plan_access(mode):
    """
    Декоратор, который проверяет есть ли у пользователя доступ на просмотр плана
    и проверяет на существование плана по id из запроса
    """

    modes = ['show']
    if mode:
        modes.append(mode)

    def decorator(function):
        def wrapper(request, id, *args, **kwargs):

            try:
                plan = Plan.objects.get(pk=id)
            except:
                return HttpResponse(json.dumps({
                    'status': 'error',
                    'message': u'План не найден.'
                }))


            for mode in modes:
                if not plan.access_check(request.user, mode):
                    return HttpResponse(json.dumps({
                        'status': 'error',
                        'message': u'У вас нет права [%s] для этого плана.' % (mode)
                    }))

            return function(request, plan, *args, **kwargs)
        return wrapper
    return decorator


def get_plan_data(plan):
    return {
        'name': plan.name,
        'is_public': plan.is_public,
        'sportsman': {
            'username': plan.sportsman.username,
            'id': plan.sportsman.id
        } if plan.sportsman else None,
        'author': {
            'username': plan.author.username,
            'id': plan.author.id
        } if plan.author else None,
        'start_date': plan.start_date.strftime("%d.%m.%Y"),
    }


def show(request, id):
    try:
        plan = Plan.objects.get(pk=id)
    except:
        raise Http404(u'План не найден.')

    if not plan.access_check(request.user, 'show'):
        raise Http404(u'У вас нет прав для этого плана.')

    plan.view_it()

    # выводим все упражнения которые использовались в этом плане
    exercises_ids = PlanPM.objects.filter(plan=plan).values_list('exercise_id')

    if request.user.is_authenticated():
        qs = Exercise.objects \
            .filter(Q(is_basic=True) | Q(pk__in=exercises_ids) | Q(is_basic=False, author=request.user))
    else:
        qs = Exercise.objects \
            .filter(Q(is_basic=True) | Q(pk__in=exercises_ids))
    qs = qs.distinct()

    exercises = {}
    for q in qs.order_by('-name'):
        exercises[q.id] = {
            'id': q.id,
            'name': q.name,
            'is_dual': q.is_dual,
        }

    user = request.user
    access = {
        'delete':           plan.access_check(user, 'delete'),
        'edit':             plan.access_check(user, 'edit'),
        'train_edit':       plan.access_check(user, 'train_edit'),
        'train_delete':     plan.access_check(user, 'train_delete'),
        'train_add':        plan.access_check(user, 'train_add'),
        'pm_edit':          plan.access_check(user, 'pm_edit'),
        'execution_edit':   plan.access_check(user, 'execution_edit'),
        'add_video':        plan.access_check(user, 'add_video'),
    }

    return render(request, 'main/plan/show/index.html', {
        'plan': plan,
        'plan_data': json.dumps(get_plan_data(plan)),
        'access': json.dumps(access),
        'exercises': json.dumps(exercises),
    })


@csrf_exempt
@plan_access('edit')
def edit(request, plan):
    try:
        data = json.loads(request.body)
    except:
        return json_error(u'Неверные данные')

    try:
        sportsman = User.objects.get(username=data['sportsman'])
    except:
        return json_error(u'Пользователь %s не найден.' % (data['sportsman']))

    try:
        author = User.objects.get(username=data['author'])
    except:
        return json_error(u'Пользователь %s не найден.' % (data['author']))

    try:
        start_date = datetime.datetime.strptime(data['start_date'], "%d.%m.%Y").date()
    except:
        return json_error(u'Неверный формат даты начала плана')

    name = data['name']
    is_public = data['is_public']

    try:
        plan = PlanService.edit_plan(request.user, plan, name, start_date, is_public, author, sportsman)
    except Exception as e:
        return json_error(unicode(e), plan=get_plan_data(plan))

    return json_response(
        status='success',
        plan=get_plan_data(plan)
    )


@plan_access(None)
def load(request, plan, ts):
    d = prev_weekday(datetime.date.fromtimestamp(float(ts)), 0)
    
    if d < plan.start_date:
        a = plan.start_date
    elif d <= plan.last_date:
        a = d
    elif (d - plan.last_date).days < 7:
        a = d - timedelta(days=7)
    else:
        a = plan.first_date
    b = a + timedelta(days=27)

    start = None if a == plan.start_date else get_timestamp(a)
    finish = None if b == plan.end_date else get_timestamp(b)

    return HttpResponse(json.dumps({
        'weeks': PlanService.get_plan_weeks(plan, a, b, request.user),
        'start': start,
        'finish': finish,
    }))


@plan_access(None)
def load_down(request, plan, ts):
    d = datetime.date.fromtimestamp(float(ts)) + timedelta(1)
    a = prev_weekday(d, 0)
    b = a + timedelta(days=27)
    
    if a > plan.end_date or a < plan.start_date:
        weeks = []
        finish = None
    elif b >= plan.end_date:
        weeks = PlanService.get_plan_weeks(plan, a, plan.end_date, request.user)
        finish = None
    else:
        weeks = PlanService.get_plan_weeks(plan, a, b, request.user)
        finish = get_timestamp(b)

    return HttpResponse(json.dumps({
        'weeks': weeks,
        'finish': finish,
    }))


@plan_access(None)
def update(request, plan, week_num1, week_num2):
    try:
        week_num1 = int(week_num1)
        week_num2 = int(week_num2)
    except:
        return HttpResponse(json.dumps({
            'status': 'error',
            'message': u'Неверные данные',
        }))

    a = plan.start_date + (week_num1-1) * timedelta(days=7)
    b = plan.start_date + (week_num2-1) * timedelta(days=7) + timedelta(days=6)
    
    # todo date validate
    weeks = PlanService.get_plan_weeks(plan, a, b, request.user)
    
    return HttpResponse(json.dumps({
        'status': 'success',
        'weeks': weeks,
    }))


@plan_access(None)
def load_up(request, plan, ts):
    d = datetime.date.fromtimestamp(float(ts)) - timedelta(1)
    b = next_weekday(d, 6)
    a = b - timedelta(days=6)
    
    if b < plan.start_date or b > plan.end_date:
        weeks = []
        start = None
    elif a <= plan.start_date:
        weeks = PlanService.get_plan_weeks(plan, plan.start_date, b, request.user)
        start = None
    else:
        weeks = PlanService.get_plan_weeks(plan, a, b, request.user)
        start = get_timestamp(a)

    return HttpResponse(json.dumps({
        'weeks': weeks,
        'start': start,
    }))


@csrf_exempt
@plan_access(None)
def train_save(request, plan):
    try:
        data = json.loads(request.body)
    except:
        return HttpResponse(json.dumps({
            'status': 'error',
            'message': u'Неверные данные',
        }))

    train_id = data.get('id')
    name = data.get('name')
    comment = data.get('comment')
    resume = data.get('resume')
    executions = data.get('executions')
    pms = data.get('pms')

    try:
        try:
            d = datetime.datetime.strptime(data['date'], "%d.%m.%Y").date()
        except:
            raise Exception(u'Ошибка при указании даты')

        if train_id:
            try:
                train = Training.objects.get(pk=train_id)
                old_date = train.date
            except:
                raise Exception(u'Тренировка не найдена')
        else:
            train = None

        train = TrainService.save_train(request.user, plan, train, d, executions, pms, name, comment, resume)

    except Exception as e:
        return HttpResponse(json.dumps({
            'status': 'error',
            'message': unicode(e),
        }))
    
    updates = []

    d = prev_weekday(train.date, 0)
    num = get_week_number(plan.start_date, d)
    stat = PlanService.get_week_stat(plan, num)
    updates.append({
        'type': 'week',
        'data': {
            'pms': {pm.exercise_id: pm.weight for pm in PlanPM.objects.filter(date=d, plan=plan)},
            'num': num,
            'tonag': stat['tonag'],
            'kps': stat['kps'],
            'percent': stat['percent'],
            'avg_weight': stat['avg_weight'],
        }
    })
    updates.append({
        'type': 'day',
        'data': {
            'num': num,
            'trains': [TrainService.get_train_data(t, request.user)
                       for t in Training.objects.filter(plan=plan, date=train.date)],
            'ts': get_timestamp(train.date),
        }    
    })

    if train_id and old_date != train.date:
        d = prev_weekday(old_date, 0)
        num = get_week_number(plan.start_date, d)
        stat = PlanService.get_week_stat(plan, num)
        updates.append({
            'type': 'week',
            'data': {
                'pms': {pm.exercise_id: pm.weight for pm in PlanPM.objects.filter(date=d, plan=plan)},
                'num': num,
                'tonag': stat['tonag'],
                'kps': stat['kps'],
                'percent': stat['percent'],
                'avg_weight': stat['avg_weight'],
            }
        })
        updates.append({
            'type': 'day',
            'data': {
                'num': num,
                'trains': [TrainService.get_train_data(t, request.user)
                           for t in Training.objects.filter(plan=plan, date=old_date)],
                'ts': get_timestamp(old_date),
            }    
        })

    # список ПМ
    qs = PlanPM.objects.filter(date=prev_weekday(train.date, 0), plan=plan)
    pms = {}
    for q in qs:
        pms[q.exercise_id] = q.weight

    return HttpResponse(json.dumps({
        'status': 'success',
        'train': TrainService.get_train_data(train, request.user),
        'pms': pms,
        'updates': updates,
    }))


@plan_access(None)
def plots(request, plan):
    data = PlanService.get_plan_stat(plan)

    return HttpResponse(json.dumps({
        'plots': data['plots'],
        'labels': range(data['first'], data['last'] + 1),
    }))


@plan_access(None)
def pm_list(request, plan, ts):
    try:
        d = datetime.date.fromtimestamp(float(ts))
    except:
        return HttpResponse(json.dumps({
            'status': 'error',
            'message': 'Ошибка при указании даты',    
        }))

    qs = PlanPM.objects \
        .filter(date=d, plan=plan) \
        .select_related('exercise')

    pms = {}
    for pm in qs:
        if pm.exercise_id in pms:
            continue

        prev = None
        if pm.prev:
            prev = {
                'date': get_timestamp(pm.prev.date),
                'weight': pm.prev.weight,
            }

        pms[pm.exercise_id] = {
            'exercise': {
                'id': pm.exercise.id,
                'name': pm.exercise.name
            },
            'weight': pm.weight,
            'correct': pm.correct,
            'prev': prev,
        }

    return HttpResponse(json.dumps({
        'list': pms,
    }))


@csrf_exempt
@plan_access('pm_edit')
def pm_save(request, plan, ts):
    try:
        d = datetime.date.fromtimestamp(float(ts))
    except:
        return HttpResponse(json.dumps({
            'status': 'error',
            'message': 'Ошибка при указании даты',    
        }))

    d = prev_weekday(d, 0)

    if d < plan.start_date or d > plan.end_date:
        raise Exception(u'Неверная дата для установки ПМ.')

    try:
        data = json.loads(request.body)
    except:
        return HttpResponse(json.dumps({
            'status': 'error',
            'message': u'Неверные данные',
        }))

    try:
        PlanService.save_plan_pm(plan, data, d)

    except Exception as e:
        return HttpResponse(json.dumps({
            'status': 'error',
            'error': unicode(e),    
        }))

    return HttpResponse(json.dumps({
        'status': 'success',    
    }))


@csrf_exempt
@auth_check(ajax=True)
@plan_access(None)
def exercise_create(request, plan):
    try:
        data = json.loads(request.body)
    except:
        return json_error(u'Неверные данные')

    if is_ratelimited(request, group='exercise_create', key='user', rate='50/d', increment=True):
        return json_error(u'Возможно добавить не более 50-ти новых упражнений в день.')

    # проверяем название упражнения
    if len(data['name']) < 3:
        return json_error(u'Название упражнения должно быть больше 3 символов')

    if not re.match(ur'[a-zа-я][a-zа-я\d \(\)-]+', data['name'], flags=re.I|re.U):
        return json_error(u'Неверное название упражнения. Допустимы: буквы, цифры, тире и скобки')

    if len(data['name']) > 50:
        return json_error(u'Название упражнения не должно быть больше 50 символов')

    # проверяем есть ли упражнения с таким же именем
    qs = Exercise.objects \
        .filter(name__iexact=data['name']) \
        .filter(Q(is_basic=True) | Q(is_basic=False, author=request.user))

    if qs.count() > 0:
        return json_error(u'Упражнение с такие названием уже существует')

    exercise = Exercise()
    exercise.is_dual = data['is_dual']
    exercise.name = data['name']
    exercise.author = request.user
    exercise.is_basic = False
    exercise.save()

    return json_response(
        status='success',
        exercise={
            'id': exercise.id,
            'name': exercise.name,
            'is_dual': exercise.is_dual,
        }
    )


@csrf_exempt
@auth_check(ajax=False)
@plan_access(None)
def pm_get(request, plan, d):
    try:
        d = datetime.datetime.strptime(d, "%d.%m.%Y").date()
    except:
        return HttpResponse(json.dumps({
            'status': 'error',
            'message': 'Ошибка при указании даты',
        }))

    query = """
        SELECT t.*
        FROM (
            SELECT id, exercise_id, MAX(date) as max_date
            FROM main_planpm
            WHERE plan_id=%d
                AND date<="%s"
            GROUP BY exercise_id
        ) r INNER JOIN main_planpm t
            ON t.exercise_id = r.exercise_id
            AND t.date=r.max_date
            AND plan_id=%d
    """ % (plan.id, d.strftime("%Y-%m-%d"), plan.id)

    qs = PlanPM.objects.raw(query)

    pms = {}
    for q in qs:
        pms[q.exercise_id] = q.weight

    return HttpResponse(json.dumps({
        'status': 'success',
        'pms': pms
    }))


@csrf_exempt
@plan_access('train_delete')
def train_delete(request, plan, train_id):
    try:
        train = Training.objects.get(pk=train_id)
    except:
        return HttpResponse(json.dumps({
            'status': 'error',
            'message': u'Тренировка не найдена',
        }))

    train_date = train.date
    TrainService.delete_train(plan, train)

    monday = prev_weekday(train_date, 0)
    num = get_week_number(plan.start_date, train_date)
    stat = PlanService.get_week_stat(plan, num)

    return HttpResponse(json.dumps({
        'status': 'success',
        'updates': [{
            'type': 'week',
            'data': {
                'pms': {pm.exercise_id: pm.weight for pm in PlanPM.objects.filter(plan=plan, date=monday)},
                'num': num,
                'tonag': stat['tonag'],
                'kps': stat['kps'],
                'percent': stat['percent'],
                'avg_weight': stat['avg_weight'],
            }
        }, {
            'type': 'day',
            'data': {
                'num': num,
                'trains': [TrainService.get_train_data(t, request.user)
                           for t in Training.objects.filter(plan=plan, date=train_date)],
                'ts': get_timestamp(train_date),
            }    
        }]    
    }))


@plan_access(None)
def get_exercises(request, plan):
    ids = PlanPM.objects \
        .filter(plan=plan) \
        .values_list('exercise_id')
    qs = Exercise.objects.filter(id__in=ids)

    res = {}
    for e in qs:
        eid = str(e.id)
        res[eid] = {
            'id': eid,
            'name': e.name,
            'group': '',
        }

    groups = plan.get_groups()

    for group_name, eids in groups.items():
        for eid in eids:
            res[eid]['group'] = group_name

    return HttpResponse(json.dumps({
        'status': 'success',
        'list': res.values(),
    }))


@csrf_exempt
@auth_check(ajax=True)
@plan_access(None)
def save_exercises(request, plan):
    try:
        data = json.loads(request.body)
    except:
        return HttpResponse(json.dumps({
            'status': 'error',
            'message': u'Неверные данные',
        }))

    groups = {}
    for item in data:
        group_name = item['group']
        group_name = group_name[:10].strip()

        if not group_name:
            continue

        if group_name not in groups:
            groups[group_name] = []
        groups[group_name].append(str(item['id']))

    PlanService.save_plan_groups(plan, groups)

    return HttpResponse(json.dumps({
        'status': 'success'
    }))


@auth_check(ajax=True)
@plan_access('train_edit')
def break_del(request, plan, week_num):
    try:
        week_num = int(week_num)
    except:
        return HttpResponse(json.dumps({
            'status': 'error',
            'message': u'Неверные данные',
        }))

    try:
        PlanService.break_del(request.user, plan, week_num)
    except Exception as e:
        return HttpResponse(json.dumps({
            'status': 'error',
            'message': unicode(e),
        }))

    return HttpResponse(json.dumps({
        'status': 'success'
    }))


@auth_check(ajax=True)
@plan_access('train_edit')
def break_add(request, plan, week_num):
    try:
        week_num = int(week_num)
    except:
        return HttpResponse(json.dumps({
            'status': 'error',
            'message': u'Неверные данные',
        }))

    try:
        PlanService.break_add(request.user, plan, week_num)
    except Exception as e:
        return HttpResponse(json.dumps({
            'status': 'error',
            'message': unicode(e),
        }))

    return HttpResponse(json.dumps({
        'status': 'success'
    }))
