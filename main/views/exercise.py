#coding=utf8

from django.shortcuts import render
from django.http import Http404
from django.db.models import Q

from main.models import *
from common.views import auth_check


def list(request):
    qs = Exercise.objects \
        .filter(Q(is_basic=True) | Q(parent__isnull=False)) \
        .order_by('group', 'name')

    return render(request, 'main/exercise/list.html', {
        'groups': ExerciseGroup.objects.all(),
        'group_id': request.GET.get('group_id', '-1'),
        'name': request.GET.get('name', ''),
        'list': qs,
        'menu': 'exercise_list',
    })


@auth_check(ajax=False)
def private_list(request):
    qs = Exercise.objects \
        .filter(is_basic=False, author=request.user) \
        .order_by('group', 'name')

    return render(request, 'main/exercise/private_list.html', {
        'list': qs,
        'menu': 'exercise_private_list',
    })


def show(request, id):
    try:
        e = Exercise.objects.get(pk=id)
    except:
        raise Http404(u'Упражнение в базе не найдено.')

    return render(request, 'main/exercise/show.html', {
        'obj': e,   
    })