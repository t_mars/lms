#coding=utf8
import datetime

from django.shortcuts import render
from django.http import HttpResponse, Http404
from django.core.urlresolvers import reverse
from django.conf import settings
from django.views.decorators.csrf import csrf_exempt
from django.utils import timezone

from common.views import auth_check, api_data, json_response, json_error

from main.models import *
from main.services.cycle import CycleService
from profile.services.user import UserService


def list(request):
    """
    Показывает список СРЦ
    """
    qs = Cycle.objects.all().order_by('order')
    
    if not settings.DEBUG:
        qs = qs.filter(is_active=True)

    return render(request, 'main/cycle/list.html', {
        'list': qs,
        'menu': 'cycle_list',
    })


def show(request, num):
    """
    Показывает конкретный СРЦ
    """
    try:
        cycle = Cycle.objects.get(num=num)
    except:
        raise Http404('Cycle not found.')
    
    if not cycle.is_active and not settings.DEBUG:
        raise Http404('Cycle not found.')

    cycle.view_it()

    return render(request, 'main/cycle/show.html', {
        'cycle_data': CycleService.get_cycle_data_new(cycle),
        'start_date': next_weekday(datetime.date.today(), 0),
        'cycle': cycle,
    })


@csrf_exempt
@auth_check(ajax=True)
@api_data
def create_plan(request, data, num):
    try:
        cycle = Cycle.objects.get(num=num)
    except:
        return json_error(u'СРЦ не найдено')

    if not cycle.is_active and not settings.DEBUG:
        return json_error(u'Цикл недоступен')

    # принимаем параметры
    try:
        week_days = data.get('week_days', [])

        if len(week_days) < len(cycle.get_week_days()):
            raise Exception(u'Неверно выбраны дни недели.')

        name = data.get('name')
        if not name:
            raise Exception(u'Введите название плана.')

        start_weights = {}
        weights = data.get('weights', [])
        for e in cycle.get_exercises():
            eid = str(e.id)
            if eid not in weights:
                raise Exception(u'Не установлен вес для упражнения "%s"' % e.name)
            try:
                w = float(weights[eid])
            except:
                raise Exception(u'Не корректный вес для упражнения "%s"' % e.name)

            if w <= 0 or w > 500:
                raise Exception(u'Указан неверный вес для упражнения "%s" (допустимо больше 0 до 500)' % e.name)

            start_weights[eid] = w
        try:
            correct_percent = float(data['correct_percent'])
        except:
            raise Exception(u'Не установлено значение для процента корректировки')
        if correct_percent < -5 or correct_percent > 5:
            raise Exception(u'Неверное значение для процента корректировки (допустимо от -5 до 5)')

        try:
            start_date = datetime.datetime.strptime(data.get('start_date'), '%d.%m.%Y').date()
        except:
            raise Exception(u'Неверная установлена дата начала.')

        if start_date.weekday() != 0:
            raise Exception(u'Начать можно только в понедельник.')

        today = timezone.now().date()
        if cycle.is_paid:
            d = prev_weekday(today, 0)
            if start_date < d:
                raise Exception(u'Дата начала не может быть раньше %s' % d.strftime('%d.%m.%y'))

        # проверка активных планов на указанный период
        plans = CycleService.check_plans(request.user, start_date)

        if not request.user.is_superuser and len(plans) > 2:
            links = []
            for p in plans:
                links.append(u'<a href="%s">%s</a>' % (reverse('plan', args=[p.id]), p.name))

            raise Exception(u"На выбранный период у вас активно %d планов по СРЦ:<br>%s<br>"
                            u"Вы не можете иметь более 2 активных планов по СРЦ одновременно. "
                            u"Удалите один из планов или поменяйте дату начала." % (len(links), '<br>'.join(links)))

    except Exception as e:
        return json_error(unicode(e))

    plan = CycleService.create_plan(cycle, name, start_date, week_days, start_weights, request.user, correct_percent)
    UserService.open_trains(request.user)

    return HttpResponse(json.dumps({
        'status': 'success',
        'redirect': reverse('plan', args=[plan.id]),
    }))