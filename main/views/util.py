#coding=utf8
from django.shortcuts import render
from django import forms

from main.models import *


def warmup(request):
    class Form(forms.Form):
        weight = forms.FloatField(label=u'Вес, кг', min_value=0, max_value=550, initial=120)
        reps = forms.IntegerField(label=u'Повторения, шт', min_value=0, max_value=100, initial=5)
        exercise = forms.ModelChoiceField(queryset=Exercise.objects.all(), label=u'Упражнение', empty_label=u'Любое', required=False)
    
    res = None
    if 'calc' in request.GET:
        form = Form(request.GET)
        if form.is_valid():
            weight = form.cleaned_data['weight']
            reps = form.cleaned_data['reps']
            exercise = form.cleaned_data['exercise']
            res = get_warmup_scheme(weight, reps, exercise)
    else:
        form = Form()
        res = get_warmup_scheme(120, 5, None)

    return render(request, 'main/util/warmup.html', {
        'res': res,    
        'form': form,
        'menu': 'util_warmup',
    })


def maximum(request):
    return render(request, 'main/util/maximum.html', {
        'menu': 'util_maximum',
    })


def norms(request):
    class Form(forms.Form):
        weight = forms.IntegerField(label=u'Вес спортсмена', min_value=0, max_value=200)
        gender = forms.ChoiceField(choices=[('', 'Все')] + GENDER_CHOICE.items(), label=u'Пол')
        federation = forms.ModelChoiceField(queryset=Federation.objects.all(),  empty_label=u'Все', label=u'Федерация')
        doping_control = forms.ChoiceField(choices=[('', 'Все')] + DC_CHOICE.items(), label=u'Допинг контроль')
        sport = forms.ChoiceField(choices=[('', 'Все')] + SPORT_CHOICE.items(), label=u'Вид спорта')
        equip = forms.ChoiceField(choices=[('', 'Все')] + EQUIP_CHOICE.items(), label=u'Экипировка')

    return render(request, 'main/util/norms.html', {
        'form': Form(),
        'norms': SportNorm.objects.all(),
        'menu': 'util_norms',
    })

analyzes = [
    {'name': u'Тестостерон общий', 'master_unit': u'нмоль/л', 'slave_units': [u'нг/мл', u'мкг/л'], 'rate': 3.467}, 
    {'name': u'Эстрадиол', 'master_unit': u'пмоль/л', 'slave_units': [u'пг/мл', u'нг/л'], 'rate': 3.671}, 
    {'name': u'Пролактин', 'master_unit': u'мЕд/л', 'slave_units': [u'нг/мл', u'мкг/л'], 'rate': 21.2}, 
    # {'name': u'ЛГ ', 'master_unit': u'мЕд/мл', 'slave_units': [u'Ед/л'], 'rate': 1.0}, 
    # {'name': u'АЛТ', 'master_unit': u'Ед/л'}, 
    # {'name': u'АСТ', 'master_unit': u'Ед/л'}, 
    {'name': u'Билирубин', 'master_unit': u'мкмоль/л', 'slave_units': [u'мг/дл'], 'rate': 17.094017}, 
    # {'name': u'Билирубин общий', 'master_unit': u'мкмоль/л', 'slave_units': [u'мг/дл'], 'rate': 17.094017}, 
    # {'name': u'Билирубин прямой', 'master_unit': u'мкмоль/л', 'slave_units': [u'мг/дл'], 'rate': 17.094017}, 
    {'name': u'Соматотропин', 'master_unit': u'мМЕ/л', 'slave_units': [u'нг/мл', u'мкг/л'], 'rate': 2.6}, 
]


def converter(request):
    return render(request, 'main/util/converter.html', {
        'objs': analyzes,
        'menu': 'util_converter',
    })
