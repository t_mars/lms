#coding=utf8

import locale
import datetime

from django.contrib.auth import logout
from django.shortcuts import render, redirect
from django.http.response import Http404, HttpResponse, HttpResponseRedirect
from django.core.urlresolvers import reverse
from django.core.validators import validate_email

from ratelimit.utils import is_ratelimited
from common.views import auth_check
from main.services.train import TrainService
from main.services.plan import PlanService
from main.services.statistic import StatisticService
from drugplan.views.edit import get_mark_form
from main.models import *

locale.setlocale(locale.LC_ALL, 'ru_RU.UTF-8')


def home(request):
    """
    Главная страница сайта
    """
    data = {
        'menu': 'home',
    }

    workspace = False
    if request.user.is_authenticated():
        plans = request.user.get_plans()
        courses = request.user.get_courses()

        if (len(plans) or len(courses)) and 'intro' not in request.GET:
            workspace = True

    if workspace:
        today = timezone.now().date()
        if 'date' in request.GET:
            d = datetime.datetime.strptime(request.GET.get('date'), '%d.%m.%y').date()
        else:
            d = today

        date_str = d.strftime(u'%d.%m.%y (%A)').lower()
        days = (d-today).days
        prefs = {
            -2: 'Позавчера',
            -1: 'Вчера',
            0: 'Сегодня',
            1: 'Завтра',
            2: 'Послезавтра',
        }
        if days in prefs:
            date_str = prefs[days] + ' ' + date_str

        qs = Training.objects \
            .filter(plan__sportsman=request.user, date=d, plan__is_deleted=False) \
            .select_related('plan')

        trains = []
        for t in qs:
            if t.plan.access_check(request.user, 'show'):
                train = TrainService.get_train_data(t, request.user)
                train['plan'] = PlanService.get_plan_data(t.plan)
                trains.append(train)

        data['date_prev'] = (d - timedelta(days=1)).strftime('%d.%m.%y')
        data['date_next'] = (d + timedelta(days=1)).strftime('%d.%m.%y')
        data['date_str'] = date_str
        data['trains'] = trains

        # приемы лекарств
        mark_forms = []
        for course in courses:
            form = get_mark_form(d, course, request.user)
            if form:
                mark_forms.append( (course, form) )
        data['mark_forms'] = mark_forms

    data['workspace'] = workspace
    data['stat'] = StatisticService.get_statistics()

    return render(request, 'main/workspace.html', data)


def auth(request):
    if request.user.is_authenticated():
        return redirect('home')
    return render(request, 'main/auth.html', {
        'hide_chat': True,    
    })


def auth_success(request):
    if request.user.is_authenticated():
        return HttpResponse(u'Авторизация завершена, вы можете закрыть окно и продолжить.')
    else:
        return HttpResponse(u'Ошибка авториазции, перейдите по <a href="%s?next=%s">ссылке</a> и попробуйте снова.' %
                            (reverse('auth'), reverse('auth_success')))


@auth_check(ajax=False)
def logout_view(request):
    logout(request)
    return redirect('home')


def about(request):
    return render(request, 'main/about.html', {
        'menu': 'about',    
    })


def stream(request):
    if not request.user.is_authenticated():
        return render(request, 'main/stream_login.html', {
            'hide_chat': True,
        })

    if request.user.is_banned:
        raise Http404('Вы забанены и не можете учавствовать в стриме.')

    return render(request, 'main/stream.html', {
        'hide_chat': True,    
    })


def news(request, mode):
    if mode not in ['video']:
        return Http404()

    return render(request, 'main/news/' + mode + '.html')


def android(request):
    return render(request, 'main/app/android.html')


def ios(request):
    message = None
    if request.method == 'POST':
        try:
            email = request.POST.get('email')
            try:
                validate_email(email)
            except:
                raise Exception(u'Не верно указана почта')

            if is_ratelimited(request, group="ios_sub3", key='user_or_ip', rate='5/d', increment=True):
                raise Exception(u'Превышено число попыток')

            with open('/var/www/ios_emails.txt', 'a') as file:
                file.write('%s\n' % email)
            raise Exception(u'Ваша почта сохранена. Мы вышлем вам ссылку на приложение в App Store как только оно будет выложено.')

        except Exception as e:
            message = unicode(e)

    return render(request, 'main/app/ios.html', {
        'message': message
    })