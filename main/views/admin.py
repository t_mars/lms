#coding=utf8

import collections

from django.shortcuts import render
from django.http import Http404
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger

from common.views import superuser_check
from main.models import *


@superuser_check
def stat(request):
    today = timezone.now().date()
    d = timezone.now() - timedelta(minutes=10)
    user = {
        'login': User.objects.filter(last_login__gte=today).count(),
        'reg': User.objects.filter(date_joined__gte=today).count(),
        'all': User.objects.all().count(),
        'online': RequestLog.objects.filter(created_at__gte=d).values_list('user').distinct().count(),
    }
    plan = {
        'deleted': Plan.objects.filter(is_deleted=True).count(),
        'all': Plan.objects.filter(is_deleted=False).count(),
        'cycle': Plan.objects.filter(is_deleted=False, source__isnull=False).count(),
        'manual': Plan.objects.filter(is_deleted=False, source__isnull=True).count(),
        'cycle_today': Plan.objects.filter(is_deleted=False, source__isnull=False, created_at__gte=today).count(),
        'manual_today': Plan.objects.filter(is_deleted=False, source__isnull=True, created_at__gte=today).count(),
    }

    from chat.models import ChatStat
    qs = ChatStat.objects.filter(created_at__gte=timezone.now()-timedelta(days=1))
    
    return render(request, 'main/admin/stat.html', {
        'user': user,
        'plan': plan,
        'stat': qs,
    })


@superuser_check
def stat2(request):
    end = timezone.now().date() + timedelta(days=1)
    begin = end - timedelta(days=20)

    c = connection.cursor()
    c.execute(
        "SELECT "
            "DATE(created_at),"
            "SUM(sum),"
            "COUNT(id)"
        "FROM payment_invoice "
        "WHERE created_at BETWEEN %s AND %s "
        "GROUP BY DATE(created_at) "
        "ORDER BY DATE(created_at) DESC",
        [begin, end])
    payments1 = c.fetchall()

    c = connection.cursor()
    c.execute(
        "SELECT "
            "DATE(modified_at),"
            "SUM(sum),"
            "COUNT(id)"
        "FROM payment_invoice "
        "WHERE modified_at BETWEEN %s AND %s AND status=1 "
        "GROUP BY DATE(modified_at) "
        "ORDER BY DATE(modified_at) DESC",
        [begin, end])
    payments2 = c.fetchall()

    c = connection.cursor()
    c.execute(
        "SELECT "
            "DATE(created_at),"
            "SUM(case when source_id IS NULL then 1 else 0 end),"
            "SUM(case when source_id IS NULL then 0 else 1 end)"
        "FROM main_plan "
        "WHERE created_at BETWEEN %s AND %s "
        "GROUP BY DATE(created_at) "
        "ORDER BY DATE(created_at) DESC",
        [begin, end]
    )
    cycles = c.fetchall()

    c = connection.cursor()
    c.execute(
        "SELECT "
            "SUM(case when status=1 then sum else 0 end),"
            "SUM(sum) "
        "FROM payment_invoice "
    )
    all_sum, total_sum = c.fetchall()[0]

    c = connection.cursor()
    c.execute(
        "SELECT COUNT(DISTINCT user_id) FROM main_requestlog WHERE path=%s",
        ['/payment/advice/']
    )
    advice_count = c.fetchall()[0][0]

    c = connection.cursor()
    c.execute(
        "SELECT "
        "YEAR(modified_at), MONTH(modified_at),"
        "SUM(case when payment_system='yk' then sum else 0 end),"
        "SUM(case when payment_system='rk' then sum else 0 end),"
        "SUM(sum) "
        "FROM payment_invoice "
        "WHERE status=1 "
        "GROUP BY MONTH(modified_at) , YEAR(modified_at) DESC "
        "ORDER BY modified_at"
    )
    months = c.fetchall()[::-1]

    stat = {}
    for d in range((end-begin).days):
        stat[begin + timedelta(d)] = {
            'total_sum': 0,
            'total_count': 0,
            'sum': 0,
            'count': 0,
            'plan_count_1': 0,
            'plan_count_2': 0,
        }

    for i, p in enumerate(payments1):
        stat[p[0]]['total_sum'] = p[1]
        stat[p[0]]['total_count'] = p[2]

    for i, p in enumerate(payments2):
        stat[p[0]]['sum'] = p[1]
        stat[p[0]]['count'] = p[2]

    for i, p in enumerate(cycles):
        stat[p[0]]['plan_count_1'] = p[1]
        stat[p[0]]['plan_count_2'] = p[2]

    stat = collections.OrderedDict(sorted(stat.items(), reverse=True))

    return render(request, 'main/admin/stat2.html', {
        'stat': stat,
        'all_sum': all_sum,
        'total_sum': total_sum,
        'advice_count': advice_count,
        'months': months,
    })


@superuser_check
def stat_users(request, mode):
    today = timezone.now().date()
    if mode == 'login':
        qs = User.objects.filter(last_login__gte=today).order_by('-last_login')
        title = 'Заходили сегодня'
    elif mode == 'reg':
        qs = User.objects.filter(date_joined__gte=today).order_by('-date_joined')
        title = 'Зарегистрировано сегодня'
    elif mode == 'online':
        d = timezone.now() - timedelta(minutes=10)
        qs = User.objects.filter(requestlog__created_at__gte=d).distinct()
        title = 'Сейчас на сайте'
    else:
        raise Http404

    paginator = Paginator(qs, 50)
    page = request.GET.get('page')
    try:
        objs = paginator.page(page)
    except PageNotAnInteger:
        objs = paginator.page(1)
    except EmptyPage:
        objs = paginator.page(paginator.num_pages)

    return render(request, 'main/admin/stat_users.html', {
        'list': objs,
        'title': title,
    })


@superuser_check
def stat_cycle(request):
    from django.db.models import Count
    qs = Cycle.objects.all().annotate(plan_count=Count('plan'))
    
    return render(request, 'main/admin/stat_cycle.html', {
        'list': qs,    
    })


@superuser_check
def stat_plan(request):
    fs = {}
    for k, v in request.GET.items():
        fs[k] = v

    qs = Plan.objects.filter(is_deleted=False).order_by('-view_count')
    qs = qs.filter(**fs)

    paginator = Paginator(qs, 50)
    page = request.GET.get('page')
    try:
        objs = paginator.page(page)
    except PageNotAnInteger:
        objs = paginator.page(1)
    except EmptyPage:
        objs = paginator.page(paginator.num_pages)

    return render(request, 'main/admin/stat_plan.html', {
        'list': objs,
    })


@superuser_check
def user(request, id):
    try:
        user = User.objects.get(pk=id)
    except:
        raise Http404

    qs = RequestLog.objects.filter(user=user)
    try:
        last_request_date = qs.order_by('-created_at')[0].created_at
    except Exception:
        last_request_date = None
    request_count = qs.count()

    plan_count = Plan.objects.filter(author=user, is_deleted=False).count()

    return render(request, 'main/admin/user.html', {
        'user': user,
        'sauth': user.social_auth.get(),
        'last_request_date': last_request_date,
        'request_count': request_count,
        'plan_count': plan_count,
    })


@superuser_check
def user_plans(request, id):
    try:
        user = User.objects.get(pk=id)
    except:
        raise Http404

    qs = Plan.objects.filter(author=user, is_deleted=False)
    paginator = Paginator(qs, 50)
    page = request.GET.get('page')
    try:
        objs = paginator.page(page)
    except PageNotAnInteger:
        objs = paginator.page(1)
    except EmptyPage:
        objs = paginator.page(paginator.num_pages)

    return render(request, 'main/admin/user_plans.html', {
        'user': user,
        'list': objs,
    })


@superuser_check
def user_requests(request, id):
    try:
        user = User.objects.get(pk=id)
    except:
        raise Http404

    qs = RequestLog.objects.filter(user=user).order_by('-created_at')
    paginator = Paginator(qs, 100)
    page = request.GET.get('page')
    try:
        objs = paginator.page(page)
    except PageNotAnInteger:
        objs = paginator.page(1)
    except EmptyPage:
        objs = paginator.page(paginator.num_pages)

    return render(request, 'main/admin/user_requests.html', {
        'user': user,
        'list': objs,
    })
