#coding=utf8
import datetime

from django.shortcuts import render
from django.views.decorators.csrf import csrf_protect
from django.http import HttpResponseRedirect, Http404
from django.core.urlresolvers import reverse
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django import forms

from main.models import *
from common.views import auth_check
from main.services.plan import PlanService


def plan_print(request, id):
    try:
        plan = Plan.objects.get(pk=id)
    except:
        raise Http404(u'План не найден.')
    
    if not plan.access_check(request.user, 'show'):
        raise Http404(u'План не найден')

    # парсим начало
    ts = request.GET.get('ts1')
    if ts:
        start = datetime.date.fromtimestamp(float(ts))
    else:
        start = plan.first_date

    # парсим конец
    ts = request.GET.get('ts2')
    if ts:
        end = datetime.date.fromtimestamp(float(ts))
    else:
        end = plan.last_date

    ts = request.GET.get('ts')
    if ts:
        start = end = datetime.date.fromtimestamp(float(ts))

    # выбираем данные
    days = PlanService.get_plan_days(plan, start, end, request.user)
    
    # определяем формат
    formats = ['standart', 'fraction', 'linear']
    f = request.GET.get('format')
    if f in formats:
        request.session['plan.print_format'] = f
    f = request.session.get('plan.print_format', 'linear')
    
    if 'warmup' in request.GET and request.GET.get('warmup') in ['0', '1']:
        request.session['plan.print_warmup'] = request.GET.get('warmup')
    warmup = request.session.get('plan.print_warmup', '1')

    if 'show_free' in request.GET and request.GET.get('show_free') in ['0', '1']:
        request.session['plan.show_free'] = request.GET.get('show_free')
    show_free = request.session.get('plan.show_free', '0')

    return render(request, 'main/plan/print/%s.html' % f, {
        'plan': plan,
        'days': days,
        'start': start,
        'end': end,
        'format': f,
        'warmup': warmup,
        'show_free': show_free,
        'hide_chat': True,
    })


@auth_check(ajax=False)
def list(request):
    created_qs = Plan.objects \
        .filter(author=request.user, is_deleted=False)
    mine_qs = Plan.objects \
        .filter(sportsman=request.user, is_deleted=False)
    public_qs = Plan.objects \
        .filter(is_public=True, is_deleted=False)
    
    if 'created' in request.GET:
        qs = created_qs
        mode = 'created'
    elif 'public' in request.GET:
        qs = public_qs
        mode = 'public'
    else:
        qs = mine_qs
        mode = 'mine'
    qs = qs.order_by('-created_at').select_related('sportsman', 'author')

    paginator = Paginator(qs, 20)
    page = request.GET.get('page')
    try:
        objs = paginator.page(page)
    except PageNotAnInteger:
        objs = paginator.page(1)
    except EmptyPage:
        objs = paginator.page(paginator.num_pages)

    for o in objs:
        o.can_delete = o.access_check(request.user, 'delete')
    
    return render(request, 'main/plan/list.html', {
        'list': objs,
        'mode': mode,
        'created_count': created_qs.count(),
        'mine_count': mine_qs.count(),
        'public_count': public_qs.count(),
        'menu': 'plan_list',
    })


@auth_check(ajax=False)
def create(request):
    class PlanForm(forms.Form):
        name = forms.CharField(label=u'Название', max_length=100)
        start_date = forms.DateField(label=u'Дата начала', 
            widget=forms.TextInput(attrs={
                'data-provide':'datepicker',
                'data-date-days-of-week-disabled':'2,3,4,5,6,0',
            }),
            input_formats=['%d.%m.%Y'],
            initial=next_weekday(timezone.now().date(), 0).strftime('%d.%m.%Y'))

    if request.method == 'POST':
        form = PlanForm(request.POST)
        if form.is_valid():
            d = form.cleaned_data['start_date']
            if d != prev_weekday(d, 0):
                form.add_error('start_date', u'Начать план можно только в понедельник')
            else:
                plan = Plan()
                plan.author = request.user
                plan.sportsman = request.user
                plan.start_date = d
                plan.name = form.cleaned_data['name']
                plan.save()

                return HttpResponseRedirect(reverse('plan', args=[plan.id]))
    else:
        form = PlanForm()

    return render(request, 'main/plan/create.html', {
        'form': form,    
    })


@csrf_protect
@auth_check(ajax=False)
def delete(request, id):
    try:
        plan = Plan.objects.get(pk=id)
    except:
        raise Http404(u'План не найден.')

    if plan.is_deleted:
        raise Http404(u'План удален.')

    if not plan.access_check(request.user, 'delete'):
        raise Http404(u'У вас нет прав для удаления этого плана id=%d.' % plan.id)

    plan.is_deleted = True
    plan.save()

    return HttpResponseRedirect(request.META.get('HTTP_REFERER',reverse('plan_list')))
