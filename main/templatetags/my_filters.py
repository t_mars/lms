#coding=utf8
import datetime

from django.template.defaulttags import register
from django.template import Node
from django.core.serializers import serialize
from django.core.cache import InvalidCacheBackendError, caches
from django.core.files.storage import default_storage
from django.db.models.query import QuerySet
from django.contrib.humanize.templatetags.humanize import naturalday, naturaltime

from main.models import *


@register.filter
def get_key(d, k):
    if not d:
        return ''
    return d.get(str(k))


@register.filter
def to_timestamp(d):
    return int(time.mktime(d.timetuple()))


@register.filter
def from_timestamp(ts):
    return datetime.datetime.fromtimestamp(ts)


@register.filter
def jsonify(object):
    if isinstance(object, QuerySet):
        return serialize('json', object)
    return json.dumps(object)


@register.filter
def divide(value, arg):
    try:
        return float(value) / float(arg)
    except:
        return 0


@register.filter
def prev_wd(value, arg): 
    return prev_weekday(value, arg)


@register.filter
def next_wd(value, arg): 
    return next_weekday(value, arg)


@register.filter 
def times(number):
    return range(number)


@register.tag
def today():
    return timezone.now().date() 


class MyCacheNode(Node):
    def __init__(self, nodelist, obj_name, obj_id_var, fragment_name, vars):
        self.nodelist = nodelist
        self.obj_name = obj_name
        self.obj_id_var = obj_id_var
        self.fragment_name = fragment_name
        self.vars = vars

    def render(self, context):
        try:
            fragment_cache = caches['template_fragments']
        except InvalidCacheBackendError:
            fragment_cache = caches['default']

        self.obj_id = self.obj_id_var.resolve(context)
        cache_key = '%s_%d_%s' % (self.obj_name, self.obj_id, self.fragment_name)
        for v in self.vars:
            cache_key += '_%s' % v.resolve(context)
        value = fragment_cache.get(cache_key)
        if value is None:
            value = self.nodelist.render(context)
            fragment_cache.set(cache_key, value, None)
        return value


@register.tag('mycache')
def do_cache(parser, token):
    nodelist = parser.parse(('endmycache',))
    parser.delete_first_token()
    tokens = token.split_contents()
    return MyCacheNode(
        nodelist,
        tokens[1],  # fragment_name can't be a variable.
        parser.compile_filter(tokens[2]),
        tokens[3],
        [parser.compile_filter(t) for t in tokens[4:]],
    )


@register.filter 
def my_naturaltime(d):
    if d.date() < datetime.datetime.today().date():
        return naturalday(d)
    return naturaltime(d)


@register.inclusion_tag('main/breadcrumbs.html', takes_context=True)
def breadcrumbs_block(context):
    return {'breadcrumbs': context.get('breadcrumbs')}

@register.filter
def image_exists(filename):
    """
    Проверяет существует ли файл
    """
    if not filename:
        return False
    return default_storage.exists(filename)