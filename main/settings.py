#coding=utf8
"""
Django settings for lms_plan project.

For more information on this file, see
https://docs.djangoproject.com/en/1.6/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.6/ref/settings/
"""

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
import os
import sys


BASE_DIR = os.path.dirname(os.path.dirname(__file__))
ROOT_DIR = os.path.dirname(BASE_DIR)

# для того чтобы модули в первую очередь загружались из папки lib
sys.path.insert(0, os.path.join(BASE_DIR, 'lib'))


from conf import *

# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.6/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = '0!qay&^2l$nek0j9ez)0q(cclvrxv--(17uosd48151g65sk)@'

# Application definition
INSTALLED_APPS = (
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.humanize',
    'django.contrib.sites',

    'debug_toolbar',
    'bootstrap3',

    'main',
    'drugplan',
    'chat',
    'profile',
    'api',
    'forum',
    'payment',
    'videohub',

    'social_auth',

    'tinymce',
    'django_activeurl',
    'awesome_avatar',

    'lib.django_summernote',
    'lib.subdomains',

    'corsheaders',
    'django_user_agents',
    'widget_tweaks',
    'sorl.thumbnail',
    'ratelimit',
    'mptt'
)
THUMBNAIL_DEBUG = True

TINYMCE_DEFAULT_CONFIG = {
    'plugins': "table,spellchecker,paste,searchreplace",
    'theme': "advanced",
    'cleanup_on_startup': True,
    'custom_undo_redo_levels': 10,
    'width': "800",
    'height': "800",
}
# INTERNAL_IPS = ['194.15.116.49']
MIDDLEWARE_CLASSES = (
    'corsheaders.middleware.CorsMiddleware',
    'lib.subdomains.middleware.SubdomainURLRoutingMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'profile.auth_backends.CustomUserMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'social_auth.middleware.SocialAuthExceptionMiddleware',
    'main.log_backends.RequestLogMiddleware',
)

CORS_ALLOW_CREDENTIALS = True
CORS_ORIGIN_WHITELIST = (
    'm.last-man.org',
    'ws.last-man.org',
    'api.last-man.org',
    'forum.last-man.org',

    'm.test.last-man.org',
    'ws.test.last-man.org',
    'api.test.last-man.org',
    'forum.test.last-man.org',

    'localhost:8100',
    'localhost:3000',
)

SUBDOMAIN_URLCONFS = {
    'ws': 'main.urls',
    'forum': 'forum.urls',
    'api': 'api.urls',
}

CSRF_COOKIE_DOMAIN = 'last-man.org'

AUTHENTICATION_BACKENDS = (
    'social_auth.backends.contrib.vk.VKOAuth2Backend',
    'social_auth.backends.facebook.FacebookBackend',
    'social_auth.backends.google.GoogleOAuth2Backend',
    'django.contrib.auth.backends.ModelBackend',
)
CUSTOM_USER_MODEL = 'profile.CustomUser'
# чтобы сессия не ограничивалась ключом авторизации
SOCIAL_AUTH_SESSION_EXPIRATION = False

ROOT_URLCONF = 'main.urls'

VK_APP_ID                    = '5148014'
VK_API_SECRET                = 'BrtBeytIQVt22b0kF656'

FACEBOOK_APP_ID              = '1488975514743012'
FACEBOOK_API_SECRET          = 'c2ad6d21c5bd9e5c92d81c825cf0403a'

GOOGLE_OAUTH2_CLIENT_ID      = '222428655300-mjlv2our1pul1qou5gg903tfqg4nuknm.apps.googleusercontent.com'
GOOGLE_OAUTH2_CLIENT_SECRET  = 'TAb-gs6Gp8CnGMdSnAmFb86m'

SESSION_SERIALIZER = 'django.contrib.sessions.serializers.PickleSerializer'

LOGIN_REDIRECT_URL = '/'
LOGIN_ERROR_URL = '/'

WSGI_APPLICATION = 'main.wsgi.application'
SESSION_COOKIE_DOMAIN = '.last-man.org'

# Database
# https://docs.djangoproject.com/en/1.8/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'lms_plan',
        'USER': 'root',
        'PASSWORD': 'fdRNHJFNds836',
        'HOST': '127.0.0.1',
        'OPTIONS': {
            'init_command': 'SET foreign_key_checks = 0;',
        },
    },
}

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
                'django.core.context_processors.request',
            ],
        },
    },
]
TEMPLATES_DIRS = [
    os.path.join(BASE_DIR, 'main', 'templates'), 
    os.path.join(BASE_DIR, 'drugplan', 'templates'),
    os.path.join(BASE_DIR, 'chat', 'templates'),
    os.path.join(BASE_DIR, 'profile', 'templates'),
    os.path.join(BASE_DIR, 'api', 'templates'),
    os.path.join(BASE_DIR, 'forum', 'templates'),
    os.path.join(BASE_DIR, 'payment', 'templates'),
    os.path.join(BASE_DIR, 'videohub', 'templates'),
]

# Internationalization
# https://docs.djangoproject.com/en/1.6/topics/i18n/

LANGUAGE_CODE = 'ru-ru'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = False

USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.6/howto/static-files/
STATIC_ROOT = os.path.join(ROOT_DIR, "httpdocs")
STATIC_URL = '/static/'
STATICFILES_DIRS = (
    os.path.join(BASE_DIR, "static"),
)

MEDIA_URL = '/static/media/'
MEDIA_ROOT = os.path.join(STATIC_ROOT, "media")
UPLOAD_DIR = os.path.join(STATIC_ROOT, "upload")

CACHES = {
    'default': {
        "BACKEND": "django_redis.cache.RedisCache",
        "LOCATION": "redis://127.0.0.1:6379/1",
        "OPTIONS": {
            "CLIENT_CLASS": "django_redis.client.DefaultClient",
        }
    },
    "temp": {
        "BACKEND": "django_redis.cache.RedisCache",
        "LOCATION": "redis://127.0.0.1:6379/1",
        "OPTIONS": {
            "CLIENT_CLASS": "django_redis.client.DefaultClient",
        }
    },
}

AWESOME_AVATAR = {
    'width': 200,
    'height': 200,
    'select_area_width': 400,
    'select_area_height': 300,
    'save_quality': 90,
    'save_format': 'png',
}