#coding=utf8
import json

from django.utils import timezone
from django.db import models, connection
from django.db.models import Max, Min
from django.contrib.auth.models import User

from main.utils.dates import *
from forum.models import Thread

from tinymce.models import HTMLField

EXECUTION_LEVEL_CHOICE = {
    'light':    u'Легкая',
    'middle':   u'Средняя',
    'hard':     u'Тяжелая',
    'other':     u'Прочее',
}
COLOR_CHOICE = {
    'default': 'default', 
    'success': 'success', 
    'info': 'info', 
    'warning': 'warning', 
    'danger': 'danger', 
}
GENDER_CHOICE = {
    'male':    u'муж',
    'female':   u'жен',
}
HW_RATION_CHOICE = {
    'dec': u'Сниженное',
    'norm': u'Нормальное',
    'inc': u'Повышенное',
}
SPORT_CHOICE = {
    'p': u'Пауэрлифтинг',
    'bp': u'Жим лежа',
    'dl': u'Становая тяга',
    'sq': u'Приседания',
}
EQUIP_CHOICE = {
    'raw': u'Без экипировки',
    '1': u'В однослойной экипировке',
    '2': u'В многослойной экипировке',
}
DC_CHOICE = {
    'yes': u'С допинг контролем',
    'no': u'Без допинг контроля',
}


def round_to(number, roundto):
  return roundto * round(number/roundto);


def my_round(number):
  if number >= 50.0:
    return round_to(number, 2.5)
  # if number <= 15.0:
    # return round_to(number, 0.1)
  return round_to(number, 1)


def _warmup(weight, reps, mode=0, with_bar=False):
    res = []
    if weight >= 50:
        steps = [20.0, 30.0]
        w = 0
        while True:
            w += steps[len(res) % len(steps)]
            if w >= weight:
                break
            if mode == 1 and w in [20.0, 50.0]:
                a = 2
            else:
                a = 1
            res.append({'weight': w, 'apps': a})

        last_step = weight - res[-1]['weight']
        if last_step < 20 and len(res) > 1:
            s = weight - res[-2]['weight']
            res[-1]['weight'] = round_to(res[-2]['weight'] + s / 2, 5)

    else:
        w = weight * 0.75
        if with_bar:
            if w > 20.0:
                w = 20.0
            else:
                w = round_to(w, 2.5)
        else:
            w = round_to(w, 1)
        if w != weight:
            res.append({'weight': w, 'apps': 1})

    if reps <= 7:
        for i,obj in enumerate(res):
            obj['reps'] = min(len(res) - i + reps -1, 6)
    else:
        for i,obj in enumerate(res):
            ind = len(res) - i -1
            if reps >= 12:
                r = 12
            elif reps >= 10:
                if ind == 0:
                    r = 10
                else:
                    r = 12
            elif reps >= 8:
                if ind == 0:
                    r = 8
                elif ind == 1:
                    r = 10
                else:
                    r = 12
            obj['reps'] = r
    return res


def get_warmup_scheme(weight, reps, exercise):
    if not exercise:
        mode = 0
        with_bar = True
    else:
        mode = 1 if int(exercise.id) in [27] else 0
        if exercise.is_dual or exercise.factor:
            with_bar = False
        else:
            with_bar = True
    return _warmup(weight, reps, mode, with_bar)


# Группа упражнений
class ExerciseGroup(models.Model):
    name = models.CharField(verbose_name=u'Название', max_length=100)

    def __str__(self):
        return self.name.encode('utf8')


# Упражнение
class ExerciseProposed(models.Model):
    name = models.CharField(verbose_name=u'Название', max_length=100)
    user = models.ForeignKey(User, verbose_name=u'Добавил')
    created_at = models.DateTimeField(auto_now_add=True)


# Упражнение
class Exercise(models.Model):
    group = models.ForeignKey(ExerciseGroup, verbose_name='Группа', null=True, default=True)

    name = models.CharField(verbose_name=u'Название', max_length=100)
    other_names = models.TextField(verbose_name=u'Другие названия', null=True, blank=True, default=True)

    description = models.TextField(verbose_name=u'Описание', null=True, blank=True, default=True)
    rate = models.FloatField(verbose_name=u'Коэффицент сложности', default=1)

    parent = models.ForeignKey('self', verbose_name=u'Упражнение', null=True, blank=True, default=None)
    parent_rate = models.FloatField(verbose_name=u'Коэффицент', null=True, blank=True, default=None)
    order = models.PositiveSmallIntegerField(verbose_name=u'Подрядок упражнения', default=0)
    
    is_dual = models.BooleanField(verbose_name=u'Двойное упражнение', default=False)
    factor = models.FloatField(verbose_name=u'С плитками', null=True, blank=True, default=None)

    content = HTMLField(verbose_name=u'Содержание', null=True, blank=True, default=None)
    is_checked = models.BooleanField(verbose_name=u'Проверено', default=False)

    author = models.ForeignKey(User, verbose_name=u'Создатель', default=None, blank=True, null=True)
    created_at = models.DateTimeField(verbose_name=u'Дата создания', auto_now_add=True, null=True, blank=True)
    is_basic = models.BooleanField(verbose_name=u'Базовое ли упражнение?', default=True)

    def add_name(self, name):
        name = name.replace('|', '')
        names = set([n for n in self.other_names.split('|') if len(n)])
        names.add(name)
        self.other_names = '|%s|' % ('|'.join(names))

    def __str__(self):
        return self.name.encode('utf8')


# СРЦ
class Cycle(models.Model):
    name = models.CharField(verbose_name=u'Название', max_length=100)
    sport = models.CharField(verbose_name=u'Вид спорта', max_length=100, default='')
    gender = models.CharField(verbose_name=u'Пол', choices=GENDER_CHOICE.items(), max_length=6)
    level = models.CharField(verbose_name=u'Уровень', max_length=1000, default='', blank=True)
    mode = models.CharField(verbose_name=u'Тип', max_length=100, default='', blank=True)
    abs_weight = models.CharField(verbose_name=u'Абс. вес', max_length=100, default='', blank=True)
    hw_ratio = models.CharField(verbose_name=u'Вес/рост соотн.', choices=HW_RATION_CHOICE.items(), max_length=6)
    extra = models.CharField(verbose_name=u'Дополнительно', max_length=1000, default='', blank=True)

    description = models.TextField(verbose_name=u'Описание', blank=True, null=True, default=None)
    defaults = models.TextField(verbose_name=u'Веса упражнения по умолчанию', null=True, blank=True, default=None)
    exercise_rates = models.TextField(verbose_name=u'Зависимости упражнений', null=True, blank=True, default=None)
    created_at = models.DateTimeField(verbose_name=u'Время создания', auto_now_add=True)
    table = models.TextField(verbose_name=u'Иабл', null=True, blank=True, default=None)
    num = models.CharField(verbose_name=u'Номер', max_length=10, unique=True)
    pm_ids = models.TextField(verbose_name=u'ID упражнения для вывода ПМ', default='', max_length=50, blank=True)

    thread = models.ForeignKey(Thread, null=True, default=True, blank=True)

    # активность для рабочей версии сайта
    is_active = models.BooleanField(verbose_name='Активность', default=False)

    # платные СРЦ
    is_paid = models.BooleanField(default=False)

    content = HTMLField(verbose_name=u'Содержание', null=True, blank=True, default=None)

    order = models.PositiveSmallIntegerField(verbose_name=u'Порядок', default=0)
    view_count = models.PositiveIntegerField(verbose_name=u'Количество просмотров', default=0)
    last_view = models.DateTimeField(null=True, blank=True, default=None)

    def view_it(self):
        self.view_count += 1
        self.last_view = timezone.now()
        self.save()

    def __str__(self):
        return self.name.encode('utf8')
    
    def save(self, *args, **kwargs):
        if isinstance(self.defaults, dict):
            self.defaults = json.dumps(self.defaults)
        super(Cycle, self).save(*args, **kwargs)

    def get_week_days(self):
        if not hasattr(self, '_week_days'):
            c = connection.cursor()
            c.execute("""
                SELECT DISTINCT day_num%%7 
                FROM main_cycleexecution
                WHERE cycle_id=%s""", [self.pk]
            )
            self._week_days = sorted([int(r[0]) for r in c.fetchall()])
        return self._week_days

    def get_week_days_variants(self):
        if not hasattr(self, '_week_days_variants'):
            ds = self.get_week_days()
            mn, mx = min(ds), max(ds)
            ds = [d-mn for d in ds]
            self._week_days_variants = [[d+s for d in ds] for s in range(6-mx+1)]
        return self._week_days_variants

    def get_defaults(self):
        try:
            return json.loads(self.defaults)
        except:
            return {}

    def get_exercise_rates(self):
        try:
            return json.loads(self.exercise_rates)
        except:
            return {}

    def get_executions(self):
        if not hasattr(self, 'executions'):
            self.executions = CycleExecution.objects \
                .filter(cycle=self) \
                .order_by('micro_num', 'day_num', 'order') \
                .select_related('exercise')
        return self.executions
    
    def get_main_exercises(self):
        if not hasattr(self, '_main_exercises'):
            qs = self.get_exercises()
            ids = [e.id for e in qs]
            self._main_exercises = []
            for e in qs:
                if e.parent == None or e.parent_id not in ids:
                    self._main_exercises.append(e)
        return self._main_exercises
    
    def get_other_exercises(self):
        if not hasattr(self, '_other_exercises'):
            qs = self.get_exercises()
            ids = [e.id for e in self.get_main_exercises()]
            self._other_exercises = []
            for e in qs:
                if e.id not in ids:
                    self._other_exercises.append(e)
        return self._other_exercises
    

    def get_exercises(self):
        if not hasattr(self, 'exercises'):
            self.exercises = Exercise.objects \
                .filter(cycleexecution__cycle=self) \
                .select_related('parent') \
                .order_by('parent','-parent__rate', '-rate') \
                .distinct()
        return self.exercises

    def get_plan(self):
        if not hasattr(self, 'plan'):
            self.plan = {}
            last_day_num = None
            for e in self.get_executions():
                
                if e.micro_num not in self.plan:
                    self.plan[e.micro_num] = {}
                
                day_num = e.day_num - (e.micro_num * 7)
                
                if day_num not in self.plan[e.micro_num]:
                    self.plan[e.micro_num][day_num] = {
                        'day_ind': len(self.plan[e.micro_num]),
                        'micro_num': e.micro_num,
                        'day_num': e.day_num,
                        'list': [],
                        'kps': 0,
                    }
                
                self.plan[e.micro_num][day_num]['list'].append(e)
                self.plan[e.micro_num][day_num]['kps'] += e.kps
            
        return self.plan

    def get_weights(self):
        # todo: найти применение и уничтожить
        # todo: найти применение и уничтожить e.factor
        defaults = self.get_defaults()

        weights = {}
        weights_factor = {}
        for e in self.get_exercises():
            eid = str(e.id)
            if eid not in defaults:
                continue
            if e.factor == None:
                if e.is_dual:
                    weights[eid] = defaults[eid] / 2.0
                else:
                    weights[eid] = defaults[eid]
        
            else:
                weights[eid] = defaults[eid]
                weights_factor[eid] = round_to(defaults[eid] / e.factor, 1.0)

        return weights, weights_factor


# Выполнение конкретного упражнения
class CycleExecution(models.Model):
    exercise = models.ForeignKey(Exercise, verbose_name=u'Упражнение')
    cycle = models.ForeignKey(Cycle, verbose_name=u'СРЦ')
    
    micro_num = models.PositiveSmallIntegerField(verbose_name=u'Номер цикла')
    day_num = models.PositiveSmallIntegerField(verbose_name=u'Номер дня')
    order = models.PositiveSmallIntegerField(verbose_name=u'Подрядок упражнения', default=0)
    level = models.CharField(verbose_name=u'Уровень нагрузки', choices=EXECUTION_LEVEL_CHOICE.items(), max_length=6)
    is_hidden = models.BooleanField(default=False)

    def get_scheme(self):
        if not hasattr(self, 'scheme'):
            self.scheme = CycleScheme.objects.filter(execution=self)
        return self.scheme

    @property
    def kps(self):
        kps = 0
        for s in self.get_scheme():
            kps += s.kps
        return kps


# Схема выполнения конкретного упражнения
class CycleScheme(models.Model):
    execution = models.ForeignKey(CycleExecution, verbose_name=u'Выполнение упражнения')
    percent = models.FloatField(verbose_name=u'Процент от максимума')
    reps = models.PositiveSmallIntegerField(verbose_name=u'Повторения')
    apps = models.PositiveSmallIntegerField(verbose_name=u'Подходы')

    @property
    def kps(self):
        return self.reps * self.apps


# Тренировочный план
class Plan(models.Model):
    name = models.CharField(verbose_name=u'Название', max_length=100)
    description = models.TextField(verbose_name=u'Описание', null=True, blank=True, default=True)
    start_date = models.DateField(verbose_name=u'Дата начала')
    created_at = models.DateTimeField(verbose_name=u'Время создания', auto_now_add=True)

    factors = models.TextField(verbose_name=u'Множители для упражнений', null=True, blank=True, default=True)

    author = models.ForeignKey(User, verbose_name=u'Создатель', default=None)
    source = models.ForeignKey(Cycle, verbose_name=u'Источник', null=True, blank=True, default=None, on_delete=models.SET_NULL)
    is_deleted = models.BooleanField(verbose_name=u'Удален', default=False)

    sportsman = models.ForeignKey(User, verbose_name=u'Создатель', default=None, null=True, blank=True, related_name='sportsman')
    is_public = models.BooleanField(verbose_name=u'Открытый доступ', default=False)
    
    view_count = models.PositiveIntegerField(verbose_name=u'Количество просмотров', default=0)
    last_view = models.DateTimeField(null=True, blank=True, default=None)
    groups = models.TextField(verbose_name=u'Группировка упражнений', null=True, blank=True, default=None)

    def view_it(self):
        self.view_count += 1
        self.last_view = timezone.now()
        self.save()

    def access_check(self, user, mode):
        if self.is_deleted:
            if user.is_superuser:
                return True
            return False

        if mode == 'show':
            if user.is_superuser:
                return True
            if self.is_public:
                return True
            if self.author_id == user.id:
                return True
            if self.sportsman_id == user.id:
                return True 
        
        if mode == 'delete':
            if self.author_id == user.id:
                return True
        
        if mode == 'edit':
            if self.author_id == user.id:
                return True
        
        if mode == 'train_add':
            if self.author_id == user.id:
                return True
        
        if mode == 'train_edit':
            if self.author_id == user.id or user.pk == 2:
                return True
            if self.sportsman_id == user.id:
                return True 
        
        if mode == 'train_delete':
            if self.author_id == user.id:
                return True
        
        if mode == 'pm_edit':
            if self.author_id == user.id:
                return True
            if self.sportsman_id == user.id:
                return True 
       
        if mode == 'execution_edit':
            if self.author_id == user.id or user.pk == 2:
                return True

        if mode == 'add_video':
            if self.sportsman_id == user.id:
                return True

        return False

    @property
    def first_date(self):
        qs = Training.objects.filter(plan=self)
        r = qs.aggregate(dmin=Min('date'))
        if r['dmin']:
            return prev_weekday(r['dmin'], 0)
        else:
            return prev_weekday(self.start_date, 0)

    @property
    def last_date(self):
        qs = Training.objects.filter(plan=self)
        r = qs.aggregate(dmax=Max('date'))
        if r['dmax']:
            return next_weekday(r['dmax'], 6)
        else:
            return next_weekday(self.start_date, 6)

    @property
    def end_date(self):
        if not hasattr(self, '_end_date'):
            self._end_date = next_weekday(self.start_date + timedelta(days=365), 6)
        return self._end_date

    def _parse_factors(self):
        if hasattr(self, '_factors'):
            return
        try:
            self._factors = json.loads(self.factors)
        except:
            self._factors = {}
    
    def get_factors(self):
        self._parse_factors()
        return self._factors

    def add_factor(self, e, factor):
        self._parse_factors()
        self._factors[str(e.id)] = float(factor)
        self.factors = json.dumps(self._factors)

    def get_factor(self, e, default=1):
        return self.get_factors().get(str(e.id), default)

    def get_scheme(self):
        if not hasattr(self, 'scheme'):
            self.scheme = CycleScheme.objects \
                .filter(execution=self)
        return self.scheme

    def get_groups(self):
        try:
            return json.loads(self.groups)
        except:
            return {}

    def __str__(self):
        return self.name.encode('utf8')


# Тренировочный план
class PlanWeekStat(models.Model):
    plan = models.ForeignKey(Plan, verbose_name=u'План')
    date = models.DateField(verbose_name=u'Дата начала недели')
    is_active = models.BooleanField(default=True)
    
    tonag = models.FloatField(verbose_name=u'Тоннаж', null=True, blank=True, default=None)
    kps = models.PositiveIntegerField(verbose_name=u'КПШ', null=True, blank=True, default=None)
    percent = models.FloatField(verbose_name=u'ИО', null=True, blank=True, default=None)
    avg_weight = models.FloatField(verbose_name=u'Средний вес', null=True, blank=True, default=None)

    @staticmethod
    def GET(plan, d):
        a = prev_weekday(d, 0)
        b = a + timedelta(days=6)

        try:
            stat = PlanWeekStat.objects.get(plan=plan, date=a)
            if stat.is_active:
                return stat
        except:
            pass

        qs = TrainingScheme.objects \
            .filter(execution__training__plan=plan, execution__training__date__range=[a, b])
        s1 = 0.0
        s2 = 0.0
        for s in qs:
            s1 += s.kps * s.percent
            s2 += s.kps
        if s2:
            percent = s1 / s2
        else:
            percent = 0

        kps = sum([t.kps for t in qs])
        tonag = sum([t.tonag for t in qs])
        if kps:
            avg_weight = tonag / kps
        else:
            avg_weight = 0

        try:
            stat = PlanWeekStat.objects.get(plan=plan,date=a)
        except:
            stat = PlanWeekStat()
            stat.plan = plan
            stat.date = a
        
        stat.tonag = tonag
        stat.kps = kps
        stat.percent = percent
        stat.avg_weight = avg_weight
        stat.save()

        return stat

    @staticmethod
    def RESET(plan, d):
        a = prev_weekday(d, 0)
        PlanWeekStat.objects \
            .filter(plan=plan, date=a) \
            .update(is_active=False)

    class Meta:
        unique_together = [
            ['plan', 'date'],
        ]


# Предельный максимум в плане по упражнению
class PlanPM(models.Model):
    exercise = models.ForeignKey(Exercise, verbose_name=u'Упражнение')
    plan = models.ForeignKey(Plan, verbose_name=u'План', default=None, on_delete=models.CASCADE)
    weight = models.FloatField(verbose_name=u'Предельный максимум')
    date = models.DateField(verbose_name=u'Дата')

    @staticmethod
    def SET(plan, date, exercise, weight):
        monday = prev_weekday(date, 0)
        qs = PlanPM.objects.filter(plan=plan, exercise=exercise)
        if qs.count() == 0:
            # создаем ПМ от начала плана до даты тренировки
            current = plan.start_date
        else:
            # создаем ПМ от последнего ПМ до даты тренировки
            current = qs.order_by('-date')[0].date + timedelta(days=7)
        
        pm = None
        while current <= monday:
            pm  = PlanPM()
            pm.plan = plan
            pm.exercise = exercise
            pm.date = current
            pm.weight = weight
            pm.save()
            current += timedelta(days=7)

        return pm

    @staticmethod
    def GET(plan, date, exercise):
        monday = prev_weekday(date, 0)
        try:
            # ищем ПМ на понедельник
            return PlanPM.objects.get(plan=plan, exercise=exercise, date=monday)
        except:
            pass

    # предыдущий ПМ
    @property
    def prev(self):
        if not hasattr(self, '_prev'):
            self._prev = PlanPM.GET(exercise=self.exercise, 
                date=self.date - timedelta(days=1), plan=self.plan)
        return self._prev
    
    # процент корректировки
    @property
    def correct(self):
        if not hasattr(self, '_correct'):
            if self.prev:
                ow = self.prev.weight
                nw = self.weight
                if ow == 0:
                    self._correct = 0
                else:
                    self._correct = round_to((nw - ow) / ow * 100.0, 0.1)
            else:
                self._correct = None 
        return self._correct

    def save(self, *args, **kwargs):
        self.date = prev_weekday(self.date, 0)
        return super(PlanPM, self).save(*args, **kwargs)

    @property
    def weight_factor(self):
        return int(round_to(self.weight / self.plan.get_factor(self.exercise), 1))

    @property
    def plan_factor(self):
        return self.plan.get_factor(self.exercise)
    
    class Meta:
        unique_together = [
            ['plan', 'exercise', 'date'],
        ]


# Тренировка
class Training(models.Model):
    plan = models.ForeignKey(Plan, verbose_name=u'План', on_delete=models.CASCADE)
    comment = models.TextField(verbose_name=u'Комментарий', null=True, blank=True, default=None)
    resume = models.TextField(verbose_name=u'Итог', null=True, blank=True, default=None)
    name = models.CharField(verbose_name=u'Название', max_length=100, null=True, blank=True, default=None)
    date = models.DateField(verbose_name=u'Дата')

    tonag_cache = models.FloatField(verbose_name=u'Тоннаж', null=True, blank=True, default=None)
    kps_cache = models.PositiveIntegerField(verbose_name=u'КПШ', null=True, blank=True, default=None)
    percent_cache = models.FloatField(verbose_name=u'ИО', null=True, blank=True, default=None)
    avg_weight_cache = models.FloatField(verbose_name=u'Средний вес', null=True, blank=True, default=None)

    # указывает видима ли тренировка (для планов по подписке)
    is_hidden = models.BooleanField(default=False)

    # время начала и конца тренировки
    start = models.DateTimeField(null=True, blank=True, default=None)
    end = models.DateTimeField(null=True, blank=True, default=None)

    # время перерывов
    time_breaks = models.TextField(null=True, blank=True, default=None)

    def reset(self):
        # tonag_cache
        self.tonag_cache = 0
        for e in self.get_executions():
            self.tonag_cache += e.tonag
    
        # kps_cache
        self.kps_cache = 0
        for e in self.get_executions():
            self.kps_cache += e.kps

        # percent_cache
        s1 = 0.0
        s2 = 0.0
        for e in self.get_executions():
            for s in e.get_scheme():
                s1 += s.kps * s.percent
                s2 += s.kps
        if s2:
            self.percent_cache = s1 / s2
        else:
            self.percent_cache = 0

        # avg_weight_cache
        if self.kps_cache:
            self.avg_weight_cache = self.tonag_cache / float(self.kps_cache)
        else:
            self.avg_weight_cache = 0

        self.save()

    def get_executions(self):
        if not hasattr(self, 'executions'):
            self.executions = TrainingExecution.objects \
                .filter(training=self) \
                .order_by('order', 'id')
        return self.executions

    @property
    def tonag(self):
        if self.tonag_cache is None:
            self.reset()        
        return self.tonag_cache

    @property
    def kps(self):
        if self.kps_cache is None:
            self.reset()        
        return self.kps_cache

    @property
    def percent(self):
        if self.percent_cache is None:
            self.reset()        
        return self.percent_cache

    @property
    def avg_weight(self):
        if self.avg_weight_cache is None:
            self.reset()
        return self.avg_weight_cache

    @property
    def is_done(self):
        return True if self.start and self.end else False


# Выполнение упражнения на тренировке
class TrainingExecution(models.Model):
    training = models.ForeignKey(Training, verbose_name=u'Тренировка')
    exercise = models.ForeignKey(Exercise, verbose_name=u'Упражнение', null=True, blank=True)
    pm = models.ForeignKey(PlanPM, verbose_name=u'Предельный максимум', null=True, blank=True)
    
    order = models.PositiveSmallIntegerField(verbose_name=u'Подрядок упражнения', default=0)
    
    level = models.CharField(verbose_name=u'Уровень нагрузки', choices=EXECUTION_LEVEL_CHOICE.items(), max_length=6, null=True, blank=True, default=None)
    comment = models.TextField(verbose_name=u'Комментарий', null=True, blank=True, default=None)
    
    other_name = models.CharField(verbose_name=u'Другое упражнение', max_length=100, null=True, blank=True, default=None)
    other_weight = models.CharField(verbose_name=u'Вес', max_length=20, null=True, blank=True, default=None)
    other_reps = models.CharField(verbose_name=u'Повторения', max_length=20, null=True, blank=True, default=None)
    other_apps = models.CharField(verbose_name=u'Подходы', max_length=20, null=True, blank=True, default=None)
    other_color = models.CharField(verbose_name=u'Статус', choices=COLOR_CHOICE.items(), max_length=10, default='default')

    source = models.ForeignKey(CycleExecution, verbose_name=u'Источник', null=True, blank=True, default=None, on_delete=models.SET_NULL)
    is_hidden = models.BooleanField(default=False)

    def get_scheme(self):
        if not hasattr(self, 'scheme'):
            self.scheme = TrainingScheme.objects \
                .filter(execution=self)
        return self.scheme

    def get_warmup_scheme(self):
        scheme = self.get_scheme()
        if scheme:
            return get_warmup_scheme(scheme[0].weight, scheme[0].reps, self.exercise)

        return []

    def get_scheme_with_warmup(self):
        return list(self.get_warmup_scheme()) + list(self.get_scheme())

    @property
    def tonag(self):
        tonag = 0
        for s in self.get_scheme():
            tonag += s.tonag
        return tonag

    @property
    def kps(self):
        kps = 0
        for s in self.get_scheme():
            kps += s.kps
        return kps

    @property
    def percent(self):
        if self.kps:
            return self.tonag / self.kps
        else:
            return 0

    @property
    def multiplier(self):
        if self.exercise.is_dual:
            return 2
        return 1
    
    @property
    def factor(self):
        if self.exercise.factor:
            return self.training.plan.get_factor(self.exercise)
        return 0


# Схема выполнения упражнения на тренировке
class TrainingScheme(models.Model):
    execution = models.ForeignKey(TrainingExecution, verbose_name=u'Выполнение упражнения')
    percent = models.FloatField(verbose_name=u'Процент от максимума')
    reps = models.PositiveSmallIntegerField(verbose_name=u'Повторения')
    apps = models.PositiveSmallIntegerField(verbose_name=u'Подходы')
    color = models.CharField(verbose_name=u'Статус', choices=COLOR_CHOICE.items(), max_length=10, default='default')

    source = models.ForeignKey(CycleScheme, verbose_name=u'Источник', null=True, blank=True, default=None, on_delete=models.SET_NULL)

    @property
    def weight(self):
        if not self.execution.pm:
            return 0
        return my_round(self.execution.pm.weight * self.percent / 100.0)

    @property
    def kps(self):
        return self.reps * self.apps

    @property
    def tonag(self):
        res = self.reps * self.apps * self.weight
        if self.execution.exercise.is_dual:
            return res * 2

        return res


class RequestLog(models.Model):
    user = models.ForeignKey(User, verbose_name=u'Пользователь', default=None, blank=True, null=True)
    host = models.CharField(verbose_name=u'Хост', max_length=50)
    path = models.CharField(verbose_name=u'Путь', max_length=200)
    method = models.CharField(verbose_name=u'method', max_length=10)
    useragent = models.CharField(verbose_name=u'UserAgent', max_length=500)
    ip = models.CharField(verbose_name=u'IP', max_length=15)
    created_at = models.DateTimeField(auto_now_add=True)


class Federation(models.Model):
    short_name = models.CharField(verbose_name=u'Короткое название', max_length=20)
    name = models.CharField(verbose_name=u'Название', max_length=50)

    def __str__(self):
        return self.short_name.encode('utf8')


class SportNorm(models.Model):
    federation = models.ForeignKey(Federation, verbose_name=u'Федерация', default=None)
    sport = models.CharField(verbose_name=u'Вид спорта', choices=SPORT_CHOICE.items(), max_length=20)
    equip = models.CharField(verbose_name=u'Экипа', choices=EQUIP_CHOICE.items(), max_length=10)
    gender = models.CharField(verbose_name=u'Пол', choices=GENDER_CHOICE.items(), max_length=6)
    doping_control = models.CharField(verbose_name=u'Допинг контроль', choices=DC_CHOICE.items(), max_length=6)
    division = models.CharField(verbose_name=u'Дивизион', max_length=20, default=None, null=True, blank=True)
    levels = models.TextField(verbose_name=u'Звания', max_length=100)
    table = models.TextField(verbose_name=u'Таблица', null=True, blank=True, default=True)

    def get_levels(self):
        if not hasattr(self, '_levels'):
            self._levels = [l.strip() for l in self.levels.strip().split('\t')]
        return self._levels

    def get_table(self):
        if not hasattr(self, '_tables'):
            self._tables = []
            min_weight = 0
            for r in self.table.strip().replace(',','.').split('\n'):
                vs = r.strip().split('\t')
                if '+' not in vs[0]:
                    max_weight = float(vs[0])
                    weight = '%.1f' % max_weight
                else:
                    max_weight = 200
                    weight = '%.1f+' % float(vs[0].replace('+', ''))
                

                row = {
                    'min_weight': min_weight, 
                    'max_weight': max_weight, 
                    'weight': weight, 
                    'values': [float(v) for v in vs[1:]]
                }
                min_weight = max_weight + 0.1
                self._tables.append(row)
        return self._tables

    def get_name(self):
        return '%s - %s (%s)' % (self.federation.short_name.encode('utf8'),
                                 SPORT_CHOICE[self.sport].encode('utf8'),
                                 EQUIP_CHOICE[self.equip].lower().encode('utf8'))