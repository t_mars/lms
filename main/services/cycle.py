#coding=utf8
from datetime import timedelta
import datetime

from main.models import Training, TrainingExecution, TrainingScheme, Plan, PlanPM, Exercise
from common.service import Service


class CycleService(Service):
    @classmethod
    def get_cycle_data(cls, cycle):
        """
        Возвращает данные цикла
        """

        def data_func():
            # план тренировок
            plan = {}
            for e in cycle.get_executions():
                if e.micro_num not in plan:
                    plan[e.micro_num] = {}

                day_num = e.day_num - (e.micro_num * 7)

                if day_num not in plan[e.micro_num]:
                    plan[e.micro_num][day_num] = {
                        'day_ind': len(plan[e.micro_num]),
                        'micro_num': e.micro_num,
                        'day_num': e.day_num,
                        'list': [],
                        'kps': 0,
                    }

                scheme = []
                if not e.is_hidden:
                    for s in e.get_scheme():
                        scheme.append({
                            'percent': s.percent,
                            'reps': s.reps,
                            'apps': s.apps,
                        })

                execution = {
                    'exercise': {
                        'id': e.exercise.id,
                        'name': e.exercise.name,
                    },
                    'is_hidden': e.is_hidden,
                    'level': e.level,
                    'scheme': scheme,
                }

                plan[e.micro_num][day_num]['list'].append(execution)
                plan[e.micro_num][day_num]['kps'] += e.kps

            # основные упражнения
            main_exercises = {}
            for e in cycle.get_main_exercises():
                main_exercises[e.id] = {
                    'parent': e.parent.name if e.parent else None,
                    'parent_rate': e.parent_rate,
                    'id': e.id,
                    'name': e.name,
                    'factor': e.factor,
                    'is_dual': e.is_dual,
                }

            # остальные упражнения
            exercises = {}
            for e in cycle.get_exercises():
                if e.id not in main_exercises:
                    exercises[e.id] = {
                        'parent_id': e.parent.id if e.parent else None,
                        'parent': e.parent.name if e.parent else None,
                        'parent_rate': e.parent_rate,
                        'id': e.id,
                        'name': e.name,
                        'factor': e.factor,
                        'is_dual': e.is_dual,
                    }

            for eid, d in cycle.get_exercise_rates().items():
                eid = int(eid)
                if eid not in main_exercises or d[0] not in main_exercises:
                    continue
                e = main_exercises[eid]
                e['parent_id'] = d[0]
                e['parent'] = main_exercises[d[0]]['name']
                e['parent_rate'] = d[1]
                exercises[eid] = e
                del main_exercises[eid]

            pms = []
            if cycle.pm_ids:
                qs = Exercise.objects.filter(pk__in=cycle.pm_ids.split(','))
                for e in qs:
                    pms.append({
                        'id': e.id,
                        'name': e.name,
                    })

            return {
                'pms': pms,
                'week_days': cycle.get_week_days(),
                'week_days_variants': cycle.get_week_days_variants(),
                'weights': cycle.get_defaults(),
                'plan': plan,
                'name': cycle.name,
                'description': cycle.description,
                'num': cycle.num,
                'sport': cycle.sport,
                'gender': cycle.gender,
                'level': cycle.level,
                'mode': cycle.mode,
                'hw_ratio': cycle.hw_ratio,
                'abs_weight': cycle.abs_weight,
                'extra': cycle.extra,
                'main_exercises': main_exercises,
                'exercises': exercises,
            }

        return cls.cache_get([cls.CK_CYCLE, cycle.id], data_func, 3600)

    @classmethod
    def get_cycle_data_new(cls, cycle):
        """
        Возвращает данные цикла
        """
        def data_func():

            groups = {
                1: {'name': 'Трицепс-группа', 'ids': [35, 21, 29]},
                2: {'name': 'Присед-группа', 'ids': [20, 28, 23]},
                3: {'name': 'Грудь-группа (база)', 'ids': [24, 36, 40]},
                4: {'name': 'Грудь-группа (изо)', 'ids': [51, 350, 331]},
                5: {'name': 'Бицепс-группа', 'ids': [38, 32, 56]},
                6: {'name': 'Вспомогательные к приседу', 'ids': [28, 23]},
                7: {'name': 'Вспомогательные к тяге', 'ids': [75, 31, 74, 53]},
                8: {'name': 'ФП присед/тяга', 'ids': [30, 54, 25, 79]},
            }
            groups_map = {
                # '0_0': 2,
                # '0_2': 6,
                # '0_3': 3,
                # '0_4': 1,
                # '2_2': 8,
                # '4_2': 2,
                # '4_3': 1,
                # '4_4': 8,
            }

            # план тренировок
            plan = {}
            exercise_ids = set()

            for e in cycle.get_executions():
                if e.micro_num not in plan:
                    plan[e.micro_num] = {}

                day_num = e.day_num - (e.micro_num * 7)

                if day_num not in plan[e.micro_num]:
                    plan[e.micro_num][day_num] = {
                        'day_ind': len(plan[e.micro_num]),
                        'micro_num': e.micro_num,
                        'day_num': e.day_num,
                        'list': [],
                        'kps': 0,
                    }

                scheme = []
                is_hidden = cycle.is_paid and e.is_hidden
                if not is_hidden:
                    for s in e.get_scheme():
                        scheme.append({
                            'percent': s.percent,
                            'reps': s.reps,
                            'apps': s.apps,
                        })

                exercise_key = '%d_%d' % (day_num, len(plan[e.micro_num][day_num]['list']))
                execution = {
                    'exercise_key': exercise_key,
                    'exercise_id': e.exercise_id,
                    'is_hidden': is_hidden,
                    'level': e.level,
                    'scheme': scheme,
                }
                exercise_ids.add(e.exercise_id)
                if exercise_key in groups_map:
                    execution['groups'] = groups[groups_map[exercise_key]]['ids']
                    for eid in execution['groups']:
                        exercise_ids.add(eid)

                plan[e.micro_num][day_num]['list'].append(execution)
                plan[e.micro_num][day_num]['kps'] += e.kps

            exercises = {}
            qs = Exercise.objects \
                .filter(id__in=exercise_ids)
            for e in qs:
                exercises[e.id] = {
                    'parent_id': e.parent_id if e.parent_id else None,
                    'parent_rate': e.parent_rate,
                    'id': e.id,
                    'name': e.name,
                    'factor': e.factor,
                    'is_dual': e.is_dual,
                }

            for eid, d in cycle.get_exercise_rates().items():
                eid = int(eid)
                if eid not in exercises or d[0] not in exercises:
                    continue
                exercises[eid]['parent_id'] = d[0]
                exercises[eid]['parent_rate'] = d[1]

            return {
                'week_days':            cycle.get_week_days(),
                'week_days_variants':   cycle.get_week_days_variants(),
                'weights':              cycle.get_defaults(),
                'plan':                 plan,
                'name':                 cycle.name,
                'num':                  cycle.num,
                'sport':                cycle.sport,
                'gender':               cycle.gender,
                'level':                cycle.level,
                'mode':                 cycle.mode,
                'hw_ratio':             cycle.hw_ratio,
                'abs_weight':           cycle.abs_weight,
                'description':          cycle.description,
                'extra':                cycle.extra,
                'content':              cycle.content,
                'exercises':            exercises,
            }

        return cls.cache_get([cls.CK_CYCLE_NEW, cycle.id], data_func, 3600)

    @classmethod
    def check_plans(cls, user, start_date):
        """
         проверка активных планов на указанный период
         """
        end_date = start_date + datetime.timedelta(days=12 * 7 - 1)
        qs = Plan.objects.filter(sportsman=user, is_deleted=False, source__isnull=False)
        plans = []
        for p in qs:
            if start_date >= p.first_date:
                if start_date <= p.last_date:
                    plans.append(p)
            elif end_date >= p.first_date:
                plans.append(p)

        return plans

    @classmethod
    def create_plan(cls, cycle, name, start_date, week_days, start_weights, user, correct_percent):
        correct_rate = 1.0 + correct_percent / 100.0

        # создаем план
        plan = Plan()
        plan.source = cycle
        plan.start_date = start_date
        plan.name = name
        plan.author = user
        plan.sportsman = user
        plan.save()

        # создаем план
        pms = {}
        ind = 1
        for micro_num, micro in cycle.get_plan().items():

            # создаем начальные предельные веса
            for e in cycle.get_exercises():
                eid = str(e.id)
                pm = PlanPM()
                pm.plan = plan
                pm.exercise = e
                pm.date = plan.start_date + timedelta(days=(micro.values()[0]['micro_num'] * 7))
                if eid in pms:
                    # если есть пм, то умножаем на корректировку
                    pm.weight = pms[eid].weight * correct_rate
                else:
                    # если нет пм, то берем начальные значения
                    pm.weight = start_weights[eid]
                pm.save()
                pms[eid] = pm

            for day_num, day in micro.items():
                days = day['micro_num'] * 7 + week_days[day['day_ind']]
                date = plan.start_date + timedelta(days=days)

                # тренировочный день
                t = Training()
                t.plan = plan
                t.date = date
                t.name = u'тренировка #%d' % ind
                t.save()
                ind += 1

                # упражнение
                for e in day['list']:
                    te = TrainingExecution()
                    te.source = e
                    te.training = t
                    te.exercise = e.exercise
                    te.is_hidden = cycle.is_paid and e.is_hidden
                    te.level = e.level
                    te.pm = pms[str(e.exercise.id)]
                    te.save()

                    # если есть скрытые раскладки - то тренировка скрытая
                    if te.is_hidden:
                        t.is_hidden = True

                    # схема выполнения
                    for s in e.get_scheme():
                        ts = TrainingScheme()
                        ts.source = s
                        ts.execution = te
                        ts.percent = s.percent
                        ts.reps = s.reps
                        ts.apps = s.apps
                        ts.save()

                # сохраняем тренировке еще раз, если скрытая
                if t.is_hidden:
                    t.save()

        return plan
