#coding=utf8
from datetime import timedelta
import datetime
import json

from django.db import connection

from common.service import Service
from main.utils.dates import get_timestamp, get_week_number, prev_weekday, next_weekday
from main.models import Training, PlanPM, TrainingScheme, round_to
from main.services.train import TrainService


class PlanService(Service):
    @classmethod
    def get_plan_data(cls, plan):
        return {
            'id': plan.id,
            'name': plan.name,
        }

    @classmethod
    def get_trains(cls, plan, a, b, user):
        """
        Возвращает тренировки по датам
        """
        qs = Training.objects \
            .filter(plan=plan, date__range=[a, b])
        trains = {}
        for train in qs:
            if train.date not in trains:
                trains[train.date] = []
            trains[train.date].append(TrainService.get_train_data(train, user))

        return trains

    @classmethod
    def get_plan_weeks(cls, plan, a, b, user):
        """
        Возвращает тренировки за определенный период
        """
        trains = cls.get_trains(plan, a, b, user)

        weeks = {}
        for i in range((b - a).days + 1):
            d = a + timedelta(days=i)
            if d.weekday() == 0:
                num = get_week_number(plan.start_date, d)
                stat = cls.get_week_stat(plan, num)

                pms = {}
                for pm in PlanPM.objects.filter(date=d, plan=plan):
                    pms[pm.exercise_id] = pm.weight

                weeks[num] = {
                    'train_count': 0,
                    'monday': get_timestamp(d),
                    'sunday': get_timestamp(next_weekday(d, 6)),
                    'num': num,
                    'pms': pms,
                    'tonag': stat['tonag'],
                    'kps': stat['kps'],
                    'avg_weight': stat['avg_weight'],
                    'percent': stat['percent'],
                    'days': {},
                }
            ts = get_timestamp(d)
            day = {
                'trains': [],
                'ts': ts,
            }
            if d in trains:
                weeks[num]['train_count'] += len(trains[d])
                for train in trains[d]:
                    day['trains'].append(train)
            weeks[num]['days'][ts] = day

        return weeks

    @classmethod
    def get_plan_days(cls, plan, a, b, user):
        trains = cls.get_trains(plan, a, b, user)

        days = []
        d = a
        while d <= b:
            day = {
                'date': get_timestamp(d),
                'date_str': d.strftime("%d.%m.%Y"),
                'trains': trains.get(d, []),
            }
            if d.weekday() == 0:
                day['weeknum'] = get_week_number(plan.start_date, d)
            days.append(day)
            d += timedelta(days=1)

        return days

    @classmethod
    def get_week_stat(cls, plan, week_num):
        """
        Возвращает статистику по плану на указанную неделю
        """

        def data_func():
            groups = plan.get_groups()

            a = plan.start_date + (week_num-1)*timedelta(days=7)
            b = a + timedelta(days=6)

            schemes = TrainingScheme.objects \
                .filter(execution__training__plan=plan, execution__training__date__range=[a, b]) \
                .select_related('execution')

            total_kps = 0
            total_tonag = 0
            total_percent_sum = 0
            total_avg_weight = 0
            total_percent = 0

            data = {}
            eids_group = {}
            for group, eids in groups.iteritems():
                data[group] = {
                    'kps': 0,
                    'tonag': 0,
                    'percent_sum': 0,
                    'percent': 0,
                    'avg_weight': 0,
                }

                for eid in eids:
                    eids_group[str(eid)] = group

            for scheme in schemes:
                kps = scheme.kps
                tonag = scheme.tonag
                percent = scheme.kps * scheme.percent

                total_kps += kps
                total_tonag += tonag
                total_percent_sum += percent

                group = eids_group.get(str(scheme.execution.exercise_id))
                if group:
                    data[group]['kps'] += kps
                    data[group]['tonag'] += tonag
                    data[group]['percent_sum'] += percent

            if total_kps:
                total_avg_weight = total_tonag / total_kps
                total_percent = total_percent_sum / total_kps

            for group, d in  data.iteritems():
                if d['kps']:
                    d['avg_weight'] = d['tonag'] / d['kps']
                    d['percent'] = d['percent_sum'] / d['kps']

            return {
                'tonag': total_tonag,
                'kps': total_kps,
                'percent': total_percent,
                'avg_weight': total_avg_weight,
                'groups': data,
            }

        return cls.cache_get([cls.CK_WEEK_STAT, plan.id, week_num], data_func, 7*24*60*60)

    @classmethod
    def get_plan_stat(cls, plan):
        """
        Возвращает статистику по плану
        """

        def data_func():
            groups = plan.get_groups()
            first = get_week_number(plan.start_date, plan.first_date)
            last = get_week_number(plan.start_date, plan.last_date)

            ks = [
                'tonag',
                'kps',
                'percent',
                'avg_weight',
            ]
            by_groups = {k: {group: [] for group in groups} for k in ks}
            total = {k: [] for k in ks}

            for week_num in xrange(first, last + 1):
                stat = cls.get_week_stat(plan, week_num)

                for k in ks:
                    total[k].append(stat[k])

                for group, data in stat['groups'].iteritems():
                    for k in ks:
                        by_groups[k][group].append(data[k])

            plots = {}
            for k in ks:
                plots[k] = [{'name': 'Общее', 'data': total[k]}] + \
                            [{'name': group, 'data': data} for group, data in by_groups[k].iteritems()]

            return {
                'plots': plots,
                'first': first,
                'last': last,
            }

        return cls.cache_get([cls.CK_PLAN_STAT, plan.id], data_func, 7 * 24 * 60 * 60)

    @classmethod
    def save_plan_groups(cls, plan, groups):
        """
        Сохранения группирования упражнения для плана
        """
        plan.groups = json.dumps(groups)
        plan.save()

        first = get_week_number(plan.start_date, plan.first_date)
        last = get_week_number(plan.start_date, plan.last_date)

        cls.clear_cache(
            [[cls.CK_PLAN_STAT, plan.id]] +
            [[cls.CK_WEEK_STAT, plan.id, week_num] for week_num in xrange(first, last + 1)],
        )

    @classmethod
    def save_plan_pm(cls, plan, list_pms, d):
        reset_dates = set()
        qs = PlanPM.objects.filter(exercise_id__in=list_pms.keys(), plan=plan, date=d)
        epms = {int(q.exercise_id): q for q in qs}

        for eid, row in list_pms.items():
            try:
                pm = epms[int(eid)]
            except:
                raise Exception(u'Ошибка №1')

            try:
                weight = float(row['weight'])
            except:
                raise Exception(u'Неверный вес для упражнения %s' % pm.exercise.name)

            # если значение не изменилось - то пропускаем
            if pm.weight == weight:
                continue

            # сохраняем корректировки
            qs = PlanPM.objects.filter(plan=plan, exercise_id=eid, date__gt=d).order_by('date')
            pms = []
            for p in qs:
                if len(pms) == 0:
                    ow = pm.weight
                else:
                    ow = pms[-1].weight
                p._correct = round_to((p.weight - ow) / ow * 100.0, 0.1) if ow > 0 else 0

                pms.append(p)
            pm.weight = weight
            pm.save()

            # пересчитываем следующие значения
            for i, p in enumerate(pms):
                if i == 0:
                    prev = pm
                else:
                    prev = pms[i - 1]
                p.weight = prev.weight + prev.weight * p._correct / 100.0
                p.save()
                reset_dates.add(pm.date)

            reset_dates.add(pm.date)

        # сбрасываем кеш
        clear_cache_keys = [[cls.CK_PLAN_STAT, plan.id]]

        for dt in reset_dates:
            clear_cache_keys.append([cls.CK_WEEK_STAT, plan.id, get_week_number(plan.start_date, dt)])

        qs = Training.objects.filter(plan=plan, date__gte=d)
        for t in qs:
            clear_cache_keys.append([cls.CK_TRAIN_VISIBLE, t.id])
            clear_cache_keys.append([cls.CK_TRAIN_HIDDEN, t.id])
            t.reset()

        cls.clear_cache(clear_cache_keys)

    @classmethod
    def break_add(cls, user, plan, week_num):
        """
        Смещение тренировок вниз на одну неделю начиная с указанной недели
        """
        if plan.source_id is not None and not user.is_subscribed():
            raise Exception(u'Чтобы сдвинуть тренировки в плане по СРЦ, необходимо иметь активную подписку. '
                            u'Пожалуйста, оплатите подписку и повторите снова.')

        monday = plan.start_date + (week_num-1) * timedelta(days=7)
        sunday = monday + timedelta(days=6)
        delta = timedelta(days=7)

        # проверка наличия тренировок
        qs = Training.objects.filter(plan=plan, date__range=[monday, sunday])
        if qs.count() == 0:
            raise Exception(u'На неделе №%d нет тренировок.' % (week_num))

        if prev_weekday(plan.last_date, 0) == prev_weekday(plan.end_date, 0):
            raise Exception(u'Невозможно сдвинуть тренировки вперед. Максимальная длина плана 1 год.')

        # сдвигаем тренировки вперед
        qs = Training.objects.filter(plan=plan, date__gte=monday)
        for t in qs:
            t.date += delta
            t.save()

        # формируем ПМ для недели перерыва
        new_pms = []
        if week_num == 1:
            # если первая неделя, то на неделе перерыва ПМ как в начале плана
            qs = PlanPM.objects.filter(plan=plan, date=monday)
        else:
            # если неделя не первая, то на неделе перерыва ПМ как в предыдущей неделе
            qs = PlanPM.objects.filter(plan=plan, date=monday-delta)
        for pm in qs:
            new_pm = PlanPM()
            new_pm.plan = plan
            new_pm.exercise_id = pm.exercise_id
            new_pm.weight = pm.weight
            new_pm.date = monday
            new_pms.append(new_pm)

        # сдвигаем ПМ вперед
        qs = PlanPM.objects.filter(plan=plan, date__gte=monday).order_by('-date')
        for pm in qs:
            pm.date += delta
            pm.save()

        # сохраняем ПМ на неделю перерыва
        for pm in new_pms:
            pm.save()

        # сбрасываем кеш
        start = get_week_number(plan.start_date, monday)
        last = get_week_number(plan.start_date, plan.last_date)

        cls.clear_cache(
            [[cls.CK_PLAN_STAT, plan.id]] +
            [[cls.CK_WEEK_STAT, plan.id, week_num] for week_num in range(start, last + 1)]
        )

    @classmethod
    def break_del(cls, user, plan, week_num):
        """
        Смещение тренировок вверх на одну неделю начиная с указанной недели
        """
        if plan.source_id is not None and not user.is_subscribed():
            raise Exception(u'Чтобы сдвинуть тренировки в плане по СРЦ, необходимо иметь активную подписку. '
                            u'Пожалуйста, оплатите подписку и повторите снова.')

        monday = plan.start_date + (week_num - 1) * timedelta(days=7)
        sunday = monday + timedelta(days=6)
        delta = timedelta(days=7)

        # проверка наличия тренировок
        qs = Training.objects.filter(plan=plan, date__range=[monday, sunday])
        if qs.count() > 0:
            raise Exception(u'На неделе №%d есть тренировки.' % (week_num))

        # сдвигаем тренировки назад
        qs = Training.objects.filter(plan=plan, date__gte=monday)
        for t in qs:
            t.date -= delta
            t.save()

        # удаляем ПМ недели перерыва
        PlanPM.objects.filter(plan=plan, date=monday).delete()

        # сдвигаем ПМ назад
        qs = PlanPM.objects.filter(plan=plan, date__gte=monday).order_by('date')
        for pm in qs:
            pm.date -= delta
            pm.save()

        # сбрасываем кеш
        start = get_week_number(plan.start_date, monday) - 1
        last = get_week_number(plan.start_date, plan.last_date)

        cls.clear_cache(
            [[cls.CK_PLAN_STAT, plan.id]] +
            [[cls.CK_WEEK_STAT, plan.id, week_num] for week_num in range(start, last + 1)]
        )

    @classmethod
    def edit_plan(cls, user, plan, name, start_date, is_public, author, sportsman):
        """
        Изменение плана
        """
        if not plan.access_check(user, 'edit'):
            raise Exception(u'У вас нет прав на редактирования плане')

        if plan.source_id is not None and not user.is_superuser:
            if author.id != plan.author_id:
                raise Exception(u'У плана созданного по срц нельзя менять редактора')
            if sportsman.id != plan.sportsman_id:
                raise Exception(u'У плана созданного по срц нельзя менять спортсмена')
        else:
            plan.author = author
            plan.sportsman = sportsman
            plan.is_public = is_public

        if start_date != plan.start_date:
            if plan.source_id is not None and not user.is_subscribed():
                raise Exception(u'Чтобы перенести дату начала плана по СРЦ, необходимо иметь активную подписку. '
                                u'Пожалуйста, оплатите подписку и повторите снова.')

            if start_date.weekday() != 0:
                raise Exception(u'Начало плана может быть только в понедельник.')

            days_diff = (start_date - datetime.date.today()).days
            if days_diff < -6:
                raise Exception(u'Невозможно перенести дату начала плана раньше понедельника текущей недели')
            elif days_diff > 365:
                raise Exception(u'Невозможно перенести план больше чем на год вперед')

            delta = timedelta(days=(start_date - plan.start_date).days)
            clear_cache_keys = [[cls.CK_PLAN_STAT, plan.id]]

            # сохранение тренировок
            c = connection.cursor()
            c.execute(
                """UPDATE main_training
                   SET date = DATE_ADD(date, INTERVAL %s DAY)
                   WHERE plan_id=%s""",
                [delta.days, plan.pk]
            )

            qs = Training.objects.filter(plan=plan)
            for train in qs:
                clear_cache_keys.extend([
                    [cls.CK_TRAIN_VISIBLE, train.id],
                    [cls.CK_TRAIN_HIDDEN, train.id],
                ])

            # сохранение пм-ов
            qs = PlanPM.objects.filter(plan=plan)
            if delta.days > 0:
                qs = qs.order_by('-date')
            else:
                qs = qs.order_by('date')
            for pm in qs:
                pm.date += delta
                pm.save()

            # сброс кеша
            cls.clear_cache(clear_cache_keys)

        plan.name = name or plan.name
        plan.start_date = start_date
        plan.save()

        return plan
