#coding=utf8
import json

from common.service import Service
from main.utils.dates import get_timestamp, prev_weekday, get_week_number
from main.models import Training, TrainingExecution, TrainingScheme, Exercise, PlanPM, COLOR_CHOICE
from videohub.models import Video


class TrainService(Service):
    @classmethod
    def get_train_data(cls, train, user):
        """
        Возвращает данные тренировки
        """

        if (user.is_authenticated() and train.plan.sportsman.id == user.id and not train.is_hidden) or user.pk == 2:
            train_is_hidden = False
            key = cls.CK_TRAIN_VISIBLE
        else:
            train_is_hidden = True
            key = cls.CK_TRAIN_HIDDEN

        def data_func():
            executions = []
            videos = {}

            executions_qs = train.get_executions()

            # извлекаем данные о видео
            videos_qs = Video.objects.get_active() \
                .filter(scheme_id__in=executions_qs.values_list('trainingscheme__id', flat=True))
            for v in videos_qs:
                if v.scheme_id not in videos:
                    videos[v.scheme_id] = {}
                videos[v.scheme_id][v.app] = {
                    'id': v.id,
                    'name': v.name,
                    'youtube_id': v.youtube_id,
                    'title': v.title,
                }

            for e in executions_qs:
                execution = {
                    'level': e.level,
                    'id': e.id,
                }
                if e.exercise:
                    if e.is_hidden and train_is_hidden:
                        execution['scheme'] = []
                        execution['warmup_scheme'] = []
                        execution['is_hidden'] = True
                    else:
                        # схема разминки
                        warmup_scheme = []
                        for s in e.get_warmup_scheme():
                            warmup_scheme.append(s)

                        # схема
                        scheme = []
                        for s in e.get_scheme():
                            scheme.append({
                                'percent': s.percent,
                                'reps': s.reps,
                                'apps': s.apps,
                                'color': s.color,
                                'weight': s.weight,
                                'id': s.id,
                                'videos': videos.get(s.id, {})
                            })

                        execution['scheme'] = scheme
                        execution['warmup_scheme'] = warmup_scheme
                        execution['is_hidden'] = False

                    execution['exercise'] = {
                        'id': e.exercise.id,
                        'name': e.exercise.name,
                        'is_dual': e.exercise.is_dual,
                    }
                elif e.level == 'other':
                    execution['other'] = {
                        'name': e.other_name,
                        'weight': e.other_weight,
                        'reps': e.other_reps,
                        'apps': e.other_apps,
                        'color': e.other_color,
                    }
                executions.append(execution)

            try:
                time_breaks = json.loads(train.time_breaks)
            except:
                time_breaks = {}

            return {
                'id': train.id,
                'is_hidden': train.is_hidden,
                'name': train.name,
                'ts': get_timestamp(train.date),
                'date': train.date.strftime("%d.%m.%Y"),
                'comment': train.comment,
                'resume': train.resume,
                'executions': executions,
                'tonag': train.tonag,
                'kps': train.kps,
                'avg_weight': train.avg_weight,
                'percent': train.percent,
                'start': get_timestamp(train.start) if train.start else None,
                'end': get_timestamp(train.end) if train.start else None,
                'is_done': train.is_done,
                'time_breaks': time_breaks,
            }

        return cls.cache_get([key, train.id], data_func, 7*24*60*60)

    @classmethod
    def save_train(cls, user, plan, train, d, executions, pms, name, comment, resume):
        """
        Сохранение тренировки
        """
        # для новых упражнений
        execution_ids = set()
        scheme_ids = set()

        if d < plan.start_date:
            raise Exception(u'Дата тренировки раньше начала плана')
        if d > plan.end_date:
            raise Exception(u'Дата тренировки позже начала плана')

        if len(executions) == 0:
            raise Exception(u'Добавьте хотя бы одно упражнение')

        if train:
            old_date = train.date

            if not plan.access_check(user, 'train_edit'):
                raise Exception(u'У вас нет прав на редактирование тренировок в этом плане')

            # todo
            # if train.is_hidden:
            #     raise Exception(u'Тренировка временно недоступна для редактирования.')

        else:
            if not plan.access_check(user, 'train_add'):
                raise Exception(u'У вас нет прав на добавление тренировок в этом плане')

            train = Training()
            train.plan = plan
            old_date = d

        train.date = d
        train.save()

        monday = prev_weekday(train.date, 0)
        if plan.access_check(user, 'execution_edit'):
            for eind, e in enumerate(executions):
                if e.get('id'):
                    try:
                        execution = TrainingExecution.objects.get(pk=e['id'])
                    except:
                        raise Exception(u'Выполнение не найдено')
                    if execution.training_id != train.id:
                        raise Exception(u'Ошибка доступа #1.')
                else:
                    execution = TrainingExecution()
                    execution.training = train

                execution.level = e['level']
                execution.order = eind

                if execution.level == 'other':
                    # упражнение вручную
                    execution.get_scheme().delete()
                    execution.exercise = None

                    execution.other_name = e['other']['name']
                    execution.other_weight = e['other']['weight']
                    execution.other_reps = e['other']['reps']
                    execution.other_apps = e['other']['apps']
                    execution.other_color = e['other']['color']
                    execution.save()
                    execution_ids.add(execution.id)
                    continue

                # упражнение из списка
                try:
                    exercise = Exercise.objects.get(pk=e['exercise']['id'])
                except:
                    raise Exception(u'Не выбрано упражнение #%d' % (eind + 1))

                try:
                    pm = PlanPM.objects.get(plan=plan, date=monday, exercise=exercise)
                except:
                    # cоздаем ПМ
                    try:
                        pm_weight = float(pms[str(e['exercise']['id'])])
                    except:
                        raise Exception(u'Не указан ПМ для упражнения 1 "%s"' % exercise.name)
                    pm = PlanPM.SET(plan, monday, exercise, pm_weight)

                if not pm:
                    raise Exception(u'Не указан ПМ для упражнения 2 "%s"' % exercise.name)

                execution.pm = pm
                execution.exercise = exercise
                execution.save()
                execution_ids.add(execution.id)

                if len(e['scheme']) == 0:
                    raise Exception(u'Не указана раскладка для упражнения %d. %s' % ((eind + 1), exercise.name))

                if len(e['scheme']) > 20:
                    raise Exception(
                        u'Раскладка для упражнения %d. %s больше чем 20 схем' % ((eind + 1), exercise.name))

                for sind, s in enumerate(e['scheme']):
                    if s.get('id'):
                        try:
                            scheme = TrainingScheme.objects.get(pk=s['id'])
                        except:
                            raise Exception(u'Схема не найдена')
                        if scheme.execution_id != execution.id:
                            raise Exception(u'Ошибка доступа #2')
                    else:
                        scheme = TrainingScheme()
                        scheme.execution = execution

                    try:
                        scheme.percent = float(s['percent'])
                        scheme.apps = int(s['apps'])
                        scheme.reps = int(s['reps'])
                    except:
                        raise Exception(
                            u'Неверная раскладка %d для упражнения %d. %s' % (sind, (eind + 1), exercise.name))

                    if s.get('color') in COLOR_CHOICE:
                        scheme.color = s['color']
                    scheme.order = sind
                    scheme.save()
                    scheme_ids.add(scheme.id)

        if plan.access_check(user, 'execution_edit'):
            # удалем подходы и схемы
            TrainingExecution.objects \
                .filter(training=train) \
                .exclude(pk__in=execution_ids) \
                .delete()
            TrainingScheme.objects \
                .filter(execution__training=train) \
                .exclude(pk__in=scheme_ids) \
                .delete()

        train.name = name
        train.comment = comment
        train.resume = resume
        train.date = d
        train.reset()

        # удаляем кешированные значения
        clear_cache_keys = [
            [cls.CK_TRAIN_VISIBLE, train.id],
            [cls.CK_TRAIN_HIDDEN, train.id],
            [cls.CK_PLAN_STAT, plan.id],
            [cls.CK_WEEK_STAT, plan.id, get_week_number(plan.start_date, train.date)]
        ]
        if old_date != train.date:
            clear_cache_keys.append(
                [cls.CK_WEEK_STAT, plan.id, get_week_number(plan.start_date, old_date)]
            )

        cls.clear_cache(clear_cache_keys)

        return train

    @classmethod
    def delete_train(cls, plan, train):
        train_id = train.id
        train_date = train.date
        train.delete()

        # удаляем кешированные значения
        cls.clear_cache([
            [cls.CK_TRAIN_VISIBLE, train_id],
            [cls.CK_TRAIN_HIDDEN, train_id],
            [cls.CK_WEEK_STAT, plan.id, get_week_number(plan.start_date, train_date)],
            [cls.CK_PLAN_STAT, plan.id],
        ])

    @classmethod
    def record_state(cls, train, user, resume, start, end, time_breaks):
        """
        Сохраняет состояние тренировки
        """
        if not train.plan.access_check(user, 'train_edit'):
            raise Exception(u'Ошибка доступа')

        qs = TrainingScheme.objects.filter(execution__training=train)
        time_breaks_new = {}
        for scheme in qs:
            sid = str(scheme.id)

            if sid not in time_breaks:
                continue

            time_breaks_new[sid] = []
            for t in time_breaks[sid]:
                try:
                    time_breaks_new[sid].append(int(t))
                except:
                    del time_breaks_new[sid]
                    break

        train.start = start
        train.end = end
        train.resume = resume
        train.time_breaks = json.dumps(time_breaks_new)
        train.save()

        cls.clear_cache([
            [cls.CK_TRAIN_VISIBLE, train.id],
            [cls.CK_TRAIN_HIDDEN, train.id]
        ])