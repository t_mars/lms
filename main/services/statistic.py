#coding=utf8

from common.service import Service
from main.models import User, Plan, Training, Exercise
from drugplan.models import Course
from chat.models import Message


class StatisticService(Service):

    @classmethod
    def get_statistics(cls):
        def data_func():
            return {
                'users': User.objects.all().count(),
                'plans': Plan.objects.all().count(),
                'trains': Training.objects.all().count(),
                'exercises': Exercise.objects.all().count(),
                'courses': Course.objects.all().count(),
                'messages': Message.objects.all().count(),
            }

        return cls.cache_get([cls.CK_ALL_STAT], data_func, 5 * 60)

