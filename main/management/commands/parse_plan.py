#coding=utf8
import pprint
import os

from django.core.management.base import BaseCommand

from main.models import *
from parser2 import Parser

class Command(BaseCommand):

    def handle(self, *args, **options):
        print u'Введите название файла'
        fn = raw_input()
    
        if not fn.endswith('.xlsm'):
            fn += '.xlsm'
        fn = os.path.join('data', 'xlsm', fn)

        p = Parser(fn)
        plan, exercises = p.parse()

        # pprint.pprint(plan)
        # for eid, name in exercises.items():
        #     print eid, name
        #     print ''
        # return

        print u'Введите название цикла'
        name = raw_input()

        print u'Введите номер цикла'
        num = raw_input()
        
        cycle = Cycle()
        cycle.name = name
        cycle.num = num

        # сохраняем значения по умолчанию и соответствие полей
        defaults = {}
        map_exercises = {}
        for eid, name in exercises.items():
            exercise = Exercise.objects.get(pk=eid)
            map_exercises[eid] = exercise

            print u'Введите вес для упражнения %s' % exercise.name
            while True:
                try:
                    defaults[exercise.id] = float(raw_input())
                    break
                except:
                    pass

        cycle.defaults = json.dumps(defaults)
        cycle.save()

        for micro_num, week in enumerate(plan):
            if micro_num == 0:
                is_hidden = False
            elif micro_num == len(plan)-1:
                is_hidden = False
            else:
                is_hidden = True
            for day_num, day in week:
                for order, exs in enumerate(day):
                    execution = CycleExecution()
                    execution.exercise = map_exercises[exs['exercise_num']]
                    execution.cycle = cycle
                    execution.micro_num = micro_num
                    execution.day_num = day_num
                    execution.order = order
                    execution.level = exs['level']
                    execution.is_hidden = is_hidden
                    execution.save()
                    for s in exs['plan']:
                        scheme = CycleScheme()
                        scheme.execution = execution
                        scheme.percent = s[0] * 100
                        scheme.reps = s[1]
                        scheme.apps = s[2]
                        scheme.save()