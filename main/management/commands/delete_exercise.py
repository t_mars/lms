#coding=utf8
from django.core.management.base import BaseCommand

from main.models import *
from common.service import Service


class Command(BaseCommand):

    def handle(self, *args, **options):
        # exercises = Exercise.objects.filter(author__isnull=False, is_basic=True, pk__gt=124)
        # print exercises.count()
        #
        # cc = 0
        # for exercise in exercises:
        #     qs = TrainingExecution.objects.filter(exercise_id=exercise.pk)
        #     user_ids1 = qs.values('training__plan__author_id').distinct().count()
        #
        #     qs = PlanPM.objects.filter(exercise_id=exercise.pk)
        #     user_ids2 = qs.values('plan__author_id').distinct().count()
        #
        #     print exercise.name, exercise.pk,
        #     if user_ids1 <= 1 and user_ids2 <= 1:
        #         print 'OK (%d,%d)' % (user_ids1, user_ids2)
        #         cc += 1
        #     else:
        #         print 'NO (%d,%d)' % (user_ids1, user_ids2)
        #         continue
        #
        #     f = raw_input()
        #     if f == '1':
        #         print 'save'
        #         exercise.is_basic = False
        #         exercise.save()
        #     print '==='
        # print 'cc =', cc
        # return

        deleted_id = 244
        replace_id = 31
        delete = True

        e1 = Exercise.objects.get(pk=deleted_id)
        print 'delete', e1.name

        e2 = Exercise.objects.get(pk=replace_id)
        print 'replace', e2.name

        service = Service()

        qs = CycleExecution.objects.filter(exercise_id=deleted_id)
        print u'СРЦ схем: ', qs.count(),
        if delete:
            qs.update(exercise_id=replace_id)
        print u'изменено'

        qs = TrainingExecution.objects.filter(exercise_id=deleted_id)
        train_ids = set(qs.values_list('training_id', flat=True))
        plan_ids = set(qs.values_list('training__plan_id', flat=True))
        user_ids = set(qs.values_list('training__plan__author_id', flat=True))
        print u'Тренировочных схем: ', qs.count(), plan_ids, user_ids
        if delete:
            qs.update(exercise_id=replace_id)
        print u'изменено'

        qs = PlanPM.objects.filter(exercise_id=deleted_id)
        plan_ids = set(qs.values_list('plan_id', flat=True))
        user_ids = set(qs.values_list('plan__author_id', flat=True))

        print u'Тренировочных ПМ: ', qs.count(), plan_ids, user_ids
        c1 = 0
        c2 = 0
        if delete:
            for pm in qs:
                pm.exercise_id = replace_id
                try:
                    pm.save()
                    c1 += 1
                except:
                    pm.delete()
                    c2 += 1
        print u'изменено =', c1, u'удалено =', c2

        if delete:
            trains = Training.objects.filter(pk__in=train_ids)
            print u'Сброс тренировок', trains.count()
            for train in trains:
                clear_cache_keys = [
                    [service.CK_TRAIN_VISIBLE, train.id],
                    [service.CK_TRAIN_HIDDEN, train.id],
                    [service.CK_PLAN_STAT, train.plan.id],
                    [service.CK_WEEK_STAT, train.plan.id, get_week_number(train.plan.start_date, train.date)]
                ]
                service.clear_cache(clear_cache_keys)
