#coding=utf8
from openpyxl import load_workbook
import pprint
import re
import os
import sys
from datetime import datetime, timedelta

def mydir(obj):
    for aname in dir(obj):
        attr = getattr(obj, aname)
        print aname,
        print 
        # try:
        #     print attr()
        # except:
        #     try:
        #         print attr
        #     except:
        #         print '--none--'

class Parser:
    def __init__(self, filename):
        self.filename = filename
        # читаем книгу
        self.wb = load_workbook(filename)

    def get_day_ind(self, cell, as_date=False):
        if cell.is_date:
            return cell.value if as_date else 0
        else:
            v = cell.value or ''
            if '+' in v:
                s,i = v.split('+')
                delta = timedelta(days=int(i)) if as_date else int(i)
                return self.get_day_ind(self.ws[s[1:]]) + delta
            else:
                return self.get_day_ind(self.ws[v[1:]])

    def get_val(self, cell):
        if isinstance(cell.value, str):
            ps = re.findall(ur'=IF\((\w\d+)="","",\w\d+\)', cell.value)
            if ps:
                return self.get_val(self.ws[ps[0]])
            
            ps = re.findall(ur'=(\w\d+)', cell.value)
            if ps:
                return self.get_val(self.ws[ps[0]])
            
        else:
            return cell.value

    def parse(self):
        # выбираем лист
        self.ws = self.wb.get_sheet_by_name(u'Цикл')
        self.exercises = {}

        dates = {}
        all_weeks = []
        current_week = []
        current_day = []
        day_index = 0
        day = None

        row = 0
        end = 500
        for row in xrange(1,end):
            try:
                cell = self.ws.cell(row=row, column=1)
                if cell.value == u'Дата' or row == end-1:
                    # сохраняем предыдущий день если нужно
                    if current_day:
                        current_week.append( (day, current_day) )
                        current_day = []

                    # сохраняем предыдущую неделю если нужно
                    if current_week:
                        all_weeks.append(current_week)
                        current_week = []
                    continue

                elif cell.number_format == 'ddd':
                    # сохраняем предыдущий день если нужно
                    if current_day:
                        current_week.append( (day, current_day) )
                        current_day = []
                    day = self.get_day_ind(cell)
                
                elif isinstance(cell.value, unicode) and cell.value.lower() in [u'пн',u'вт',u'ср',u'чт',u'пт',u'сб',u'вс']:
                    # сохраняем предыдущий день если нужно
                    if current_day:
                        current_week.append( (day, current_day) )
                        current_day = []
                    day = self.get_day_ind(self.ws.cell(row=row+1, column=1))
                    
                res = self.parse_exs(row)
                if res:
                    current_day.append(res)
            except Exception as e:
                exc_type, exc_obj, exc_tb = sys.exc_info()
                fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
                print 'row =', row
                print e.__class__.__name__, e, exc_type, fname, exc_tb.tb_lineno
                raise e

        for n in self.exercises:
            if self.exercises[n]['parent']:
                n2 = self.exercises[n]['parent']
                self.exercises[n]['parent'] = self.exercises[n2]['num']
        return all_weeks, self.exercises

    def parse_exs(self, row):
        level = {
            u'Легкая': 'light',
            u'Средняя': 'middle',
            u'Тяжелая': 'hard',
        }
        
        v = self.get_val(self.ws['B%d' % row])
        if v not in level:
            return None
        
        # парсим раскладку
        plan = []
        for c in [6, 10, 14, 18]:
            if self.ws.cell(row=row, column=c+3).value != 0:
                plan.append((
                    float(self.ws.cell(row=row, column=c+3).value), # процент
                    int(self.ws.cell(row=row, column=c+1).value), # повторения
                    int(self.ws.cell(row=row, column=c+2).value), # подходы
                ))
        
        if not plan:
            return None

        # парсим упражнение
        name = self.get_val(self.ws['C%d' % row]).strip()
        if name in self.exercises:
            enum = self.exercises[name]['num']
        else:
            enum = len(self.exercises)
            # парсим зависимость ПМ
            value = self.ws.cell(row=row, column=25).value
            if isinstance(value, str) and value.startswith('='):
                value = value[1:]
                if '*' in value:
                    # если указано =Y17*0.32
                    a, b = value.split('*')
                    if a[0].isalpha():
                        coord, rate = a, b
                    else:
                        rate, coord = a, b
                    rate = float(eval(rate))
                else:
                    # если указано без коэффицента =Y17
                    coord = value
                    rate = 1.0
                r = int(coord[1:])
                parent = self.get_val(self.ws.cell(row=r, column=3))
                default = None
            else:
                parent = None
                rate = None
                try:
                    k = int(self.ws.cell(row=row, column=5).value)
                except:
                    k = 1
                default = int(value) * k
            
            # парсим множитель
            factor = None
            is_dual = False
            value = self.get_val(self.ws['E%d' % row])
            if value:
                if value == 2:
                    is_dual = True
                else:
                    factor = value

            self.exercises[name] = {
                'num': enum,
                'parent': parent,
                'parent_rate': rate,
                'is_dual': is_dual,
                'factor': factor,
                'rate': self.get_val(self.ws['D%d' % row]),
                'default': default,
            }
        res = {
            'level': level[v],
            'exercise_num': enum,
            'plan': plan,
        }

        return res