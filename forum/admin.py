from django.contrib import admin
from forum.models import *

admin.site.register(Message)
admin.site.register(Topic)
admin.site.register(Thread)
admin.site.register(Tag)