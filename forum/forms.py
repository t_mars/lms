#coding=utf8
from django import forms

from lxml.html.clean import Cleaner

from lib.django_summernote.widgets import SummernoteWidget
from forum.models import Message

__all__ = ['ThreadForm', 'MessageForm']

""" Виджет для содержания сообщения """
content_widget = SummernoteWidget(
    config={
        'iframe': False,
        'airMode': False,
        'cleanPaste': True,
        'letterCount': False,
        'styleWithTags': False,
        'direction': 'ltr',
        'width': '100%',
        'height': '357',
        'lang': 'ru',
        'toolbar': [
            ['style', ['bold', 'italic', 'underline', 'clear']],
            ['insert', ['link', 'picture', 'video']],
        ],
        'attachment_require_authentication': True,
    }
)


def message_content_clean(content):
    """
    Удаляет все лишние теги из содержания сообщения
    """
    cleaner = Cleaner()
    cleaner.remove_unknown_tags = False
    cleaner.javascript = True  # This is True because we want to activate the javascript filter
    cleaner.style = True  # This is True because we want to activate the styles & stylesheet filter
    cleaner.embedded = False  # iframe save
    cleaner.allow_tags = ['b', 'i', 'u', 'img', 'a', 'iframe', 'p']
    cleaner.safe_attrs_only = True
    cleaner.safe_attrs = set(['src', 'height', 'width', 'href', 'target'])
    return cleaner.clean_html(content)


class ThreadForm(forms.Form):
    title = forms.CharField(min_length=5, max_length=150)
    content = forms.CharField(widget=content_widget, required=False)

    def clean_content(self):
        return message_content_clean(self.cleaned_data['content'])

    def clean(self):
        return super(ThreadForm, self).clean()


class MessageForm(forms.Form):
    content = forms.CharField(widget=content_widget)
    reply_to = forms.IntegerField(required=False, widget=forms.HiddenInput())

    def clean_content(self):
        return message_content_clean(self.cleaned_data['content'])

    def clean(self):
        cleaned_data = super(MessageForm, self).clean()

        if cleaned_data.get('reply_to'):
            try:
                message = Message.objects.get(pk=cleaned_data['reply_to'])
                cleaned_data['reply_to'] = message
                cleaned_data['root'] = message.root or message
            except:
                self.add_error('reply_to', u'ошибка #1')

        return cleaned_data