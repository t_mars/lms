#coding=utf8
from django.core.urlresolvers import reverse
from django.core.paginator import Paginator, EmptyPage
from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect, Http404

from ratelimit.utils import is_ratelimited

from forum.models import *
from forum.forms import *
from common.views import auth_check
from profile.services.notification import NotificationService
from main.services.statistic import StatisticService


def home(request):
    topics = Topic.objects.filter(level=0)

    qs = Thread.objects \
        .filter(top_message__show_on_main=True) \
        .order_by('-created_at')

    paginator = Paginator(qs, 10)
    page = request.GET.get('page')
    try:
        threads = paginator.page(page)
    except:
        threads = paginator.page(1)

    return render(request, 'forum/home.html', {
        'breadcrumbs': [('Форум', '')],
        'title': 'Форум',
        'topics': topics,
        'threads': threads,
        'stat': StatisticService.get_statistics()
    })


def topic(request, id, slug):
    try:
        topic = Topic.objects.get(id=id)
    except:
        raise Http404
    
    if slug != topic.slug:
        url = reverse('forum_topic', args=[id, topic.slug])
        return HttpResponseRedirect(url)

    if not is_ratelimited(request, group="topic_%d" % topic.id, key='user_or_ip', rate='1/m', increment=True):
        topic.view_count += 1
        topic.last_view = timezone.now()
    topic.save()

    breadcrumbs = [('Форум', reverse('forum_home'))]
    for t in topic.get_ancestors():
        breadcrumbs.append( 
            (t.title, reverse('forum_topic', args=[t.id,t.slug])) 
        )
    breadcrumbs.append( (topic.title, '') )

    if topic.is_leaf_node():
        threads = Thread.objects\
            .filter(topic=topic)\
            .order_by('-created_at')
        
        paginator = Paginator(threads, 20)
        page = request.GET.get('page', 1)
        try:
            threads = paginator.page(page)
        except EmptyPage:
            threads = paginator.page(paginator.num_pages)

        return render(request, 'forum/topic.html', {
            'breadcrumbs': breadcrumbs,
            'topic': topic,
            'threads': threads,
        })

    return render(request, 'forum/subtopics.html', {
        'topic': topic,
        'breadcrumbs': breadcrumbs,
        'title': topic.title,
        'topics': topic.children.all(),
    })


def thread(request, id, slug):
    try:
        thread = Thread.objects.get(id=id)
    except:
        raise Http404
    
    if slug != thread.slug:
        url = reverse('forum_thread', args=[id, thread.slug])
        return HttpResponseRedirect(url)

    if not is_ratelimited(request, group="thread_%d" % thread.id, key='user_or_ip', rate='1/m', increment=True):
        thread.view_count += 1
    thread.last_view = timezone.now()
    thread.save()

    form = None
    if request.user.is_authenticated() and not request.user.is_banned:
        if request.method == 'POST':
            form = MessageForm(request.POST)

            if is_ratelimited(request, group="all", key='user', rate='10/m', increment=True):
                form.add_error('content', u'Вы можете писать не более 10 сообщений в минуту')

            elif form.is_valid():
                
                message = Message()
                message.thread = thread
                message.user = request.user

                message.content = form.cleaned_data['content']
                message.root = form.cleaned_data.get('root')
                
                # не возможно ответить на свое сообщение
                reply_to = form.cleaned_data.get('reply_to')
                if reply_to and not reply_to.show_on_top:
                    message.reply_to = reply_to
                message.save()

                if message.reply_to:
                    NotificationService.notify('forum_answer', message, message.reply_to.user)
                NotificationService.notify('forum_thread_answer', message, thread.user)

                return HttpResponseRedirect('%s#%d' % (request.META.get('HTTP_REFERER'), message.id))
        else:
            form = MessageForm()

    messages = Message.objects \
        .filter(thread=thread, reply_to=None, show_on_top=False)

    paginator = Paginator(messages, 20)
    page = request.GET.get('page', paginator.num_pages)
    try:
        messages = paginator.page(page)
    except EmptyPage:
        messages = paginator.page(paginator.num_pages)

    breadcrumbs = [('Форум', reverse('forum_home'))]
    for topic in thread.topic.get_ancestors(include_self=True):
        breadcrumbs.append( 
            (topic.title, reverse('forum_topic', args=[topic.id,topic.slug])) 
        )
    breadcrumbs.append((thread.title, ''))

    return render(request, 'forum/thread.html', {
        'breadcrumbs': breadcrumbs,
        'messages': messages,
        'thread': thread,
        'form': form,
    })


@auth_check(ajax=False)
def create_thread(request, id):
    try:
        topic = Topic.objects.get(id=id)
    except:
        raise Http404

    if request.method == 'POST':
        form = ThreadForm(request.POST)

        if form.is_valid():
            data = form.cleaned_data

            thread = Thread()
            thread.title = data['title']
            thread.topic = topic
            thread.user = request.user
            thread.save()

            if data['content']:
                m = Message()
                m.thread = thread
                m.user = request.user
                m.content = data['content']
                m.show_on_top = True
                m.save()

            thread.top_message = m
            thread.save()

            url = reverse('forum_thread', args=[thread.id, thread.slug])

            return HttpResponseRedirect(url)
    else:
        form = ThreadForm()
    
    breadcrumbs = [('Форум', reverse('forum_home'))]
    for t in topic.get_ancestors(include_self=True):
        breadcrumbs.append( 
            (t.title, reverse('forum_topic', args=[t.id,t.slug])) 
        )
    breadcrumbs.append( ('Создать тему', '') )

    return render(request, 'forum/create_thread.html', {
        'topic': topic,
        'form': form,    
        'breadcrumbs': breadcrumbs,    
    })


@auth_check(ajax=False)
def create_blog(request):
    if not request.user.blog_topic:
        topic = Topic()
        topic.title = request.user.fullname
        topic.parent_id = 3
        topic.save()

        request.user.blog_topic = topic
        request.user.save()

    return redirect('forum_topic', request.user.blog_topic.id, request.user.blog_topic.slug)


def ajax_prev_messages(request):
    message = Message.objects\
        .get(pk=request.GET.get('message_id'))
    messages = Message.objects\
        .filter(root__id=request.GET.get('message_id'))\
        .order_by('-created_at')

    paginator = Paginator(messages, Message.per_page)
    page = request.GET.get('page')
    try:
        messages = paginator.page(page)
    except:
        messages = paginator.page(1)
    messages.object_list = messages.object_list[::-1]
    messages.end_index = min(messages.end_index()+Message.per_page, messages.paginator.count)

    return render(request, 'forum/messages.html', {
        'message': message,
        'messages': messages,
    })


@auth_check(ajax=False)
def message_delete(request, id):
    if not request.user.is_moderator:
        raise Http404

    try:
        message = Message.objects.get(id=id)
        message.delete()
    except Exception as e:
        raise e

    return HttpResponseRedirect(request.META.get('HTTP_REFERER'))


@auth_check(ajax=False)
def message_edit(request, id):
    try:
        message = Message.objects.get(id=id)
    except Exception as e:
        raise Http404

    if not message.can_edit():
        raise Http404

    if message.user_id != request.user.id and not request.user.is_superuser:
        raise Http404

    form = MessageForm({
        'content': message.content
    })
    if request.method == 'POST':
        form = MessageForm(request.POST)

        if form.is_valid():
            message.content = form.cleaned_data['content']
            message.save()

            return redirect('forum_thread', message.thread.id, message.thread.slug)

    return render(request, 'forum/message_edit.html', {
        'form': form,
        'message': message,
    })


@auth_check(ajax=False)
def message_on_main_toggle(request, id):
    if not request.user.is_moderator:
        raise Http404

    try:
        message = Message.objects.get(id=id)
        message.show_on_main = not message.show_on_main
        message.save()
    except Exception as e:
        raise e

    return HttpResponseRedirect(request.META.get('HTTP_REFERER'))


def by_tag(request):
    word = request.GET.get('q')

    try:
        tag = Tag.objects.get(name=word.lower())

        paginator = Paginator(tag.messages.all(), Message.per_page)
        page = request.GET.get('page')
        try:
            messages = paginator.page(page)
        except:
            messages = paginator.page(1)
    except:
        messages = []

    breadcrumbs = [('Форум', reverse('forum_home')), ('#'+word, '')]

    return render(request, 'forum/tag.html', {
        'breadcrumbs': breadcrumbs,
        'q': word,
        'messages': messages,
    })