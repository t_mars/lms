#coding=utf8

from django.template.defaulttags import register

from forum.models import Message


@register.inclusion_tag('forum/last_messages_widget.html')
def last_messages_widget(show_on_top=False, limit=10):
    qs = Message.objects.all().order_by('-created_at')

    if not show_on_top:
        qs = qs.filter(show_on_main=False)

    return {
        'last_messages': qs[:limit]
    }
