from django.conf.urls import patterns, include, url

from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    url(ur'^summernote/', include('lib.django_summernote.urls')),
    url(ur'^$', 'forum.views.home', name='forum_home'),

    url(ur'^ajax/prev_messages/$', 'forum.views.ajax_prev_messages', name='ajax_prev_messages'),
    url(ur'^message/(?P<id>\d+)/delete/$', 'forum.views.message_delete', name='admin_message_delete'),
    url(ur'^message/(?P<id>\d+)/on_main/toggle/$', 'forum.views.message_on_main_toggle', name='admin_message_on_main_toggle'),
    url(ur'^message/(?P<id>\d+)/edit/$', 'forum.views.message_edit', name='admin_message_edit'),

    url(ur'^topic/(?P<id>\d+)/$', 'forum.views.topic', {'slug': ''}),
    url(ur'^topic/(?P<id>\d+)/(?P<slug>[\w\s-]+)/$', 'forum.views.topic', name='forum_topic'),
    
    url(ur'^thread/(?P<id>\d+)/(?P<slug>[\w\s-]+)/$', 'forum.views.thread', name='forum_thread'),
    url(ur'^topic/(?P<id>\d+)/thread/create/$', 'forum.views.create_thread', name='forum_create_thread'),
    url(ur'^blog/create/$', 'forum.views.create_blog', name='forum_create_blog'),
    url(ur'^tag/$', 'forum.views.by_tag', name='forum_tag'),
)