#coding=utf8
import re

from django.db import models
from django.utils import timezone
from django.contrib.auth.models import User
from django.template.defaultfilters import slugify
from unidecode import unidecode

from mptt.models import MPTTModel, TreeForeignKey


def get_slug(name):
    return slugify(unidecode(name))


class Topic(MPTTModel):
    title = models.CharField(max_length=200)
    slug = models.CharField(max_length=200, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    parent = TreeForeignKey('self', null=True, blank=True, related_name='children')

    view_count = models.PositiveIntegerField(verbose_name=u'Количество просмотров', default=0)
    last_view = models.DateTimeField(null=True, blank=True, default=None)
    
    def __unicode__(self):
        return self.title

    @property
    def topic_count(self):
        return self.get_descendant_count()

    @property
    def thread_count(self):
        return Thread.objects.filter(
            topic_id__in=[t.id for t in self.get_descendants(include_self=True)]
        ).count()

    @property
    def last_message(self):
        if not hasattr(self, '_last_message'):
            threads = Thread.objects.filter(
                topic_id__in=[t.id for t in self.get_descendants(include_self=True)]
            )    
            messages = Message.objects\
                .filter(thread_id__in=[t.id for t in threads])\
                .order_by('-created_at')
            if len(messages):
                self._last_message = messages[0]
            else:
                self._last_message = None
        return self._last_message

    @property
    def message_count(self):
        threads = Thread.objects.filter(
            topic_id__in=[t.id for t in self.get_descendants(include_self=True)]
        )
        messages = Message.objects.filter(thread_id__in=[t.id for t in threads])
        
        return messages.count()

    def save(self, *args, **kwargs):
        self.slug = get_slug(self.title)
        super(Topic, self).save(*args, **kwargs)


class Thread(models.Model):
    topic = models.ForeignKey(Topic)
    title = models.CharField(max_length=200)
    slug = models.CharField(max_length=200)
    user = models.ForeignKey(User)
    created_at = models.DateTimeField(auto_now_add=True)
    top_message = models.ForeignKey('Message', default=None, null=True, blank=True, related_name='top_message')

    view_count = models.PositiveIntegerField(verbose_name=u'Количество просмотров', default=0)
    last_view = models.DateTimeField(null=True, blank=True, default=None)

    def __unicode__(self):
        return self.title

    @property
    def message_count(self):
        return Message.objects.filter(thread=self).count()

    @property
    def last_message(self):
        if not hasattr(self, '_last_message'):
            messages = Message.objects\
                .filter(thread=self)\
                .order_by('-created_at')
            if len(messages):
                self._last_message = messages[0]
            else:
                self._last_message = None
        return self._last_message

    def save(self, *args, **kwargs):
        self.slug = get_slug(self.title)
        super(Thread, self).save(*args, **kwargs)


class Message(models.Model):
    thread = models.ForeignKey(Thread)
    user = models.ForeignKey(User, related_name='user')
    reply_to = models.ForeignKey('self', related_name='reply_to_reverse', null=True, default=None, blank=True)
    root = models.ForeignKey('self', related_name='root_reverse', null=True, default=None, blank=True)

    content = models.TextField()
    created_at = models.DateTimeField(auto_now_add=True)
    per_page = 20

    show_on_main = models.BooleanField(default=False)
    show_on_top = models.BooleanField(default=False)

    @property
    def last_replies(self):
        if not hasattr(self, '_last_replies'):
            self._last_replies = None
            
            messages = Message.objects.filter(root=self)
            count = messages.count()
            if count:
                messages = messages.order_by('-created_at')[:3][::-1]
                has_next = True if count > 3 else False
                end_index = min(self.per_page, count)
                self._last_replies = {
                    'object_list': messages,
                    'end_index': end_index,
                    'has_next': has_next,
                    'next_page_number': 1,
                    'paginator': {'count': count},
                }
        return self._last_replies

    def save(self):
        super(Message, self).save()

        tag_words = re.findall(r'(#[\w]+)', self.content, flags=re.U)
        for word in tag_words:
            tag, _ = Tag.objects.get_or_create(name=word[1:].lower())
            tag.messages.add(self)

    def get_content(self):
        content = self.content

        return re.sub(r'(#)([\w]+)', r'<a href="/tag/?q=\2">#\2</a>', content, flags=re.U)

    def can_edit(self):
        return (timezone.now() - self.created_at).days <= 1


class Tag(models.Model):
    name = models.CharField(max_length=50)
    messages = models.ManyToManyField(Message)