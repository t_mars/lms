#coding=utf8
from datetime import timedelta

from django.utils import timezone
from django.core.management.base import BaseCommand

from main.models import Training
from main.utils.dates import prev_weekday
from profile.models import CustomUser


class Command(BaseCommand):
    """
    Помечает тренировки доступными для подписанных пользователей
    """
    def handle(self, *args, **options):
        import datetime
        from profile.services.user import UserService
        from main.models import *

        u = User.objects.get(username='talipov.mars')
        u.subscription_date = datetime.date(2016, 10, 14)
        UserService.open_trains(u)
