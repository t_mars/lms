#coding=utf8
import os

from django.core.validators import RegexValidator
from django.conf import settings
from django import forms

import awesome_avatar

from profile.models import USER_GENDER_CHOICE, CustomUser


tzchoices = []
f = open(os.path.join(settings.BASE_DIR, 'data', 'timezones'))
for l in f:
    l = l.split('\t')
    tzchoices.append( (l[0], l[1]) )


class MyAvatarWidget(awesome_avatar.widgets.AvatarWidget):
    def value_from_datadict(self, data, files, name):
        if data and files:
            return super(MyAvatarWidget, self).value_from_datadict(data, files, name)

    def render(self, name, value, attrs=None):
        try:
            return super(MyAvatarWidget, self).render(name, value, attrs)
        except:
            pass


class MyAvatarField(awesome_avatar.forms.AvatarField):
    def to_python(self, data):
        if data:
            return super(MyAvatarField, self).to_python(data)
        return data


class UserEditForm(forms.Form):
    avatar = MyAvatarField(label=u'Аватар', required=False, widget=MyAvatarWidget)
    username = forms.CharField(
        label=u'Имя пользователя',
        min_length=4,
        max_length=30,
        validators=[
            RegexValidator(
                regex=r'^[a-zA-Z][a-zA-Z0-9_]+$',
                message=u'Допустимы латинские символы и символ `_`',
                code='invalid_value'
            ),
        ],
        required=True
    )
    first_name = forms.CharField(label=u'Имя', min_length=4, max_length=30)
    last_name = forms.CharField(label=u'Фамилия', min_length=4, max_length=30)
    gender = forms.ChoiceField(label=u'Пол', choices=[('', 'неизвестно')] + USER_GENDER_CHOICE.items())
    timezone = forms.ChoiceField(label=u'Часовой пояс', choices=tzchoices, required=False, initial='Europe/Moscow')
    allpowerlifting = forms.URLField(
        label=u'Страница на AllPowerlifting.com',
        required=False,
        validators=[
            RegexValidator(
                regex=r'^http:\/\/allpowerlifting\.com\/lifters\/[A-Z]{3}\/[^/]+\/$',
                message=u'Неверная ссылка, пример: http://allpowerlifting.com/lifters/RUS/talipov-marsel-68878/',
                code='invalid_value'
            ),
        ]
    )

    def clean(self, *args, **kwargs):
        data = super(UserEditForm, self).clean(*args, **kwargs)
        if 'username' in data:
            data['username'] = data['username'].lower()
            qs = CustomUser.objects \
                .filter(username=data['username']) \
                .exclude(pk=self.user.pk)
            if qs.count() > 0:
                self.add_error('username', u'Пользователь с таким именем пользователя уже сушествует.')
        return data