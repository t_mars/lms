# coding=utf8
import json
from datetime import timedelta
import datetime

from django.db import models
from django.contrib.sessions.models import Session
from django.contrib.auth.models import User, UserManager
from django.contrib.contenttypes.models import ContentType
from django.contrib.contenttypes.fields import GenericForeignKey
from django.utils import timezone

from sorl.thumbnail import get_thumbnail
from awesome_avatar.fields import AvatarField

from main.models import Plan
from drugplan.models import Course
from videohub.models import Video
from forum.models import Topic


USER_GENDER_CHOICE = {
    'female': u'женский',
    'male': u'мужской',
}
RANK_CHOICE = {
    'brother': u'Бразер',
    'sect': u'Сектант',
    'devoted': u'Посвященный',
    'lms': u'Совет LMS',
}


def avatar_upload_to(instance, filename):
    return 'avatars/%s.%s' % (instance.id, filename.split('.')[-1])


BAN_REASON_CHOICE = {
    1: 'спам',
    2: 'оскорбления',
    3: 'флуд',
}


class CustomUser(User):
    """User with app settings."""
    timezone = models.CharField(max_length=50, default='Europe/Moscow')
    gender = models.CharField(max_length=6, default=None, null=True, blank=True, choices=USER_GENDER_CHOICE.items())
    allpowerlifting = models.URLField(max_length=200, default=None, null=True, blank=True)
    avatar = AvatarField(upload_to=avatar_upload_to, width=200, height=200,
        default=None, null=True, blank=True)
    favorites = models.TextField(default=None, blank=True, null=True)

    is_moderator = models.BooleanField(default=False)
    rank = models.CharField(max_length=10, default='brother', choices=RANK_CHOICE.items())

    is_banned = models.BooleanField(default=False)
    banned_at = models.DateTimeField(null=True, blank=True, default=None)
    ban_reason = models.PositiveSmallIntegerField(null=True, blank=True, default=None)
    ban_comment = models.CharField(max_length=200, null=True, blank=True, default=None)

    subscription_date = models.DateField(verbose_name='Дата окончания подписки', default=None, blank=True, null=True)

    blog_topic = models.ForeignKey(Topic, blank=True, default=None, null=True, on_delete=models.SET_NULL)

    trainer = models.ForeignKey(User, null=True, default=None, blank=True, related_name='students')
    trainer_confirmed = models.BooleanField(default=False)

    last_request = models.DateTimeField(null=True, default=None, blank=True)

    objects = UserManager()

    def is_subscribed(self):
        if self.subscription_date and self.subscription_date >= timezone.now().date():
            return True
        return False

    def add_subscribe(self, months):
        if not self.is_subscribed():
            self.subscription_date = timezone.now().date()
        self.subscription_date += timedelta(days=months * 30)
        self.save()

    def get_rank(self):
        return RANK_CHOICE[self.rank]

    def get_favorite_list(self):
        self.get_favorites()
        
        if not hasattr(self, '_favorite_list'):
            from drugplan.models import Course
            from main.models import Plan
        
            self._favorite_list = []
            for f in self._favorites:
                try:
                    if f['type'] == 'plan':
                        obj = Plan.objects.get(pk=f['id'])
                    elif f['type'] == 'spm':
                        obj = Course.objects.get(pk=f['id'])
                except:
                    obj = None
                self._favorite_list.append({
                    'type': f['type'],
                    'id': f['id'],
                    'obj': obj
                })
                
        return self._favorite_list

    def get_avatar(self):
        if self.avatar:
            return 'http://ws.last-man.org/static/media/' + str(self.avatar)
        else:
            return 'http://ws.last-man.org/static/img/unknown.jpg'

    def get_avatar_50(self):
        im = get_thumbnail(self.get_avatar(), '50x50', crop='center')
        return 'http://ws.last-man.org' + im.url

    def get_avatar_100(self):
        im = get_thumbnail(self.get_avatar(), '100x100', crop='center')
        return 'http://ws.last-man.org' + im.url

    def get_favorites(self):
        if not hasattr(self, '_favorites'):
            try:
                self._favorites = json.loads(self.favorites)
            except:
                self._favorites = []
        return self._favorites

    def del_favorite(self, type, _id):
        self.get_favorites()

        new_favorites = []
        for f in self._favorites:
            if f['type'] == type and f['id'] == _id:
                continue
            new_favorites.append(f)

        self.favorites = json.dumps(new_favorites)
        self.save()

    def add_favorite(self, obj):
        self.get_favorites()
        if len(self._favorites) >= 12:
            self._favorites.pop(0)
        self._favorites.append(obj)
        
        self.favorites = json.dumps(self._favorites)
        self.save()

    def clear_favorites(self):
        self.favorites = None
        self.save()

    @staticmethod
    def GET(**kwargs):
        user = User.objects.get(**kwargs)
        try:
            return CustomUser.objects.get(pk=user.pk)
        except:
            custom_user = CustomUser(user_ptr=user)
            custom_user.__dict__.update(user.__dict__)
            custom_user.save()
            return custom_user

    @property
    def fullname(self):
        if not hasattr(self, '_fullname'):
            if self.first_name and self.last_name:
                self._fullname = u'%s %s' % (self.first_name, self.last_name)
            else:
                self._fullname = self.username
        return self._fullname

    def get_plans_for_user(self, user):
        """
        Возвращает все планы пользователя self, доступные для user
        """
        qs = Plan.objects.filter(sportsman=self, is_deleted=False)
        if not user.is_superuser and user.id != self.trainer_id:
            qs = qs.filter(is_public=True)
        return qs.select_related('sportsman', 'author')

    def get_courses_for_user(self, user):
        """
        Возвращает все курсу пользователя self, доступные для user
        """
        qs = Course.objects.filter(patient=self, is_deleted=False)
        if not user.is_superuser and user.id != self.trainer_id:
            qs = qs.filter(is_public=True)
        return qs.select_related('patient', 'author')

    def get_videos_for_user(self, user):
        """
        Возвращает все видео пользователя self, доступные для user
        """
        qs = Video.objects.filter(user=self, is_deleted=False)
        if not user.is_superuser and user.id != self.trainer_id:
            qs = qs.filter(is_public=True)
        return qs.select_related('user')

    def get_plans(self):
        qs = Plan.objects \
            .filter(models.Q(sportsman_id=self.pk), is_deleted=False)
        return [p for p in qs if p.access_check(self, 'show')]
        
    def get_courses(self):
        qs = Course.objects \
            .filter(models.Q(patient_id=self.pk), is_deleted=False)
        return  [c for c in qs if c.access_check(self, 'show')]

    def get_public_plans(self):
        return Plan.objects \
            .filter(is_public=True, author=self, is_deleted=False) \
            .select_related('sportsman', 'author')

    def get_public_spms(self):
        return Course.objects \
            .filter(is_public=True, author=self, is_deleted=False) \
            .select_related('patient', 'author')

    def get_public_videos(self):
        return Video.objects \
            .get_active() \
            .filter(is_public=True, user=self) \
            .select_related('exercise', 'user')

    def ban(self, ban_reason, ban_comment):
        if self.is_banned:
            return

        self.is_moderator = False
        self.is_banned = True
        self.banned_at = timezone.now()
        self.ban_reason = ban_reason
        self.ban_comment = ban_comment
        self.save()

    def unban(self):
        if not self.is_banned:
            return

        self.is_banned = False
        self.banned_at = None
        self.ban_reason = None
        self.ban_comment = None
        self.save()

    def get_ban_reason(self):
        if self.ban_reason in BAN_REASON_CHOICE:
            return BAN_REASON_CHOICE[self.ban_reason]

    def has_discount(self):
        return self.date_joined.date() <= datetime.date(2016, 9, 1)


def user_get_custom_user(self, name):
    if self.__class__.__name__ == 'User':
        if '_custom_user' not in self.__dict__:
            self._custom_user = CustomUser.GET(pk=self.pk)
        if hasattr(self._custom_user, name):
            return getattr(self._custom_user, name)
    raise AttributeError

User.add_to_class('__getattr__', user_get_custom_user)


class WeightRecord(models.Model):
    user = models.ForeignKey(User)
    date = models.DateTimeField()
    value = models.FloatField()


class NotificationQuerySet(models.query.QuerySet):

    def unread(self):
        return self.filter(unread=True)

    def read(self):
        return self.filter(unread=False)

    def mark_all_as_read(self, recipient=None):
        """Mark as read any unread messages in the current queryset.
        Optionally, filter these by recipient first.
        """
        # We want to filter out read ones, as later we will store
        # the time they were marked as read.
        qs = self.unread()
        if recipient:
            qs = qs.filter(recipient=recipient)

        qs.update(unread=False)

    def mark_all_as_unread(self, recipient=None):
        """Mark as unread any read messages in the current queryset.
        Optionally, filter these by recipient first.
        """
        qs = self.read()

        if recipient:
            qs = qs.filter(recipient=recipient)

        qs.update(unread=True)


class Notification(models.Model):

    class Meta:
        ordering = ['-created_at']

    VERBS = {
        'forum_answer': 'Ответ в форуме',
        'forum_thread_answer': 'Ответ на ветку',
        'open_trains': 'Открыты тренировки',
        'forum_message_like': 'Понравился комментарий',
    }

    verb = models.CharField(choices=VERBS.items(), max_length=20)
    description = models.TextField(blank=True, null=True)

    target_content_type = models.ForeignKey(ContentType, related_name='notify_target', blank=True, null=True)
    target_object_id = models.CharField(max_length=255, blank=True, null=True)
    target = GenericForeignKey('target_content_type', 'target_object_id')

    recipient = models.ForeignKey(User, blank=False, related_name='notifications')
    unread = models.BooleanField(default=True, blank=False)

    created_at = models.DateTimeField(default=timezone.now)

    objects = NotificationQuerySet.as_manager()

    def get_data(self):
        try:
            return json.loads(self.description)
        except:
            pass

    def mark_as_read(self):
        if self.unread:
            self.unread = False
            self.save()

    def mark_as_unread(self):
        if not self.unread:
            self.unread = True
            self.save()


class Like(models.Model):

    class Meta:
        ordering = ['-created_at']

    target_content_type = models.ForeignKey(ContentType, related_name='like_target', blank=True, null=True)
    target_object_id = models.CharField(max_length=255, blank=True, null=True)
    target = GenericForeignKey('target_content_type', 'target_object_id')

    user = models.ForeignKey(User, blank=False, related_name='likes')
    created_at = models.DateTimeField(default=timezone.now)
    is_active = models.BooleanField(default=True)


class UserSession(models.Model):
    user = models.ForeignKey(User)
    session = models.ForeignKey(Session)
    platform = models.CharField(max_length=10, blank=True, null=True, default=None)
    version = models.CharField(max_length=5, blank=True, null=True, default=None)
    useragent = models.CharField(max_length=500, blank=True, null=True, default=None)
    modified_at = models.DateTimeField(default=timezone.now)

# class MeasurementRecord(models.Model):
#     user = models.ForeignKey(User)
#     date = models.DateField()
#
#     shoulder_value = models.PositiveSmallIntegerField(verbose_name='Плечи (см)', default=None, null=True, blank=True)
#     chest_value = models.PositiveSmallIntegerField(verbose_name='Грудь (см)', default=None, null=True, blank=True)
#     waist_value = models.PositiveSmallIntegerField(verbose_name='Талия (см)', default=None, null=True, blank=True)
#     buttocks_value = models.PositiveSmallIntegerField(verbose_name='Ягодицы (см)', default=None, null=True, blank=True)
#
#     biceps_right_value = models.PositiveSmallIntegerField(verbose_name='Бицепс, прав. (см)', default=None, null=True, blank=True)
#     biceps_left_value = models.PositiveSmallIntegerField(verbose_name='Бицепс, лев. (см)', default=None, null=True, blank=True)
#
#     forearm_right_value = models.PositiveSmallIntegerField(verbose_name='Предплечье, прав. (см)', default=None, null=True, blank=True)
#     forearm_left_value = models.PositiveSmallIntegerField(verbose_name='Предплечье, лев. (см)', default=None, null=True, blank=True)
#
#     hips_right_value = models.PositiveSmallIntegerField(verbose_name='Бедро, прав. (см)', default=None, null=True, blank=True)
#     hips_left_value = models.PositiveSmallIntegerField(verbose_name='Бедро, лев. (см)', default=None, null=True, blank=True)
#
#     shin_right_value = models.PositiveSmallIntegerField(verbose_name='Голень, прав. (см)', default=None, null=True, blank=True)
#     shin_left_value = models.PositiveSmallIntegerField(verbose_name='Голень, лев. (см)', default=None, null=True, blank=True)
#
#     fat_percentage = models.PositiveSmallIntegerField(verbose_name='Жировая ткань (%)', default=None, null=True, blank=True)
#     muscle_percentage = models.PositiveSmallIntegerField(verbose_name='Мышечная ткань (%)', default=None, null=True, blank=True)
