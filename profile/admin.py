#coding=utf8
from django.contrib import admin
from django.contrib.admin import SimpleListFilter
from django.utils import timezone
from django.db.models import Q

from profile.models import *


class SubscribeFilter(SimpleListFilter):
    title = u'Подписка'
    parameter_name = 'subscription_date'

    def lookups(self, request, model_admin):
        return [('1', u'с подпиской'), ('0', u'без подписки')]

    def queryset(self, request, queryset):
        if self.value() == '1':
            return queryset.filter(subscription_date__gte=timezone.now().date())
        else:
            return queryset.filter(
                Q(subscription_date__lt=timezone.now().date()) |
                Q(subscription_date__isnull=True)
            )


class CustomUserAdmin(admin.ModelAdmin):
    list_display = ['id', 'username', 'is_subscribed', 'subscription_date']
    readonly_fields = ['avatar', 'password']
    list_filter = [SubscribeFilter]
admin.site.register(CustomUser, CustomUserAdmin)


class NotificationAdmin(admin.ModelAdmin):
    list_display = ['recipient', 'verb', 'created_at', 'target', 'unread']
admin.site.register(Notification, NotificationAdmin)


class UserSessionAdmin(admin.ModelAdmin):
    list_display = ['user', 'platform', 'version', 'modified_at', 'useragent']
admin.site.register(UserSession, UserSessionAdmin)
