# coding=utf8
import pytz

from django.contrib.auth.backends import ModelBackend
from django.utils import timezone

from profile.models import CustomUser, UserSession


class CustomUserMiddleware(ModelBackend):

    def process_request(self, request):
        if request.user.is_authenticated():
            try:
                custom_user = CustomUser.objects.get(pk=request.user.pk)
            except:
                custom_user = CustomUser(user_ptr=request.user)
                custom_user.__dict__.update(request.user._wrapped.__dict__)
                custom_user.save()

            request.user_session, _ = UserSession.objects.get_or_create(user=custom_user, session_id=request.session.session_key)
            request.user = custom_user

            request.user.last_request = timezone.now()
            request.user.save()