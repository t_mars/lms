#coding=utf8
import re
from urlparse import urlparse
import awesome_avatar

from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect, Http404
from django.template.loader import get_template
from django.core.urlresolvers import reverse
from django import forms
from django.db.models import Q
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.core.urlresolvers import resolve


from common.views import auth_check, json_response, json_error
from profile.models import *
from profile.forms import UserEditForm


def get_user_counts(profile, user):
    return {
        'plans': profile.get_plans_for_user(user).count(),
        'courses': profile.get_courses_for_user(user).count(),
        'videos': profile.get_videos_for_user(user).count()
    }

@auth_check(ajax=False)
def edit(request):
    if request.method == 'POST':
        form = UserEditForm(request.POST, request.FILES, initial={
            'avatar': request.user.avatar
        })
        form.user = request.user
        if form.is_valid():
            data = form.clean()
            if data['avatar'] and isinstance(data['avatar'], dict):
                avatar_field = awesome_avatar.fields.AvatarField()
                avatar_field.name = 'avatar'
                avatar_field.save_form_data(request.user, data['avatar'])

            request.user.username = data['username'].lower()
            request.user.first_name = data['first_name']
            request.user.last_name = data['last_name']
            request.user.gender = data['gender']
            request.user.timezone = data['timezone']
            request.user.allpowerlifting = data['allpowerlifting']
            request.user.save()

            return redirect('profile_show', username=request.user.username)        
    else:
        form = UserEditForm(initial={
            'avatar': request.user.avatar,
            'username': request.user.username,
            'first_name': request.user.first_name,
            'last_name': request.user.last_name,
            'gender': request.user.gender,
            'timezone': request.user.timezone,
            'allpowerlifting': request.user.allpowerlifting,
        })

    return render(request, 'profile/profile_edit.html', {
        'form': form,
    })


def info(request, username):
    try:
        user = User.objects.get(pk=username)
        return HttpResponseRedirect(reverse('profile_show', args=[user.username]))
    except:
        pass
    
    try:
        user = CustomUser.GET(username=username)
    except:
        raise Http404(u'Пользователь не найден.')

    breadcrumbs = [
        ('Пользователи', reverse('user_list')),
        (user.fullname, ''),
    ]

    return render(request, 'profile/show/info.html', {
        'user': user,    
        'sauth': user.social_auth.get(),    
        'mode': 'info',
        'breadcrumbs': breadcrumbs,
        'counts': get_user_counts(user, request.user)
    })


def plan(request, username):
    try:
        user = CustomUser.GET(username=username)
    except:
        raise Http404(u'Пользователь не найден.')

    qs = user.get_plans_for_user(request.user).order_by('-created_at')

    paginator = Paginator(qs, 10)
    page = request.GET.get('page')
    try:
        objs = paginator.page(page)
    except PageNotAnInteger:
        objs = paginator.page(1)
    except EmptyPage:
        objs = paginator.page(paginator.num_pages)

    breadcrumbs = [
        ('Пользователи', reverse('user_list')),
        (user.fullname, reverse('profile_show', args=[user.username])),
        ('Планы', ''),
    ]

    return render(request, 'profile/show/plans.html', {
        'user': user,
        'mode': 'plan',
        'list': objs,
        'breadcrumbs': breadcrumbs,
        'counts': get_user_counts(user, request.user)
    })


def spm(request, username):
    try:
        user = CustomUser.GET(username=username)
    except:
        raise Http404(u'Пользователь не найден.')

    qs = user.get_courses_for_user(request.user).order_by('-created_at')

    paginator = Paginator(qs, 10)
    page = request.GET.get('page')
    try:
        objs = paginator.page(page)
    except PageNotAnInteger:
        objs = paginator.page(1)
    except EmptyPage:
        objs = paginator.page(paginator.num_pages)

    breadcrumbs = [
        ('Пользователи', reverse('user_list')),
        (user.fullname, reverse('profile_show', args=[user.username])),
        ('СПМ', ''),
    ]

    return render(request, 'profile/show/spm.html', {
        'user': user,
        'mode': 'spm',
        'list': objs,
        'breadcrumbs': breadcrumbs,
        'counts': get_user_counts(user, request.user)
    })


def video(request, username):
    try:
        user = CustomUser.GET(username=username)
    except:
        raise Http404(u'Пользователь не найден.')

    qs = user.get_videos_for_user(request.user).order_by('-created_at')

    paginator = Paginator(qs, 40)
    page = request.GET.get('page')
    try:
        objs = paginator.page(page)
    except PageNotAnInteger:
        objs = paginator.page(1)
    except EmptyPage:
        objs = paginator.page(paginator.num_pages)

    breadcrumbs = [
        ('Пользователи', reverse('user_list')),
        (user.fullname, reverse('profile_show', args=[user.username])),
        ('Видео', ''),
    ]

    return render(request, 'profile/show/video.html', {
        'user': user,
        'mode': 'video',
        'list': objs,
        'breadcrumbs': breadcrumbs,
        'counts': get_user_counts(user, request.user)
    })


def admin(request, username):
    if not request.user.is_moderator:
        raise Http404

    try:
        user = CustomUser.GET(username=username)
    except:
        raise Http404(u'Пользователь не найден.')

    class Form(forms.Form):
        def __init__(self, user, *args, **kwargs):
            super(Form, self).__init__(*args, **kwargs)
            if not user.is_superuser:
                self.fields['rank'].widget.attrs['disabled'] = True

        rank = forms.ChoiceField(label=u'Звание', choices=RANK_CHOICE.items(), required=False)
        is_banned = forms.BooleanField(label=u'Забанить?', required=False)
        ban_reason = forms.ChoiceField(label=u'Причина', choices=BAN_REASON_CHOICE.items(), required=False)
        ban_comment = forms.CharField(label=u'Комментарий', max_length=200, required=False)

        def clean(self, *args, **kwargs):
            data = super(Form, self).clean(*args, **kwargs)
            if data['is_banned'] and not data['ban_reason']:
                self.add_error('ban_reason', u'Необходимо указать причину')
            return data

    if request.method == 'POST':
        form = Form(request.user, request.POST)
        if form.is_valid():
            data = form.clean()

            if user.is_banned != data['is_banned']:
                if data['is_banned']:
                    user.ban(data['ban_reason'], data['ban_comment'])
                else:
                    user.unban()

            if request.user.is_superuser and data['rank'] and data['rank'] != user.rank:
                user.rank = data['rank']
                user.save()

            return redirect('profile_admin', user.username)
    else:
        form = Form(request.user, initial={
            'rank': user.rank,
            'is_banned': user.is_banned,
            'ban_reason': user.ban_reason,
            'ban_comment': user.ban_comment,
        })

    breadcrumbs = [
        ('Пользователи', reverse('user_list')),
        (user.fullname, reverse('profile_show', args=[user.username])),
        ('Админ', ''),
    ]

    return render(request, 'profile/show/admin.html', {
        'user': user,
        'mode': 'admin',
        'form': form,
        'breadcrumbs': breadcrumbs,
        'counts': get_user_counts(user, request.user)
    })


@auth_check(ajax=False)
def favorite_clear(request):
    request.user.clear_favorites()
    return json_response(
        status='success'
    )


@auth_check(ajax=False)
def favorite_del(request):
    type = request.GET.get('type', '')
    _id = request.GET.get('id', '')

    request.user.del_favorite(type, _id)

    return json_response(status='success')


@auth_check(ajax=False)
def favorite_add(request):
    url_pattern = re.compile('http[s]?://(?:[a-zA-Z]|[0-9]|[$-_@.&+]|[!*\(\),]|(?:%[0-9a-fA-F][0-9a-fA-F]))+')

    url = request.GET.get('url', '')
    if not url_pattern.match(url):
        return json_error(u'Неверная ссылка')

    p = urlparse(url)
    try:
        current_url = resolve(p.path).url_name
    except:
        current_url = None
    
    from drugplan.models import Course
    from main.models import Plan
        
    f = None
    if current_url == 'plan':
        id = p.path.split('/')[-2]
        f = {'type': 'plan', 'id': id}
        obj = Plan.objects.get(pk=id)

    elif current_url == 'spm_show':
        id = p.path.split('/')[-2]
        f = {'type': 'spm', 'id': id}
        obj = Course.objects.get(pk=id)

    if f:
        request.user.add_favorite(f)

        f['obj'] = obj
        template = get_template('profile/favorite.html')
        html = template.render({'f': f})
    else:
        html = ''

    return json_response(status='success', html=html)


@auth_check(ajax=False)
def weight_stat(request):
    qs = WeightRecord.objects \
        .filter(user=request.user) \
        .order_by('date')

    class Form(forms.Form):
        date = forms.DateField(label=u'Дата', 
            widget=forms.TextInput(attrs={
                'data-provide':'datepicker',
            }),
            input_formats=['%d.%m.%Y'],
            initial=timezone.now().date().strftime('%d.%m.%Y'))
        value = forms.FloatField(label=u'Вес', max_value=200, min_value=10)

        def clean(self, *args, **kwargs):
            data = super(Form, self).clean(*args, **kwargs)
            d = data['date']
            if d < datetime.date(2016,1,1):
                self.add_error('date', u'Дата не может быть раньше 1 января 2016 года.')
            if d > timezone.now().date():
                self.add_error('date', u'Дата не может быть больше сегодняшнего числа.')
            return data

    if request.method == 'POST':
        form = Form(request.POST)
        if form.is_valid():
            data = form.clean()
            try:
                r = WeightRecord.objects.get(user=request.user, date=data['date'])
            except:
                r = WeightRecord()
                r.user = request.user
                r.date = data['date']
            r.value = data['value']
            r.save()
            return redirect('profile_weight_stat')
    else:
        form = Form()

    return render(request, 'profile/weight_stat.html', {
        'records': qs,
        'form': form,
        'menu': 'profile_weight_stat',
    })


def user_list(request):
    qs = CustomUser.objects.all()

    class Form(forms.Form):
        name = forms.CharField(label=u'Имя', max_length=50, required=False)
        is_banned = forms.BooleanField(label=u'Забанен', required=False)
        is_moderator = forms.BooleanField(label=u'Модератор', required=False)
        rank = forms.ChoiceField(label=u'Звание', choices=[('', 'выберите')] + RANK_CHOICE.items(),
                                 required=False, initial='')

    if 'search' in request.GET:
        form = Form(request.GET)
        if form.is_valid():
            data = form.clean()
            if data['name']:
                qs = qs.filter(
                    Q(first_name__icontains=data['name']) | \
                    Q(last_name__icontains=data['name']) | \
                    Q(username__icontains=data['name'])
                )
            if data['is_banned']:
                qs = qs.filter(is_banned=True)
            if data['is_moderator']:
                qs = qs.filter(is_moderator=True)
            if data['rank'] in RANK_CHOICE:
                qs = qs.filter(rank=data['rank'])

    else:
        form = Form()

    paginator = Paginator(qs, 100)
    page = request.GET.get('page')
    try:
        objs = paginator.page(page)
    except PageNotAnInteger:
        objs = paginator.page(1)
    except EmptyPage:
        objs = paginator.page(paginator.num_pages)

    return render(request, 'profile/user_list.html', {
        'list': objs,
        'form': form,
        'breadcrumbs': [('Пользователи', '')],
    })


@auth_check(ajax=False)
def notifications(request):
    qs = request.user.notifications.all()
    qs.mark_all_as_read()
    objs = [n for n in qs if n.target]

    return render(request, 'profile/notifications.html', {
        'notifications': objs,
    })

