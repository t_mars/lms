from django.conf.urls import patterns, include, url

from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    url(r'^profile/edit/$', 'profile.views.edit', name='profile_edit'),
    url(r'^profile/favorite/del/$', 'profile.views.favorite_del', name='profile_favorite_del'),
    url(r'^profile/favorite/add/$', 'profile.views.favorite_add', name='profile_favorite_add'),
    url(r'^profile/favorite/clear/$', 'profile.views.favorite_clear', name='profile_favorite_clear'),
    url(r'^profile/weight-stat/$', 'profile.views.weight_stat', name='profile_weight_stat'),
    url(r'^profile/notifications/$', 'profile.views.notifications', name='profile_notifications'),
    url(ur'^users/$', 'profile.views.user_list', name='user_list'),
    url(ur'^user/(?P<username>[\w\d\._-]+)/$', 'profile.views.info', name='profile_show'),
    url(ur'^user/(?P<username>[\w\d\._-]+)/plan/$', 'profile.views.plan', name='profile_plan'),
    url(ur'^user/(?P<username>[\w\d\._-]+)/spm/$', 'profile.views.spm', name='profile_spm'),
    url(ur'^user/(?P<username>[\w\d\._-]+)/video/$', 'profile.views.video', name='profile_video'),
    url(ur'^user/(?P<username>[\w\d\._-]+)/admin/$', 'profile.views.admin', name='profile_admin'),
)