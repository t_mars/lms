# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import awesome_avatar.fields


class Migration(migrations.Migration):

    dependencies = [
        ('profile', '0003_customuser_allpowerlifting'),
    ]

    operations = [
        migrations.AddField(
            model_name='customuser',
            name='avatar',
            field=awesome_avatar.fields.AvatarField(default='', upload_to=b'avatars'),
            preserve_default=False,
        ),
    ]
