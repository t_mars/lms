# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('profile', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='customuser',
            name='gender',
            field=models.CharField(default=None, max_length=6, null=True, blank=True, choices=[(b'male', '\u043c\u0443\u0436\u0441\u043a\u043e\u0439'), (b'female', '\u0436\u0435\u043d\u0441\u043a\u0438\u0439')]),
        ),
    ]
