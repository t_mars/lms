# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import profile.models
import awesome_avatar.fields


class Migration(migrations.Migration):

    dependencies = [
        ('profile', '0005_auto_20160122_1553'),
    ]

    operations = [
        migrations.AlterField(
            model_name='customuser',
            name='avatar',
            field=awesome_avatar.fields.AvatarField(default=None, null=True, upload_to=profile.models.avatar_upload_to, blank=True),
        ),
    ]
