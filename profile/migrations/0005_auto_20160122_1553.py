# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import awesome_avatar.fields


class Migration(migrations.Migration):

    dependencies = [
        ('profile', '0004_customuser_avatar'),
    ]

    operations = [
        migrations.AlterField(
            model_name='customuser',
            name='avatar',
            field=awesome_avatar.fields.AvatarField(default=None, null=True, upload_to=b'avatars', blank=True),
        ),
    ]
