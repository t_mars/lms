# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('profile', '0002_customuser_gender'),
    ]

    operations = [
        migrations.AddField(
            model_name='customuser',
            name='allpowerlifting',
            field=models.URLField(default=None, null=True, blank=True),
        ),
    ]
