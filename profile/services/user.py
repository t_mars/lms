#coding=utf8
from django.utils import timezone
from django.contrib.auth.models import User

from common.service import Service
from profile.services.notification import NotificationService
from main.utils.dates import prev_weekday, get_timestamp
from main.models import Training, Plan


class UserService(Service):

    @classmethod
    def open_trains(cls, user):
        """
        Открывает тренировки для пользователя
        """

        today = timezone.now().date()
        if not user.subscription_date or user.subscription_date < today:
            return

        min_date = prev_weekday(today, 0)
        max_date = user.subscription_date

        trains = Training.objects.filter(
            plan__sportsman=user,
            plan__source__isnull=False,
            plan__is_deleted=False,
            is_hidden=True,
            date__gte=min_date,
            date__lte=max_date
        )
        if trains.count() == 0:
            return

        plans = {}
        keys = []
        for train in trains:
            keys.append([cls.CK_TRAIN_VISIBLE, train.id])
            keys.append([cls.CK_TRAIN_HIDDEN, train.id])
            if train.plan_id not in plans:
                plans[train.plan_id] = 0
            plans[train.plan_id] += 1

        trains.update(is_hidden=False)
        cls.clear_cache(keys)

        for plan_id, count in plans.iteritems():
            NotificationService.notify('open_trains', Plan.objects.get(pk=plan_id), user, {'count': count})

    @classmethod
    def get_user_info(cls, user_id):
        """
        Возвращает данные о пользователе
        """
        try:
            user = User.objects.get(id=user_id)
        except:
            return None

        return {
            'id': user.id,
            'first_name': user.first_name,
            'last_name': user.last_name,
            'fullname': user.fullname,
            'username': user.username,
            'avatar_50': user.get_avatar_50(),
            'avatar_100': user.get_avatar_100(),
            'gender': user.gender,
            'is_subscribed': user.is_subscribed(),
            'allpowerlifting': user.allpowerlifting,
            'subscription_date': user.subscription_date.strftime('%d.%m.%y') if user.is_subscribed() else None,
            'trainer': cls.get_user_short_info(user.trainer_id) if user.trainer else None,
            'last_request': get_timestamp(user.last_request) if user.last_request else None
        }

    @classmethod
    def get_user_short_info(cls, user_id):
        """
        Возвращает данные о пользователе
        """
        try:
            user = User.objects.get(id=user_id)
        except:
            return None

        return {
            'id': user.id,
            'fullname': user.fullname,
            'username': user.username,
            'avatar_50': user.get_avatar_50(),
            'avatar_100': user.get_avatar_100(),
            'last_request': get_timestamp(user.last_request) if user.last_request else None
        }