#coding=utf8
import json

from django.contrib.contenttypes.models import ContentType

from common.service import Service
from profile.models import Notification


class NotificationService(Service):

    @classmethod
    def notify(cls, verb, target, recipient, data=None):
        n = Notification()
        n.recipient = recipient
        n.verb = verb
        n.target = target
        if data:
            n.description = json.dumps(data)
        n.save()

    @classmethod
    def delete_notifies(cls, verb, target, recipient):
        content_type = ContentType.objects.get_for_model(target)

        Notification.objects \
            .filter(target_content_type_id=content_type.id, target_object_id=target.id,
                    verb=verb, recipient=recipient) \
            .delete()
