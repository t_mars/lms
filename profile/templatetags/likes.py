#coding=utf8

from django.template.defaulttags import register
from django.contrib.contenttypes.models import ContentType

from profile.models import Like


@register.inclusion_tag('profile/like_widget.html')
def like_widget(object, user):
    content_type = ContentType.objects.get_for_model(object)

    likes = Like.objects.filter(
        target_content_type_id=content_type.id,
        target_object_id=object.id,
        is_active=True
    )
    is_active = True if user.is_authenticated() and likes.filter(user=user).count() > 0 else False
    count = likes.count()

    users = []
    for l in likes[:3]:
        users.append({
            'username': l.user.username,
            'fullname': l.user.fullname,
            'avatar': l.user.get_avatar(),
            'id': l.user.id,
        })

    return {
        'is_active': is_active,
        'count': count,
        'users': users,
        'id': '%d_%d' % (content_type.id, object.id)
    }