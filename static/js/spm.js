$.ajaxSetup({ cache: false });
function reset_drugplan_head() {
  var cols = $('#drugplan tbody tr:first-child td');
  var head = $('#drugplan_head');
  head.find('thead tr.heads td').each(function(ind){
    $(this).width(cols.eq(ind).width());
  });
  head.css({
    'top': $('.navbar').outerHeight(true)-22,
    'width': $('#drugplan').outerWidth(true),
  });
  $('#drugplan').css('margin-top', head.height()-$('#navbar_head').outerHeight()-2);
}
function reset_selected_info() {
  var elems = $('#drugplan tbody td.selected');
  if (elems.length) {
    $('#base_info').hide();
    $('#selected_info span').text('Выбрано '+elems.length+' дней.');
    $('#selected_info').show();
  }
  else {
    $('#base_info').show();
    $('#selected_info').hide();
  }
}

function add_dose(obj) {
  var f = false;
  $('.amount_comment').each(function(){
    if (f) return;
    if ($(this).css('display') == 'none') {
      $(this).show();
      f = true;
    }
  })
}
// открытие формы добавления нового препарата
function add_new_drug(obj) {
  var btn = $(obj);
  var icon = btn.find('i');
  var drug = $('#add #id_drug');
  if (icon.hasClass('fa-plus')) {
    drug.prop('disabled', true);
    drug.find('option').prop('selected', false);
    drug.find('option:first-child').prop('selected', true);
    drug.prop('required', false);
    icon.removeClass('fa-plus').addClass('fa-minus');
    btn.removeClass('btn-default').addClass('btn-danger');
    
    $('#add .new_drug').fadeIn();
    $('#add .new_drug input').prop('required', true);
    $('#add .new_drug select').prop('required', true);
  } else {
    drug.prop('disabled', false);
    drug.prop('required', true);
    icon.removeClass('fa-minus').addClass('fa-plus');
    btn.removeClass('btn-danger').addClass('btn-default');
    
    $('#add .new_drug').fadeOut();
    $('#add .new_drug input').prop('required', false);
    $('#add .new_drug select').prop('required', false);
  }
}
function del_dose(obj) {
  var div = $(obj).closest('.amount_comment')
  div.hide();
  div.find('input').val('');
}

function add_form_reset() {
  $('#modal_edit #add form')[0].reset();
  $('#modal_edit #add .amount_comment:not(:eq(0))').hide();
  $('#modal_edit #add #id_scheme').change();
}

function get_selected_days() {
  var elems = $('#drugplan tbody td.selected');
  if (range_start && range_end) {
    return {type: 'range', start: range_start, end: range_end}
  }
  else if (elems.length > 0) {
    var days = [];
    elems.each(function(){
      days.push(parseInt($(this).attr('id').substr(5)));
    })
    return {type: 'days', days: days};
  }
  return false;
}
function update(start, end) {
  $.ajax({
    type: "GET",
    url: url['update'],
    data: {start: start, end: end},
    dataType: 'json',
  }).done(function( data ) {
    if (data.status == 'success') {
      $(data.content).find('td').each(function(){
        var obj = $(this);
        var obj2 = $('#' + obj.attr('id'));        
        
        if (!obj2.hasClass('selected') && !obj2.hasClass('total')) {
          return;
        }
        obj2.html(obj.html());
      });
    }
  });
}
function edit() {
  var selected_days = get_selected_days();

  $('#modal_edit').modal('show');

  if (can_edit) {
    if (selected_days.type == 'range') {
      $('#modal_edit #add #id_scheme').closest('.form-group').show();
    }
    else {
      $('#modal_edit #add #id_scheme').closest('.form-group').hide();
    }
    
    $('#modal_edit #add .loading').show();
    $('#modal_edit #add form').hide();
    $('#modal_edit #del .loading').show();
    $('#modal_edit #del form').hide();
    
    // загружаем форму
    $.ajax({
      type: "GET",
      url: url['add_form'],
      dataType: 'json',
    }).done(function( data ) {
      $('#modal_edit #add .loading').hide();
      $('#modal_edit #add form').show();
      $('#modal_edit #add .modal-body').html(data.form);
      set_add_actions();
      add_form_reset();
    });

  }
  
  $('#modal_edit #calc .loading').show();
  $('#modal_edit #calc table').hide();
  
  $.ajax({
    type: "GET",
    url: url['period'],
    data: {days: JSON.stringify(selected_days)},
    dataType: 'json',
  }).done(function( data ) {
    if (data.status == 'error') {
      alert(data.error);
    }
    else {
      // форма удаления
      if (can_edit) {
        var html = '';
        if (data.data.length == 0) {
          $('#modal_edit .nav-tabs li.add a').click();
          $('#modal_edit .nav-tabs li.del').hide();
          $('#modal_edit .nav-tabs li.calc').hide();
          return;
        }

        $('#modal_edit .nav-tabs li').show();
        for (var i = 0; i < data.data.length; i++) {
          var d = data.data[i];
          html += '<tr>';
          html += '<td><input type="checkbox" name="for_delete" value="' + d.drug.id + '"></td>';
          html += '<td>' + d.drug.name + '</td>';
          html += '<td>' + d.amount + ' ' + d.drug.measure + ' ('+d.count+')</td>';
          html += '<td>' + round_to(d.avg_day, 0.1).toFixed(1) + '</td>';
          html += '<td>' + round_to(d.avg_week, 0.1).toFixed(1) + '</td>';
          html += '</tr>';    
        }
        $('#modal_edit #del .loading').hide();
        $('#modal_edit #del form').show();
        $('#modal_edit #del table tbody').html(html);
      } else {
        $('#modal_edit .nav-tabs li.calc a').click();
      }

      // форма расчета вещества
      var html = '';
      for (var i = 0; i < data.data.length; i++) {
        var d = data.data[i];
        html += '<tr>';
        html += '<td>' + d.drug.name + '</td>';
        html += '<td>' + d.amount + ' ' + d.drug.measure + '</td>';
        html += '<td><input type="number" onkeyup="calc_drug(this)" data-amount="'+d.amount+
          '" class="pack_amount form-control" value="'+ d.drug.pack_size +'"></td>';
        html += '<td class="pack_count">-</td>';
        html += '</tr>';    
      }
      $('#modal_edit #calc .loading').hide();
      $('#modal_edit #calc table').show();
      $('#modal_edit #calc table tbody').html(html);   
      $('#modal_edit #calc table tbody .pack_amount').keyup();   
    }
  });
}
function calc_drug(obj) {
  var row = $(obj).closest('tr');
  var pa = row.find('.pack_amount');
  var pc = row.find('.pack_count');
  var pack_amount = parseInt(pa.val());
  var amount = parseInt(pa.attr('data-amount'));

  pc.html('-');
  if (isNaN(pack_amount)) return;
  if (pack_amount == 0) {
    v = 0
  } else {
    var v = Math.ceil(amount / pack_amount);
  }
  pc.html(v);
}
last_cell = 0;
range_start = 0;
range_end = 0;
function cell_click(event, obj) {
  if (event.shiftKey && last_cell) {
    $('#drugplan tbody td.selected').not(obj).removeClass('selected');
    var a = parseInt($(obj).attr('id').substr(5));
    var b = parseInt(last_cell.attr('id').substr(5));
    if (a > b) {
      t = a; a = b; b = t;
    }
    range_start = a;
    range_end = b;
    for (var i = a; i <= b; i += 86400) {
      $('#date_'+i).addClass('selected');
    }
  }
  else {
    range_start = range_end = 0;
    if (!event.ctrlKey) {
      $('#drugplan tbody td.selected').not(obj).removeClass('selected');
    }
    if ($(obj).hasClass('selected')) {
      $(obj).removeClass('selected');
      last_cell = 0;
    }
    else {
      last_cell = $(obj);
      $(obj).addClass('selected');
    }
  }
  reset_selected_info();
}

function set_add_actions() {
  if (!can_edit) return;
  $('#modal_edit #add #id_drug').select2();
  $('#modal_edit #add #id_scheme').change(function(){
    if ($(this).val() == 'every') {
      $(this).closest('div').removeClass('col-md-12').addClass('col-md-6');
      $('#id_every_n').closest('div').show();
      $('#id_weekdays').hide();
    }
    else if($(this).val() == 'weekdays') {
      $(this).closest('div').removeClass('col-md-12').addClass('col-md-6');
      $('#id_every_n').closest('div').hide();
      $('#id_weekdays').show();
    }
    else {
      $(this).closest('div').addClass('col-md-12').removeClass('col-md-6');
      $('#id_every_n').closest('div').hide();
      $('#id_weekdays').hide();
    }
  });
  add_form_reset();
}

function mark_form(ts) {
  $('#modal_mark').modal('show');
  $('#modal_mark .modal-body').html('');
  $('#modal_mark .loading').show();
  $.ajax({
    type: "GET",
    url: url['mark'],
    data: {ts: ts},
    dataType: 'json',
  }).done(function( data ) {
    $('#modal_mark .loading').hide();
    $('#modal_mark .modal-body').html(data.content);
    $('#modal_mark').modal('show');
  });
}


var load_up_active = 0;
function load_up() {
  if (load_up_active)
    return;
  var tbody = $('#drugplan tbody');

  var first = tbody.find('td').eq(0);
  var diff = first.offset().top - $(window).scrollTop();
  
  load_up_active = 1;
  $.ajax({
    type: "GET",
    url: url['ajax'],
    data: {ts: start_ts, direct: 'up'},
    dataType: 'json',
  }).done(function( data ) {
    start_ts = data.ts;
    if (data.ts) {
      load_up_active = 0;
    }
    tbody.prepend(data.content);
    diff = first.offset().top - diff;
    $(window).scrollTop(diff);
  });
}

var load_down_active = 0;
function load_down() {
  if (load_down_active)
    return;
  $("#load_down_icon").show();
  load_down_active = 1
  $.ajax({
    type: "GET",
    url: url['ajax'],
    data: {ts: end_ts, direct: 'down'},
    dataType: 'json',
  }).done(function( data ) {
    $("#load_down_icon").hide();
    end_ts = data.ts;
    if (data.ts) {
      load_down_active = 0;
    }
    $('#drugplan tbody').append(data.content);
  });
}
$(document).ready(function(){
  $('#modal_edit #add form').submit(function(e){
    data = $(this).serialize();
    data += '&days=' + JSON.stringify(get_selected_days())
    $.ajax({
      type: "POST",
      url: url['add'],
      data: data,
      dataType: 'json',
    }).done(function( data ) {
      if (data.status == 'success') {
        update(data.min_date, data.max_date);
        $('#modal_edit').modal('hide');
      } else {
        $('#modal_edit #add .modal-body').html(data.form);
        set_add_actions();
      }
    });
    e.preventDefault();
    return false;
  })

  $('#modal_edit #del form').submit(function(e){
    data = $(this).serialize();
    data += '&days=' + JSON.stringify(get_selected_days())
    $.ajax({
      type: "POST",
      url: url['delete'],
      data: data,
      dataType: 'json',
    }).done(function( data ) {
      if (data.status == 'success') {
        update(data.min_date, data.max_date);
        $('#modal_edit').modal('hide');
      }
    });
    e.preventDefault();
    return false;
  })

  set_add_actions();
  load_down();
  load_up();
  reset_drugplan_head();
})

$(window).resize(function(event){
  reset_drugplan_head();
});
$(window).scroll(function(event){
  if ($(this).scrollTop() ==  $(document).height()-$(window).height()) {
    load_down();
  }
  if ($(this).scrollTop() < 200) {
    load_up();
  }
});
