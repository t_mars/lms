function paneResize() {
  $('.pane').each(function () {
    var h = $(window).height() - $(this).offset().top;
    $(this).css({'height': h});
  })
}
$(document).ready(function(){
  $(window).resize(paneResize);
  paneResize();

  $('#edit-pane').mCustomScrollbar({
    theme: 'inset-3',
    axis:"y",
    mouseWheel: { scrollAmount: 300 },
    scrollInertia: 500,
  });
  $('#calendar-pane').mCustomScrollbar({
    theme: 'inset-3',
    axis:"yx",
    mouseWheel: { scrollAmount: 300 },
    scrollInertia: 500,
    callbacks: {
      onTotalScroll: function () {$('#load-down-button').click()},
      //onTotalScrollBack: function () {$('#load-up-button').click()},
    },
  });

});
function api(path) {
  return 'http://test.last-man.org/' + path;
}
function get_timestamp(date) {
  return Math.round(date.getTime()/1000);
}
function sleep(ms) {
  ms += new Date().getTime();
  while (new Date() < ms){}
}
function showError(msg) {
  alert('Произошла ошибка: '+ msg);
}
angular.module('filters', [])
.filter('ts2date', function() {
  return function(input) {
    return new Date(input * 1000);
  };
})
.filter('isEmpty', [function() {
  return function(object) {
    return angular.equals({}, object);
  }
}])
.directive('popover', function() {
  return {
    link: function ($scope, element, attr) {
      $(element).popover({
        trigger: 'hover',
        html : true,
      });
    }
  }
})
.directive('selectExercise', ['$parse', function() {
  return {
    link: function($scope, element, attr) {
      $(element)
        .focus(function () {
          $(this).select();
        })
        .autocomplete({
          minLength: 0,
          source: $scope.exerciseSelect,
          focus: function(event, ui) {
            //this.value = ui.item.label;
            event.preventDefault();
          },
          response: function (event, ui) {
            if (ui.content.length == 0) {
              ui.content.push({
                label: 'Создать упражнение',
                value: 0
              });
            }
          },
          select: function(event, ui) {
            if (ui.item.value == 0) {
              var block = $(this).closest('.execution');
              block.find('.exercise,.scheme').hide();
              block.find('.exercise-form .name').val($(this).val()).trigger('input');
              block.find('.exercise-form').show();
            } else {
              $(this).val(ui.item.label);
              var ind = parseInt($(this).attr('ng-index'));

              $scope.selected_train.executions[ind].exercise.id = parseInt(ui.item.value);
              $scope.$apply(function () {
                $scope.resetStat();
              });
            }
            return false;
          },
        })
        .click(function () {
          $(this).autocomplete("search", $(this).val());
        })
        .autocomplete("instance")._renderItem = function(ul, item) {
          if (item.value == 0) {
            return $('<li class="btn-success">')
              .html('<i class="fa fa-plus"></i> создать упражнение')
              .appendTo(ul);
          }

          return $("<li>")
            .html(item.label)
            .appendTo(ul);
        };
    }
  };
}])
.directive('hcChart', function () {
  return {
    restrict: 'E',
    template: '<div></div>',
    scope: {
      options: '='
    },
    link: function (scope, element) {
      Highcharts.chart(element[0], scope.options);
    }
  };
})
.directive('selectOnClick', ['$window', function ($window) {
    return {
        restrict: 'A',
        link: function (scope, element, attrs) {
            element.on('click', function () {
                $(this).select();
            });
        }
    };
}]);

var mainApp = angular.module('mainApp', ['filters', 'ui.bootstrap', 'ui.sortable', 'ui.bootstrap.contextMenu']).config(function($interpolateProvider) {
  $interpolateProvider.startSymbol('{$');
  $interpolateProvider.endSymbol('$}');
});

mainApp.controller('ShowCtrl', function ($scope, $http, $modal, $filter, $timeout) {
  $scope.scheme_format = $.cookie("scheme_format") ? $.cookie("scheme_format") : 3;
  $scope.show_stats = $.cookie("show_stats") == 1;
  $scope.exercises = exercises;
  $scope.access = access;
  $scope.plan = plan;
  $scope.today = new Date();
  $scope.start = null;
  $scope.finish = null;
  $scope.loadindUp = false;
  $scope.loading_down = true;
  $scope.show_warmup = false;
  $scope.selected_train = null;
  $scope.selected_exercise = {id: null};
  $scope.max = {
    'reps': 100,
    'apps': 10,
    'weight': 500,
    'percent': 200,
  };
  $scope.sortableOptions = {
    axis: 'y'
  };

  $('#calendar-pane').show();

  $scope.showMessage = function (msg) {
    $scope.message = msg;
    $scope.editPaneShow('message');
  };
  $scope.exerciseSelect = $.map(exercises, function (value, index) {
    return {
      label: value.name,
      value: value.id,
    }
  });
  $scope.exerciseSelect.sort(function (a, b) {
    if (a.label < b.label) return -1;
    if (a.label > b.label) return 1;
    return 0;
  });

  $http.get('load/' + get_timestamp($scope.today))
    .success(function (data) {
      $scope.loading_down = false;
      $scope.resetDates(data.weeks);
      $scope.weeks = data.weeks;
      $scope.start = data.start;
      $scope.finish = data.finish;
    });

  $scope.loadUp = function () {
    if (!$scope.start) return;
    $scope.loading_up = true;
    $http.get('load/up/' + $scope.start)
      .success(function (data) {
        $scope.loading_up = false;
        $scope.resetDates(data.weeks);
        $scope.weeks = angular.extend(data.weeks, $scope.weeks);
        $scope.start = data.start;
      });
  };

  $scope.loadDown = function () {
    if (!$scope.finish) return;
    $scope.loading_down = true;
    $http.get('load/down/' + $scope.finish)
      .success(function (data) {
        $scope.loading_down = false;
        $scope.resetDates(data.weeks);
        $scope.weeks = angular.extend($scope.weeks, data.weeks);
        $scope.finish = data.finish;
      });
  };

  $scope.resetDates = function (weeks) {
    angular.forEach(weeks, function (week) {
      angular.forEach(week.days, function (day) {
        day.date = new Date(day.ts * 1000);
        if (day.date.getDate() != $scope.today.getDate()) return;
        if (day.date.getMonth() != $scope.today.getMonth()) return;
        if (day.date.getYear() != $scope.today.getYear()) return;
        day.is_today = true;
        week.is_current = true;
      });
    })
  };

  $scope.removeExecution = function (array, index) {
    array.splice(index, 1);
    $scope.resetStat();
  };

  $scope.removeScheme = function (array, index) {
    array.splice(index, 1);
    $scope.resetStat();
  };

  $scope.addScheme = function (scheme) {
    if (scheme.length) {
      s = scheme[scheme.length - 1];
      scheme.push({
        'weight': s.weight,
        'percent': s.percent,
        'reps': s.reps,
        'apps': s.apps,
        'color': s.color,
      });
    } else {
      scheme.push({
        'weight': 0,
        'percent': 0,
        'reps': 0,
        'apps': 0,
      });
    }
    $scope.resetStat();
  };
  $scope.addExecution = function () {
    $scope.selected_train.executions.push({
      exercise: {
        id: 0,
        name: '',
      },
      other: {
        name: '',
        weight: '',
        apps: '',
        reps: '',
        color: 'default',
      },
      level: 'light',
      scheme: [{
        reps: 0,
        apps: 0,
        percent: 0,
        weight: 0,
        kps: 0,
        tonag: 0,
        color: 'default',
      }]
    });
  };

  $scope.changeColor = function (s) {
    if (s.color == 'default')
      s.color = 'success';
    else if (s.color == 'success')
      s.color = 'info';
    else if (s.color == 'info')
      s.color = 'warning';
    else if (s.color == 'warning')
      s.color = 'danger';
    else if (s.color == 'danger')
      s.color = 'default';
  };
  $scope.resetDate = function () {
    $http.get('pm/get/' + $scope.selected_train.date + '/').success(function (data) {
      if (data.status == 'success') {
        $scope.selected_train.pms = data.pms;
        $scope.resetStat();
      } else {
        showError(data.message);
      }
    });
  };

  $scope.validateMax = function (val, key) {
    if (isNaN(val) || val < 0)
      return 0;
    else if (val > $scope.max[key])
      return $scope.max[key];
    return val;
  };
  // при изменении веса а не ОИ
  $scope.changeWeight = function (execution, scheme) {
    var pm = $scope.selected_train.pms[execution.exercise.id];
    if (!pm) return;

    scheme.weight = $scope.validateMax(scheme.weight, 'weight');

    var new_percent = scheme.weight / pm * 100;
    var new_percent_ceil = Math.round(new_percent);

    if (new_percent_ceil != scheme.percent) {
      scheme.percent = new_percent_ceil;
    } else {
      scheme.percent = new_percent_ceil + (new_percent > scheme.percent ? 1 : -1);
    }
    $scope.resetStat();
  };

  $scope.resetStat = function () {
    $scope.selected_train.kps = 0;
    $scope.selected_train.tonag = 0;

    angular.forEach($scope.selected_train.executions, function (e) {
      if (!e.exercise) return;
      if (!$scope.selected_train.pms[e.exercise.id]) {
        $scope.selected_train.pms[e.exercise.id] = 0;
      }

      e.exercise = angular.copy($scope.exercises[e.exercise.id]);
      
      pm = $scope.selected_train.pms[e.exercise.id];

      angular.forEach(e.scheme, function (s) {
        s.reps = $scope.validateMax(s.reps, 'reps');
        s.apps = $scope.validateMax(s.apps, 'apps');
        s.percent = $scope.validateMax(s.percent, 'percent');
        s.weight = $scope.validateMax(s.weight, 'weight');


        s.weight = my_round(pm * s.percent / 100);

        s.kps = s.apps * s.reps;
        s.tonag = s.kps * s.weight;

        if (e.exercise.is_dual) s.tonag *= 2;

        $scope.selected_train.kps += s.kps;
        $scope.selected_train.tonag += s.tonag;
      });
    });
  };

  $scope.editTrain = function (train, pms) {
    if (train.is_hidden) {
      alert('Тренировка еще не открыта, вы не можете ее редактировать.');
      return;
    }
    $scope.selected_train = angular.copy(train);
    $scope.selected_train.pms = angular.copy(pms);
    $('#train_date').datepicker('update', $scope.selected_train.date);

    $scope.resetStat();
    $scope.editPaneShow('train');
  };
  $scope.addTrain = function (date, pms) {
    $scope.selected_train = {
      comment: '',
      resume: '',
      tonag: '',
      kps: 0,
      tonag: 0,
      avg_weight: 0,
      percent: 0,
      executions: [],
      date: $filter('date')(date, 'dd.MM.yyyy'),
      pms: angular.copy(pms),
    };

    $scope.resetDate();
    $scope.resetStat();
    $scope.editPaneShow('train');
  };
  $scope.copyTrain = function (train, pms) {
    if (train.is_hidden) {
      alert('Тренировка еще не открыта, вы не можете ее копировать.');
      return;
    }
    $scope.selected_train = angular.copy(train);
    $scope.selected_train.pms = angular.copy(pms);
    $('#train_date').datepicker('update', $scope.selected_train.date);

    $scope.selected_train.id = null;
    angular.forEach($scope.selected_train.executions, function (execution) {
      execution.id = null;
      angular.forEach(execution.scheme, function (s) {
        s.id = null;
      });
    });

    $scope.resetStat();
    $scope.editPaneShow('train');
    $('#train-tab input[data-provide="datepicker"]').datepicker('show');
  };

  $scope.closeExerciseForm = function (execution, $event) {
    delete execution.new_exercise;
    var block = $($event.currentTarget).closest('.execution');
    block.find('.exercise,.scheme').show();
    block.find('.exercise-form').hide();
  };

  $scope.createExercise = function (execution, $event) {
    $http.post('exercise/create/', execution.new_exercise).success(function (data) {
      if (data.status == 'success') {
        $scope.exercises[data.exercise.id] = data.exercise;
        $scope.exerciseSelect.push({
          value: data.exercise.id,
          label: data.exercise.name,
        });
        execution.exercise = data.exercise;
        $scope.closeExerciseForm(execution, $event);
      } else {
        showError(data.message);
      }
    });
  };

  $scope.editPaneShow = function (tab) {
    $('#calendar').css('width', $('#calendar').outerWidth());

    $('#menu-button').find('span').hide();
    $('#menu-button').animate({left: '375px'});

    $('#edit-pane .button-block').hide();
    $('#edit-pane').show().animate({width: '365px'}, function () {
      $('#edit-pane .button-block').show();
    });
    $('#edit-pane .tab').hide();
    $('#edit-pane #' + tab + '-tab').show();
    paneResize();
  };
  $scope.editPaneHide = function () {
    $('#menu-button').find('span').show();
    $('#menu-button').animate({left: '10px'});

    $('#edit-pane .tab').hide();
    $('#edit-pane .button-block').hide();
    $('#edit-pane').animate({width: '0px'}, function () {
      $(this).hide();
      $('#calendar').css('width', '');
    });
  };

  $scope.update = function (updates) {
    for (i = 0; i < updates.length; i++) {
      var update = updates[i];
      if (update.type == 'week') {
        week = $scope.weeks[update.data.num];
        week.pms = update.data.pms;
        week.tonag = update.data.tonag;
        week.kps = update.data.kps;
        week.percent = update.data.percent;
        week.avg_weight = update.data.avg_weight;
      }
      else if (update.type == 'day') {
        day = $scope.weeks[update.data.num].days[update.data.ts];
        day.trains = update.data.trains;
      }
    }
  };

  $scope.saveTrain = function () {
    $scope.isSavingTrain = true;
    $http.post('train/save/', $scope.selected_train)
      .success(function (data) {
        $scope.isSavingTrain = false;
        if (data.status == 'success') {
          $scope.update(data.updates);

          $scope.selected_train = null;
          $scope.editPaneHide();
        } else {
          showError(data.message);
        }
      });
  };
  $scope.deleteTrain = function (id) {
    if (!id) return;
    if (confirm('Вы действительно хотите удалить тренировку?')) {
      $http.post('train/' + id + '/delete/').success(function (data) {
        if (data.status == 'success') {
          $scope.update(data.updates);
          if ($scope.selected_train.id == id) {
            $scope.selected_train = {id: null};
            $scope.editPaneHide();
          }
        } else if (data.status == 'error') {
          showError(data.message);
        }
      });
    }
  };

  // plots
  $scope.chartOptions = function (label, categories, series) {
    return {
      plotOptions: {
        series: {
          animation: false
        }
      },
      title: {
        text: label,
      },
      chart: {
        type: 'spline'
      },
      xAxis: {
        categories: categories
      },
      yAxis: {
        title: {
          text: ''
        },
      },
      series: series
    }
  };
  $scope.showPlots = function () {
    $http.get('plots/').success(function (data) {
      $('#plots-tab .tonag').highcharts($scope.chartOptions('Тоннаж недельный', data.labels, data.plots['tonag']));
      $('#plots-tab .kps').highcharts($scope.chartOptions('КПШ', data.labels, data.plots['kps']));
      $('#plots-tab .percent').highcharts($scope.chartOptions('Относительная интенсивность', data.labels, data.plots['percent']));
      $('#plots-tab .avg-weight').highcharts($scope.chartOptions('Средний вес', data.labels, data.plots['avg_weight']));
    });

    $scope.editPaneShow('plots');
  };

  // PMList
  $scope.editPMList = function (num) {
    week = $scope.weeks[num];
    $scope.pm_list = [];
    $http.get('pm/list/' + week.monday + '/').success(function (data) {
      $scope.pm_list = data.list;
      $scope.pm_num = num;
      $scope.all_correct = {
        value: 0.5,
        flag: false,
      }
    });

    $scope.editPaneShow('pm');
  };
  $scope.pmSetCorrect = function (pm) {
    pm.weight = (1 + pm.correct / 100) * pm.prev.weight;
  };
  $scope.pmSetAllCorrect = function (pm) {
    angular.forEach($scope.pm_list, function (pm) {
      pm.correct = $scope.all_correct.value;
      $scope.pmSetCorrect(pm);
    });
  };
  $scope.pmSetWeight = function (pm) {
    pm.correct = (pm.weight - pm.prev.weight) / pm.prev.weight * 100;
  };
  $scope.savePMList = function () {
    $scope.isSavingPM = true;
    week = $scope.weeks[$scope.pm_num];

    $http.post('pm/save/' + week.monday + '/', $scope.pm_list).success(function (data) {
      if (data.status == 'success') {
        $scope.updateWeeks(week.num);

        $scope.selected_train = null;
        $scope.editPaneHide();
      } else if (data.status == 'error') {
        showError(data.message);
      }
      $scope.isSavingPM = false;
    });
  };

  // план
  $scope.editPlan = function () {
    $scope.edit_plan = {
      name: $scope.plan.name,
      start_date: $scope.plan.start_date,
      is_public: $scope.plan.is_public,
      author: $scope.plan.author.username,
      sportsman: $scope.plan.sportsman.username,
    };
    $scope.editPaneShow('plan');
  };
  $scope.savePlan = function () {
    $scope.isSavingPlan = true;
    $http.post('edit/', $scope.edit_plan).success(function (data) {
      if (data.status == 'success') {
        $scope.editPaneHide();
        if ($scope.plan.start_date != $scope.edit_plan.start_date) {
          window.location.reload();
        }
        $scope.plan = data.plan;
      } else if (data.status == 'error') {
        showError(data.message);
      }
      $scope.isSavingPlan = false;
    });
  };

  // группировка упражений
  $scope.showGroupExercises = function () {
    $http.get('exercises/get/').success(function (data) {
      if (data.status == 'success') {
        $scope.groupExercises = data.list;
        $scope.editPaneShow('groups');
      } else {
        showError(data.message);
      }
    });
  };
  $scope.saveGroupExercises = function () {
    $http.post('exercises/save/', $scope.groupExercises).success(function (data) {
      if (data.status == 'error') {
        showError(data.message);
      }
      $scope.editPaneHide();
    });
  };

  // перемещение тренировок
  $scope.updateWeeks = function (week_num) {
    last_week = $scope.weeks[Object.keys($scope.weeks)[Object.keys($scope.weeks).length - 1]];
    $http.get('update/' + week_num + '/' + last_week.num + '/').success(function (data) {
      if (data.status == 'success') {
        $scope.resetDates(data.weeks);
        $scope.weeks = angular.extend($scope.weeks, data.weeks);
      } else if (data.status == 'error') {
        showError(data.message);
      }
    });
  };
  $scope.break_del_loading = false;
  $scope.breakDel = function (week_num) {
    if (!confirm('Вы действительно хотите удалить перерыв на неделе №' + week_num + '?')) {
      return;
    }
    $scope.break_del_loading = true;
    $http.get('break/del/' + week_num + '/').success(function (data) {
      $scope.break_del_loading = false;
      if (data.status == 'success') {
        $scope.updateWeeks(week_num);
      } else {
        showError(data.message);
      }
    });
  };
  $scope.break_add_loading = false;
  $scope.breakAdd = function (week_num) {
    if (!confirm('Вы действительно хотите добавить перерыв на неделю №' + week_num + '?')) {
      return;
    }
    $scope.break_add_loading = true;
    $http.get('break/add/' + week_num + '/').success(function (data) {
      $scope.break_add_loading = false;
      if (data.status == 'success') {
        $scope.updateWeeks(week_num);
      } else {
        showError(data.message);
      }
    });
  };
  // сохранение формата вывода схем
  $scope.saveSchemeFormat = function () {
    $.cookie('scheme_format', $scope.scheme_format, {path: '/'});
  };
  $scope.saveShowStats = function () {
    $.cookie('show_stats', $scope.show_stats ? 1 : 0, {path: '/'});
  };

  // видео
  $scope.showVideos = function (videos) {
    $scope.videos = videos;
    $scope.editPaneShow('videos');
  };
  $scope.showVideo = function (video_id) {
    showVideo(video_id);
  };
  $scope.menuOptions = function(scheme) {
    if (!$scope.access.add_video) {
      return;
    }
    var savedVideo = function (data) {
      scheme.videos[data.video.app] = data.video;
      $scope.$apply();
    };
    var menu = [{
        text: 'Добавить видео:',
        enabled: function() {return false},
    }];
    for (var i = 1; i <= scheme.apps; i++) {
      (function (app) {
        menu.push(
          [scheme.weight + 'кг на ' + scheme.reps + ' (' + app + ' подход)', function ($itemScope) {
            showVideoForm(scheme, app, savedVideo);
          }]
        )
      })(i);

    }
    return menu;
  }
});