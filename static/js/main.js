/**
 * Created by Марсель on 30.04.2016.
 */
var originalLeave = $.fn.popover.Constructor.prototype.leave;
$.fn.popover.Constructor.prototype.leave = function(obj){
  var self = obj instanceof this.constructor ?
    obj : $(obj.currentTarget)[this.type](this.getDelegateOptions()).data('bs.' + this.type)
  var container, timeout;

  originalLeave.call(this, obj);

  if(obj.currentTarget) {
    container = $(obj.currentTarget).siblings('.popover')
    timeout = self.timeout;
    container.one('mouseenter', function(){
      //We entered the actual popover – call off the dogs
      clearTimeout(timeout);
      //Let's monitor popover content instead
      container.one('mouseleave', function(){
        $.fn.popover.Constructor.prototype.leave.call(self, self);
      });
    })
  }
};

$(document).ready(function(){
    $.fn.datepicker.defaults.language = 'ru';
    $.fn.datepicker.defaults.format = 'dd.mm.yyyy';
    $.fn.datepicker.defaults.autoclose = true;

    $('#signin').popover({
        html: true,
        placement: 'bottom',
        trigger: 'hover',
        delay: {
            show: 50,
            hide: 400
        },
        title: function () {
            return $(this).parent().find('.head').html();
        },
        content: function () {
            return $(this).parent().find('.content').html();
        }
    });

    if (!is_authenticated) {
        $('.auth-check').click(function () {
            show_signin($(this).attr('data-message'));

            return false;
        })
    }
});

function show_signin(message) {
    $('#modal_signin .message').html(message);
    $('#modal_signin').modal('show');
}