var read_info = false;
var speed = 0;
function show_drug(id) {
  $('.drug_' + id).show();
  $('.drug_' + id).each(function(){
    var b = $(this).css('background-color');
    $(this).animate({
      'background-color': '#62C462'
    }, speed, function(){
      $(this).animate({
        'background-color': b,
      }, speed)
    });
  })
}
function hide_drug(id, callback) {
  $('.drug_' + id).each(function(){
    var b = $(this).css('background-color');
    $(this).animate({
      'background-color': '#EE5F5B',
    }, speed, function(){
      $(this).css('background-color', b)  
      $(this).hide()
    });
  })
}
function resize() {
  if ($(window).width() > 992) {
    var h = $(window).height() - $('.tab-pane').offset().top;
    $('.tab-pane').css({
      'height': h,
      'overflow-y': 'scroll',
    });
  } else {
    $('.tab-pane').css({
      'height': 'auto',
      'overflow-y': 'hidden',
    });
  }
}

function set_dates() {
  p = $('#id_start_date').val().split('.');
  var start_date = new Date(p[2],parseInt(p[1])-1,p[0]);
  var days = ['вс','пн','вт','ср','чт','пт','сб'];
  $('#spm_table .date').each(function(){
    var d = new Date(start_date);
    var shift = parseInt($(this).attr('data-day-ind')) -1;
    d.setDate(d.getDate() + shift);
    $(this).html(formatDate(d) + ' ' + days[d.getDay()]);
  })
}

$(document).ready(function(){
  $('#spm_form').submit(function(e){
    e.preventDefault();

    if (!is_authenticated) {
      show_signin('Для продолжения, авторизируйтесь');
      return;
    }
    if (!read_info) {
      $('#modal_info').modal('show');
      return;
    }

    $('#modal_loading').modal('show');
    $.ajax({
      type: "POST",
      url: $(this).attr('action'),
      data: $(this).serialize(),
      dataType: 'json',
    }).done(function( data ) {
      if (data.status == 'success') {
        window.location.href = data.redirect;
      } else {
        $('#modal_loading').modal('hide');
        $('#spm_form #spm_fields').html(data.form);
      }
    });
  })
  $('#spm_form select').change(function(){
    var nid = $(this).val();
    var ids = [];
    $(this).find('option').each(function(){
      var id = $(this).attr('value');
      if (nid != id) {
        hide_drug(id)
      }
    })
    setTimeout(function(){show_drug(nid)}, speed);
  })
  $('#spm_form input[type=checkbox]').change(function(){
    var id = $(this).attr('name').substr(2);
    if ($(this).prop('checked')) {
      show_drug(id);
    } else {
      hide_drug(id);
    }
  });
  $('#spm_form #id_start_date').change(set_dates);

  $('#spm_form select').change();
  $('#spm_form input').change();
  speed = 500;

  $(window).resize(resize);
  resize();
})
function show_detail(obj) {
  if ($(obj).hasClass('btn-info')) {
    $(obj).removeClass('btn-info').addClass('btn-default');
    $('#spm_table tr.day').fadeOut();
  } else {
    $(obj).removeClass('btn-default').addClass('btn-info');
    $('#spm_table tr.day').fadeIn();
  }
}