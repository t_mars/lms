var likePopoverOps = {
  html: true,
  placement: 'top',
  trigger: 'hover',
  delay: {
    show: 10,
    hide: 400
  },
  title: function () {
      return $(this).parent().find('.head').html();
  },
  content: function () {
      return $(this).parent().find('.content').html();
  }
};
function likeToggle(id) {
  var cls = '.like_'+id;
  $(cls).find('.like-icon').replaceWith('<i class="fa fa-spinner fa-spin"></i>');
  $.ajax({
    url: urls.api_like_toggle,
    method: 'POST',
    crossDomain: true,
    data: {id: id},
    dataType: 'JSONP',
  }).success(function(data) {
    if (data.status == 'success') {
      $(cls).replaceWith(data.template);
      $(cls).find('.like-icon').popover(likePopoverOps).popover('show');
    } else if (data.status == 'error') {
      alert(data.message);
    }
  });
}
function showLikes(id) {
  var modal = $('#modal_like_list');
  modal.find('.modal-body').html('<i class="fa fa-spinner fa-spin fa-stack-2x"></i>');
  modal.modal('show');
  $.ajax({
    url: urls.api_like_list,
    method: 'POST',
    crossDomain: true,
    data: {id: id},
    dataType: 'JSONP',
  }).success(function(data) {
    if (data.status == 'success') {
      modal.find('.modal-body').html(data.template);
    } else if (data.status == 'error') {
      alert(data.message);
    }
  });
}
$(document).ready(function () {
  $('.like-icon').popover(likePopoverOps);
});