var addVideoApp = angular.module('addVideoApp', []).config(function($interpolateProvider) {
  $interpolateProvider.startSymbol('{$');
  $interpolateProvider.endSymbol('$}');
})
.decorator("$xhrFactory", [
	"$delegate", "$injector",
	function($delegate, $injector) {
    return function(method, url) {
		  var xhr = $delegate(method, url);
      var $http = $injector.get("$http");
			var callConfig = $http.pendingRequests[$http.pendingRequests.length - 1];
			if (angular.isFunction(callConfig.onProgress)) {
      	xhr.upload.addEventListener("progress", callConfig.onProgress);
      	xhr.addEventListener("progress", callConfig.onProgress);
			}
			return xhr;
		};
	}
]);
addVideoApp.config(['$httpProvider', function($httpProvider) {
  $httpProvider.defaults.useXDomain = true;
  delete $httpProvider.defaults.headers.common['X-Requested-With'];
}]);
addVideoApp.directive('select2', function () {
  return function(scope, element, attrs) {
    $(element).select2();
  }
});
addVideoApp.controller('AddVideoCtrl', function ($scope, $http, $sce, $interval) {
  $scope.exercises = [];
  $http.get(urls.api_video_exercise_list, {withCredentials: true})
    .success(function (data) {
      $scope.exercises = data.exercises;
    });
  $scope.callback = null;

  $scope.reset = function () {
    $scope.data = {
      state: 'select_type',
      preview: null,
      url: null,
      file: null,
      type: null,
      error: null,
      percent: null
    };

    var date = new Date();
    $scope.video = {
      title: '',
      description: '',
      is_public: true,
      scheme_id: null,
      exercise_id: null,
      app: 1,
      reps: null,
      weight: null,
      executed_at: date.format('dd.mm.yyyy')
    };
  };
  $scope.reset();

  $scope.setUrl = function () {
    if (!$scope.data.url) {
      return;
    }
    var config = {
      params: {url: $scope.data.url},
      withCredentials: true
    };
    $http.get(urls.api_video_info, config)
    .success(function(data) {
      if (data.status == 'error') {
        alert(data.message);
      } else {
        $scope.data.state = 'send_form';
        $scope.data.type = 'youtube';
        $scope.data.preview = data.preview;
        $scope.video.title = data.title;
      }
    });
  };
  $scope.fileChange = function (element) {
    var file = element.files[0];
    var type = file.type;
    var videoNode = document.createElement('video');
    var canPlay = videoNode.canPlayType(type);
    if (canPlay !== '') {
      $scope.data.preview = $sce.trustAsResourceUrl(URL.createObjectURL(file));
    }
    $scope.data.file = file;
    $scope.data.type = 'file';
    $scope.data.state = 'send_form';
    $scope.$apply();
  };

  $scope.showProgress = function (video_id) {
    $scope.data.state = 'progress';
    $scope.data.percent = 0;
    $scope.data.error = null;

    var count = 0;
    var config = {
      withCredentials: true
    };
    stop = $interval(function() {
      $http.get(urls.api_video_get_progress+'?video_id='+video_id, config)
      .success(function (data) {
        if (data.status == 'process') {
          $scope.data.percent  = data.percent;
        } else if (data.status == 'error') {
          $scope.data.state = 'send_form';
          $scope.data.error = data.message;
          $interval.cancel(stop);
        } else if (data.status == 'success') {
          $scope.data.state = 'saved';
          $interval.cancel(stop);
          if ($scope.callback) $scope.callback(data);
        } else {
          count += 1;
        }

        if (count == 50) {
          $scope.data.error = 'Ошибка при обработке видео. Превышено максимальное количество попыток.';
          $interval.cancel(stop);
          $scope.data.state = 'send_form';
        }
      })
      .error(function (data) {
        $scope.data.error = 'Ошибка №1. Обратитесь к администратору';
        $scope.data.percent = null;
      })
    }, 500);
  };

  $scope.videoSave  = function() {
    $scope.data.error = null;

    if (!$scope.video.scheme_id) {
      if (!$scope.video.exercise_id) {
        $scope.data.error = 'Укажите упражнение';
        return;
      }
      if (!$scope.video.weight) {
        $scope.data.error = 'Укажите вес снаряда';
        return;
      }
      if (!$scope.video.reps) {
        $scope.data.error = 'Укажите количество повторений';
        return;
      }
      if (!$scope.video.executed_at) {
        $scope.data.error = 'Укажите дату выполнения';
        return;
      }
    }

    var data = new FormData();

    data.append("csrfmiddlewaretoken", csrf_token);
    data.append("title", $scope.video.title);
    data.append("description", $scope.video.description);
    data.append("is_public", $scope.video.is_public ? '1' : '0');
    data.append("type", $scope.data.type);
    if ($scope.video.scheme_id) {
      data.append("scheme_id", $scope.video.scheme_id);
      data.append("app", $scope.video.app);
    } else {
      data.append("exercise_id", $scope.video.exercise_id);
      data.append("weight", $scope.video.weight);
      data.append("reps", $scope.video.reps);
      data.append("executed_at", $scope.video.executed_at);
    }

    if ($scope.data.type == 'file') {
      data.append("file", $scope.data.file);
    } else if ($scope.data.type == 'youtube') {
      data.append("url", $scope.data.url);
    } else {
      alert('Ошибка');
      return;
    }

    var config = {
      method: 'post',
      withCredentials: true,
      headers: {'Content-Type': undefined },
      transformRequest: angular.identity,
      onProgress: function(evt) {
        if (evt.lengthComputable) {
          $scope.data.percent = parseInt(evt.loaded / evt.total * 100);
          $scope.$apply();
        }
      }
    };
    $scope.data.state = 'upload';
    $scope.data.error = null;

    $http.post(urls.api_video_save, data, config)
      .success(function (data) {
        $scope.data.percent = null;
        $scope.data.video_id = data.video_id;
        if (data.status == 'success') {
          $scope.data.state = 'saved';
          if ($scope.callback) $scope.callback(data);
        }
        else if (data.status == 'progress') {
          $scope.showProgress($scope.data.video_id);
        }
        else {
          $scope.data.state = 'send_form';
          $scope.data.error = data.message;
        }
      }).error(function () {
        $scope.data.state = 'send_form';
        $scope.data.error ='Ошибка №2. Сообщите администратору.';
      });
  };
});
function showVideoForm(scheme, app, callback) {
  var form = $('#video_add_form');
  var scope = angular.element(form).scope();
  scope.reset();
  if (scheme && app) {
    scope.video.scheme_id = scheme.id;
    scope.video.app = app;
  }
  if (callback) {
    scope.callback = callback;
  }
  scope.$apply();
  form.modal('show');
}