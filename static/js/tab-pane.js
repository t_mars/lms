function resize() {
  if ($(window).width() > 992) {
    var h = $(window).height() - $('.tab-pane').offset().top;
    $('.tab-pane').css({
      'height': h,
      'overflow-y': 'scroll',
    });
  } else {
    $('.tab-pane').css({
      'height': 'auto',
      'overflow-y': 'hidden',
    });
  }
}

$(document).ready(function(){
  $(window).resize(resize);
  resize();
});