function dose_mark(id, ts) {
  var b = $('.dose-mark-'+id)
  var i = b.find('i')
  
  b.prop('disabled', true)
  i.attr('class', 'fa fa-refresh fa-spin');

  $.ajax({
    type: "GET",
    url: "/spm/dose_mark/",
    data: {id: id},
    dataType: 'json',
  }).done(function( data ) {
    b.prop('disabled', false);
    b.removeClass('btn-danger btn-success btn-primary');
    i.attr('class', 'fa fa-square-o')
    if (data.status == 'error') {
      alert(data.error);
    } else {
      if (data.result == null) {
        b.addClass('btn-primary');
        i.attr('class', 'fa fa-square-o')
      }
      else if (data.result == true) {
        b.addClass('btn-success');
        i.attr('class', 'fa fa-check')
      }
      else if (data.result == false) {
        b.addClass('btn-danger');
        i.attr('class', 'fa fa-minus')
      }
      update(ts, ts);
    }
  });
}