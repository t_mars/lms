function calc() {
  $('.e-start-weight').change();
}

function set_val(obj) {
  var id = $(obj).attr('data-id');
  var cp = my_parse_float($('#correct-percent').val()) / 100.0;
  
  // устанавливаем ПМ на каждую неделю
  $('.e-micro-pm[data-id="'+id+'"]').each(function(){
    var micro = parseInt($(this).attr('data-micro'));
    var val = my_parse_float($('#e-start-weight-' + id).val());
    for (i = 0; i < micro; i++) val += val * cp;
    $(this).html(round_to(val, 0.1).toFixed(1));
  });

  // устанавливаем веса на каждый день
  $('.e-weight-' + id).each(function(){
    var percent = my_parse_float($(this).attr('data-percent'));
    var micro = parseInt($(this).attr('data-micro'));
    var val = my_parse_float($('#e-start-weight-' + id).val());
    for (i = 0; i < micro; i++) val += val * cp;
    
    var v = my_round(val * percent / 100.0);
    v = v.toFixed(1);
    $(this).html(v);
  });
}

function set_dates() {
  console.log('week_days', week_days);

  p = $('#start_date').val().split('.');
  var start_date = new Date(p[2],parseInt(p[1])-1,p[0]);
  var days = ['Воскресенье','Понедельник','Вторник','Среда','Четверг','Пятница','Суббота'];
  $('.date-head').each(function(){
    var d = new Date(start_date);
    var micro_num = parseInt($(this).attr('data-micro-num'));
    var day_ind = parseInt($(this).attr('data-day-ind'));
    shift = micro_num * 7 + week_days[day_ind];
    d.setDate(d.getDate() + shift);
    $(this).html(formatDate(d) + ' ' + days[d.getDay()]);
  })
}
function create_plan() {
  if (!is_authenticated) {
    show_signin('Для продолжения, авторизируйтесь');
    return;
  }
  if (!read_info) {
    $('#modal_info').modal('show');
    return;
  }

  var well = $('#cycle_form').find('.well');
  var form = $('#cycle_form').find('form');
  
  well.addClass('disabled');
  $('#modal_loading').modal('show');
  $('#error_block').hide();

  $.ajax({
    type: "POST",
    url: form.attr('action'),
    data: form.serialize(),
  }).done(function( msg ) {
    data = JSON.parse(msg);
    
    well.removeClass('disabled');
    $('#modal_loading').modal('hide');
    
    if (data.status == 'success') {
      window.location.href = data.redirect;
    } else {
      $('#error_block').text(data.error).show();
      $('#cycle_form').animate({
        scrollTop: $('#descr').outerHeight() + 20
      });
    }
  });
}

function resize() {
  if ($(window).width() > 992) {
    var h = $(window).height() - $('.tab-pane').offset().top;
    $('.tab-pane').css({
      'height': h,
      'overflow-y': 'scroll',
    });
  } else {
    $('.tab-pane').css({
      'height': 'auto',
      'overflow-y': 'hidden',
    });
  }
}
$(document).ready(function(){
  // изменение даты
  $('#start_date').change(function(){
    set_dates();
  })
  

  $('.week_day').change(function(){
    // ДНИ НЕДЕЛИ
    var days = $('.week_day');
    var active_days = [];
    $('.week_day_label').removeClass('red').removeClass('green');
    days.each(function(ind){
      var label = $('#label_'+ind);
      if ($(this).prop('checked')) {
        if (active_days.length >= day_count) {
          label.addClass('red');
        } else {
          active_days.push(ind)
          label.addClass('green');
        }
      }
    });

    // убираем сообщение
    $('#week_days_block .alert').remove();

    // выводим сообщение мало дней
    if (active_days.length < day_count) {
        $('#week_days_block').append('<p class="alert alert-danger">Необходимо выбрать еще '+(day_count-active_days.length)+' дней</p>');
        week_days = variants[0];
    }
    else {
      week_days = active_days;
      // проверяем варианты
      flag = false;
      for (var i = 0; i < variants.length; i++) {
        if (arrays_equal(variants[i], active_days)) {
          flag = true;
          break;
        }
      }
      if (!flag) {
        $('#week_days_block').append('<p class="alert alert-info">Выбранные дни недели не рекоммендуются для данного СРЦ, но могут использоваться</p>');
      }
    }
    set_dates();
  })
  $('.week_day').eq(0).change();

  // изменение важных весов
  $('.e-main').change(function(){
    var id = $(this).attr('data-id');
    var val = $(this).val();
    set_val($(this));
    $('.e-parent-' + id).each(function(){
      var rate = my_parse_float($(this).attr('data-rate'));
      var v = my_round(val * rate);
      console.log(id, val, rate, v);
      $(this).val(v);
      set_val($(this));
    });
  });

  $('#correct-percent').change(function(){
    calc();
  })
  calc();  

  $(window).resize(resize);
  resize();

});