function del_favorite(type, id) {
  $('#favorite_list>a>i').removeClass('fa-star').addClass('fa-refresh fa-spin');

  $.ajax({
    type: "GET",
    url: "/profile/favorite/del/",
    data: {type: type, id: id},
    dataType: 'json',
  }).done(function(data) {
    $('#favorite_list>a>i').addClass('fa-star').removeClass('fa-refresh fa-spin');
    if (data.status == 'error') {
      alert(data.error);
    }
    else if (data.status == 'success') {
      $('.favorite-'+type+'-'+id).remove();
    }
  });
}

function add_favorite_drag(e) {
  e.dataTransfer.setData("href", e.target.href);
  e.dataTransfer.setData("text", e.target.text);
  console.log('drag', e.target.href);
  $('#favorite_list').addClass('open');
}
function add_favorite_drop(e) {
  e.preventDefault();
  var href = e.dataTransfer.getData("href");
  var text = e.dataTransfer.getData("text");
  $('#favorite_list>a>i').removeClass('fa-star').addClass('fa-refresh fa-spin');
  
  $.ajax({
    type: "GET",
    url: "/profile/favorite/add/",
    data: {url: href},
    dataType: 'json',
  }).done(function(data) {
    $('#favorite_list>a>i').addClass('fa-star').removeClass('fa-refresh fa-spin');
    if (data.status == 'error') {
      alert(data.error);
    }
    else if (data.status == 'success') {
      if (data.html) {
        $('#favorite_list>ul').append(data.html);
        $('#favorite_list_empty').hide();
      }
    }
  });
}
function clear_favorite() {
  $.ajax({
    type: "GET",
    url: "/profile/favorite/clear/",
    dataType: 'json',
  }).done(function(data) {
    if (data.status == 'success') {
      $('#favorites_block').html('');
      $('#favorite_list .favorite').remove();
      $('#favorite_list_empty').show();
    }
  });
}
function dragOver(e) {
    e.preventDefault();
}
function dragEnter(e) {
   e.preventDefault();
   return true;
}
$(document).ready(function(){
  $('.possible_favorite').attr('ondragstart', 'add_favorite_drag(event);')
});