var chat = {
  ws: 0,
  user: 0,
  users: [],
  connected: 0,
  size: '',
  reset: function() {
    chat.connected = 0;
    chat.is_loading_up = 0;
    $('#chatbox .online-line').hide();
    $('#chatbox .connect-line').show();
    $('#chatbox .messages table, #chatbox .users table').html('');
    chat.users = [];
  },
  init: function() {
    chat.size = $.cookie("chat_collapsed") == '1' ? 'collapsed' : 'mini';
    chat.init_ws();
    $('#chatbox form').submit(function(e){
      if (!is_authenticated) show_signin('Авторизируйтесь, чтобы писать в чате');
      $massage = $("input[name='msg']");
      var msg = $massage.val();
      $massage.val('');
      chat.send({type:'message', 'content': msg});
      e.preventDefault();
      return false;
    });
    $('#chatbox .messages').scroll(function(){
      if (!chat.connected || chat.is_loading_up == 1)
        return;
      if ($(this).scrollTop() == 0) {
        chat.is_loading_up = 1;
        chat.send({type: 'get_messages', id: $('#chatbox .messages table tr:first-child').attr('data-id')});
      }
    });
    $(window).resize(chat.box_resize);
    chat.resize_content();
  },
  init_ws: function () {
    chat.reset();

    var pt = location.protocol === 'https:' ? 'wss' : 'ws';
    chat.ws = new WebSocket(pt+"://ws.last-man.org:"+CHAT_SERVER_PORT+"/ws");

    chat.ws.onclose = function(){
      setTimeout(chat.init_ws, 1000);
    };
    
    chat.ws.onopen = function(){
      chat.connected = 1;
      $('#chatbox input, #chatbox button').prop('disabled', false);
      $('#chatbox .online-line').show();
      $('#chatbox .connect-line').hide();
    };
    
    chat.ws.onmessage = function (evt) {
      var data = JSON.parse(evt.data);
      if (data['type'] == 'msg') {
        var m = $(chat.render_message(data['msg']));
        $('#chatbox .messages table').append(m);
        
        var offset = $('#chatbox .messages table').height() - $('#chatbox .messages').scrollTop() - $('#chatbox .messages').height();
        if (offset < 100) {
          $('#chatbox .messages').animate({scrollTop: $('#chatbox .messages table').height()}, 'fast'); 
        }

        chat.audio();
        var back = m.css('background-color');
        var color = m.css('color');
        m.animate(
          {'background-color': '#660404', 'color': 'white'}, {
            'duration': 400,
            'complete': function(){m.animate({'background-color': back, 'color': color}, 400)},
          }
        );
      } else if (data['type'] == 'load') {
        chat.user = data['user'];
        for (var i=0; i<data['users'].length; i++) {
          chat.put_user(data['users'][i]);
        }
        
        var c = '';
        for (var i=0; i<data['msgs'].length; i++) {
          c += chat.render_message(data['msgs'][i]);
        }
        $('#chatbox .messages table').append(c);
        $('#chatbox .messages').scrollTop($('#chatbox .messages table').height());  
      } 
      else if (data['type'] == 'online') {
        if (data['user']) {
          chat.put_user(data['user']);
        }
        chat.set_counts(data['count'], data['guests_count']);
      } 
      else if (data['type'] == 'offline') {
        delete chat.users[data['user_id']];
        $('#chatbox .users table .user-'+data['user_id']).fadeOut('slow', function(){$(this).remove();});
        $('#chatbox .user-online-'+data['user_id']).removeClass('online');
        chat.set_counts(data['count'], data['guests_count']);
      } 
      else if (data['type'] == 'reload') {
        window.location.reload();
      }
      else if (data['type'] == 'messages') {
        var first = $('#chatbox .messages table tr.message').eq(0);
        for (var i=0; i < data['msgs'].length; i++) {
          m = data['msgs'][i];
          
          d = new Date(m['date'] * 1000);
          prev_d = new Date($('#chatbox .messages table tr.message').eq(0).attr('data-date') * 1000);
          if (d.getDate() != prev_d.getDate()) {
            $('#chatbox .messages table').prepend('<tr class="info"><td class="time"></td><td class="day-start">'+prev_d.format('d.m.yyyy')+'</td></tr>');
          }
          
          $('#chatbox .messages table').prepend(chat.render_message(m));
        }
        $('#chatbox .messages').scrollTop(first.offset().top);
        if (data['msgs'].length > 0)
          chat.is_loading_up = 0;
      }
      else if (data['type'] == 'alert') {
        alert(data['content']);
      }
      else if (data['type'] == 'ban') {
        var d = new Date();
        var content = '<tr class="message"><td class="time">' + d.format("H:MM") + '</td><td><i>Забанены:';
        for (var i in data['users']) {
          var u = data['users'][i];
          content += ' ' + u.name ;
        }
        content += '</i></td></tr>';
        $('#chatbox .messages table').append(content);
      }
      else if (data['type'] == 'del') {
        for (var i in data['ids']) {
          var id = data['ids'][i];
          console.log('delete', id);
          $('#chatbox .messages .message[data-id="'+id+'"]').remove();
        }
      }
    };
  },
  set_counts: function (count, guests_count) {
    $('#chatbox .online-count').html(count + '/' + guests_count);
  },
  send: function(data) {
    if (!chat.connected)
      return;
    chat.ws.send(JSON.stringify(data));
  },
  audio: function() {
    if (chat.size != 'collapsed') {
      $('#audio')[0].play()
    }
  },
  resize_content: function () {
    var h = $('#chatbox').height() - $('#chatbox .header').height() - $('#chatbox form').height();
    $('#chatbox .scroll').height(h);
    $('#chatbox .messages').scrollTop($('#chatbox .messages table').height());
    if (!$('#chatbox .expand-button i').hasClass('fa-expand')) {
      if ($(window).width() < 900) {
        var all_width = $('#chatbox .collapse-block').width();
        var width = 180;
        $('#chatbox .messages').css('width', all_width-width);
        $('#chatbox .users').css('width', width); 
      } else {
        $('#chatbox .messages').css('width', '80%');
        $('#chatbox .users').css('width', '20%');
      }
    } else {
      $('#chatbox .messages').css('width', '100%');
      $('#chatbox .users').css('width', '0');
    }
  },
  set_size_collapsed: function() {
    $('#chatbox .collapse-button i').removeClass('fa-caret-down').addClass('fa-caret-up');
    $('#chatbox .expand-button').hide();
    $('#chatbox .collapse-block').hide();
    $('#chatbox').animate(
      {
        'height': '60px', 
        'width': '240px'
      }, {
        'duration': 'fast',
        'progress': chat.resize_content,
      }
    );
    $.cookie("chat_collapsed", 1, {path: '/'});
    chat.size = 'collapsed';
    $('#chatbox').removeClass('expanded').removeClass('mini').addClass('collapsed');
  },
  set_size_mini: function () {
    $('#chatbox .collapse-button i').removeClass('fa-caret-up').addClass('fa-caret-down');
    $('#chatbox .expand-button').show();
    $('#chatbox .expand-button i').removeClass('fa-compress').addClass('fa-expand');
    $('#chatbox .collapse-button').show();
    $('#chatbox .collapse-block').show();
    $('#chatbox').animate(
      {
        'height': '300px', 
        'width': '400px',
        'right': '0px',
      }, {
        'duration': 'fast',
        'progress': chat.resize_content,
      }
    );
    $.cookie("chat_collapsed", 0, {path: '/'});
    chat.size = 'mini';
    $('#chatbox').removeClass('expanded').removeClass('collapsed').addClass('mini');
  },
  set_size_expanded: function () {
    $('#chatbox .expand-button i').removeClass('fa-expand').addClass('fa-compress');
    $('#chatbox .collapse-button').hide();
    $('#chatbox').animate(
      {
        'width': $(window).width(),
        'height': $(window).height(),
        'right': 0,
      }, {
        'duration': 'fast',
        'progress': chat.resize_content
      }
    );
    $('#chatbox').removeClass('mini').removeClass('collapsed').addClass('expanded');
    chat.size = 'expanded';
  },
  collapse: function (f) {
    if ($('#chatbox .collapse-button i').hasClass('fa-caret-up')) {
      // разворачиваем
      chat.set_size_mini();
    }
    else {
      // своравичваем
      chat.set_size_collapsed();
    }
  },
  expand: function (f) {
    if ($('#chatbox .expand-button i').hasClass('fa-expand')) {
      // раскрываем
      chat.set_size_expanded();
    }
    else {
      // закрываем
      chat.set_size_mini();
    }
  },
  box_resize: function () {
    if (chat.size == 'mini') {
      $('#chatbox').css('width', Math.min($(window).width(), 400));
    }
    else if (chat.size == 'expanded') {
      $('#chatbox').css({
        'width': $(window).width(),
        'height': $(window).height(),
        'right': 0,
      });
      chat.resize_content();
    }
  },
  put_user: function (u) {
    if (u['id'] in chat.users)
      return;
    chat.users[u['id']] = u;

    var id = parseInt(u['id']);
    var keys = Object.keys(chat.users);
    var t = 0;
    for (var i=0; i<keys.length; i++) {
      var k = parseInt(keys[i]);
      if (k < id && k > t)
        t = k;
    }
    
    var i = u['is_superuser'] ? 'star' : 'user';
    var banned = '';
    if (u.is_banned) {
      banned = ' (забанен)';
      i = 'ban';
    }
    var icon = '<a href="http://ws.last-man.org/user/'+u['username']+'" target="_blank"><i class="fa fa-'+i+' user-online-status user-online-'+u['id']+' online"></i></a>';
    var name = '<a style="cursor:pointer" onclick="chat.add_recipient(\''+u['username']+'\')">' + u['name'] + '</a>';

    var c = $('<tr class="user-'+u['id']+'"><td>'+icon+' ' + name + banned + '</a></td></tr>');
    $('#chatbox .messages .online-user-'+u['id']).addClass('online');
    if (t == 0) {
      $('#chatbox .users table').prepend(c);
    } else {
      $('#chatbox .users table tr.user-'+t).after(c);
    }
    var back = c.css('background-color');
    var color = c.css('color');
    c.animate(
      {'background-color': '#660404', 'color': 'white'}, {
        'duration': 400,
        'complete': function(){c.animate({'background-color': back, 'color': color}, 400)},
      }
    );
    $('#chatbox .user-online-'+u['id']).addClass('online')
  },
  render_message: function (m) {
    d = new Date(m['date'] * 1000);
    
    name = '<a style="cursor:pointer" onclick="chat.add_recipient(\''+m['user']['username']+'\')">' + m['user']['name'] + '</a>';
    
    i = m['user']['is_superuser'] ? 'star' : 'user';
    if (m['user']['is_banned']) i = 'ban';
    o = m['user']['id'] in chat.users ? ' online' : '';
    icon = '<a href="http://ws.last-man.org/user/'+m['user']['username']+'" target="_blank"><i class="fa fa-'+i+' user-online-status user-online-'+m['user']['id']+o+'"></i></a>';
    
    if (m['user']['is_superuser']) {
      active = 'recipient';
    } else if (chat.user) {
      active = $.inArray(chat.user['id'], m['recipients']) >= 0 ? 'recipient' : '';
    } else {
      active = '';
    }
    
    return '<tr class="message '+active+'" data-id="'+m['id']+'" data-date="'+m['date']+'"><td class="time">' + 
      d.format("H:MM") + '</td><td><b>' + icon + ' ' + name + '</b>&nbsp;<span class="content">' +
      m['content'] +'</span></td></tr>';
  },
  add_recipient: function(username) {
    var input = $('#chatbox [name=msg]');
    if (input.val() == '') {
      input.val('@'+username+' ');
    }
    else {
      v = input.val();
      if (v[v.length-1] != ' ')
        v += ' ';
      input.val(v + '@' + username + ' ');
    }
    input.focus();
  },
};
$(document).ready(function(){
  chat.init()
});