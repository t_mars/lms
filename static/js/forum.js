$(function() {
  // увеличение изображений
  $('.message-content img').on('click', function() {
    var sr=$(this).attr('src');
    var obj = $(this).closest('.message-content').find('img[src="' + sr + '"]');
    var prev = obj.prev('img');
    var next = obj.next('img');

    if (prev.length) {
      $('#image_modal .modal-footer .pull-left').show().unbind('click').click(function(){prev.click()})
    } else {
      $('#image_modal .modal-footer .pull-left').hide();
    }

    if (next.length) {
      $('#image_modal .modal-footer .pull-right').show().unbind('click').click(function(){next.click()})
    } else {
      $('#image_modal .modal-footer .pull-right').hide();
    }

    $('#image_modal #img').attr('src',sr);
    $('#image_modal').modal('show');

    $('#image_modal').keydown(function(e) {
      console.log(e.keyCode);
      if(e.keyCode == 37) { // left
        if (prev.length) {
          prev.click();
        }
      }
      else if(e.keyCode == 39) { // right
        if (next.length) {
          next.click()
        }
      }
      else if (e.keyCode == 27) {
        $('#image_modal').modal('hide');
      }
    });
  });
});

