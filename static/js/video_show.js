var showVideoApp = angular.module('showVideoApp', []).config(function($interpolateProvider) {
  $interpolateProvider.startSymbol('{$');
  $interpolateProvider.endSymbol('$}');
});
showVideoApp.controller('ShowVideoCtrl', function ($scope, $http, $sce) {
  $scope.reset = function (video_id) {
    $scope.data = {
      title: 'Загрузка ...',
    };
    $scope.video = null;

    if (!video_id) return;

    var config = {
      params: {id: video_id},
      withCredentials: true
    };
    $http.get(urls.api_video_show, config)
      .success(function (data) {
        if (data.status == 'success') {
          $scope.data.title = data.video.name;
          $scope.video = data.video;
        } else {
          alert(data.message);
        }
      })
  };
  $scope.getIframeSrc = function (video_id) {
    return $sce.trustAsResourceUrl('https://www.youtube.com/embed/'+video_id+'?autoplay=1&fs=1&modestbranding=1&rel=0');
  };
});
function showVideo(video_id, flag) {
  var modal = $('#video_show_modal');
  var scope = angular.element(modal).scope();
  scope.reset(video_id);
  scope.$apply();
  if (flag) return;

  modal.modal('show');
  modal.on('hide.bs.modal', function () {
    scope.reset();
    scope.$apply();
  })
}