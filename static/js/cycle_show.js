angular.module('filters', [])
.filter('my_round', function() {
  return function(input) {
    return my_round(input);
  };
})
.directive('popover', function() {
  return {
    link: function ($scope, element, attr) {
      $(element).popover({
        placement: "top",
        trigger: "hover",
        html : true
      });
    }
  }
})
.directive('slider', function() {
  return {
    link: function ($scope, element, attr) {
      $(element).slider({
        animate: true,
        value: $scope.params.correct_percent,
        min: -1,
        max: 2,
        step: 0.1,
        slide: function( event, ui ) {
          $scope.params.correct_percent = ui.value;
          $scope.reset();
          $scope.$apply();
        }
      });
    }
  }
})
.directive('popoverdual', function() {
  return {
    link: function ($scope, element, attr) {
      $(element).popover({
        placement: "top",
        trigger: "hover",
        content: "Указан вес для одной руки",
      });
    }
  }
});

var mainApp = angular.module('mainApp', ['filters']).config(function($interpolateProvider) {
  $interpolateProvider.startSymbol('{$');
  $interpolateProvider.endSymbol('$}');
});
mainApp.controller('ShowCycleCtrl', function ($scope, $http, $sce) {
  $scope.cycle = cycle;

  // параметры
  var week_days = {};
  for (var i = 0; i < 7; i++) {
    week_days[i] = $scope.cycle.week_days.indexOf(i) >= 0;
  }
  $scope.params = {
    name: $scope.cycle.name,
    start_date: start_date,
    group_exercise: {},
    weights: {},
    correct_percent: 0.5,
    week_days: week_days
  };

  angular.forEach($scope.cycle.plan[0], function (day) {
    angular.forEach(day.list, function (execution) {
      if (execution.groups) {
        $scope.params.group_exercise[execution.exercise_key] = execution.exercise_id;
      }
    });
  });

  // чтение параметров
  $scope.getExerciseList = function () {
    var list = {};
    angular.forEach($scope.cycle.plan, function (week, micro_num) {
      angular.forEach($scope.cycle.plan[micro_num], function (day) {
        angular.forEach(day.list, function (execution) {
          var eid;
          if (execution.groups) {
            eid = $scope.params.group_exercise[execution.exercise_key];
          } else {
            eid = execution.exercise_id;
          }
          list[eid] = $scope.cycle.exercises[eid];
        });
      });
    });

    return list;
  };

  $scope.resetWeights = function () {
    var list = $scope.getExerciseList();
    $scope.params.weights = {};
    $scope.main_weights = [];
    $scope.other_weights = [];

    angular.forEach(list, function (e, eid) {
      var w = parseInt(localStorage.getItem('weight_' + eid));
      if (w) {
      } else if (e.parent_id in $scope.params.weights) {
        w = my_round($scope.params.weights[e.parent_id] * e.parent_rate);
      } else if (eid in $scope.cycle.weights) {
        w = $scope.cycle.weights[eid];
      } else {
        w = 0;
      }
      $scope.params.weights[eid] = w;

      // установка весов для дочерних упражнений
      angular.forEach($scope.params.weights, function (weight, exercise_id) {
        var e = $scope.cycle.exercises[exercise_id];
        if (e.parent_id == eid) {
          $scope.params.weights[e.id] = my_round(w * e.parent_rate);
        }
      });

      if (e.parent_id && e.parent_id in $scope.cycle.exercises) {
        $scope.other_weights.push(eid);
      } else {
        $scope.main_weights.push(eid);
      }
    });
  };

  $scope.setWeight = function (eid) {
    localStorage.setItem('weight_'+eid, $scope.params.weights[eid]);

    angular.forEach($scope.params.weights, function (weight, exercise_id) {
      var e = $scope.cycle.exercises[exercise_id];
      if (e.parent_id == eid) {
        $scope.params.weights[e.id] = my_round($scope.params.weights[eid] * e.parent_rate);
      }
    });
    $scope.reset();
  };

  $scope.reset = function () {
    if ($scope.params.correct_percent <= 0.1) {
      $scope.correct_percent_msg = 'Период восстановаления (ПКТ)';
    } else if ($scope.params.correct_percent <= 0.5) {
      $scope.correct_percent_msg = 'Без использования ААС';
    } else if ($scope.params.correct_percent <= 0.7) {
      $scope.correct_percent_msg = 'Курс оральный препарат соло';
    } else if ($scope.params.correct_percent < 1.1) {
      $scope.correct_percent_msg = 'Курс из 2 препаратов (малые дозировки)';
    } else {
      $scope.correct_percent_msg = 'Курс из 3 и более препаратов (средние дозировки)';
    }

    $scope.stat = {};
    var cp = 1+$scope.params.correct_percent/100;
    angular.forEach($scope.cycle.plan, function (week, micro_num) {
      $scope.stat[micro_num] = {
        kps: 0,
        tonag: 0
      };
      var week_has_hidden = false;
      angular.forEach($scope.cycle.plan[micro_num], function (day) {
        day.tonag = 0;
        var day_has_hidden = false;
        var p = Math.pow(cp, micro_num);
        angular.forEach(day.list, function (execution) {
          if (execution.is_hidden) {
            day_has_hidden = true;
            week_has_hidden = true;
          }
          if (execution.groups) {
            execution.exercise_id = $scope.params.group_exercise[execution.exercise_key];
          }
          var eid = execution.exercise_id;

          execution.exercise = $scope.cycle.exercises[eid];
          execution.weight = $scope.params.weights[eid] * p;
          angular.forEach(execution.scheme, function (s) {
            s.weight = my_round(execution.weight * s.percent / 100);
            day.tonag += s.weight * s.apps * s.reps * (execution.exercise.is_dual ? 2 : 1);
          });
        });
        $scope.stat[micro_num].kps += day.kps;
        $scope.stat[micro_num].tonag += day.tonag;
        if (day_has_hidden) {
          day.tonag = null;
        }
      });
      if (week_has_hidden) {
        $scope.stat[micro_num].tonag = null;
      }
    });
  };
  $scope.resetDates = function () {
    var week_days_array = [];
    for (var i = 0; i < 7; i++) {
      if ($scope.params.week_days[i]) {
        week_days_array.push(i);
      }
    }
    $scope.params.week_days_array = week_days_array;

    if ($scope.cycle.week_days.length > week_days_array.length) {
      week_days_array = $scope.cycle.week_days_variants[0];
    }

    $scope.week_days_msg = '';
    if ($scope.cycle.week_days.length < week_days_array.length) {
      $scope.week_days_msg = 'Необходимо выбрать только '+$scope.cycle.week_days.length+' дней';
    } else {
      var flag = false;
      for (var i = 0; i < $scope.cycle.week_days_variants.length; i++) {
        if (arrays_equal($scope.cycle.week_days_variants[i], week_days_array)) {
          flag = true;
          break;
        }
      }
      if (!flag) {
        $scope.week_days_msg = 'Выбранные дни недели не рекоммендуются для данного СРЦ, но могут использоваться';
      }
    }

    var p = $scope.params.start_date.split('.');
    var start_date = new Date(p[2], parseInt(p[1])-1, p[0]);

    angular.forEach($scope.cycle.plan, function (week, micro_num) {
      angular.forEach($scope.cycle.plan[micro_num], function (day) {
        var shift = 7 * parseInt(micro_num) + week_days_array[parseInt(day.day_ind)];
        day.date = new Date();
        day.date.setDate(start_date.getDate() + shift);
      });
    });
  };

  $scope.setExercise = function (execution, eid) {
    $scope.params.group_exercise[execution.exercise_key] = eid;

    $scope.resetWeights();
    $scope.reset();
  };

  $scope.showExercises = function () {
    var obj = $('#other_exercises_btn');
    if ($(obj).hasClass('btn-info')) {
      $('#other_exercises').slideUp('slow');
      $(obj).addClass('btn-default');
      $(obj).removeClass('btn-info');
      $(obj).html('Показать все упражнения');
    } else {
      $('#other_exercises').slideDown('slow');
      $(obj).addClass('btn-info');
      $(obj).removeClass('btn-default');
      $(obj).html('Скрыть упражнения');
    }
  };

  $scope.createPlan = function () {
    $('#modal_loading').modal({backdrop: 'static', keyboard: false});
    $('#modal_loading').modal('show');

    var data = {
      week_days:        $scope.params.week_days_array,
      correct_percent:  $scope.params.correct_percent,
      weights:          $scope.params.weights,
      start_date:       $scope.params.start_date,
      name:             $scope.params.name
    };
    $scope.error_msg = '';
    $http.post(urls.create_plan, data)
      .success(function (data) {
        if (data.status == 'error') {
          $scope.error_msg = $sce.trustAsHtml(data.message);
          if (data.error == 'Ошибка доступа.') {
            $scope.error_msg = 'Для того чтобы создать план необходимо авторизироваться.';
            $.ajax({
             url: urls.auth,
             success: function(){
               var w = window.open(urls.auth, '_blank', 'width=780,height=410,toolbar=0,scrollbars=0,status=0,resizable=1,location=0,menuBar=0');
             },
             async: false
            });
          }
        } else {
          window.location.href = data.redirect;
        }
      })
      .error(function () {
        $scope.error_msg = 'Ошибка сети. Пожалуйста повторите.'
      })
      .finally(function () {
        $('#modal_loading').modal('hide');
      })
  };

  $scope.resetWeights();
  $scope.reset();
  $scope.resetDates();
});