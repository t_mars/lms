function get_timestamp(date) {
  return Math.round(date.getTime()/1000);
}
function float_format (f) {
  return Math.round(f*100)/100;
}
function round_to(number, roundto){
  return roundto * Math.round(number/roundto);
}
function my_round(number){
  if (number >= 50.0)
    return round_to(number, 2.5)
/*  if (number <= 15.0)
    return round_to(number, 0.1)
*/  return round_to(number, 1)
}
function intcomma(v) {
  val = v.toFixed(1);
  while (/(\d+)(\d{3})/.test(val.toString())){
    val = val.toString().replace(/(\d+)(\d{3})/, '$1'+','+'$2');
  }
  return val;
}
function formatDate(date) {

  var dd = date.getDate();
  if (dd < 10) dd = '0' + dd;

  var mm = date.getMonth() + 1;
  if (mm < 10) mm = '0' + mm;

  var yy = date.getFullYear() % 100;
  if (yy < 10) yy = '0' + yy;

  return dd + '.' + mm + '.' + yy;
}
function arrays_equal(a, b) {
  if (a === b) return true;
  if (a == null || b == null) return false;
  if (a.length != b.length) return false;
  for (var i = 0; i < a.length; ++i) {
    if (a[i] !== b[i]) return false;
  }
  return true;
}
function my_parse_float(str) {
	if (typeof str == 'undefined')
		return
	str = str.replace(',', '.');
	return parseFloat(str);
}