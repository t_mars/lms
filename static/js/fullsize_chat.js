var chat = {
  ws: 0,
  user: 0,
  users: [],
  connected: 0,
  reset: function() {
    chat.connected = 0;
    chat.is_loading_up = 0;
    $('#fullsize_chatbox .online-line').hide();
    $('#fullsize_chatbox .connect-line').show();
    $('#fullsize_chatbox .messages table, #chatbox .users table').html('');
    chat.users = [];
  },
  init: function() {
    chat.init_ws();
    $('#fullsize_chatbox form').submit(function(e){
      if (!is_authenticated) show_signin('Авторизируйтесь, чтобы писать в чате');
      $massage = $("input[name='msg']");
      var msg = $massage.val();
      $massage.val('');
      chat.send({type:'message', 'content': msg});
      e.preventDefault();
      return false;
    });
    $('#fullsize_chatbox .messages').scroll(function(){
      if (!chat.connected || chat.is_loading_up == 1)
        return;
      if ($(this).scrollTop() == 0) {
        chat.is_loading_up = 1;
        chat.send({type: 'get_messages', id: $('#fullsize_chatbox .messages table tr:first-child').attr('data-id')});
      }
    })
  },
  init_ws: function () {
    chat.reset();

    chat.ws = new WebSocket("ws://ws.last-man.org:"+CHAT_SERVER_PORT+"/ws");
    
    chat.ws.onclose = function(){
      setTimeout(chat.init_ws, 1000);
    };
    
    chat.ws.onopen = function(){
      chat.connected = 1;
      $('#fullsize_chatbox input, #chatbox button').prop('disabled', false);
      $('#fullsize_chatbox .online-line').show();
      $('#fullsize_chatbox .connect-line').hide();
    };
    
    chat.ws.onmessage = function (evt) {
      var data = JSON.parse(evt.data);
      if (data['type'] == 'msg') {
        var m = $(chat.render_message(data['msg']));
        $('#fullsize_chatbox .messages table').append(m);
        
        var offset = $('#fullsize_chatbox .messages table').height() - $('#fullsize_chatbox .messages').scrollTop() - $('#fullsize_chatbox .messages').height();
        if (offset < 100) {
          $('#fullsize_chatbox .messages').animate({scrollTop: $('#fullsize_chatbox .messages table').height()}, 'fast'); 
        }

        chat.audio();
        var back = m.css('background-color');
        var color = m.css('color');
        m.animate(
          {'background-color': '#660404', 'color': 'white'}, {
            'duration': 400,
            'complete': function(){m.animate({'background-color': back, 'color': color}, 400)},
          }
        );
      } else if (data['type'] == 'load') {
        chat.user = data['user'];
        for (var i=0; i<data['users'].length; i++) {
          chat.put_user(data['users'][i]);
        }
        
        var c = '';
        for (var i=0; i<data['msgs'].length; i++) {
          c += chat.render_message(data['msgs'][i]);
        }
        $('#fullsize_chatbox .messages table').append(c);
        $('#fullsize_chatbox .messages').scrollTop($('#fullsize_chatbox .messages table').height());  
      } 
      else if (data['type'] == 'online') {
        if (data['user']) {
          chat.put_user(data['user']);
        }
        chat.set_counts(data['count'], data['guests_count']);
      }
      else if (data['type'] == 'offline') {
        delete chat.users[data['user_id']];
        $('#fullsize_chatbox .users table .user-'+data['user_id']).fadeOut('slow', function(){$(this).remove();});
        $('#fullsize_chatbox .user-online-'+data['user_id']).removeClass('online');
        chat.set_counts(data['count'], data['guests_count']);
      } 
      else if (data['type'] == 'reload') {
        window.location.reload();
      }
      else if (data['type'] == 'reset') {
        chat.init();
      }
      else if (data['type'] == 'messages') {
        var first = $('#fullsize_chatbox .messages table tr.message').eq(0);
        for (var i=0; i < data['msgs'].length; i++) {
          m = data['msgs'][i];
          
          d = new Date(m['date'] * 1000);
          prev_d = new Date($('#fullsize_chatbox .messages table tr.message').eq(0).attr('data-date') * 1000);
          if (d.getDate() != prev_d.getDate()) {
            $('#fullsize_chatbox .messages table').prepend('<tr class="info"><td class="time"></td><td class="day-start">'+prev_d.format('d.m.yyyy')+'</td></tr>');
          }
          
          $('#fullsize_chatbox .messages table').prepend(chat.render_message(m));
        }
        $('#fullsize_chatbox .messages').scrollTop(first.offset().top);
        if (data['msgs'].length > 0)
          chat.is_loading_up = 0;
      }
      else if (data['type'] == 'alert') {
        alert(data['content']);
      }
      else if (data['type'] == 'ban') {
        var d = new Date();
        var content = '<tr class="message"><td class="time">' + d.format("H:MM") + '</td><td><i>Забанены:';
        for (var i in data['users']) {
          var u = data['users'][i];
          content += ' ' + u.name ;
        }
        content += '</i></td></tr>';
        $('#fullsize_chatbox .messages table').append(content);
      }
      else if (data['type'] == 'del') {
        for (var i in data['ids']) {
          var id = data['ids'][i];
          console.log('delete', id);
          $('#fullsize_chatbox .messages .message[data-id="'+id+'"]').remove();
        }
      }
    };
  },
  set_counts: function (count, guests_count) {
    $('#fullsize_chatbox .online-count').html(count + '/' + guests_count);
  },
  send: function(data) {
    if (!chat.connected)
      return; 
    chat.ws.send(JSON.stringify(data));
  },
  audio: function() {
    //$('#audio')[0].play()
  },
  put_user: function (u) {
    if (chat.users[u['id']]) return;
    chat.users[u['id']] = u;

    var id = parseInt(u['id']);
    var keys = Object.keys(chat.users);
    var t = 0;
    for (var i=0; i<keys.length; i++) {
      var k = parseInt(keys[i]);
      if (k < id && k > t)
        t = k;
    }
    
    var i = u['is_superuser'] ? 'star' : 'user';
    var banned = '';
    if (u.is_banned) {
      banned = ' (забанен)';
      i = 'ban';
    }
    var icon = '<a href="http://ws.last-man.org/user/'+u['username']+'" target="_blank"><i class="fa fa-'+i+' user-online-status user-online-'+u['id']+' online"></i></a>';
    var name = '<a style="cursor:pointer" onclick="chat.add_recipient(\''+u['username']+'\')">' + u['name'] + '</a>';

    var c = $('<tr class="user-'+u['id']+'"><td>'+icon+' ' + name + banned + '</a></td></tr>');
    $('#fullsize_chatbox .messages .online-user-'+u['id']).addClass('online');
    if (t == 0) {
      $('#fullsize_chatbox .users table').prepend(c);
    } else {
      $('#fullsize_chatbox .users table tr.user-'+t).after(c);
    }
    var back = c.css('background-color');
    var color = c.css('color');
    c.animate(
      {'background-color': '#660404', 'color': 'white'}, {
        'duration': 400,
        'complete': function(){c.animate({'background-color': back, 'color': color}, 400)},
      }
    );
    $('#fullsize_chatbox .user-online-'+u['id']).addClass('online')
  },
  render_message: function (m) {
    d = new Date(m['date'] * 1000);
    
    name = '<a style="cursor:pointer" onclick="chat.add_recipient(\''+m['user']['username']+'\')">' + m['user']['name'] + '</a>';
    
    i = m['user']['is_superuser'] ? 'star' : 'user';
    if (m['user']['is_banned']) i = 'ban';
    o = m['user']['id'] in chat.users ? ' online' : '';
    icon = '<a href="http://ws.last-man.org/user/'+m['user']['username']+'" target="_blank"><i class="fa fa-'+i+' user-online-status user-online-'+m['user']['id']+o+'"></i></a>';
    
    if (m['user']['is_superuser']) {
      active = 'recipient';
    } else if (chat.user) {
      active = $.inArray(chat.user['id'], m['recipients']) >= 0 ? 'recipient' : '';
    } else {
      active = '';
    }

    var id = '';
    if (chat.user.is_superuser) {
      id = '<small><a style="cursor:pointer" onclick="chat.add_id(\''+m['id']+'\')">#' + m['id'] + '</a></small>&nbsp;';
    }
    
    return '<tr class="message '+active+'" data-id="'+m['id']+'" data-date="'+m['date']+'"><td class="time">' + 
      d.format("H:MM") + '</td><td>' + id + '<b>' + icon + ' ' + name + '</b> ' +
      m['content'] +'</td></tr>';
  },
  add_recipient: function(username) {
    var input = $('#fullsize_chatbox [name=msg]');
    if (input.val() == '') {
      input.val('@'+username+' ');
    }
    else {
      v = input.val();
      if (v[v.length-1] != ' ')
        v += ' ';
      input.val(v + '@' + username + ' ');
    }
    input.focus();
  },
  add_id: function(id) {
    var input = $('#fullsize_chatbox [name=msg]');
    if (input.val() == '') {
      input.val(id+' ');
    }
    else {
      v = input.val();
      if (v[v.length-1] != ' ')
        v += ' ';
      input.val(v + id + ' ');
    }
    input.focus();
  },
};
$(document).ready(function(){
  chat.init()
});