function reply(reply_to, username) {
  $('#id_reply_to').val(reply_to);
  $('#reply_message').show().find('input').val(username + ' Сообщение #'+reply_to);
  $('html, body').animate({
      scrollTop: $('#id_content_iframe').offset().top
  }, 1000);
}
function show_prev(obj, message_id, page) {
  $.ajax({
    url: urls['prev_messages'],
    method: "GET",
    data: {
      message_id: message_id,
      page: page,
    },
  })
  .done(function( data ) {
    if (page == 1) {
      $(obj).parent().html(data);
    }
    else {
      $(obj).replaceWith(data);
    }
  });
}
function resetReply() {
  $('#id_reply_to').val('');
  $('#reply_message').hide().find('input').val('');
}