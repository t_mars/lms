$(document).ready(function () {
  var t = 300;
  $('.tarif').hover(
     function () {
       $(this).animate({
         'color': 'black',
         'background-color': 'rgb(203, 241, 204)'
       }, t);
       $(this).find('.price').animate({
         'color': 'black',
         'background-color': 'rgb(203, 241, 204)'
       }, t);
       $(this).find('.subprice2').animate({
         'color': 'red',
       }, t);
     },
     function () {
       $(this).animate({
         'color': 'white',
         'background-color': '#1c1e22'
       }, t);
       $(this).find('.price').animate({
         'color': 'white',
         'background-color': '#08485a'
       }, t);
       $(this).find('.subprice2').animate({
         'color': '#888',
       }, t);
     }
  );

});